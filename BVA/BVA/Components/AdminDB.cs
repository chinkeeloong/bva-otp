﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Web.Mvc; 
using ECFBase.Models;

namespace ECFBase.Components
{
    public class AdminDB
    {
        #region TrackingLog
        public static void InsertUserTrackingLog(string userType, string UserName, string locationIP, string operationType, string PreValue, string CurrentValue, string CreatedBy)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_InsertUserTrackingLog", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserType = sqlComm.Parameters.Add("@UserType", SqlDbType.NVarChar, 50);
            pUserType.Direction = ParameterDirection.Input;
            pUserType.Value = userType;

            SqlParameter pUserName = sqlComm.Parameters.Add("@UserName", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = UserName;

            SqlParameter plocationIP = sqlComm.Parameters.Add("@LocationIP", SqlDbType.NVarChar, 100);
            plocationIP.Direction = ParameterDirection.Input;
            plocationIP.Value = locationIP;

            SqlParameter pOperationType = sqlComm.Parameters.Add("@OperationType", SqlDbType.NVarChar, 100);
            pOperationType.Direction = ParameterDirection.Input;
            pOperationType.Value = operationType;

            SqlParameter pPreValue = sqlComm.Parameters.Add("@PreValue", SqlDbType.NVarChar, 100);
            pPreValue.Direction = ParameterDirection.Input;
            pPreValue.Value = PreValue;

            SqlParameter pCurrentValue = sqlComm.Parameters.Add("@CurValue", SqlDbType.NVarChar, 100);
            pCurrentValue.Direction = ParameterDirection.Input;
            pCurrentValue.Value = CurrentValue;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 50);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = CreatedBy;

            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }
        #endregion
    }
}