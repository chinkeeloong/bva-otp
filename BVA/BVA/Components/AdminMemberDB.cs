﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace ECFBase.Components
{
    public class AdminMemberDB
    {
        #region UpdateChangePassword
        public static void UpdateChangePassword(string storedProcedure, string UserName, string CurrentPassword, string NewPassword, string updatedBy,
                                                out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand(storedProcedure, sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@UserName", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = UserName;

            SqlParameter pCurrentPassword = sqlComm.Parameters.Add("@Current", SqlDbType.NVarChar, 50);
            pCurrentPassword.Direction = ParameterDirection.Input;
            pCurrentPassword.Value = CurrentPassword;

            SqlParameter pNewPassword = sqlComm.Parameters.Add("@New", SqlDbType.NVarChar, 50);
            pNewPassword.Direction = ParameterDirection.Input;
            pNewPassword.Value = NewPassword;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }
        #endregion

        public static void TransferMember(string storedProcedure, string Username , string Fullname , string IC , string HP , string Email, int Country,  out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand(storedProcedure, sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@UserName", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = Username;

            SqlParameter pFullname = sqlComm.Parameters.Add("@UserFullName", SqlDbType.NVarChar, 50);
            pFullname.Direction = ParameterDirection.Input;
            pFullname.Value = Fullname;

            SqlParameter pIC = sqlComm.Parameters.Add("@IC", SqlDbType.NVarChar, 50);
            pIC.Direction = ParameterDirection.Input;
            pIC.Value = IC;

            SqlParameter pHP = sqlComm.Parameters.Add("@HP", SqlDbType.NVarChar, 50);
            pHP.Direction = ParameterDirection.Input;
            pHP.Value = HP;

            SqlParameter pEmail = sqlComm.Parameters.Add("@EMAIL", SqlDbType.NVarChar, 50);
            pEmail.Direction = ParameterDirection.Input;
            pEmail.Value = Email;

            SqlParameter pCountryID = sqlComm.Parameters.Add("@CountryID", SqlDbType.Int);
            pCountryID.Direction = ParameterDirection.Input;
            pCountryID.Value = Country;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        #region UpdateMemberRanking

        public static void UpdateMemberRanking(string username, string ranking, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_ChangeRanking", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@UserName", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = username;

            SqlParameter pRanking = sqlComm.Parameters.Add("@Ranking", SqlDbType.NVarChar, 100);
            pRanking.Direction = ParameterDirection.Input;
            pRanking.Value = ranking;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        #endregion

        #region ChangeMemberPermission
        public static void ChangeMemberPermission(string UserName, bool isBlock, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_ChangeMemberPermission", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@UserName", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = UserName;

            SqlParameter pIsBlock = sqlComm.Parameters.Add("@IsBlock", SqlDbType.Bit);
            pIsBlock.Direction = ParameterDirection.Input;
            pIsBlock.Value = isBlock;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }
        #endregion

        #region ChangeOwnership
        public static void UpdateChangeOwnership(string username, string surname, string updatedBy,
                                                out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_ChangeOwnership", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@UserName", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = username;

            //SqlParameter pCurrentPassword = sqlComm.Parameters.Add("@FirstName", SqlDbType.NVarChar, 60);
            //pCurrentPassword.Direction = ParameterDirection.Input;
            //pCurrentPassword.Value = firstname;

            SqlParameter pFullName = sqlComm.Parameters.Add("@Surname", SqlDbType.NVarChar, 60);
            pFullName.Direction = ParameterDirection.Input;
            pFullName.Value = surname;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }
        #endregion

        public static DataSet GetMemberStatistic()
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMemberStatistic", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }

        public static DataSet GetMemberIncome()
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMemberIncome", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }

        public static DataSet GetMemberIncomeAll()
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMemberIncomeAll", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }

        #region GetMemberByUserName

        public static DataSet GetMemberByUserName(string UserName, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMemberByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = UserName;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetMemberInfoByUserName(string UserName, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMemberInfoByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = UserName;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        #endregion
            
        #region View Member Password

        public static DataSet GetMemberPassword(string searchMember, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetMemberPassword", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    var pSearchName = sqlComm.Parameters.Add("@SearchName", SqlDbType.NVarChar, 120);
                    pSearchName.Direction = ParameterDirection.Input;
                    pSearchName.Value = searchMember;

                    var pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    var pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        #endregion

        #region GetMemberTotalSponsor
        public static DataSet GetMemberTotalSponsor(double sponsorAmount, DateTime? startDate, DateTime? endDate, string SearchFirstName, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetMemberTotalSponsor", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pSponsorAmount = sqlComm.Parameters.Add("@SponsorAmount", SqlDbType.Float);
                    pSponsorAmount.Direction = ParameterDirection.Input;
                    pSponsorAmount.Value = sponsorAmount;

                    SqlParameter pStartDate = sqlComm.Parameters.Add("@StartDate", SqlDbType.DateTime);
                    pStartDate.Direction = ParameterDirection.Input;
                    pStartDate.Value = startDate;

                    SqlParameter pEndDate = sqlComm.Parameters.Add("@EndDate", SqlDbType.DateTime);
                    pEndDate.Direction = ParameterDirection.Input;
                    pEndDate.Value = endDate;

                    SqlParameter pFirstName = sqlComm.Parameters.Add("@FirstName", SqlDbType.NVarChar, 60);
                    pFirstName.Direction = ParameterDirection.Input;
                    pFirstName.Value = SearchFirstName;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }
        #endregion

        #region ChangeSponsor

        public static void UpdateChangeSponsor(string UserName, string CurrIntro, string NewIntro, string updatedBy,
                                                out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_ChangeSponsor", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@UserName", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = UserName;

            SqlParameter pCurrIntro = sqlComm.Parameters.Add("@CurrIntro", SqlDbType.NVarChar, 20);
            pCurrIntro.Direction = ParameterDirection.Input;
            pCurrIntro.Value = CurrIntro;

            SqlParameter pNewIntro = sqlComm.Parameters.Add("@NewIntro", SqlDbType.NVarChar, 20);
            pNewIntro.Direction = ParameterDirection.Input;
            pNewIntro.Value = NewIntro;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }
        #endregion

        #region ChangeUpline
        public static void UpdateChangeUpline(string UserName, string NewUpline, string Position, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_ChangeUpline", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@UserName", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = UserName;

            SqlParameter pCurrIntro = sqlComm.Parameters.Add("@NewUpline", SqlDbType.NVarChar, 50);
            pCurrIntro.Direction = ParameterDirection.Input;
            pCurrIntro.Value = NewUpline;

            SqlParameter pNewIntro = sqlComm.Parameters.Add("@NewPosition", SqlDbType.NVarChar, 50);
            pNewIntro.Direction = ParameterDirection.Input;
            pNewIntro.Value = Position;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }
        #endregion
    }
}