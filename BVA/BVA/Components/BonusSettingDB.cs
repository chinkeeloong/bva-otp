﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ECFBase.Components
{
    public class BonusSettingDB
    {
        #region Get
       
        public static DataSet GetCurrentSales(int selectedPage, int selectedYear, int selectedMonth,int Country, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetCurrentSales", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pYear = sqlComm.Parameters.Add("@year", SqlDbType.Int);
            pYear.Direction = ParameterDirection.Input;
            pYear.Value = selectedYear;

            var pMonth = sqlComm.Parameters.Add("@month", SqlDbType.Int);
            pMonth.Direction = ParameterDirection.Input;
            pMonth.Value = selectedMonth;

            var pCountry = sqlComm.Parameters.Add("@Country", SqlDbType.Int);
            pCountry.Direction = ParameterDirection.Input;
            pCountry.Value = Country;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetBonusListingByDate(int selectedPage, string DateFrom, string DateTo, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_BonusListingByDate", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pDateFrom = sqlComm.Parameters.Add("@DateFrom", SqlDbType.NVarChar, 50);
            pDateFrom.Direction = ParameterDirection.Input;
            pDateFrom.Value = DateFrom;

            var pDateTo = sqlComm.Parameters.Add("@DateTo", SqlDbType.NVarChar, 50);
            pDateTo.Direction = ParameterDirection.Input;
            pDateTo.Value = DateTo;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetCurrentDailySales(int selectedPage, int selectedYear, int selectedMonth, int Country, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetCurrentDailySales", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pYear = sqlComm.Parameters.Add("@year", SqlDbType.Int);
            pYear.Direction = ParameterDirection.Input;
            pYear.Value = selectedYear;

            var pMonth = sqlComm.Parameters.Add("@month", SqlDbType.Int);
            pMonth.Direction = ParameterDirection.Input;
            pMonth.Value = selectedMonth;

            var pCountry = sqlComm.Parameters.Add("@Country", SqlDbType.Int);
            pCountry.Direction = ParameterDirection.Input;
            pCountry.Value = Country;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetRPReport(int selectedPage, string DateFrom, string DateTo, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetRPReport", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pDateFrom = sqlComm.Parameters.Add("@DateFrom", SqlDbType.NVarChar, 50);
            pDateFrom.Direction = ParameterDirection.Input;
            pDateFrom.Value = DateFrom;

            var pDateTo = sqlComm.Parameters.Add("@DateTo", SqlDbType.NVarChar, 50);
            pDateTo.Direction = ParameterDirection.Input;
            pDateTo.Value = DateTo;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }
        public static DataSet GetCWReport(int selectedPage, string DateFrom, string DateTo, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetCWReport", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pDateFrom = sqlComm.Parameters.Add("@DateFrom", SqlDbType.NVarChar, 50);
            pDateFrom.Direction = ParameterDirection.Input;
            pDateFrom.Value = DateFrom;

            var pDateTo = sqlComm.Parameters.Add("@DateTo", SqlDbType.NVarChar, 50);
            pDateTo.Direction = ParameterDirection.Input;
            pDateTo.Value = DateTo;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }
        public static DataSet GetMemberReport(int selectedPage, string DateFrom, string DateTo, string Country , string Rank, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetMemberReport", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pDateFrom = sqlComm.Parameters.Add("@DateFrom", SqlDbType.NVarChar, 50);
            pDateFrom.Direction = ParameterDirection.Input;
            pDateFrom.Value = DateFrom;

            var pDateTo = sqlComm.Parameters.Add("@DateTo", SqlDbType.NVarChar, 50);
            pDateTo.Direction = ParameterDirection.Input;
            pDateTo.Value = DateTo;

            var pCountry = sqlComm.Parameters.Add("@Country", SqlDbType.NVarChar, 50);
            pCountry.Direction = ParameterDirection.Input;
            pCountry.Value = Country;

            var pRank = sqlComm.Parameters.Add("@RANK", SqlDbType.NVarChar, 50);
            pRank.Direction = ParameterDirection.Input;
            pRank.Value = Rank;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllRankFromMemberReport(out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetRankfromMemberReport", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            //SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            //pViewPage.Direction = ParameterDirection.Input;
            //pViewPage.Value = viewPage;

            //SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            //pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            //pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllCountryFromMemberReport(out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetCountryfromMemberReport", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            //SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            //pViewPage.Direction = ParameterDirection.Input;
            //pViewPage.Value = viewPage;

            //SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            //pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            //pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }
        #endregion

    }
}