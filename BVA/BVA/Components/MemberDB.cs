﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace ECFBase.Components
{
    public class MemberDB
    {

        #region Share
        public static DataSet GetMemberShareDetail(string username, string Rate)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_MemberShareDetail", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pRATE = sqlComm.Parameters.Add("@RATE", SqlDbType.Money);
            pRATE.Direction = ParameterDirection.Input;
            pRATE.Value = Convert.ToDecimal(Rate);

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }
              
        public static DataSet ShareDetail(string Username)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_ShareDetail", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
                    pUsername.Direction = ParameterDirection.Input;
                    pUsername.Value = Username;


                    dataAdapter.Fill(dataSet);
                    sqlConn.Close();
                }
            }

            return dataSet;
        }
        public static DataSet GetShareSellingDetailByRate(string Rate )
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_Share_GetShareSellingDetailByRate", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pRate = sqlComm.Parameters.Add("@RATE", SqlDbType.Money);
                    pRate.Direction = ParameterDirection.Input;
                    pRate.Value = Rate;
                    
                    dataAdapter.Fill(dataSet);
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetAllCompanyShare()
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_Share_GetAllCompanyShare", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    

                    dataAdapter.Fill(dataSet);
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetAllBuyingQueue()
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_Share_GetAllBuyingQueue", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;



                    dataAdapter.Fill(dataSet);
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetAllSellingQueue(string Rate)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_Share_GetAllSellingQueue", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;


                    SqlParameter pRate = sqlComm.Parameters.Add("@RATE", SqlDbType.Money);
                    pRate.Direction = ParameterDirection.Input;
                    pRate.Value = float.Parse(Rate);

                    dataAdapter.Fill(dataSet);
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetMemberEfund()
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_Share_GetMemberEfund", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;



                    dataAdapter.Fill(dataSet);
                    sqlConn.Close();
                }
            }

            return dataSet;
        }
        public static void CancelPlaceShare(string ID, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_Share_CancelPlaceOrder", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.Int);
            pID.Direction = ParameterDirection.Input;
            pID.Value = Convert.ToInt32(ID);

            SqlParameter pMessage = sqlComm.Parameters.Add("@MSG", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;


            sqlComm.ExecuteNonQuery();
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }


        public static void CancelPlaceShare(string ID)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_Share_CancelPlaceOrder", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.Int);
            pID.Direction = ParameterDirection.Input;
            pID.Value = Convert.ToInt32(ID);


            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }

        public static void CompanyAccCancelPlaceShare(string ID)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_Share_AdminCancelPlaceOrder", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.Int);
            pID.Direction = ParameterDirection.Input;
            pID.Value = Convert.ToInt32(ID);


            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }

        public static void SellShare(string Username, string Quantity, string Rate)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_SellShare", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pQuantity = sqlComm.Parameters.Add("@QUANTITY", SqlDbType.Int);
            pQuantity.Direction = ParameterDirection.Input;
            pQuantity.Value = Convert.ToInt32(Quantity);

            SqlParameter pRate = sqlComm.Parameters.Add("@RATE", SqlDbType.Money);
            pRate.Direction = ParameterDirection.Input;
            pRate.Value = Rate;

            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }

        public static void InsertShareUnitSell(string Rate, string Unit, string Type)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_InsertSellShareUnit", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pRate = sqlComm.Parameters.Add("@RATE", SqlDbType.Money);
            pRate.Direction = ParameterDirection.Input;
            pRate.Value = Rate;

            SqlParameter pUnit = sqlComm.Parameters.Add("@UNIT", SqlDbType.Int);
            pUnit.Direction = ParameterDirection.Input;
            pUnit.Value = Convert.ToInt32(Unit);

            SqlParameter pType = sqlComm.Parameters.Add("@TYPE", SqlDbType.NVarChar, 50);
            pType.Direction = ParameterDirection.Input;
            pType.Value = Type;

            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }

        #endregion

        #region GetAllMembers
        
        public static DataSet GetAllMembers(string Text, int Day , int Month , int Year, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMember", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;
                    sqlComm.CommandTimeout = 60;

                    SqlParameter pText = sqlComm.Parameters.Add("@Text", SqlDbType.NVarChar, 50);
                    pText.Direction = ParameterDirection.Input;
                    pText.Value = Text;

                    SqlParameter pDay = sqlComm.Parameters.Add("@Day", SqlDbType.Int);
                    pDay.Direction = ParameterDirection.Input;
                    pDay.Value = Day;

                    SqlParameter pMonth = sqlComm.Parameters.Add("@Month", SqlDbType.Int);
                    pMonth.Direction = ParameterDirection.Input;
                    pMonth.Value = Month;

                    SqlParameter pYear = sqlComm.Parameters.Add("@Year", SqlDbType.Int);
                    pYear.Direction = ParameterDirection.Input;
                    pYear.Value = Year;                    

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetAllMemberByUsername(string Text, int Day, int Month, int Year, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMemberByUsername", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pText = sqlComm.Parameters.Add("@Text", SqlDbType.NVarChar, 50);
                    pText.Direction = ParameterDirection.Input;
                    pText.Value = Text;

                    SqlParameter pDay = sqlComm.Parameters.Add("@Day", SqlDbType.Int);
                    pDay.Direction = ParameterDirection.Input;
                    pDay.Value = Day;

                    SqlParameter pMonth = sqlComm.Parameters.Add("@Month", SqlDbType.Int);
                    pMonth.Direction = ParameterDirection.Input;
                    pMonth.Value = Month;

                    SqlParameter pYear = sqlComm.Parameters.Add("@Year", SqlDbType.Int);
                    pYear.Direction = ParameterDirection.Input;
                    pYear.Value = Year;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetAllMemberByFullName(string Text, int Day, int Month, int Year, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMemberByFullName", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pText = sqlComm.Parameters.Add("@Text", SqlDbType.NVarChar, 50);
                    pText.Direction = ParameterDirection.Input;
                    pText.Value = Text;

                    SqlParameter pDay = sqlComm.Parameters.Add("@Day", SqlDbType.Int);
                    pDay.Direction = ParameterDirection.Input;
                    pDay.Value = Day;

                    SqlParameter pMonth = sqlComm.Parameters.Add("@Month", SqlDbType.Int);
                    pMonth.Direction = ParameterDirection.Input;
                    pMonth.Value = Month;

                    SqlParameter pYear = sqlComm.Parameters.Add("@Year", SqlDbType.Int);
                    pYear.Direction = ParameterDirection.Input;
                    pYear.Value = Year;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetAllMemberByRanking(string Text, int Day, int Month, int Year, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMemberByRanking", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pText = sqlComm.Parameters.Add("@Text", SqlDbType.NVarChar, 50);
                    pText.Direction = ParameterDirection.Input;
                    pText.Value = Text;

                    SqlParameter pDay = sqlComm.Parameters.Add("@Day", SqlDbType.Int);
                    pDay.Direction = ParameterDirection.Input;
                    pDay.Value = Day;

                    SqlParameter pMonth = sqlComm.Parameters.Add("@Month", SqlDbType.Int);
                    pMonth.Direction = ParameterDirection.Input;
                    pMonth.Value = Month;

                    SqlParameter pYear = sqlComm.Parameters.Add("@Year", SqlDbType.Int);
                    pYear.Direction = ParameterDirection.Input;
                    pYear.Value = Year;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetAllMemberByPackage(string Text, int Day, int Month, int Year, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMemberByPackage", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pText = sqlComm.Parameters.Add("@Text", SqlDbType.NVarChar, 50);
                    pText.Direction = ParameterDirection.Input;
                    pText.Value = Text;

                    SqlParameter pDay = sqlComm.Parameters.Add("@Day", SqlDbType.Int);
                    pDay.Direction = ParameterDirection.Input;
                    pDay.Value = Day;

                    SqlParameter pMonth = sqlComm.Parameters.Add("@Month", SqlDbType.Int);
                    pMonth.Direction = ParameterDirection.Input;
                    pMonth.Value = Month;

                    SqlParameter pYear = sqlComm.Parameters.Add("@Year", SqlDbType.Int);
                    pYear.Direction = ParameterDirection.Input;
                    pYear.Value = Year;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetAllMemberBySponsor(string Text, int Day, int Month, int Year, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMemberBySponsor", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pText = sqlComm.Parameters.Add("@Text", SqlDbType.NVarChar, 50);
                    pText.Direction = ParameterDirection.Input;
                    pText.Value = Text;

                    SqlParameter pDay = sqlComm.Parameters.Add("@Day", SqlDbType.Int);
                    pDay.Direction = ParameterDirection.Input;
                    pDay.Value = Day;

                    SqlParameter pMonth = sqlComm.Parameters.Add("@Month", SqlDbType.Int);
                    pMonth.Direction = ParameterDirection.Input;
                    pMonth.Value = Month;

                    SqlParameter pYear = sqlComm.Parameters.Add("@Year", SqlDbType.Int);
                    pYear.Direction = ParameterDirection.Input;
                    pYear.Value = Year;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetAllMemberByCountry(string Text, int Day, int Month, int Year, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMemberByCountry", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pText = sqlComm.Parameters.Add("@Text", SqlDbType.NVarChar, 50);
                    pText.Direction = ParameterDirection.Input;
                    pText.Value = Text;

                    SqlParameter pDay = sqlComm.Parameters.Add("@Day", SqlDbType.Int);
                    pDay.Direction = ParameterDirection.Input;
                    pDay.Value = Day;

                    SqlParameter pMonth = sqlComm.Parameters.Add("@Month", SqlDbType.Int);
                    pMonth.Direction = ParameterDirection.Input;
                    pMonth.Value = Month;

                    SqlParameter pYear = sqlComm.Parameters.Add("@Year", SqlDbType.Int);
                    pYear.Direction = ParameterDirection.Input;
                    pYear.Value = Year;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetAllMembersUpgrade(string Option, string Text, int Day, int Month, int Year, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMemberUpgrade", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pOption = sqlComm.Parameters.Add("@Option", SqlDbType.NVarChar, 50);
                    pOption.Direction = ParameterDirection.Input;
                    pOption.Value = Option;

                    SqlParameter pText = sqlComm.Parameters.Add("@Text", SqlDbType.NVarChar, 50);
                    pText.Direction = ParameterDirection.Input;
                    pText.Value = Text;

                    SqlParameter pDay = sqlComm.Parameters.Add("@Day", SqlDbType.Int);
                    pDay.Direction = ParameterDirection.Input;
                    pDay.Value = Day;

                    SqlParameter pMonth = sqlComm.Parameters.Add("@Month", SqlDbType.Int);
                    pMonth.Direction = ParameterDirection.Input;
                    pMonth.Value = Month;

                    SqlParameter pYear = sqlComm.Parameters.Add("@Year", SqlDbType.Int);
                    pYear.Direction = ParameterDirection.Input;
                    pYear.Value = Year;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetAllMembersUpgradeExport()
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMemberUpgradeExport", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    dataAdapter.Fill(dataSet);
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetAllMembersExport()
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMemberExport", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;                 

                    dataAdapter.Fill(dataSet);
                    sqlConn.Close();
                }
            }

            return dataSet;
        }
#endregion

        #region GetAllMembersByJoinedDate
        public static DataSet GetAllMembersByJoinedDate(string month, string year, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMembersByJoinedDate", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pSearchMonth = sqlComm.Parameters.Add("@searchMonth", SqlDbType.NVarChar, 50);
                    pSearchMonth.Direction = ParameterDirection.Input;
                    pSearchMonth.Value = month;

                    SqlParameter pYear = sqlComm.Parameters.Add("@searchYear", SqlDbType.NVarChar, 50);
                    pYear.Direction = ParameterDirection.Input;
                    pYear.Value = year;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }
        #endregion

        #region GetAllMembersByFilteringCriteria
        public static DataSet GetAllMembersByFilteringCriteria(string fc, string searchFC, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMembersByFilteringCriteria", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pFc = sqlComm.Parameters.Add("@fc", SqlDbType.NVarChar, 50);
                    pFc.Direction = ParameterDirection.Input;
                    pFc.Value = fc;

                    SqlParameter pSearchFc = sqlComm.Parameters.Add("@searchFc", SqlDbType.NVarChar, 100);
                    pSearchFc.Direction = ParameterDirection.Input;
                    pSearchFc.Value = searchFC;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }
        #endregion
       
        public static DataSet GetTime(out string Status)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetTime", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;
            
            SqlParameter pStatus = sqlComm.Parameters.Add("@Status", SqlDbType.NVarChar,50);
            pStatus.Direction = ParameterDirection.Output;     

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            Status = pStatus.Value.ToString(); 
            sqlConn.Close();

            return ds;
        }
        
        #region GetMemberByUsername
        public static DataSet GetMemberByUsername(string username, out int ok, out string message)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMemberByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            message = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }
        #endregion

        #region GetMemberMarketTreeByUsername
        public static DataSet GetMemberMarketTreeByUsername(string username, out int ok, out string message)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMemberMarketTreeByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            message = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }
        #endregion

        #region GetMemberByUsername
        public static DataSet GetMemberShareListBlockByMember(string username)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMemberShareListBlockByMember", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;
            

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetMemberShareListCompanyByMember(string username)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMemberShareListCompanyByMember", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;


            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();

            return ds;
        }
        #endregion

        #region GetMemberOperationLog
        public static DataSet GetMemberOperationLog(string username)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetUserOperationLogByUserName", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;


            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }
        #endregion

        #region UpdatePassAndPin
        public static void UpdatePassAndPin(string username, string password, string pin, out int ok)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdatePassAndPin", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = username;

            SqlParameter pPassword = sqlComm.Parameters.Add("@password", SqlDbType.NVarChar, 200);
            pPassword.Direction = ParameterDirection.Input;
            pPassword.Value = password;

            SqlParameter pPin = sqlComm.Parameters.Add("@pin", SqlDbType.NVarChar, 200);
            pPin.Direction = ParameterDirection.Input;
            pPin.Value = pin;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();
            ok = (int)pOk.Value;
            sqlConn.Close();
        }
        #endregion

        public static DataSet GetParameterBasedOnName(string Name)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetParameterBasedOnName", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pName = sqlComm.Parameters.Add("@NAME", SqlDbType.NVarChar, 50);
            pName.Direction = ParameterDirection.Input;
            pName.Value = Name;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);


            sqlConn.Close();

            return ds;
        }
      
        #region MakeMemberID
        public static DataSet MakeMemberID(out string username)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_MAKEID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.VarChar, 10);
            pID.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            username = pID.Value.ToString();
            sqlConn.Close();

            return ds;
        }
        #endregion

        #region ValidateUserPIN
        public static DataSet ValidateUserPIN(string username, string PIN, out int ok, out string message)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_ValidateUserPIN", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pPIN = sqlComm.Parameters.Add("@PIN", SqlDbType.NVarChar, 50);
            pPIN.Direction = ParameterDirection.Input;
            pPIN.Value = PIN;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            message = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }
        #endregion

        #region ActivateMember
        public static void InsertUserOperation(string UserName, string operation, string appOther, float appNumber)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_InsertUserOperation", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@UserName", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = UserName;

            SqlParameter pOperation = sqlComm.Parameters.Add("@Operation", SqlDbType.NVarChar, 100);
            pOperation.Direction = ParameterDirection.Input;
            pOperation.Value = operation;

            SqlParameter pAppOther = sqlComm.Parameters.Add("@AppOther", SqlDbType.NVarChar, 100);
            pAppOther.Direction = ParameterDirection.Input;
            pAppOther.Value = appOther;

            SqlParameter pAppNumber = sqlComm.Parameters.Add("@AppNumber", SqlDbType.Float);
            pAppNumber.Direction = ParameterDirection.Input;
            pAppNumber.Value = appNumber;

            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }
        #endregion
        
        #region GetAllMobileAgents
        public static DataSet GetAllMobileAgents(string searchMemberList, int viewPage, string languageCode, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMobileAgents", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pSearch = sqlComm.Parameters.Add("@searchMember", SqlDbType.NVarChar, 50);
                    pSearch.Direction = ParameterDirection.Input;
                    pSearch.Value = searchMemberList;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
                    pLanguageCode.Direction = ParameterDirection.Input;
                    pLanguageCode.Value = languageCode;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static void DisqualifiedMobile(string username, string admin)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DisqualifiedMobile", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pAdmin = sqlComm.Parameters.Add("@Admin", SqlDbType.NVarChar, 50);
            pAdmin.Direction = ParameterDirection.Input;
            pAdmin.Value = admin;

            sqlComm.ExecuteNonQuery();

            sqlConn.Close();
        }

        public static void AppointMobileUser(string username, string admin, string percentage)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_AppointMobileUser", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            var pPercentage = sqlComm.Parameters.Add("@percentage", SqlDbType.Int);
            pPercentage.Direction = ParameterDirection.Input;
            pPercentage.Value = percentage;

            SqlParameter pAdmin = sqlComm.Parameters.Add("@Admin", SqlDbType.NVarChar, 50);
            pAdmin.Direction = ParameterDirection.Input;
            pAdmin.Value = admin;

            sqlComm.ExecuteNonQuery();

            sqlConn.Close();
        }
        #endregion

        #region Share Block Member List
        public static DataSet GetAllBlockShareMember(string searchMemberList, int viewPage, string languageCode, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllBlockShareMember", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pSearch = sqlComm.Parameters.Add("@searchMember", SqlDbType.NVarChar, 50);
                    pSearch.Direction = ParameterDirection.Input;
                    pSearch.Value = searchMemberList;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
                    pLanguageCode.Direction = ParameterDirection.Input;
                    pLanguageCode.Value = languageCode;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static void DisqualifiedBlockShareMember(string username, string admin)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DisqualifiedBlockShareMember", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pAdmin = sqlComm.Parameters.Add("@Admin", SqlDbType.NVarChar, 50);
            pAdmin.Direction = ParameterDirection.Input;
            pAdmin.Value = admin;

            sqlComm.ExecuteNonQuery();

            sqlConn.Close();
        }

        public static void AppointBlockShareMember(string username, string admin)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_AppointBlockShareMember", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pAdmin = sqlComm.Parameters.Add("@Admin", SqlDbType.NVarChar, 50);
            pAdmin.Direction = ParameterDirection.Input;
            pAdmin.Value = admin;

            sqlComm.ExecuteNonQuery();

            sqlConn.Close();
        }

        #endregion

        #region Company Share Member List
        public static DataSet GetAllCompanyShareMember(string searchMemberList, int viewPage, string languageCode, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllCompanyShareMember", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pSearch = sqlComm.Parameters.Add("@searchMember", SqlDbType.NVarChar, 50);
                    pSearch.Direction = ParameterDirection.Input;
                    pSearch.Value = searchMemberList;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
                    pLanguageCode.Direction = ParameterDirection.Input;
                    pLanguageCode.Value = languageCode;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static void DisqualifiedCompanyShareMember(string username, string admin)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DisqualifiedCompanyShareMember", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pAdmin = sqlComm.Parameters.Add("@Admin", SqlDbType.NVarChar, 50);
            pAdmin.Direction = ParameterDirection.Input;
            pAdmin.Value = admin;

            sqlComm.ExecuteNonQuery();

            sqlConn.Close();
        }

        public static void AppointCompanyShareMember(string username, string admin)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_AppointCompanyShareMember", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pAdmin = sqlComm.Parameters.Add("@Admin", SqlDbType.NVarChar, 50);
            pAdmin.Direction = ParameterDirection.Input;
            pAdmin.Value = admin;

            sqlComm.ExecuteNonQuery();

            sqlConn.Close();
        }

        #endregion

        #region GetAllMobileAgents
        public static DataSet GetAllSpecialMember(string searchMemberList, int viewPage, string languageCode, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllSpecialMember", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pSearch = sqlComm.Parameters.Add("@searchMember", SqlDbType.NVarChar, 50);
                    pSearch.Direction = ParameterDirection.Input;
                    pSearch.Value = searchMemberList;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
                    pLanguageCode.Direction = ParameterDirection.Input;
                    pLanguageCode.Value = languageCode;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static void DisqualifiedSpecialMember(string username, string admin)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DisqualifiedSpecialMember", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pAdmin = sqlComm.Parameters.Add("@Admin", SqlDbType.NVarChar, 50);
            pAdmin.Direction = ParameterDirection.Input;
            pAdmin.Value = admin;

            sqlComm.ExecuteNonQuery();

            sqlConn.Close();
        }

        public static void AppointSpecialMember(string username, string admin)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_AppointSpecialMember", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pAdmin = sqlComm.Parameters.Add("@Admin", SqlDbType.NVarChar, 50);
            pAdmin.Direction = ParameterDirection.Input;
            pAdmin.Value = admin;

            sqlComm.ExecuteNonQuery();

            sqlConn.Close();
        }
        #endregion

        #region GetFollowID
        public static DataSet GetFollowID(string username)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetFollowID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }
        #endregion


        public static DataSet CheckRanking()
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_CheckRanking", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }

        #region MemberIsInMarketTreeByUsername
        public static void MemberIsInMarketTreeByUsername(string findUser, string belongToUser, out int isBelongTo)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();
            SqlCommand sqlComm = new SqlCommand("SP_MemberIsInMarketTreeByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pFindUser = sqlComm.Parameters.Add("@findUser", SqlDbType.NVarChar, 20);
            pFindUser.Direction = ParameterDirection.Input;
            pFindUser.Value = findUser;

            SqlParameter pbelongToUser = sqlComm.Parameters.Add("@belongToUser", SqlDbType.NVarChar, 20);
            pbelongToUser.Direction = ParameterDirection.Input;
            pbelongToUser.Value = belongToUser;

            SqlParameter pOk = sqlComm.Parameters.Add("@isBelongTo", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            isBelongTo = (int)pOk.Value;
            sqlConn.Close();
        }
        #endregion
        public static DataSet CheckMarketTreeSearchUser(string Username, string SearchUser)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_CheckMarketTreeSearchUser", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pSearchUser = sqlComm.Parameters.Add("@SEARCHUSER", SqlDbType.NVarChar, 50);
            pSearchUser.Direction = ParameterDirection.Input;
            pSearchUser.Value = SearchUser;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }

        #region GetMarketTreeByUsername
        public static DataSet GetMarketTreeByUsername(string username)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMarketTreeByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }
        #endregion

        #region GetAllMemberWalletBalance
        public static DataSet GetAllMemberWalletBalance(string searchMemberList, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMemberWalletBalance", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pSearch = sqlComm.Parameters.Add("@searchMember", SqlDbType.NVarChar, 50);
                    pSearch.Direction = ParameterDirection.Input;
                    pSearch.Value = searchMemberList;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }
        #endregion

        #region GetAllMemberWalletBalanceByJoinedDate
        public static DataSet GetAllMemberWalletBalanceByJoinedDate(string month, string year, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMemberWalletBalanceByJoinedDate", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pSearchMonth = sqlComm.Parameters.Add("@searchMonth", SqlDbType.NVarChar, 50);
                    pSearchMonth.Direction = ParameterDirection.Input;
                    pSearchMonth.Value = month;

                    SqlParameter pYear = sqlComm.Parameters.Add("@searchYear", SqlDbType.NVarChar, 50);
                    pYear.Direction = ParameterDirection.Input;
                    pYear.Value = year;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }
        #endregion

        #region GetAllMemberWalletBalanceByFilteringCriteria
        public static DataSet GetAllMemberWalletBalanceByFilteringCriteria(string fc, string searchFC, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMemberWalletBalanceByFilteringCriteria", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pFc = sqlComm.Parameters.Add("@fc", SqlDbType.NVarChar, 50);
                    pFc.Direction = ParameterDirection.Input;
                    pFc.Value = fc;

                    SqlParameter pSearchFc = sqlComm.Parameters.Add("@searchFc", SqlDbType.NVarChar, 100);
                    pSearchFc.Direction = ParameterDirection.Input;
                    pSearchFc.Value = searchFC;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }
        public static DataSet GetAllMemberWalletBalanceByRanking(string fc, string searchFC, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMemberWalletBalanceByRanking", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pFc = sqlComm.Parameters.Add("@fc", SqlDbType.NVarChar, 50);
                    pFc.Direction = ParameterDirection.Input;
                    pFc.Value = fc;

                    SqlParameter pSearchFc = sqlComm.Parameters.Add("@searchFc", SqlDbType.NVarChar, 100);
                    pSearchFc.Direction = ParameterDirection.Input;
                    pSearchFc.Value = searchFC;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }
        public static DataSet GetAllMemberWalletBalanceByPackage(string fc, string searchFC, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMemberWalletBalanceByPackage", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pFc = sqlComm.Parameters.Add("@fc", SqlDbType.NVarChar, 50);
                    pFc.Direction = ParameterDirection.Input;
                    pFc.Value = fc;

                    SqlParameter pSearchFc = sqlComm.Parameters.Add("@searchFc", SqlDbType.NVarChar, 100);
                    pSearchFc.Direction = ParameterDirection.Input;
                    pSearchFc.Value = searchFC;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }
        public static DataSet GetAllMemberWalletBalanceBySponsor(string fc, string searchFC, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMemberWalletBalanceBySponsor", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pFc = sqlComm.Parameters.Add("@fc", SqlDbType.NVarChar, 50);
                    pFc.Direction = ParameterDirection.Input;
                    pFc.Value = fc;

                    SqlParameter pSearchFc = sqlComm.Parameters.Add("@searchFc", SqlDbType.NVarChar, 100);
                    pSearchFc.Direction = ParameterDirection.Input;
                    pSearchFc.Value = searchFC;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }
        public static DataSet GetAllMemberWalletBalanceByCountry(string fc, string searchFC, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllMemberWalletBalanceByCountry", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pFc = sqlComm.Parameters.Add("@fc", SqlDbType.NVarChar, 50);
                    pFc.Direction = ParameterDirection.Input;
                    pFc.Value = fc;

                    SqlParameter pSearchFc = sqlComm.Parameters.Add("@searchFc", SqlDbType.NVarChar, 100);
                    pSearchFc.Direction = ParameterDirection.Input;
                    pSearchFc.Value = searchFC;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        #endregion

        #region GetAllPaymentMode
        public static DataSet GetAllPaymentMode(out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllPaymentMode", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        #endregion

        #region
        public static DataSet HasMultipleAccFunction(string username, out int ok, out string message)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_CheckHasMultipleAccFunction", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            message = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }
        #endregion
       
        public static DataSet GetMarketTreeMostLeft(string username)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_MarketTreeMostLeft", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar,50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }

        public static DataSet GetMarketTreeMostRight(string username)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_MarketTreeMostRight", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }
        
    }
}