﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using ECFBase.Models;

namespace ECFBase.Components
{
    public class MemberProfileDB
    {

        #region "View"

        public static DataSet GetMemberBankByUsername(string Username, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMemberBankByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllProvinceByCountry(string LanguageCode, string CountryCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllProvinceByCountry", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = CountryCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllCityByProvince(string LanguageCode, string ProvinceCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllCityByProvince", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pProvinceCode = sqlComm.Parameters.Add("@ProvinceCode", SqlDbType.NVarChar, 10);
            pProvinceCode.Direction = ParameterDirection.Input;
            pProvinceCode.Value = ProvinceCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllDistrictByCity(string LanguageCode, string CityCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllDistrictByCity", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pCityCode = sqlComm.Parameters.Add("@CityCode", SqlDbType.NVarChar, 10);
            pCityCode.Direction = ParameterDirection.Input;
            pCityCode.Value = CityCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        #endregion

        #region "Create"

        public static void CreateNewMemberBank(string CountryCode, string BankCode, string BankName, string Username, string Address1, string Address2, string swiftcode,
                                                string BranchName, string AccountNumber, string BeneficiaryName, string BeneficiaryIC,
                                                string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateMemberBank", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = CountryCode;

            SqlParameter pBankCode = sqlComm.Parameters.Add("@BankCode", SqlDbType.NVarChar, 10);
            pBankCode.Direction = ParameterDirection.Input;
            pBankCode.Value = BankCode;

            SqlParameter pBankName = sqlComm.Parameters.Add("@BankName", SqlDbType.NVarChar, 10);
            pBankName.Direction = ParameterDirection.Input;
            pBankName.Value = BankName;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pBranchName = sqlComm.Parameters.Add("@BranchName", SqlDbType.NVarChar, 100);
            pBranchName.Direction = ParameterDirection.Input;
            pBranchName.Value = BranchName;

            SqlParameter pBranchAdd1 = sqlComm.Parameters.Add("@Address1", SqlDbType.NVarChar, 100);
            pBranchAdd1.Direction = ParameterDirection.Input;
            pBranchAdd1.Value = Address1;

            SqlParameter pBranchAdd2 = sqlComm.Parameters.Add("@Address2", SqlDbType.NVarChar, 100);
            pBranchAdd2.Direction = ParameterDirection.Input;
            pBranchAdd2.Value = Address2;

            SqlParameter pswiftcode = sqlComm.Parameters.Add("@swiftcode", SqlDbType.NVarChar, 100);
            pswiftcode.Direction = ParameterDirection.Input;
            pswiftcode.Value = swiftcode;

            SqlParameter pAccountNumber = sqlComm.Parameters.Add("@AccountNumber", SqlDbType.NVarChar, 100);
            pAccountNumber.Direction = ParameterDirection.Input;
            pAccountNumber.Value = AccountNumber;

            SqlParameter pBeneficiary = sqlComm.Parameters.Add("@Beneficiary", SqlDbType.NVarChar, 100);
            pBeneficiary.Direction = ParameterDirection.Input;
            pBeneficiary.Value = BeneficiaryName;

            SqlParameter pBeneficiaryIC = sqlComm.Parameters.Add("@BeneficiaryIC", SqlDbType.NVarChar, 100);
            pBeneficiaryIC.Direction = ParameterDirection.Input;
            pBeneficiaryIC.Value = BeneficiaryIC;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pCreatedOn = sqlComm.Parameters.Add("@CreatedOn", SqlDbType.DateTime);
            pCreatedOn.Direction = ParameterDirection.Input;
            pCreatedOn.Value = DateTime.Now;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pDeletionState = sqlComm.Parameters.Add("@DeletionState", SqlDbType.Bit);
            pDeletionState.Direction = ParameterDirection.Input;
            pDeletionState.Value = false;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        #endregion

        #region "Update"

        public static void ChangePassword(string UserName, string CurrentPassword, string NewPassword, string updatedBy,
                                                out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_ChangePassword", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@UserName", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = UserName;

            SqlParameter pCurrentPassword = sqlComm.Parameters.Add("@Current", SqlDbType.NVarChar, 50);
            pCurrentPassword.Direction = ParameterDirection.Input;
            pCurrentPassword.Value = CurrentPassword;

            SqlParameter pNewPassword = sqlComm.Parameters.Add("@New", SqlDbType.NVarChar, 50);
            pNewPassword.Direction = ParameterDirection.Input;
            pNewPassword.Value = NewPassword;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void ChangePIN(string UserName, string CurrentPIN, string NewPIN, string updatedBy,
                                                out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_ChangePIN", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@UserName", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = UserName;

            SqlParameter pCurrentPIN = sqlComm.Parameters.Add("@Current", SqlDbType.NVarChar, 50);
            pCurrentPIN.Direction = ParameterDirection.Input;
            pCurrentPIN.Value = CurrentPIN;

            SqlParameter pNewPIN = sqlComm.Parameters.Add("@New", SqlDbType.NVarChar, 50);
            pNewPIN.Direction = ParameterDirection.Input;
            pNewPIN.Value = NewPIN;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateMemberBank(string CountryCode, string BankCode, string BankName, string Username, string Address1, string Address2, string swiftcode,
                                                string BranchName, string AccountNumber, string BeneficiaryName,
                                                string BeneficiaryIC, string BeneficiaryPhone, string BeneficiaryRelationship, string updatedBy,
                                                out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateMemberBank", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = CountryCode;

            SqlParameter pBankCode = sqlComm.Parameters.Add("@BankCode", SqlDbType.NVarChar, 10);
            pBankCode.Direction = ParameterDirection.Input;
            pBankCode.Value = BankCode;

            SqlParameter pBankName = sqlComm.Parameters.Add("@BankName", SqlDbType.NVarChar, 10);
            pBankName.Direction = ParameterDirection.Input;
            pBankName.Value = BankName;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pBranchName = sqlComm.Parameters.Add("@BranchName", SqlDbType.NVarChar, 100);
            pBranchName.Direction = ParameterDirection.Input;
            pBranchName.Value = BranchName;

            SqlParameter pBranchAdd1 = sqlComm.Parameters.Add("@Address1", SqlDbType.NVarChar, 100);
            pBranchAdd1.Direction = ParameterDirection.Input;
            pBranchAdd1.Value = Address1;

            SqlParameter pBranchAdd2 = sqlComm.Parameters.Add("@Address2", SqlDbType.NVarChar, 100);
            pBranchAdd2.Direction = ParameterDirection.Input;
            pBranchAdd2.Value = Address2;

            SqlParameter pswiftcode = sqlComm.Parameters.Add("@swiftcode", SqlDbType.NVarChar, 100);
            pswiftcode.Direction = ParameterDirection.Input;
            pswiftcode.Value = swiftcode;

            SqlParameter pAccountNumber = sqlComm.Parameters.Add("@AccountNumber", SqlDbType.NVarChar, 100);
            pAccountNumber.Direction = ParameterDirection.Input;
            pAccountNumber.Value = AccountNumber;

            SqlParameter pBeneficiary = sqlComm.Parameters.Add("@Beneficiary", SqlDbType.NVarChar, 100);
            pBeneficiary.Direction = ParameterDirection.Input;
            pBeneficiary.Value = BeneficiaryName;

            SqlParameter pBeneficiaryIC = sqlComm.Parameters.Add("@BeneficiaryIC", SqlDbType.NVarChar, 100);
            pBeneficiaryIC.Direction = ParameterDirection.Input;
            pBeneficiaryIC.Value = BeneficiaryIC;

            SqlParameter pBeneficiaryPhone = sqlComm.Parameters.Add("@BeneficiaryPhone", SqlDbType.NVarChar, 100);
            pBeneficiaryPhone.Direction = ParameterDirection.Input;
            pBeneficiaryPhone.Value = BeneficiaryPhone;

            SqlParameter pBeneficiaryRela = sqlComm.Parameters.Add("@BeneficiaryRela", SqlDbType.NVarChar, 100);
            pBeneficiaryRela.Direction = ParameterDirection.Input;
            pBeneficiaryRela.Value = " ";

            var pUpdatingUser = sqlComm.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar, 100);
            pUpdatingUser.Direction = ParameterDirection.Input;
            pUpdatingUser.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateMember(EditMemberModel mem, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateMember", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = mem.Member.Username;

            SqlParameter pFirstName = sqlComm.Parameters.Add("@FULLNAME", SqlDbType.NVarChar, 50);
            pFirstName.Direction = ParameterDirection.Input;
            pFirstName.Value = (mem.Member.FirstName == null) ? "" : mem.Member.FirstName;

            SqlParameter pIC = sqlComm.Parameters.Add("@IC", SqlDbType.NVarChar, 50);
            pIC.Direction = ParameterDirection.Input;
            pIC.Value = (mem.UserInfo.IC == null) ? "" : mem.UserInfo.IC;

            SqlParameter pDOB = sqlComm.Parameters.Add("@DOB", SqlDbType.DateTime);
            pDOB.Direction = ParameterDirection.Input;
            pDOB.Value = mem.UserInfo.DOB;

            SqlParameter pCellPhone = sqlComm.Parameters.Add("@HPPHONE", SqlDbType.NVarChar, 50);
            pCellPhone.Direction = ParameterDirection.Input;
            pCellPhone.Value = (mem.UserInfo.CellPhone == null) ? "" : mem.UserInfo.CellPhone;

            SqlParameter pEmail = sqlComm.Parameters.Add("@EMAIL", SqlDbType.NVarChar, 100);
            pEmail.Direction = ParameterDirection.Input;
            pEmail.Value = (mem.Member.MemberEmail == null) ? "" : mem.Member.MemberEmail;
            
            SqlParameter pAddress = sqlComm.Parameters.Add("@MailAddress", SqlDbType.NVarChar, 100);
            pAddress.Direction = ParameterDirection.Input;
            pAddress.Value = (mem.UserInfo.Address == null) ? "" : mem.UserInfo.Address;
                        
            SqlParameter pCity = sqlComm.Parameters.Add("@City", SqlDbType.NVarChar, 100);
            pCity.Direction = ParameterDirection.Input;
            pCity.Value = (mem.UserInfo.SelectedCity == null) ? "" : mem.UserInfo.SelectedCity;

            SqlParameter pState = sqlComm.Parameters.Add("@State", SqlDbType.NVarChar, 100);
            pState.Direction = ParameterDirection.Input;
            pState.Value = (mem.UserInfo.State == null) ? "" : mem.UserInfo.State;

            SqlParameter pPostcode = sqlComm.Parameters.Add("@Postcode", SqlDbType.NVarChar, 100);
            pPostcode.Direction = ParameterDirection.Input;
            pPostcode.Value = (mem.UserInfo.Postcode == null) ? "" : mem.UserInfo.Postcode;

            SqlParameter pCountry = sqlComm.Parameters.Add("@Country", SqlDbType.NVarChar, 100);
            pCountry.Direction = ParameterDirection.Input;
            pCountry.Value = (mem.SelectedCountry == null) ? "" : mem.SelectedCountry;

            SqlParameter pMailCountry = sqlComm.Parameters.Add("@MailCountry", SqlDbType.NVarChar, 100);
            pMailCountry.Direction = ParameterDirection.Input;
            pMailCountry.Value = (mem.SelectedCountry == null) ? "" : mem.SelectedCountry;

            SqlParameter pMobileCountry = sqlComm.Parameters.Add("@MobileCountry", SqlDbType.NVarChar, 50);
            pMobileCountry.Direction = ParameterDirection.Input;
            pMobileCountry.Value = (mem.UserInfo.MobileCountryCode == null) ? "" : mem.UserInfo.MobileCountryCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        #endregion

        #region "Delete"
        #endregion

    }
}