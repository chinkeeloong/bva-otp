﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using ECFBase.Models;

namespace ECFBase.Components
{
    public class MemberShareDB
    {
        public static DataSet GetShareDetail(string username)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Share_GetSharePageRelatedInformation ", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();
            return ds;
        }

        public static DataSet GetTotalInOutUnit(string username)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Share_GetTotalCreaditDebitUnit ", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();
            return ds;
        }

        public static DataSet GetAdminManualBuyShareRelatedInformation(string username)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Share_GetAdminManualBuyShareRelatedInformation ", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();
            return ds;
        }

        public static DataSet GetShareSellingDetail(string Rate)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Share_GetShareSellingRelatedInformation ", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pRate = sqlComm.Parameters.Add("@RATE", SqlDbType.Money);
            pRate.Direction = ParameterDirection.Input;
            pRate.Value = Rate;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();
            return ds;
        }
        public static DataSet GetRateDetail(string Rate)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Share_GetRateDetail ", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pRate = sqlComm.Parameters.Add("@RATE", SqlDbType.Money);
            pRate.Direction = ParameterDirection.Input;
            pRate.Value = Rate;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();
            return ds;
        }

        public static DataSet SellingQueueByMember(string Username, string Rate)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Share_SellingQueueByMember", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pRate = sqlComm.Parameters.Add("@Rate", SqlDbType.Money);
            pRate.Direction = ParameterDirection.Input;
            pRate.Value = Rate;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();
            return ds;
        }
        public static void BuyEFund(string Username,string WALLETTYPE, string Quantity, string Rate, out int ok)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_Share_BuyEFund", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pWALLETTYPE = sqlComm.Parameters.Add("@WALLETTYPE", SqlDbType.NVarChar, 50);
            pWALLETTYPE.Direction = ParameterDirection.Input;
            pWALLETTYPE.Value = WALLETTYPE;

            SqlParameter pQuantity = sqlComm.Parameters.Add("@QUANTITY", SqlDbType.Money);
            pQuantity.Direction = ParameterDirection.Input;
            pQuantity.Value = float.Parse(Quantity);

            SqlParameter pRate = sqlComm.Parameters.Add("@RATE", SqlDbType.Money);
            pRate.Direction = ParameterDirection.Input;
            pRate.Value = Rate;

            SqlParameter pOk = sqlComm.Parameters.Add("@OK", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            sqlConn.Close();
        }
        public static void BuyEFundAdmin(string Username, string Quantity, string Rate)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_Share_Admin_BuyEFund", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pQuantity = sqlComm.Parameters.Add("@QUANTITY", SqlDbType.Int);
            pQuantity.Direction = ParameterDirection.Input;
            pQuantity.Value = Convert.ToInt32(Quantity);

            SqlParameter pRate = sqlComm.Parameters.Add("@RATE", SqlDbType.Money);
            pRate.Direction = ParameterDirection.Input;
            pRate.Value = Rate;

            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }

        public static void Oversubscribed(string Username, string Quantity)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_Share_Insert_Oversubscribed", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pQuantity = sqlComm.Parameters.Add("@QUANTITY", SqlDbType.Int);
            pQuantity.Direction = ParameterDirection.Input;
            pQuantity.Value = Convert.ToInt32(Quantity);
            

            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }
        public static DataSet GetAllBuyingQueueByMember(string Username)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_Share_GetAllBuyingQueueByMember", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
                    pUsername.Direction = ParameterDirection.Input;
                    pUsername.Value = Username;


                    dataAdapter.Fill(dataSet);
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetAllSellingQueueByMember(string Username)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_Share_GetAllSellingQueueByMember", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
                    pUsername.Direction = ParameterDirection.Input;
                    pUsername.Value = Username;


                    dataAdapter.Fill(dataSet);
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetRateList()
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Share_GetRateList ", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();
            return ds;
        }       

        public static void SellEFund(string Username, string Unit, string Rate, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_Share_SellEFund", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pUnit = sqlComm.Parameters.Add("@UNIT", SqlDbType.Int);
            pUnit.Direction = ParameterDirection.Input;
            pUnit.Value = Unit;
           
            SqlParameter pRate = sqlComm.Parameters.Add("@RATE", SqlDbType.Money);
            pRate.Direction = ParameterDirection.Input;
            pRate.Value = Rate;

            SqlParameter pMessage = sqlComm.Parameters.Add("@MSG", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;



            sqlComm.ExecuteNonQuery();
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void SellEFundAdmin(string Username, string Unit, string Rate)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_Share_SellEFundAdmin", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pUnit = sqlComm.Parameters.Add("@UNIT", SqlDbType.Int);
            pUnit.Direction = ParameterDirection.Input;
            pUnit.Value = Unit;

            SqlParameter pRate = sqlComm.Parameters.Add("@RATE", SqlDbType.Money);
            pRate.Direction = ParameterDirection.Input;
            pRate.Value = Rate;


            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }

    }
}
