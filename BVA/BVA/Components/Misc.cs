﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using ECFBase.Models;
using System.Data;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Net.Mail;

namespace ECFBase.Components
{
    public class Misc
    {
        private static string pickupLocation = @"C:\inetpub\mailroot\Pickup";
        private static string smtpServer = "localhost";
        //Test Side
        private static string username = "testjuta_admin";
        private static string password = "UbjZxK0Uoj69n1";
        //Live Side
        //private static string username = "juta_admin";
        //private static string password = "lijkhfVHJL9875^(";
        private static int port = 25;

        #region Get Listing For DropBox 
        public static List<SelectListItem> GetLanguageList(ref int ok, ref string message)
        {
            DataSet dsLanguage = GetAllLanguageList(out ok, out message);
            List<SelectListItem> languages = new List<SelectListItem>();

            foreach (DataRow dr in dsLanguage.Tables[0].Rows)
            {
                SelectListItem language = new SelectListItem();
                language.Text = dr["CLANG_NAME"].ToString();
                language.Value = dr["CLANG_CODE"].ToString();
                language.Selected = false;
                languages.Add(language);
            }
            return languages;
        }

        public static List<SelectListItem> GetMultipleAccountChoice()
        {
            var accounts = new List<SelectListItem>();

            var firstAcc = new SelectListItem();
            firstAcc.Text = "1";
            firstAcc.Value = "1";
            accounts.Add(firstAcc);

            var secondAcc = new SelectListItem();
            secondAcc.Text = "3";
            secondAcc.Value = "3";
            accounts.Add(secondAcc);

            var thirdAcc = new SelectListItem();
            thirdAcc.Text = "9";
            thirdAcc.Value = "9";
            accounts.Add(thirdAcc);

            var FourthAcc = new SelectListItem();
            FourthAcc.Text = "30";
            FourthAcc.Value = "30";
            accounts.Add(FourthAcc);

            return accounts;
        }

        //contain alphabet + number
        public static bool IsAlphaNum(string str)
        {
            return str.Any(char.IsLetter) && str.Any(char.IsNumber);
        }

        public static List<SelectListItem> GetAllOwnAccount(string Username)
        {
            int ok = 0;
            string msg = "";

            DataSet dsPaymentMode = PaymentModeSetupDB.GetAllOwnAccount(Username);
            List<SelectListItem> Method = new List<SelectListItem>();

            SelectListItem Methods = new SelectListItem();
            Methods.Value = "0";
            Methods.Text = Resources.OneForAll.OneForAll.dropboxSelect;
            Methods.Selected = false;
            Method.Add(Methods);

            foreach (DataRow dr in dsPaymentMode.Tables[0].Rows)
            {
                Methods = new SelectListItem();
                Methods.Value = dr["CUSR_USERNAME"].ToString();
                Methods.Text = dr["CUSR_USERNAME"].ToString();
                Methods.Selected = false;
                Method.Add(Methods);
            }

            return Method;
        }

        public static List<SelectListItem> GetAllPaymentMode()
        {
            int ok = 0;
            string msg = "";

            DataSet dsPaymentMode = PaymentModeSetupDB.GetAllPaymentMode(out ok, out msg);
            List<SelectListItem> Method = new List<SelectListItem>();

            foreach (DataRow dr in dsPaymentMode.Tables[0].Rows)
            {
                SelectListItem Methods = new SelectListItem();
                Methods.Value = dr["CPAYMD_TYPE"].ToString();
                Methods.Text = dr["CPAYMD_TYPE"].ToString();
                Methods.Selected = false;
                Method.Add(Methods);
            }

            return Method;
        }

        public static List<SelectListItem> GetallRankList()
        {
            var dsRank = GetallRank();
            List<SelectListItem> Rank = new List<SelectListItem>();

            foreach (var item in dsRank)
            {
                SelectListItem Rankss = new SelectListItem();
                Rankss.Text = item.RankName;
                Rankss.Value = item.RankCode;
                Rankss.Selected = false;
                Rank.Add(Rankss);
            }

            return Rank;
        }

        public static List<RankModel> GetallRank()
        {
            DataSet dsCategory = AdminGeneralDB.GetAllRankList();
            List<RankModel> Rank = new List<RankModel>();

            foreach (DataRow dr in dsCategory.Tables[0].Rows)
            {
                RankModel Ranks = new RankModel();
                Ranks.RankCode = dr["CRANKSET_CODE"].ToString();
                Ranks.RankName = dr["CRANKSET_NAME"].ToString();
                Rank.Add(Ranks);
            }
            return Rank;
        }

        public static List<CategoryModel> GetAllCategoryList(string LanguageCode, ref int ok, ref string message)
        {
            DataSet dsCategory = AdminGeneralDB.GetAllCategoryList(LanguageCode, out ok, out message);
            List<CategoryModel> categories = new List<CategoryModel>();

            foreach (DataRow dr in dsCategory.Tables[0].Rows)
            {
                CategoryModel category = new CategoryModel();
                category.CategoryCode = dr["CCAT_CODE"].ToString();
                category.CategoryName = dr["CATNAME"].ToString();
                categories.Add(category);
            }
            return categories;
        }

        public static List<SelectListItem> GetCurrencyList(ref int ok, ref string message)
        {
            var dsCurrency = GetAllCurrencyList(ref ok, ref message);
            List<SelectListItem> currencies = new List<SelectListItem>();

            foreach (var item in dsCurrency)
            {
                SelectListItem currency = new SelectListItem();
                currency.Text = item.CurrencyName;
                currency.Value = item.CountryCode;
                currency.Selected = false;
                currencies.Add(currency);
            }

            return currencies;
        }

        public static List<CurrencyModel> GetAllCurrencyList(ref int ok, ref string message)
        {
            int pages = 0;
            DataSet dsCountry = AdminGeneralDB.GetAllCurrency(1, out pages, out ok, out message);
            var currencies = new List<CurrencyModel>();

            foreach (DataRow dr in dsCountry.Tables[0].Rows)
            {
                CurrencyModel currency = new CurrencyModel();
                currency.CountryCode = dr["CCOUNTRY_CODE"].ToString();
                currency.CurrencyName = dr["CCURRENCY_NAME"].ToString();
                currencies.Add(currency);
            }

            return currencies;
        }

        public static List<SelectListItem> GetCountryList(string languageCode, ref int ok, ref string message)
        {
            var dsCountry = GetAllCountryList(languageCode, ref ok, ref message);
            List<SelectListItem> countries = new List<SelectListItem>();

            foreach (var item in dsCountry)
            {
                SelectListItem country = new SelectListItem();
                country.Text = item.CountryName;
                country.Value = item.CountryCode;
                country.Selected = false;
                countries.Add(country);
            }

            return countries;
        }

        public static List<CountrySetupModel> GetAllCountryList(string LanguageCode, ref int ok, ref string message)
        {
            DataSet dsCountry = AdminGeneralDB.GetAllCountryList(LanguageCode, out ok, out message);
            List<CountrySetupModel> countries = new List<CountrySetupModel>();

            CountrySetupModel country = new CountrySetupModel();
            country.CountryCode = "0";
            country.CountryName =  Resources.OneForAll.OneForAll.dropboxSelect;
            country.MobileCode = "0";
            country.CountryId = 0;
            countries.Add(country);

            foreach (DataRow dr in dsCountry.Tables[0].Rows)
            {
                country = new CountrySetupModel();
                country.CountryCode = dr["CCOUNTRY_CODE"].ToString();
                country.CountryName = dr["COUNTRYNAME"].ToString();
                country.MobileCode = dr["CCOUNTRY_MOBILECODE"].ToString();
                country.CountryId = Convert.ToInt32(dr["CCOUNTRY_ID"].ToString());
                countries.Add(country);
            }
            return countries;
        }

        public static List<CountrySetupModel> GetAllMobileCountryList(string LanguageCode, ref int ok, ref string message)
        {
            DataSet dsCountry = AdminGeneralDB.GetAllCountryList(LanguageCode, out ok, out message);
            List<CountrySetupModel> countries = new List<CountrySetupModel>();

            CountrySetupModel country = new CountrySetupModel();
            country.CountryCode = Resources.OneForAll.OneForAll.dropboxSelect;
            country.CountryName = Resources.OneForAll.OneForAll.dropboxSelect;
            country.MobileCode = "0";
            country.CountryId = 0;
            countries.Add(country);

            foreach (DataRow dr in dsCountry.Tables[0].Rows)
            {
                country = new CountrySetupModel();
                country.CountryCode = dr["CCOUNTRY_CODE"].ToString();
                country.CountryName = dr["COUNTRYNAME"].ToString();
                country.MobileCode = dr["CCOUNTRY_MOBILECODE"].ToString();
                country.CountryId = Convert.ToInt32(dr["CCOUNTRY_ID"].ToString());
                countries.Add(country);
            }
            return countries;
        }

        //public static List<PaginationPackageSetupModel> GetAllPackageList(string LanguageCode, int selectedPage, ref int pages, ref int ok, ref string message)
        //{
        //    DataSet dsPackage = AdminGeneralDB.GetAllExpiredPackage(LanguageCode, "R", selectedPage, out pages, out ok, out message);
        //    List<PaginationPackageSetupModel> Packages = new List<PaginationPackageSetupModel>();

        //    foreach (DataRow dr in dsPackage.Tables[0].Rows)
        //    {
        //        PackageSetupModel package = new PackageSetupModel();
        //        package.PackageCode = dr["CPKG_CODE"].ToString();
        //        package.PackageName = dr["CMULTILANGPACKAGE_NAME"].ToString();
        //        Packages.Add(package);
        //    }
        //    return Packages;
        //}

        public static List<CountrySetupModel> GetAllCountryID(string LanguageCode)
        {
            int ok = 0;
            string message = string.Empty;
            DataSet dsCountry = AdminGeneralDB.GetAllCountryList(LanguageCode, out ok, out message);
            List<CountrySetupModel> countries = new List<CountrySetupModel>();

            CountrySetupModel country = new CountrySetupModel();
            country.CountryCode = "0";
            country.CountryName = "ALL";
            country.MobileCode = "0";
            countries.Add(country);

            foreach (DataRow dr in dsCountry.Tables[0].Rows)
            {
                country = new CountrySetupModel();
                country.CountryCode = dr["CCOUNTRY_ID"].ToString();
                country.CountryName = dr["COUNTRYNAME"].ToString();
                country.MobileCode = dr["CCOUNTRY_MOBILECODE"].ToString();
                countries.Add(country);
            }
            return countries;
        }
        
        public static List<CashName> DBGetAllCashNameList(string wallettype)
        {
            List<CashName> Name = new List<CashName>();
            if (wallettype == "RP")
            {
                CashName CashName = new CashName();
                CashName.cashname = "0";
                CashName.cashnamedisplay = Resources.OneForAll.OneForAll.cnAll;
                Name.Add(CashName);

                DataSet ds = AdminGeneralDB.GetCashnameList(1);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    CashName = new CashName();
                    CashName.cashname = "";
                    CashName.cashnamedisplay = "";
                    Name.Add(CashName);
                }
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    CashName = new CashName();
                    CashName.cashname = dr["CREGWL_CASHNAME"].ToString();
                    CashName.cashnamedisplay = Misc.GetReadableCashName(dr["CREGWL_CASHNAME"].ToString());
                    Name.Add(CashName);
                }
            }
            else if (wallettype == "CRP")
            {
                CashName CashName = new CashName();
                CashName.cashname = "0";
                CashName.cashnamedisplay = Resources.OneForAll.OneForAll.cnAll;
                Name.Add(CashName);

                DataSet ds = AdminGeneralDB.GetCashnameList(8);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    CashName = new CashName();
                    CashName.cashname = "";
                    CashName.cashnamedisplay = "";
                    Name.Add(CashName);
                }
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    CashName = new CashName();
                    CashName.cashname = dr["CCOMWL_CASHNAME"].ToString();
                    CashName.cashnamedisplay = Misc.GetReadableCashName(dr["CCOMWL_CASHNAME"].ToString());
                    Name.Add(CashName);
                }
            }
            else if (wallettype == "TP")
            {
                CashName CashName = new CashName();
                CashName.cashname = "0";
                CashName.cashnamedisplay = Resources.OneForAll.OneForAll.cnAll;
                Name.Add(CashName);

                DataSet ds = AdminGeneralDB.GetCashnameList(9);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    CashName = new CashName();
                    CashName.cashname = "";
                    CashName.cashnamedisplay = "";
                    Name.Add(CashName);
                }
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    CashName = new CashName();
                    CashName.cashname = dr["CTRVWL_CASHNAME"].ToString();
                    CashName.cashnamedisplay = Misc.GetReadableCashName(dr["CTRVWL_CASHNAME"].ToString());
                    Name.Add(CashName);
                }
            }
            else if (wallettype == "CP")
            {
                CashName CashName = new CashName();
                CashName.cashname = "0";
                CashName.cashnamedisplay = Resources.OneForAll.OneForAll.cnAll;
                Name.Add(CashName);

                DataSet ds = AdminGeneralDB.GetCashnameList(2);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    CashName = new CashName();
                    CashName.cashname = "";
                    CashName.cashnamedisplay = "";
                    Name.Add(CashName);
                }
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    CashName = new CashName();
                    CashName.cashname = dr["CCSHWL_CASHNAME"].ToString();
                    CashName.cashnamedisplay = Misc.GetReadableCashName(dr["CCSHWL_CASHNAME"].ToString());
                    Name.Add(CashName);
                }
            }
            else if (wallettype == "MP")
            {
                CashName CashName = new CashName();
                CashName.cashname = "0";
                CashName.cashnamedisplay = Resources.OneForAll.OneForAll.cnAll;
                Name.Add(CashName);

                DataSet ds = AdminGeneralDB.GetCashnameList(3);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    CashName = new CashName();
                    CashName.cashname = "";
                    CashName.cashnamedisplay = "";
                    Name.Add(CashName);
                }
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    CashName = new CashName();
                    CashName.cashname = dr["CMPWL_CASHNAME"].ToString();
                    CashName.cashnamedisplay = Misc.GetReadableCashName(dr["CMPWL_CASHNAME"].ToString());
                    Name.Add(CashName);
                }
            }
            else if (wallettype == "GP")
            {
                CashName CashName = new CashName();
                CashName.cashname = "0";
                CashName.cashnamedisplay = Resources.OneForAll.OneForAll.cnAll;
                Name.Add(CashName);

                DataSet ds = AdminGeneralDB.GetCashnameList(4);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    CashName = new CashName();
                    CashName.cashname = "";
                    CashName.cashnamedisplay = "";
                    Name.Add(CashName);
                }
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    CashName = new CashName();
                    CashName.cashname = dr["GLDWL_CASHNAME"].ToString();
                    CashName.cashnamedisplay = Misc.GetReadableCashName(dr["GLDWL_CASHNAME"].ToString());
                    Name.Add(CashName);
                }
            }
            else if (wallettype == "RPC")
            {
                CashName CashName = new CashName();
                CashName.cashname = "0";
                CashName.cashnamedisplay = Resources.OneForAll.OneForAll.cnAll;
                Name.Add(CashName);

                DataSet ds = AdminGeneralDB.GetCashnameList(5);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    CashName = new CashName();
                    CashName.cashname = "";
                    CashName.cashnamedisplay = "";
                    Name.Add(CashName);
                }
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    CashName = new CashName();
                    CashName.cashname = dr["CRPCWL_CASHNAME"].ToString();
                    CashName.cashnamedisplay = Misc.GetReadableCashName(dr["CRPCWL_CASHNAME"].ToString());
                    Name.Add(CashName);
                }
            }
            else if (wallettype == "ICO")
            {
                CashName CashName = new CashName();
                CashName.cashname = "0";
                CashName.cashnamedisplay = Resources.OneForAll.OneForAll.cnAll;
                Name.Add(CashName);

                DataSet ds = AdminGeneralDB.GetCashnameList(6);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    CashName = new CashName();
                    CashName.cashname = "";
                    CashName.cashnamedisplay = "";
                    Name.Add(CashName);
                }
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    CashName = new CashName();
                    CashName.cashname = dr["CICOWL_CASHNAME"].ToString();
                    CashName.cashnamedisplay = Misc.GetReadableCashName(dr["CICOWL_CASHNAME"].ToString());
                    Name.Add(CashName);
                }
            }
            else if (wallettype == "RMP")
            {
                CashName CashName = new CashName();
                CashName.cashname = "0";
                CashName.cashnamedisplay = Resources.OneForAll.OneForAll.cnAll;
                Name.Add(CashName);

                DataSet ds = AdminGeneralDB.GetCashnameList(7);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    CashName = new CashName();
                    CashName.cashname = "";
                    CashName.cashnamedisplay = "";
                    Name.Add(CashName);
                }
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    CashName = new CashName();
                    CashName.cashname = dr["CRMPWL_CASHNAME"].ToString();
                    CashName.cashnamedisplay = Misc.GetReadableCashName(dr["CRMPWL_CASHNAME"].ToString());
                    Name.Add(CashName);
                }
            }

            return Name;
        }

        public static List<RateModel> GetTheOpenRate()
        {
            List<RateModel> RateList = new List<RateModel>();           

            DataSet ds = MemberShareDB.GetRateList();            
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                RateModel Rate = new RateModel();
                Rate.RateString = Convert.ToDecimal(dr["CSHARE_RATE"].ToString()).ToString("#.00");
                Rate.TheRate = Convert.ToDecimal(dr["CSHARE_RATE"].ToString()).ToString("#.00");
                RateList.Add(Rate);
            }            

            return RateList;
        }

        public static List<SelectListItem> GetRankList(int from, string languageCode, ref int ok, ref string message)
        {
            var dsRank = GetAllRankList(from, languageCode, ref ok, ref message);
            List<SelectListItem> allRank = new List<SelectListItem>();

            foreach (var item in dsRank)
            {
                SelectListItem rank = new SelectListItem();
                rank.Text = item.RankName;
                rank.Value = item.RankCode;
                rank.Selected = false;
                allRank.Add(rank);
            }

            return allRank;
        }

        public static List<RankSetupModel> GetAllRankList(int from, string LanguageCode, ref int ok, ref string message)
        {
            int page = 0;
            DataSet dsRank = new DataSet();

            if (from == 1)
            {
                dsRank = AdminGeneralDB.GetAllRankSetup(-1, out page, out ok, out message);
            }
            else
            {
                dsRank = AdminGeneralDB.GetAllRank(-1, out page, out ok, out message);
            }
            List<RankSetupModel> allRank = new List<RankSetupModel>();

            foreach (DataRow dr in dsRank.Tables[0].Rows)
            {
                RankSetupModel rank = new RankSetupModel();
                rank.RankCode = dr["CRANKSET_CODE"].ToString();
                rank.RankName = dr["CRANKSET_NAME"].ToString();
                allRank.Add(rank);
            }
            return allRank;
        }

        public static List<RankSetupModel> GetAllRankfromMemberRe(int from, string LanguageCode, ref int ok, ref string message)
        {
            int page = 0;
            DataSet dsRank = new DataSet();

            if (from == 1)
            {
                dsRank = AdminGeneralDB.GetAllRankSetup(-1, out page, out ok, out message);
            }
            else
            {
                dsRank = AdminGeneralDB.GetAllRank(-1, out page, out ok, out message);
            }
            List<RankSetupModel> allRank = new List<RankSetupModel>();

            foreach (DataRow dr in dsRank.Tables[0].Rows)
            {
                RankSetupModel rank = new RankSetupModel();
                rank.RankCode = dr["CRANKSET_CODE"].ToString();
                rank.RankName = dr["CRANKSET_NAME"].ToString();
                allRank.Add(rank);
            }
            return allRank;
        }
        public static List<ProductModel> GetAllProductByCategory(string LanguageCode, string categoryCode, ref int ok, ref string message)
        {
            DataSet dsProduct = AdminGeneralDB.GetAllProductByCategory(LanguageCode, categoryCode, out ok, out message);
            List<ProductModel> products = new List<ProductModel>();

            foreach (DataRow dr in dsProduct.Tables[0].Rows)
            {
                ProductModel product = new ProductModel();
                product.ProductCode = dr["CPRD_CODE"].ToString();
                product.ProductName = dr["PRODNAME"].ToString();
                products.Add(product);
            }
            return products;
        }

        public static List<ProvinceModel> GetAllProvinceByCountry(string LanguageCode, string CountryCode, ref int ok, ref string message)
        {
            DataSet dsProvince = MemberProfileDB.GetAllProvinceByCountry(LanguageCode, CountryCode, out ok, out message);
            List<ProvinceModel> provinces = new List<ProvinceModel>();
            ProvinceModel province = new ProvinceModel();
            foreach (DataRow dr in dsProvince.Tables[0].Rows)
            {
                province = new ProvinceModel();
                province.ProvinceCode = dr["CPROVINCE_CODE"].ToString();
                province.ProvinceName = dr["PROVINCENAME"].ToString();
                provinces.Add(province);
            }

            province = new ProvinceModel();
            province.ProvinceCode = "0";
            province.ProvinceName = "--- Please Select---";
            provinces.Add(province);

            return provinces;
        }

      

        public static List<CityModel> GetAllCityByProvince(string LanguageCode, string ProvinceCode, ref int ok, ref string message)
        {
            DataSet dsCity = MemberProfileDB.GetAllCityByProvince(LanguageCode, ProvinceCode, out ok, out message);
            List<CityModel> cities = new List<CityModel>();

            foreach (DataRow dr in dsCity.Tables[0].Rows)
            {
                CityModel city = new CityModel();
                city.CityCode = dr["CCITY_CODE"].ToString();
                city.CityName = dr["CITYNAME"].ToString();
                cities.Add(city);
            }
            return cities;
        }

        public static List<BankModel> GetAllBankByCountry(string LanguageCode, string CountryCode, ref int ok, ref string message)
        {
            DataSet dsBank = AdminGeneralDB.GetAllBankByCountry(LanguageCode, CountryCode, out ok, out message);
            List<BankModel> banks = new List<BankModel>();

            foreach (DataRow dr in dsBank.Tables[0].Rows)
            {
                BankModel bank = new BankModel();
                bank.BankCode = dr["CBANK_CODE"].ToString();
                bank.BankName = dr["BANKNAME"].ToString();
                banks.Add(bank);
            }

            BankModel bnk = new BankModel();
            bnk.BankCode = "null";
            bnk.BankName = Resources.OneForAll.OneForAll.lblSelected;
            banks.Add(bnk);

            return banks;
        }

        public static List<BankModel> GetAllBankByCountryProvince(string Country , string Province , string Language)
        {
           
            List<BankModel> banks = new List<BankModel>();

            if (Province != "0")
            {
                DataSet dsBank = AdminGeneralDB.GetAllBankByCountryProvince(Country, Province, Language);
                BankModel bank = new BankModel();
                foreach (DataRow dr in dsBank.Tables[0].Rows)
                {
                    bank = new BankModel();
                    bank.BankCode = dr["CBP_ID"].ToString();
                    bank.BankName = dr["CMULTILANGBANK_NAME"].ToString();
                    banks.Add(bank);
                }
            }

            

            BankModel bnk = new BankModel();
            bnk.BankCode = "0";
            bnk.BankName = "--- Please Select---";
            banks.Add(bnk);

            return banks;
        }
        public static string GetBankCountryProvincebyID(string ID, string language)
        {
            int ok = 0;
            string msg = string.Empty;
            string Result = string.Empty;

            var ds = AdminGeneralDB.GetBankProvinceByID(ID, language, out ok, out msg);

            if (ds.Tables[0].Rows.Count == 0)
            {
                Result = "";
            }
            else
            {
                Result = ds.Tables[0].Rows[0]["CMULTILANGBANK_NAME"].ToString();
            }
            return Result;
        }

        #endregion

        public static string GetParaByName(string Name)
        {

            string Result = string.Empty;

            var ds = MemberDB.GetParameterBasedOnName(Name);
            string result = ds.Tables[0].Rows[0]["CPARA_FLOATVALUE"].ToString();

            return result;
        }

        public static string GetCountryImageByCountryCode(string Code)
        {
            int ok = 0;
            string msg = string.Empty;
            string Result = string.Empty;

            var ds = AdminGeneralDB.GetCountryByCountryCode(Code, out ok, out msg);

            if (ds.Tables[0].Rows.Count == 0)
            {
                Result = "";
            }
            else
            {
                Result = ds.Tables[0].Rows[0]["CCOUNTRY_IMAGEPATH"].ToString();
            }
            return Result;
        }

        public static string GetCountryNameByCountryCode(string Code)
        {
            int ok = 0;
            string msg = string.Empty;
            string Result = string.Empty;

            var ds = AdminGeneralDB.GetCountryByCountryCode(Code, out ok, out msg);

            if (ds.Tables[0].Rows.Count == 0)
            {
                Result = "";
            }
            else
            {
                Result = ds.Tables[0].Rows[0]["CMULTILANGCOUNTRY_NAME"].ToString();
            }
            return Result;
        }
              
        public static string GetInboxType(string type)
        {
            if (Resources.OneForAll.OneForAll.lblGeneral == type)
            {
                return InboxType.General.ToString();
            }

            // Instead of empty, better with general type
            return InboxType.General.ToString();
        }

        public static int GetMaintainProductCharge(int quantity)
        {
            int shipCharges = 0;

            if (quantity <= 4)
                shipCharges = 2;
            else if (quantity <= 14)
                shipCharges = 3;
            else if (quantity >= 15)
                shipCharges = 5;

            return shipCharges;
        }

        public static string DeliveryStatus(string status)
        {
            switch (status.ToUpper())
            {
                case "0":
                    return Resources.OneForAll.OneForAll.lblStatusPending;
                case "1":
                    return Resources.OneForAll.OneForAll.lblStatusDelivered;
                case "ALL":
                    return "";
                case "PENDING":
                    return "0";
                case "DELIVERED":
                    return "1";
                default:
                    return Resources.OneForAll.OneForAll.lblStatusPending;
            }
        }

        public static string RankNumber(string Rank)
        {
            switch (Rank.ToUpper())
            {
                case "0":
                    return "Free Account";
                case "3":
                    return "BVA3";
                case "2":
                    return "BVA2";
                case "1":
                    return "BVA1";
                default:
                    return Rank;
            }
        }
        
        public static string GetMemberRanking(string ranking)
        {
            int ok = 0;
            string msg = "";
            int page = 0;

            DataSet ds = AdminGeneralDB.GetAllRank(-1, out page, out ok, out msg);

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (dr["CRANKSET_CODE"].ToString().Contains(ranking))
                {
                    return dr["CRANKSET_NAME"].ToString();
                }

            }

            return Resources.OneForAll.OneForAll.rankAgent;
        }

        public static string GetMemberRankingIcon(string ranking)
        {
            int ok = 0;
            string msg = "";
            int page = 0;

            DataSet ds = AdminGeneralDB.GetAllRank(-1, out page, out ok, out msg);

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (dr["CRANKSET_CODE"].ToString().Contains(ranking))
                {
                    return dr["CRANKSET_ICON"].ToString();
                }

            }

            return Resources.OneForAll.OneForAll.rankAgent;
        }

        public static DataSet GetAllLanguageList(out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllLanguageList", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }
        
        public static string GetWithdrawalStatus(string status)
        {
            switch (status)
            {
                case "0":
                    return Resources.OneForAll.OneForAll.lblStatusPending;
                case "1":
                    return Resources.OneForAll.OneForAll.lblStatusApproved;
                case "-1":
                    return Resources.OneForAll.OneForAll.lblStatusRefunded;
                case "-2":
                    return Resources.OneForAll.OneForAll.lblStatusRejected;
            }

            return Resources.OneForAll.OneForAll.lblStatusPending;
        }

        public static string GetDeliveryType(string status)
        {
            if (status == "0")
            {
                return "Send To Member";
            }
            else if (status == "1")
            {
                return "Pick Up";
            }
            else
                return "";


            //return 0;
        }

        public static void InsertWithdrawalStatus(ref List<string> list)
        {
            list.Add(Resources.OneForAll.OneForAll.lblStatusPending);
            list.Add(Resources.OneForAll.OneForAll.lblStatusApproved);
            list.Add(Resources.OneForAll.OneForAll.lblStatusRefunded);
        }

        public static string GetReadableCashName(string cashname)
        {
            string readableString = "";
            switch (cashname)
            {
                case "TRANRWLTTOCRP":
                    readableString = Resources.OneForAll.OneForAll.lblTransfer;
                    break;
                case "WB Adjustment":
                    readableString = Resources.OneForAll.OneForAll.cnWBAdjustment;
                    break;
                case "Trading Bonus ADJ":
                    readableString = Resources.OneForAll.OneForAll.cnTradingBonusADJ;
                    break;
                case "TRANRMWLTTORMWLT":
                    readableString = Resources.OneForAll.OneForAll.cnRMPTransfer;
                    break;
                case "Cancel Order":
                    readableString = Resources.OneForAll.OneForAll.cnCancelOrder;
                    break;
                case "Split Share":
                    readableString = Resources.OneForAll.OneForAll.cnSplitShare;
                    break;
                case "Register Buy E-Fund":
                    readableString = Resources.OneForAll.OneForAll.cnRegisterBuyEFund;
                    break;
                case "GP":
                    readableString = Resources.OneForAll.OneForAll.cnGP;
                    break;
                case "SELL":
                    readableString = Resources.OneForAll.OneForAll.cnSell;
                    break;
                case "To RPC":
                    readableString = Resources.OneForAll.OneForAll.cnToRPC;
                break;
                case "Processing Fees":
                    readableString = Resources.OneForAll.OneForAll.cnProcessingFees;
                break;
                case "Sell eFund":
                    readableString = Resources.OneForAll.OneForAll.cnSelleFund;
                    break;
                case "Trading Bonus":
                    readableString = Resources.OneForAll.OneForAll.cnTradingBonus;
                    break;
                case "Transfer Mp":
                    readableString = Resources.OneForAll.OneForAll.cnTransferMP;
                    break;
                case "RPC Refund":
                    readableString = Resources.OneForAll.OneForAll.cnRPCRefund;
                    break;
                case "RPC Buy eFund":
                    readableString = Resources.OneForAll.OneForAll.cnRPCBuyeFund;
                    break;
                case "CP Transfer MP":
                    readableString = Resources.OneForAll.OneForAll.cnCPTransferMP;
                    break;
                case "CP Transfer RP":
                    readableString = Resources.OneForAll.OneForAll.cnCPTransferRP;
                    break;
                case "Exchange MP":
                    readableString = Resources.OneForAll.OneForAll.cnExchangeMP;
                    break;
                case "Upgrade Package":
                    readableString = Resources.OneForAll.OneForAll.cnUpgradePackage;
                    break;                    
                case "All":
                    readableString = Resources.OneForAll.OneForAll.cnGPRefund;
                    break;
                case "GP Refund":
                    readableString = Resources.OneForAll.OneForAll.cnGPRefund;
                    break;
                case "Gold Mine":
                    readableString = Resources.OneForAll.OneForAll.cnGoldMine;
                    break;
                case "GP Buy eFund":
                    readableString = Resources.OneForAll.OneForAll.cnGPBuyeFund;
                    break;
                case "Direct Roll Up Bonus":
                    readableString = Resources.OneForAll.OneForAll.cnDirectRollUpBonus;
                    break;
                case "CPTRAN":
                    readableString = Resources.OneForAll.OneForAll.cnCPTransfer;
                    break;
                case "E-Share":
                    readableString = Resources.OneForAll.OneForAll.cnEshare;
                    break;
                case "To MP":
                    readableString = Resources.OneForAll.OneForAll.cnToMP;
                    break;
                case "To PP ":
                    readableString = Resources.OneForAll.OneForAll.cnToPP;
                    break;
                case "Admin Charges":
                    readableString = Resources.OneForAll.OneForAll.cnAdminCharges;
                    break;
                case "Sell Share":
                    readableString = Resources.OneForAll.OneForAll.cnEshareIncome;
                    break;
                case "Buy Share":
                    readableString = Resources.OneForAll.OneForAll.cnBuyshare;
                    break;
                case "TOPUP":
                    readableString = Resources.OneForAll.OneForAll.cncompanytopiup;
                    break;
                case "TRANRWLTTORWLT":
                    readableString = Resources.OneForAll.OneForAll.cntransferRP;
                    break;
                case "Register":
                    readableString = Resources.OneForAll.OneForAll.cnregister;
                    break;
                case "REG":
                    readableString = Resources.OneForAll.OneForAll.cnregister;
                    break;
                case "lblregister":
                    readableString = Resources.OneForAll.OneForAll.cnregister;
                    break;
                case "UPG":
                    readableString = Resources.OneForAll.OneForAll.cnUPG;
                    break;
                case "DED":
                    readableString = Resources.OneForAll.OneForAll.cnDED;
                    break;
                case "TRANRWLTTOBWLT":
                    readableString = Resources.OneForAll.OneForAll.cntransferPP;
                    break;
                case "TRANRWLTTOHWLT":
                    readableString = Resources.OneForAll.OneForAll.cnTransferHP;
                    break;
                case "TRANRWLTTOCWLT":
                    readableString = Resources.OneForAll.OneForAll.cnGPTransfer;
                    break;
                
                case "TRANRWLTTOMWLT":
                    readableString = Resources.OneForAll.OneForAll.cnTransferMP;
                    break; 
                case "TRAN":
                case "ADMTRAN":               
                    readableString = Resources.OneForAll.OneForAll.cnTRAN;
                    break;
                case "CHARGE":
                    readableString = Resources.OneForAll.OneForAll.cnAdminFees;
                    break;
                case "DAILYBONUS":
                    readableString = Resources.OneForAll.OneForAll.cnDAILYBONUS;
                    break;
                case "Pairing Bonus":
                    readableString = Resources.OneForAll.OneForAll.cnPairBonus;
                    break;
                case "Sponsor Bonus":
                    readableString = Resources.OneForAll.OneForAll.cnSponsorBonus;
                    break;
                case "ROLLUPSPONSOR":
                    readableString = Resources.OneForAll.OneForAll.cnROLLUPSPONSOR;
                    break;
                case "Matching Bonus":
                    readableString = Resources.OneForAll.OneForAll.cnMatchingBonus;
                    break;
                case "WDR":
                    readableString = Resources.OneForAll.OneForAll.cnWDR;
                    break;
                case "REJWDR":
                    readableString = Resources.OneForAll.OneForAll.cnREJWDR;
                    break;
                case "REFWDR":
                    readableString = Resources.OneForAll.OneForAll.cnREFWDR;
                    break;
                case "ADJUSTMENT":
                case "ADMTOPUP":
                case "ADMDED":
                    readableString = Resources.OneForAll.OneForAll.cnADJUSTMENT;
                    break;
                case "TRANEWLTTORWLT":
                    readableString = Resources.OneForAll.OneForAll.cnTransferToMember;
                    break;             
                case "Remark":
                    readableString = Resources.OneForAll.OneForAll.cnRemark;
                    break;
                case "SHAREFUND":
                    readableString = Resources.OneForAll.OneForAll.cnSHAREFUND;
                    break;
                case "mnuShareCert":
                    readableString = Resources.OneForAll.OneForAll.cnShareCert;
                    break;
                case "UPGRADE":
                    readableString = Resources.OneForAll.OneForAll.cnUPG;
                    break;
                case "SMSCHARGE":
                    readableString = Resources.OneForAll.OneForAll.cnSmsCharge;
                    break;
                case "TOSHARE":
                     readableString = Resources.OneForAll.OneForAll.cnTOSHAREWLT;
                     break;
                default:
                    readableString = cashname.Replace("YEAR", "年");
                    readableString = readableString.Replace("MONTH", "月份");
                    readableString = readableString.Replace("THENO_", "第");
                    readableString = readableString.Replace("PAYMENTNO_", "期");
                    break;
            }

            return readableString;
        }

        public static string GetTranslatedGeneralSetting(string paramName)
        {
            switch (paramName)
            {
                case "paraNameTableRows":
                    return Resources.OneForAll.OneForAll.paraNameTableRows;
                case "MinWithdrawAmt":
                    return Resources.OneForAll.OneForAll.paraNameMinWithdrawAmt;
                case "paraNameMaxWithdrawAmt":
                    return Resources.OneForAll.OneForAll.paraNameMaxWithdrawAmt;
                case "WithdrawalCharge":
                    return Resources.OneForAll.OneForAll.paraNameWithdrawalCharge;
                case "WithdrawalMore1000":
                    return Resources.OneForAll.OneForAll.paraNameWithdrawalMore1000;
                case "Number":
                    return Resources.OneForAll.OneForAll.paraNameNumber;
                case "USD":
                    return Resources.OneForAll.OneForAll.paraNameUSD;
                case "Money":
                    return Resources.OneForAll.OneForAll.paraNameuomMoney;
            }

            return paramName;
        }

        #region ConstructYears
        public static IEnumerable<SelectListItem> ConstructYears(int year = 1920)
        {
            var yearsList = new List<string>();
            DateTime dt = new DateTime(year, 1, 1);

            while (true)
            {
                yearsList.Add(dt.Year.ToString());
                dt = dt.AddYears(1);
                if (dt.Year > DateTime.Now.Year)
                {
                    break;
                }
            }
            

            yearsList.Insert(0 , "Years");

            var years = from c in yearsList
                        select new SelectListItem
                        {
                            Text = c,
                            Value = c
                        };

            return years;
        }

        #endregion

        #region ConstructsMonth
        public static IEnumerable<SelectListItem> ConstructsMonth()
        {
            List<string> monthsList = new List<string>();
            for (int z = 1; z <= 12; z++)
            {
                monthsList.Add(z.ToString());
            }

            monthsList.Insert(0, "Months");

            var months = from c in monthsList
                         select new SelectListItem
                         {
                             Text = c.ToString(),
                             Value = c.ToString()
                         };

            return months;
        }
        #endregion

        #region ConstructsDay
        public static IEnumerable<SelectListItem> ConstructsDay()
        {
            List<string> daysList = new List<string>();
            for (int z = 1; z <= 31; z++)
            {
                daysList.Add(z.ToString());
            }

            daysList.Insert(0, "Days");

            var months = from c in daysList
                         select new SelectListItem
                         {                     

                             Text = c.ToString(),
                             Value = c.ToString()
                         };
            

            return months;
        }
        #endregion

        #region Constructs Status Criteria
        public static IEnumerable<SelectListItem> ConstructsStatusCriteria()
        {
            List<SelectListItem> filteringCriteriaList = new List<SelectListItem>();

            SelectListItem allSelection = new SelectListItem();
            allSelection.Text = Resources.OneForAll.OneForAll.lblStatusAll;
            allSelection.Value = "ALL";
            filteringCriteriaList.Add(allSelection);

            SelectListItem pendingSelection = new SelectListItem();
            pendingSelection.Text = Resources.OneForAll.OneForAll.lblStatusPending;
            pendingSelection.Value = "PENDING";
            filteringCriteriaList.Add(pendingSelection);

            SelectListItem deliveredSelection = new SelectListItem();
            deliveredSelection.Text = Resources.OneForAll.OneForAll.lblStatusDelivered;
            deliveredSelection.Value = "DELIVERED";
            filteringCriteriaList.Add(deliveredSelection);

            return filteringCriteriaList;
        }

        #endregion

        #region Constructs Filtering Criteria
        public static IEnumerable<SelectListItem> ConstructsFilteringCriteria()
        {
            List<SelectListItem> filteringCriteriaList = new List<SelectListItem>();

            SelectListItem fcALL = new SelectListItem();
            fcALL.Text = Resources.OneForAll.OneForAll.lblAll;
            fcALL.Value = "0";
            filteringCriteriaList.Add(fcALL);

            SelectListItem fcUserName = new SelectListItem();
            fcUserName.Text = Resources.OneForAll.OneForAll.lblUsername;
            fcUserName.Value = "USERNAME";
            filteringCriteriaList.Add(fcUserName);

            SelectListItem fcFullName = new SelectListItem();
            fcFullName.Text = Resources.OneForAll.OneForAll.lblFullname;
            fcFullName.Value = "FULLNAME";
            filteringCriteriaList.Add(fcFullName);

            SelectListItem fcRanking = new SelectListItem();
            fcRanking.Text = Resources.OneForAll.OneForAll.lblRanking;
            fcRanking.Value = "RANKING";
            filteringCriteriaList.Add(fcRanking);

            SelectListItem fdPackage = new SelectListItem();
            fdPackage.Text = Resources.OneForAll.OneForAll.lblPackage;
            fdPackage.Value = "PACKAGE";
            filteringCriteriaList.Add(fdPackage);

            SelectListItem fcSponsor = new SelectListItem();
            fcSponsor.Text = Resources.OneForAll.OneForAll.lblSponsor;
            fcSponsor.Value = "SPONSOR";
            filteringCriteriaList.Add(fcSponsor);

            SelectListItem fcCountry = new SelectListItem();
            fcCountry.Text = Resources.OneForAll.OneForAll.lblCountryName;
            fcCountry.Value = "COUNTRY";
            filteringCriteriaList.Add(fcCountry);

            return filteringCriteriaList;
        }

        public static IEnumerable<SelectListItem> ConstructsNoticeFilteringCriteria()
        {
            List<SelectListItem> filteringCriteriaList = new List<SelectListItem>();

            var fcUserName = new SelectListItem();
            fcUserName.Text = Resources.OneForAll.OneForAll.lblUsername;
            fcUserName.Value = "CUSR_USERNAME";
            filteringCriteriaList.Add(fcUserName);

            var fcTitle = new SelectListItem();
            fcTitle.Text = Resources.OneForAll.OneForAll.lblTitle;
            fcTitle.Value = "CMULTILANGNOT_TITLE";
            filteringCriteriaList.Add(fcTitle);          

            return filteringCriteriaList;
        }
        #endregion

        #region Constructs Delivery Status Filtering Criteria
        public static IEnumerable<SelectListItem> ConstructDeliveryStatusFilteringCriteria()
        {
            var filteringCriteriaList = new List<SelectListItem>();

            var fcAllStatus = new SelectListItem();
            fcAllStatus.Text = "All";
            fcAllStatus.Value = "ALL";
            filteringCriteriaList.Add(fcAllStatus);

            var fcPendingStatus = new SelectListItem();
            fcPendingStatus.Text = "Pending";
            fcPendingStatus.Value = "PENDING";
            filteringCriteriaList.Add(fcPendingStatus);

            var fcDeliveredStatus = new SelectListItem();
            fcDeliveredStatus.Text = "Delivered";
            fcDeliveredStatus.Value = "DELIVERED";
            filteringCriteriaList.Add(fcDeliveredStatus);

            return filteringCriteriaList;
        }
        #endregion

        #region ConstructGender

        public static IEnumerable<SelectListItem> ConstructGender(string currentGender = "")
        {
            var genderList = new List<SelectListItem>();

            var maleSelection = new SelectListItem
            {
                Text = Resources.OneForAll.OneForAll.lblMale,
                Value = "MALE"
            };
            genderList.Add(maleSelection);

            var femaleSelection = new SelectListItem
            {
                Text = Resources.OneForAll.OneForAll.lblFemale,
                Value = "FEMALE"
            };
            genderList.Add(femaleSelection);

            if (!string.IsNullOrEmpty(currentGender))
            {
                foreach (var g in genderList)
                {
                    if (System.String.Compare(g.Value, currentGender, System.StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        g.Selected = true;
                    }
                }
            }

            return genderList;
        }

        #endregion
                
        #region SendEmail
        public static string SendEmail(string strFrom, string strto, string strSubject, string strBody, string imagePath)
        {
            MailMessage message = new MailMessage();
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.IsBodyHtml = true;

            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

            //Add Image
            //LinkedResource EmailImage = new LinkedResource(imagePath);
            //EmailImage.ContentId = "myImageID";

            ////Add the Image to the Alternate view
            //htmlView.LinkedResources.Add(EmailImage);

            //Add view to the Email Message
            message.AlternateViews.Add(htmlView);

            message.From = new MailAddress(strFrom, "BVI");
            message.To.Add(strto);
            //message.Bcc.Add(new MailAddress("CVDiamondSupport99@hotmail.com"));
            message.Subject = strSubject;

            var client = new SmtpClient(smtpServer, port);
            client.Credentials = new System.Net.NetworkCredential(username, password);
            client.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
            client.PickupDirectoryLocation = pickupLocation;

            try
            {
                client.Send(message);
                return "Successfully";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static string SendEmailGood(string strFrom, string strto, string strSubject, string strBody, string imagePath)
        {
            MailMessage message = new MailMessage();
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.IsBodyHtml = true;

            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

            //Add Image
            LinkedResource EmailImage = new LinkedResource(imagePath);
            EmailImage.ContentId = "myImageID";
            

            //Add the Image to the Alternate view
            htmlView.LinkedResources.Add(EmailImage);

            //Add view to the Email Message
            message.AlternateViews.Add(htmlView);

            message.From = new MailAddress(strFrom, "BVI");
            message.To.Add(strto);
            //message.Bcc.Add(new MailAddress("CVDiamondSupport99@hotmail.com"));
            message.Subject = strSubject;

            var client = new SmtpClient(smtpServer, port);
            client.Credentials = new System.Net.NetworkCredential(username, password);
            client.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
            client.PickupDirectoryLocation = pickupLocation;

            try
            {
                client.Send(message);
                return "Successfully";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        #endregion

        #region FileExtensionValidation
        public static bool IsFileExtensionValid(string file)
        {
            bool isValid = false;
            string extension = Path.GetExtension(file);

            if (extension != null)
            {
                switch (extension.ToLower())
                {
                    case ".jpeg":
                    case ".png":
                    case ".gif":
                    case ".bmp":
                    case ".jpg":
                        //Upload file as it is an image
                        isValid = true;
                        break;
                    default:
                        //Not an image - ignore
                        isValid = false;
                        break;
                }
            }
            return isValid;
        }
        #endregion

        #region ConvertToAppOtherBasedOnCashName
        public static string ConvertToAppOtherBasedOnCashName(string input, string cashname)
        {
            if (cashname == "MATCHINGBONUS")
            {
                if (input == "1")
                {
                    return ECFBase.Resources.OneForAll.OneForAll.lblMatchLvl1;
                }
                else if (input == "2")
                {
                    return ECFBase.Resources.OneForAll.OneForAll.lblMatchLvl2;
                }
                else if (input == "3")
                {
                    return ECFBase.Resources.OneForAll.OneForAll.lblMatchLvl3;
                }
                else if (input == "4")
                {
                    return ECFBase.Resources.OneForAll.OneForAll.lblMatchLvl4;
                }
                else if (input == "5")
                {
                    return ECFBase.Resources.OneForAll.OneForAll.lblMatchLvl5;
                }
            }
            else if (cashname == "SPONSORBONUS")
            {
                if (input == "1")
                {
                    return ECFBase.Resources.OneForAll.OneForAll.lblLevel1;
                }
                else if (input == "2")
                {
                    return ECFBase.Resources.OneForAll.OneForAll.lblLevel2;
                }
                else if (input == "3")
                {
                    return ECFBase.Resources.OneForAll.OneForAll.lblLevel3;
                }
            }

            return input;
        }
        #endregion

        #region GetStatusBasedOnSDONO
        public static string GetStatusBasedOnSDONO(string state)
        {
            switch (state)
            {
                case "1":
                    return Resources.OneForAll.OneForAll.lblStatusDelivered;
                case "0":
                    return Resources.OneForAll.OneForAll.lblStatusPending;
            }

            return state.ToString();
        }
        #endregion


        #region MemberReport


        public static List<RankSetupModel> GetAllRankfromMemberReport()
        {
            int ok = 0;
            string message = string.Empty;
            DataSet dsRank = new DataSet();
                       
            dsRank = BonusSettingDB.GetAllRankFromMemberReport(out ok, out message);

            RankSetupModel rank = new RankSetupModel();
            List<RankSetupModel> allRank = new List<RankSetupModel>();
            rank.RankName = "0";
            rank.RankName = "ALL";
            allRank.Add(rank);

            foreach (DataRow dr in dsRank.Tables[0].Rows)
            {
                rank = new RankSetupModel();
                rank.RankName = dr["CMEM_RANK"].ToString();
                allRank.Add(rank);
            }
            return allRank;
        }      

        public static List<CountrySetupModel> GetAllCountryfromMemberReport()
        {
            int ok = 0;
            string message = string.Empty;
            DataSet dsCountry = BonusSettingDB.GetAllCountryFromMemberReport(out ok, out message);
            List<CountrySetupModel> countries = new List<CountrySetupModel>();

            CountrySetupModel country = new CountrySetupModel();
            country.CountryCode = "0";
            country.CountryName = "ALL";
            countries.Add(country);

            foreach (DataRow dr in dsCountry.Tables[0].Rows)
            {
                country = new CountrySetupModel();
                country.CountryName = dr["CMEM_COUNTRY"].ToString();
                countries.Add(country);
            }
            return countries;
        }
        #endregion
    }
}