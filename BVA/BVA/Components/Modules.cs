﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECFBase.Components
{
    public class Modules
    {
        /// <summary>
        /// Get module name
        /// </summary>
        /// <param name="moduleCode"></param>
        /// <returns></returns>
        public static string GetModuleName(string moduleCode)
        {
            switch (moduleCode)
            {
                case "ADMINUSER":
                    return Resources.OneForAll.OneForAll.lblGeneral + " - " + Resources.OneForAll.OneForAll.mnuAdminUser;
                case "MASTERSETUP":
                    return Resources.OneForAll.OneForAll.lblGeneral + " - " + Resources.OneForAll.OneForAll.mnuMasterSetup;
                case "PRODUCT":
                    return Resources.OneForAll.OneForAll.lblGeneral + " - " + Resources.OneForAll.OneForAll.mnuProduct;
                case "MEMBERUTILITY":
                    return Resources.OneForAll.OneForAll.lblMembers + " - " + Resources.OneForAll.OneForAll.mnuUtility;
                case "MEMBERREPORTS":
                    return Resources.OneForAll.OneForAll.lblMembers + " - " + Resources.OneForAll.OneForAll.mnuReports;
                case "BONUSUTILITY":
                    return Resources.OneForAll.OneForAll.mnuBonus + " - " + Resources.OneForAll.OneForAll.mnuUtility;
                case "WALLETUTILITY":
                    return Resources.OneForAll.OneForAll.lblWallet + " - " + Resources.OneForAll.OneForAll.mnuUtility;
                case "WALLETREPORTS":
                    return Resources.OneForAll.OneForAll.lblWallet + " - " + Resources.OneForAll.OneForAll.mnuReports;
                case "CORPORATENEWS":
                    return Resources.OneForAll.OneForAll.mnuInfoDesk + " - " + Resources.OneForAll.OneForAll.mnuCorpNews;
                case "EVENTSANDPROMOTIONS":
                    return Resources.OneForAll.OneForAll.mnuInfoDesk + " - " + Resources.OneForAll.OneForAll.mnuEventsPromo;
                case "UPLOADCENTER":
                    return Resources.OneForAll.OneForAll.mnuInfoDesk + " - " + Resources.OneForAll.OneForAll.mnuUploadCenter;
                case "SALESORDERUTILITY":
                    return Resources.OneForAll.OneForAll.lblSalesOrder + " - " + Resources.OneForAll.OneForAll.mnuUtility;
                case "SALESORDERREPORT":
                    return Resources.OneForAll.OneForAll.lblSalesOrder + " - " + Resources.OneForAll.OneForAll.mnuReports;
                case "HELPDESKINBOX":
                    return Resources.OneForAll.OneForAll.mnuHelpDesk + " - " + Resources.OneForAll.OneForAll.mnuInbox;
                case "HELPDESKNOTICE":
                    return Resources.OneForAll.OneForAll.mnuHelpDesk + " - " + Resources.OneForAll.OneForAll.mnuNotice;


                default:
                    return moduleCode;
            }
        }

        /// <summary>
        /// Get sub module name
        /// </summary>
        /// <param name="moduleCode"></param>
        /// <returns></returns>
        public static string GetSubModuleName(string moduleCode)
        {
            switch (moduleCode)
            {
                // SubModules
                case "CHANGEUPLINE":
                    return Resources.OneForAll.OneForAll.mnuChangeUpline;
                case "USERSETUP":
                    return Resources.OneForAll.OneForAll.mnuAdminUserSetup;
                case "PERMISSIONSETUP":
                    return Resources.OneForAll.OneForAll.mnuAccessControl;
                case "RESETUSERPASSWORD":
                    return Resources.OneForAll.OneForAll.mnuResetAdminPwd;
                case "GENERALSETTINGS":
                    return Resources.OneForAll.OneForAll.mnuGeneralSettings;
                case "MEMBERUPGRADELISTING":
                    return "Member Upgrade Listing";
                case "SAHRESETTING":
                    return "eFund Setting";
                case "OVERSUBSCRIBEDSETTING":
                    return "Oversubscribed Setting";
                case "BLOCKSELLINGEFUND":
                    return "Block Selling eFund";
                case "COMPANYACCOUNT":
                    return "Company Account";
                case "COMPANYACCOUNTSELLORDER":
                    return "Company Account Sell Order";
                case "QUEUESUMMARY":
                    return "Queue Summary";
                case "EFUNDSELLINGDETAIL":
                    return "eFund Selling Detail";
                case "MANUALBUYSHARE":
                    return "Manual Buy Share";
                case "BUYINGQUEUE":
                    return "Buying Queue"; 
                case "MEMBERSELLINGQUEUE":
                    return "Member Selling Queue";
                case "EFUNDLISTING":
                    return "Efund Listing";

                case "SPECIALREGISTRATION":
                    return Resources.OneForAll.OneForAll.mnuSpecialRegistration;
                case "TRANSFERMEMBER":
                    return "Transfer Member";
                case "MEMBERSTATISTIC":
                    return "Member Statistic";
                case "MEMBERINCOME":
                    return "Member Income";
                case "RMPTOPUP":
                    return Resources.OneForAll.OneForAll.mnuRMPTopUp;
                case "CPWITHDRAWAL":
                    return Resources.OneForAll.OneForAll.mnuCashWithdrawalList;
                case "PURCHASERP":
                    return Resources.OneForAll.OneForAll.mnuMobileRequestTopUp;
                case "RMPLOGREPORT":
                    return "RMP Log";
                case "CPWITHDRAWALLOGREPORT":
                    return Resources.OneForAll.OneForAll.mnuCPWithdrawalLog;
                case "PURCHASERPHISTORY":
                    return Resources.OneForAll.OneForAll.mnuMobileRequestTopUpHis;
                case "APPOINTMOBILE":
                    return Resources.OneForAll.OneForAll.mnuMobileListing;
                case "MOBILEREQUESTTOPUP":
                    return Resources.OneForAll.OneForAll.mnuMobileRequestTopUp;
                case "MOBILEREQUESTTOPUPHISTORY":
                    return Resources.OneForAll.OneForAll.mnuMobileRequestTopUpHis;

                case "COUNTRY":
                    return Resources.OneForAll.OneForAll.mnuCountry;
                case "PROVINCE":
                    return Resources.OneForAll.OneForAll.mnuProvince;
                case "CITY":
                    return Resources.OneForAll.OneForAll.mnuCity;
                case "DISTRICT":
                    return Resources.OneForAll.OneForAll.mnuDistrict;
                case "CURRENCYEXCHANGE":
                    return Resources.OneForAll.OneForAll.mnuCurrExc;
                case "PAYMENTMODE":
                    return Resources.OneForAll.OneForAll.mnuPaymentmode;
                case "SALESDELIVERYMODE":
                    return Resources.OneForAll.OneForAll.mnuDeliveryTo;
                case "BANK":
                    return Resources.OneForAll.OneForAll.mnuBank;
                case "LANGUAGE":
                    return Resources.OneForAll.OneForAll.mnuLanguage;
                case "UPGRADEPACKAGE":
                    return Resources.OneForAll.OneForAll.mnuUDPackage;
                case "RANKSETUP":
                    return Resources.OneForAll.OneForAll.mnuRankSetup;
                case "MEGABONUSQUALIFIER":
                    return Resources.OneForAll.OneForAll.mnuMegaBonusQualifier;
                case "MEMBERAUTOWITHDRAWAL":
                    return Resources.OneForAll.OneForAll.mnuMemberAutoWithdrawal;
                case "WALLETSBALANCE":
                    return Resources.OneForAll.OneForAll.mnuWalletBalance;
                case "SEARCHSPONSOR":
                    return Resources.OneForAll.OneForAll.mnuSearchSponsor;
                case "SEARCHUPLINE":
                    return Resources.OneForAll.OneForAll.mnuSearchUpline;
                case "WRPSETUP":
                    return Resources.OneForAll.OneForAll.mnuWRPSetup;
                case "WRPSUBACCOUNTSETUP":
                    return Resources.OneForAll.OneForAll.mnuWRPSubAccountSetup;
                case "WRPSUMMARY":
                    return Resources.OneForAll.OneForAll.mnuWRPSummary;
                case "WRPLISTING":
                    return Resources.OneForAll.OneForAll.mnuWRPListing;
                case "WRPDETAILSUMMARY":
                    return Resources.OneForAll.OneForAll.mnuWRPDetailSummary;
                case "WRPLOG":
                    return Resources.OneForAll.OneForAll.mnuWRPLog;
                case "UPGRADEMEMBER":
                    return Resources.OneForAll.OneForAll.mnuUpgradeMember;
                case "DAILYSALES":
                    return Resources.OneForAll.OneForAll.mnuDailySales;
                case "SPONSORSALES":
                    return Resources.OneForAll.OneForAll.mnuSponsorSales;
                case "SPONSORSALESCALCULATION":
                    return Resources.OneForAll.OneForAll.mnuSponsorSalesCalculation;
                
                case "RPTOPUP":
                    return Resources.OneForAll.OneForAll.mnuRwalletTopUp;
                case "CRPTOPUP":
                    return Resources.OneForAll.OneForAll.mnuCVRwalletTopUp;
                case "EPTOPUP":
                    return Resources.OneForAll.OneForAll.mnuEwalletTopUp;
                case "SPTOPUP":
                    return Resources.OneForAll.OneForAll.mnuSwalletTopUp;
                case "VPTOPUP":
                    return Resources.OneForAll.OneForAll.mnuMwalletTopUp;
                case "VPWITHDRAWALLIST":
                    return Resources.OneForAll.OneForAll.mnuMWithdrawalList;

                case "RPLOGREPORT":
                    return Resources.OneForAll.OneForAll.mnuRegWalletLog;
                case "CRPLOGREPORT":
                    return Resources.OneForAll.OneForAll.mnuCVRegWalletLog;
                case "EPLOGREPORT":
                    return Resources.OneForAll.OneForAll.mnuEwalletLog;
                case "SPLOGREPORT":
                    return Resources.OneForAll.OneForAll.mnuSWalletLog;
                case "VPLOGREPORT":
                    return Resources.OneForAll.OneForAll.mnuMWalletLog;
                case "EPWITHDRAWALLOGREPORT":
                    return Resources.OneForAll.OneForAll.mnuWithdrawalLog;
                case "VPWITHDRAWALLOGREPORT":
                    return Resources.OneForAll.OneForAll.mnuMWithdrawalLog;
                case "RPTOPUPREPORT":
                    return Resources.OneForAll.OneForAll.mnuRwalletTopUp;
                case "CRPTOPUPREPORT":
                    return Resources.OneForAll.OneForAll.mnuCVRwalletTopUp;
                case "EPTOPUPREPORT":
                    return Resources.OneForAll.OneForAll.mnuEwalletTopUp;
                case "VPTOPUPREPORT":
                    return Resources.OneForAll.OneForAll.mnuMwalletTopUp;

                case "CATEGORY":
                    return Resources.OneForAll.OneForAll.mnuCategory;
                case "PRODUCT":
                    return Resources.OneForAll.OneForAll.mnuProduct;
                case "REGISTRATIONPACKAGE":
                    return Resources.OneForAll.OneForAll.mnuRegistrationPackage;
                case "MAINTENANCEPACKAGE":
                    return Resources.OneForAll.OneForAll.mnuMaintPackage;
                case "STOCKMENTREPORT":
                    return Resources.OneForAll.OneForAll.mnuStockmentReport;
                case "STOCKADJUSTMENTNOTE":
                    return Resources.OneForAll.OneForAll.mnuStockAdjustment;
                case "STOCKISTSTOCKADJUSTMENTNOTE":
                    return Resources.OneForAll.OneForAll.mnuStockistStockAdjustment;
                case "SALESANALYSIS":
                    return Resources.OneForAll.OneForAll.mnuSalesAnalysisByCtryOrProv;
                case "MEMBERANALYSIS":
                    return Resources.OneForAll.OneForAll.mnuMembershipAnalysisByCtryOrProv;


                case "FREEREGISTRATION":
                    return Resources.OneForAll.OneForAll.mnuFreeReg;
                case "CHANGEPASSWORD":
                    return Resources.OneForAll.OneForAll.mnuChgPassword;
                case "CHANGEPIN":
                    return Resources.OneForAll.OneForAll.mnuChgPIN;
                case "MEMBERPERMISSION":
                    return Resources.OneForAll.OneForAll.mnuChangePermission;
                case "CHANGENAME":
                    return Resources.OneForAll.OneForAll.mnuChangeOwnership;
                case "CHANGERANKING":
                    return Resources.OneForAll.OneForAll.mnuChangeRanking;
                case "CHANGESPONSOR":
                    return Resources.OneForAll.OneForAll.mnuChangeSponsor;
                case "VIEWPASSWORD":
                    return Resources.OneForAll.OneForAll.mnuViewPassword;
                case "COURIER":
                    return Resources.OneForAll.OneForAll.mnuCourierCompany;


                case "MEMBERLISTING":
                    return Resources.OneForAll.OneForAll.mnuMemberListing;
                case "SPONSORCHART":
                    return Resources.OneForAll.OneForAll.mnuSponsorList;
                case "PLACEMENTCHART":
                    return Resources.OneForAll.OneForAll.mnuPlacementChart;
                case "DIRECTSPONSOR":
                    return Resources.OneForAll.OneForAll.mnuDirectSponsor;


                case "BONUSPARAMETERSETTING":
                    return Resources.OneForAll.OneForAll.mnuBonusParamSetting;
                case "BONUSPAYOUTSUMMARY":
                    return Resources.OneForAll.OneForAll.mnuBonusPayoutSummary;

                case "EWALLETTOPUP":
                    return Resources.OneForAll.OneForAll.mnuEwalletTopUp;
                case "PVWALLETTOPUP":
                    return Resources.OneForAll.OneForAll.mnuPVwalletTopup;
                case "WITHDRAWALHISTORY":
                    return Resources.OneForAll.OneForAll.mnuWithdrawalLog;


                case "EWALLETLOG":
                    return Resources.OneForAll.OneForAll.mnuEwalletLog;
                case "PVWALLETLOG":
                    return Resources.OneForAll.OneForAll.mnuPVwalletLog;
                case "EWALLETSUMMARY":
                    return Resources.OneForAll.OneForAll.mnuDailyEwalletSummary;
                case "PVWALLETSUMMARY":
                    return Resources.OneForAll.OneForAll.mnuDailyPVwalletSummary;


                case "CORPORATENEWS":
                    return Resources.OneForAll.OneForAll.lblNewCorpNews;
                case "EVENTSANDPROMOTIONS":
                    return Resources.OneForAll.OneForAll.mnuEventsPromo;
                case "UPLOADCENTER":
                    return Resources.OneForAll.OneForAll.mnuUploadCenter;


                case "INBOX":
                    return Resources.OneForAll.OneForAll.mnuInbox;
                case "NOTICE":
                    return Resources.OneForAll.OneForAll.mnuNotice;


                case "INVOICE":
                    return Resources.OneForAll.OneForAll.mnuInvoice;
                case "DELIVERYORDER":
                    return Resources.OneForAll.OneForAll.mnuDeliveryOrder;

                default:
                    return moduleCode;
            }
        }

        /// <summary>
        /// Get submodule method
        /// </summary>
        /// <param name="moduleCode"></param>
        /// <returns></returns>
        public static string GetSubModuleMethod(string moduleCode)
        {
            if (moduleCode.ToLower().Contains("view"))
            {
               return Resources.OneForAll.OneForAll.mnuAllowView;
            }
            else
            {
                return Resources.OneForAll.OneForAll.mnuAllowEdit;
            }

        }
    }
}