﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECFBase.Components
{
    //public class DaYuanStaticString
    //{
    //    public static string ChangePassword = "ChangePassword";
    //    public static string ChangePin = "ChangePin";
    //    public static string ChangeMemberPermission = "ChangeMemberPermission";
    //    public static string RegisterwalletTopUp = "RegisterwalletAdjustment";
    //    public static string CashwalletTopUp = "CashwalletAdjustment";
    //    public static string MultiPointTopUp = "MultiPointWalletAdjustment";
    //    public static string GoldPointTopUp = "GoldPointWalletAdjustment";
    //    public static string RPCTopUp = "RepurchaseWalletAdjustment";
    //    public static string ChangeOwnership = "ChangeOwnership";
    //    public static string ChangeSponsor = "ChangeSponsor";
    //    public static string MultipleAccount = "MultipleAccount";
    //    public static string ChangeRanking = "ChangeRanking";
    //    public static string ChangeCenter = "ChangeCenter";
    //    public static string ChangeCommitment = "ChangeCommitment";
    //    public static string ChangeMemberTree = "ChangeMemberTree";
    //    public static string AjdustMemberUpgrade = "AdjustMemberUpgrade";
    //    public static string CVRwalletTopUp = "CVRwalletAdjustment";
    //    public static string EditProfile = "EditProfile";
    //    public static string DefaultLanguage = "en-US";
    //    public static string BankAccInfo = "BankAccInfo";
    //}

    public class ProjectStaticString
    {        
        public static string ChangePassword         = "ChangePassword";
        public static string TransferMember         = "TransferMember";
        public static string ChangePin              = "ChangePin";
        public static string ChangeRanking          = "ChangeRanking";
        public static string ChangeMemberPermission = "ChangeMemberPermission";
        public static string ChangeOwnership        = "ChangeOwnership";
        public static string ChangeSponsor          = "ChangeSponsor";
        public static string ChangeUpline           = "ChangeUpline";
        

        public static string RegisterwalletTopUp    = "RegisterwalletAdjustment";
        public static string CashwalletTopUp        = "CashwalletAdjustment";
        public static string MultiPointTopUp        = "MultiPointWalletAdjustment";
        public static string GoldPointTopUp         = "GoldPointWalletAdjustment";
        public static string RPCTopUp               = "RepurchaseWalletAdjustment";
        public static string RMPTopUp               = "RMPWalletAdjustment";
        public static string ICOTopUp               = "ICOWalletAdjustment";
        public static string CompanyTopUp = "CompanyWalletAdjustment";
        public static string TPTopUp = "TPWalletAdjustment";

        public static string DefaultLanguage        = "en-US";
        public static string EditProfile            = "EditProfile";
        public static string BankAccInfo            = "BankAccInfo";





        //public static string MultipleAccount = "MultipleAccount";

        //public static string ChangeCenter = "ChangeCenter";
        //public static string ChangeCommitment = "ChangeCommitment";
        //public static string ChangeMemberTree = "ChangeMemberTree";
        //public static string AjdustMemberUpgrade = "AdjustMemberUpgrade";
        //public static string CVRwalletTopUp = "CVRwalletAdjustment";
        //
        //
        //
    }
}