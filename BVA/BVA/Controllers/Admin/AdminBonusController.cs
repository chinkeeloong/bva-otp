﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using ECFBase.Components;
using ECFBase.Helpers;
using ECFBase.Models;
using System.Text;
using System.IO;
using System.Globalization;

namespace ECFBase.Controllers.Admin
{
    public class AdminBonusController : Controller
    {

        #region Reports

        public ActionResult BonusPayoutSummarySetup(int selectedPage = 1, int selectedYear = -1, int selectedMonth = -1, int Country = 0)
        { 
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            int ok;
            string msg;
            int pages;

            if (selectedYear == -1)
                selectedYear = DateTime.Now.Year;
            if (selectedMonth == -1)
                selectedMonth = DateTime.Now.Month;

            var model = new PaginationCurrentSalesModel();

            #region Country
            //combobox for country

            List<CountrySetupModel> countries = Misc.GetAllCountryID("en-US");

            model.Countries = from c in countries
                            select new SelectListItem
                            {
                                Text = c.CountryName,
                                Value = c.CountryCode
                            };

            model.SelectedCountry = Convert.ToString(Country);
            #endregion

            var dsCurrentSales = BonusSettingDB.GetCurrentSales(selectedPage, selectedYear, selectedMonth, Country, out pages, out ok, out msg);
            selectedPage = ConstructPageList(selectedPage, pages, model);

            //New Register
            foreach (DataRow dr in dsCurrentSales.Tables[0].Rows)
            {
                var csm = new CurrentSalesModel();
                csm.SalesDate = dr["SalesDate"].ToString();
                csm.NewRegSalesAmount = float.Parse(dr["SalesAmount"].ToString());
                csm.TotalSalesAmount += float.Parse(dr["SalesAmount"].ToString());
                csm.TotalSalesInBVAmount += float.Parse(dr["SalesInBV"].ToString());
                model.CurrentSalesModel.Add(csm);
            }

            //Upgrade Package
            foreach (DataRow dr in dsCurrentSales.Tables[1].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].MemberUpgrade = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel[nIndex].TotalSalesAmount += float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel[nIndex].TotalSalesInBVAmount += float.Parse(dr["SalesInBV"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.MemberUpgrade = float.Parse(dr["SalesAmount"].ToString());
                    csm.TotalSalesAmount = float.Parse(dr["SalesAmount"].ToString());
                    csm.TotalSalesInBVAmount = float.Parse(dr["SalesInBV"].ToString());
                    model.CurrentSalesModel.Add(csm);
                }
            }

            //Register Wallet
            foreach (DataRow dr in dsCurrentSales.Tables[2].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].RPP = float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.RPP = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);
                }
            }

            //Multi Wallet
            foreach (DataRow dr in dsCurrentSales.Tables[3].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].GP = float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.GP = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);
                }
            }

            //Cash Wallet
            foreach (DataRow dr in dsCurrentSales.Tables[4].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].CP = float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.CP = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);
                }
            }

            //Sponsor Bonus
            foreach (DataRow dr in dsCurrentSales.Tables[5].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].SponsorBonusAmount = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel[nIndex].TotalBonusAmount += float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.SponsorBonusAmount = float.Parse(dr["SalesAmount"].ToString());
                    csm.TotalBonusAmount = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);
                }
            }

            //Pairing Bonus
            foreach (DataRow dr in dsCurrentSales.Tables[6].Rows)
            {               
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    var sales = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel[nIndex].PairingBonusAmount = sales;
                    model.CurrentSalesModel[nIndex].TotalBonusAmount += sales;
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    var sales = float.Parse(dr["SalesAmount"].ToString());
                    csm.PairingBonusAmount = sales;
                    csm.TotalBonusAmount += sales;
                    model.CurrentSalesModel.Add(csm);
                }
            }

            //Matching Bonus
            foreach (DataRow dr in dsCurrentSales.Tables[7].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    var sales = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel[nIndex].MatchingBonusAmount = sales;
                    model.CurrentSalesModel[nIndex].TotalBonusAmount += sales;
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    var sales = float.Parse(dr["SalesAmount"].ToString());
                    csm.MatchingBonusAmount = sales;
                    csm.TotalBonusAmount += sales;
                    model.CurrentSalesModel.Add(csm);
                }
            }           

            //sort first before add total
            var sort = new sortOnSalesDate();
            model.CurrentSalesModel.Sort(sort);

            //add up the total
            float totalUpdateSales = 0;
            float totalRegSales = 0;
            float totalMaintainanceSales = 0;
            float totalPVTopupSales = 0;
            float totalTopupBonusSales = 0;
            float totalSponsorSales = 0;
            float totalPairingSales = 0;
            float totalMatchingSales = 0;
            float totalWorldPoolSales = 0;
            float totalUniLevelSales = 0;
            float totalRP = 0;
            float totalGP = 0;
            float totalPP = 0;

            foreach (CurrentSalesModel salesModel in model.CurrentSalesModel)
            {
                totalRegSales += salesModel.NewRegSalesAmount;
                totalUpdateSales += salesModel.MemberUpgrade;
                totalMaintainanceSales += salesModel.MaintainanceSalesAmount;
                totalPVTopupSales += salesModel.TopupPVSalesAmount;
                totalTopupBonusSales += salesModel.TotalSalesInBVAmount;
                totalSponsorSales += salesModel.SponsorBonusAmount;
                totalPairingSales += salesModel.PairingBonusAmount;
                totalMatchingSales += salesModel.MatchingBonusAmount;
                totalWorldPoolSales += salesModel.WorldPoolBonusAmount;
                totalUniLevelSales += salesModel.UniLevelAmount;
                totalRP += salesModel.RPP;
                totalPP += salesModel.PP;
                totalGP += salesModel.GP;
            }

            var totalCSM = new CurrentSalesModel();
            totalCSM.SalesDate = Resources.OneForAll.OneForAll.lblTotal;
            totalCSM.NewRegSalesAmount = totalRegSales;
            totalCSM.MemberUpgrade = totalUpdateSales;
            totalCSM.MaintainanceSalesAmount = totalMaintainanceSales;
            totalCSM.TopupPVSalesAmount = totalPVTopupSales;
            totalCSM.TotalSalesInBVAmount = totalTopupBonusSales;
            totalCSM.RPP = totalRP;
            totalCSM.GP = totalGP;
            totalCSM.PP = totalPP;
            totalCSM.SponsorBonusAmount = totalSponsorSales;
            totalCSM.PairingBonusAmount = totalPairingSales;
            totalCSM.MatchingBonusAmount = totalMatchingSales;
            totalCSM.WorldPoolBonusAmount = totalWorldPoolSales;
            totalCSM.UniLevelAmount = totalUniLevelSales;
            totalCSM.TotalSalesAmount = totalRegSales + totalUpdateSales;
            totalCSM.TotalBonusAmount = totalSponsorSales + totalPairingSales + totalMatchingSales + totalWorldPoolSales + totalUniLevelSales;

            model.CurrentSalesModel.Add(totalCSM);

            //ViewBag.totalSales = totalRegSales.ToString("n2");
            ViewBag.selectedYear = selectedYear;
            ViewBag.selectedMonth = selectedMonth;

            return PartialView("BonusPayoutSummarySetup", model);
        }

        public ActionResult BonusListing(int selectedPage = 1, string DateFrom = "", string DateTo ="")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            int ok;
            string msg;
            int pages;

            if (DateFrom == "")
                DateFrom = "2014-11-01";
            if (DateTo == "")
                DateTo = DateTime.Now.ToString("yyyy-MM-dd");

            var model = new PaginationCurrentSalesModel();

            var dsCurrentSales = BonusSettingDB.GetBonusListingByDate(selectedPage, DateFrom, DateTo, out pages, out ok, out msg);
            selectedPage = ConstructPageList(selectedPage, pages, model);

            float TotalPairing = 0;
            float TotalAmount = 0;
            float TotalMatching = 0;
            float TotalSponsor = 0;

            //add in new register
            foreach (DataRow dr in dsCurrentSales.Tables[0].Rows)
            {
                var csm = new CurrentSalesModel();
                csm.MemberID = dr["CUSR_USERNAME"].ToString();
                var ds = MemberDB.GetMemberByUsername(csm.MemberID,out ok,out msg);
                csm.Name = ds.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                csm.Rank = Misc.GetMemberRanking(ds.Tables[0].Rows[0]["CRANK_CODE"].ToString());
                csm.SalesDate = dr["DATE"].ToString();
                TotalSponsor += float.Parse(dr["Sponsor"].ToString());
                TotalPairing += float.Parse(dr["Pairing"].ToString());
                TotalMatching += float.Parse(dr["Matching"].ToString());                
                csm.SponsorBonusAmount = float.Parse(dr["Sponsor"].ToString());
                csm.PairingBonusAmount = float.Parse(dr["Pairing"].ToString());
                csm.MatchingBonusAmount = float.Parse(dr["Matching"].ToString());
                csm.TotalBonusAmount = csm.SponsorBonusAmount + csm.PairingBonusAmount + csm.MatchingBonusAmount;
                TotalAmount += csm.TotalBonusAmount;
                model.CurrentSalesModel.Add(csm);
            }
            
            model.TotalPairing =TotalPairing.ToString("n2");
            model.TotalAmount = TotalAmount.ToString("n2");
            model.TotalMatching =TotalMatching.ToString("n2");
            model.TotalSponsor = TotalSponsor.ToString("n2");
         

            return PartialView("BonusListing", model);
        }

        public ActionResult DailySalesSummarySetup(int selectedPage = 1, int selectedYear = -1, int selectedMonth = -1, int Country = 0)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            int ok;
            string msg;
            int pages;

            if (selectedYear == -1)
                selectedYear = DateTime.Now.Year;
            if (selectedMonth == -1)
                selectedMonth = DateTime.Now.Month;

            var model = new PaginationCurrentSalesModel();

            #region Country
            //combobox for country

            List<CountrySetupModel> countries = Misc.GetAllCountryID("en-US");

            model.Countries = from c in countries
                              select new SelectListItem
                              {
                                  Text = c.CountryName,
                                  Value = c.CountryCode
                              };

            model.SelectedCountry = Convert.ToString(Country);
            #endregion

            var dsCurrentSales = BonusSettingDB.GetCurrentDailySales(selectedPage, selectedYear, selectedMonth, Country, out pages, out ok, out msg);
            selectedPage = ConstructPageList(selectedPage, pages, model);

            //add in new register
            foreach (DataRow dr in dsCurrentSales.Tables[0].Rows)
            {
                var csm = new CurrentSalesModel();
                csm.SalesDate = dr["SalesDate"].ToString();
                csm.NewRegSalesAmount = float.Parse(dr["SalesAmount"].ToString());
                csm.RegisterRpoint = float.Parse(dr["RpointAmount"].ToString());
                csm.RegisterCRpoint = csm.NewRegSalesAmount - csm.RegisterRpoint;
                csm.TotalSalesAmount += float.Parse(dr["SalesAmount"].ToString());
                csm.TotalSalesInBVAmount += float.Parse(dr["SalesInBV"].ToString());
                csm.EPBV += float.Parse(dr["SalesInBV"].ToString()) * 80 / 100;
                csm.SPBV += float.Parse(dr["SalesInBV"].ToString()) * 10 / 100;
                csm.WRPBV += float.Parse(dr["SalesInBV"].ToString()) * 10 / 100;
                model.CurrentSalesModel.Add(csm);
            }

            //add in Upgrade
            foreach (DataRow dr in dsCurrentSales.Tables[1].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].MemberUpgrade = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel[nIndex].TotalSalesAmount += float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel[nIndex].UpgradeRpoint = float.Parse(dr["RpointAmount"].ToString());
                    model.CurrentSalesModel[nIndex].UpgradeCRpoint = float.Parse(dr["SalesAmount"].ToString()) - float.Parse(dr["RpointAmount"].ToString());                    
                    model.CurrentSalesModel[nIndex].TotalSalesInBVAmount += float.Parse(dr["SalesInBV"].ToString());
                    model.CurrentSalesModel[nIndex].EPBV += float.Parse(dr["SalesInBV"].ToString()) * 80 / 100;
                    model.CurrentSalesModel[nIndex].SPBV += float.Parse(dr["SalesInBV"].ToString()) * 10 / 100;
                    model.CurrentSalesModel[nIndex].WRPBV += float.Parse(dr["SalesInBV"].ToString()) * 10 / 100;
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.MemberUpgrade = float.Parse(dr["SalesAmount"].ToString());
                    csm.TotalSalesAmount += float.Parse(dr["SalesAmount"].ToString());
                    csm.UpgradeRpoint = float.Parse(dr["RpointAmount"].ToString());
                    csm.UpgradeCRpoint = float.Parse(dr["SalesAmount"].ToString()) - float.Parse(dr["RpointAmount"].ToString()); 
                    csm.TotalSalesInBVAmount += float.Parse(dr["SalesInBV"].ToString());

                    csm.EPBV += float.Parse(dr["SalesInBV"].ToString()) * 80 / 100;
                    csm.SPBV += float.Parse(dr["SalesInBV"].ToString()) * 10 / 100;
                    csm.WRPBV += float.Parse(dr["SalesInBV"].ToString()) * 10 / 100;
                    model.CurrentSalesModel.Add(csm);
                }
            }

            //sort first before add total
            var sort = new sortOnSalesDate();
            model.CurrentSalesModel.Sort(sort);

            //add up the total
            float totalUpdateSales = 0;
            float totalRegSales = 0;
            float totalMaintainanceSales = 0;
            float totalPVTopupSales = 0;
            float totalTopupBonusSales = 0;
            float totalSponsorSales = 0;
            float totalPairingSales = 0;
            float totalMatchingSales = 0;
            float totalWorldPoolSales = 0;
            float totalUniLevelSales = 0;

            foreach (CurrentSalesModel salesModel in model.CurrentSalesModel)
            {
                totalRegSales += salesModel.NewRegSalesAmount;
                totalUpdateSales += salesModel.MemberUpgrade;
                totalMaintainanceSales += salesModel.MaintainanceSalesAmount;
                totalPVTopupSales += salesModel.TopupPVSalesAmount;
                totalTopupBonusSales += salesModel.TotalSalesInBVAmount;
                totalSponsorSales += salesModel.SponsorBonusAmount;
                totalPairingSales += salesModel.PairingBonusAmount;
                totalMatchingSales += salesModel.MatchingBonusAmount;
                totalWorldPoolSales += salesModel.WorldPoolBonusAmount;
                totalUniLevelSales += salesModel.UniLevelAmount;
            }

            var totalCSM = new CurrentSalesModel();
            totalCSM.SalesDate = Resources.OneForAll.OneForAll.lblTotal;
            totalCSM.NewRegSalesAmount = totalRegSales;
            totalCSM.MemberUpgrade = totalUpdateSales;
            totalCSM.MaintainanceSalesAmount = totalMaintainanceSales;
            totalCSM.TopupPVSalesAmount = totalPVTopupSales;
            totalCSM.TotalSalesInBVAmount = totalTopupBonusSales;
            totalCSM.SponsorBonusAmount = totalSponsorSales;
            totalCSM.PairingBonusAmount = totalPairingSales;
            totalCSM.MatchingBonusAmount = totalMatchingSales;
            totalCSM.WorldPoolBonusAmount = totalWorldPoolSales;
            totalCSM.UniLevelAmount = totalUniLevelSales;
            totalCSM.TotalSalesAmount = totalRegSales + totalUpdateSales;
            totalCSM.TotalBonusAmount = totalSponsorSales + totalPairingSales + totalMatchingSales + totalWorldPoolSales + totalUniLevelSales;

            model.CurrentSalesModel.Add(totalCSM);

            //ViewBag.totalSales = totalRegSales.ToString("n2");
            ViewBag.selectedYear = selectedYear;
            ViewBag.selectedMonth = selectedMonth;

            return PartialView("DailyBonusPayoutSummarySetup", model);
        }

        public ActionResult DailySponserReport()
        {
            try
            {
                var model = new DailySponserLogModel();
                model.ShowTable = true;

                //float Total = 0;
                //string Date = string.Empty;
                //string username = string.Empty;
                //string PDate = string.Empty;


                //var ds = AdminGeneralDB.GetSalesReportByMemberID("platinum2u");
                //foreach (DataRow dr in ds.Tables[0].Rows)
                //{
                // var SponserModel = new DailySponserModel();
                //    username = dr["CUSR_USERNAME"].ToString();
                //    Date = dr["CSRP_DATE"].ToString();
                //    Total += Convert.ToInt32(dr["CSRP_SALES"].ToString());
                //    if (Date != PDate)
                //    {
                //        SponserModel.Username = username;
                //        SponserModel.Total = Total;
                //        SponserModel.Date = Date;
                //   model.ModelCollection.Add(SponserModel);
                //        Total = 0;
                //    }
                //    PDate = Date;
                //}

                return PartialView("DailySponserSales", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DailySponserReportMethod(string username, string StartDate = "", string EndDate = "")
        {
            try
            {
                var model = new DailySponserLogModel();
                model.ShowTable = true;

                float Total = 0;
                DateTime Date = new DateTime();
                string name = string.Empty;
                string PDate = string.Empty;
                float TotalAmount = 0;
                string startdate = string.Empty;
                string enddate = string.Empty;

                if (StartDate == "" || EndDate == "")
                {
                    DateTime sdate = DateTime.ParseExact("01/01/1990", "dd/MM/yyyy", null);
                    startdate = sdate.ToString("yyyy-MM-dd");

                    DateTime edate = DateTime.Now;
                    //DateTime ENDDATE = edate.AddDays(1);
                    enddate = edate.ToString("yyyy-MM-dd");
                }
                else
                {
                    DateTime sdate = DateTime.ParseExact(StartDate, "dd/MM/yyyy", null);
                    startdate = sdate.ToString("yyyy-MM-dd");

                    DateTime edate = DateTime.ParseExact(EndDate, "dd/MM/yyyy", null);
                    //DateTime ENDDATE = edate.AddDays(1);
                    enddate = edate.ToString("yyyy-MM-dd");
                }

                var ds = AdminGeneralDB.GetSalesReportByMemberID(username, startdate, enddate);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    var SponserModel = new DailySponserModel();
                    name = dr["CUSR_USERNAME"].ToString();
                    Date = Convert.ToDateTime(dr["CSRP_DATE"].ToString());
                    Total = Convert.ToInt32(dr["CSRP_SALES"].ToString());
                    TotalAmount += Convert.ToInt32(dr["CSRP_SALES"].ToString());
                    string dt = Date.ToString("dd/MM/yyyy");

                    SponserModel.Username = name;
                    SponserModel.Total = Total;
                    SponserModel.Date = dt;
                    model.ModelCollection.Add(SponserModel);



                }
                model.Total = TotalAmount;

                return PartialView("DailySponserSales", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RPReport(int selectedPage = 1, string DateFrom = "", string DateTo = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            int ok;
            string msg;
            int pages;

            if (DateFrom == "")
                DateFrom = "29/01/2018";

            if (DateTo == "")
                DateTo = DateTime.Now.ToString("dd/MM/yyyy");

            DateTime sdate = DateTime.ParseExact(DateFrom, "dd/MM/yyyy", null);
            string SD = sdate.ToString("yyyy-MM-dd");

            DateTime tdate = DateTime.ParseExact(DateTo, "dd/MM/yyyy", null);
            string TD = tdate.ToString("yyyy-MM-dd");


            var model = new PaginationCurrentSalesModel();

            var dsCurrentSales = BonusSettingDB.GetRPReport(selectedPage, SD, TD, out pages, out ok, out msg);
            selectedPage = ConstructPageList(selectedPage, pages, model);

            
            float RegisterOut = 0;
            float UpgradeOut = 0;
            float ExgMP = 0;
            float TransferMember = 0;
            float TransferFromCP = 0;
            float TopUpIn = 0;
            float AdjOut = 0;
            float TotalIn = 0;
            float TotalOut = 0;
            float TotalBalance = 0;

            //add in new register
            foreach (DataRow dr in dsCurrentSales.Tables[0].Rows)
            {
                var csm = new CurrentSalesModel();
              
                csm.SalesDate = Convert.ToDateTime(dr["CREGWL_DATE"].ToString()).ToString("dd/MM/yyyy");
                RegisterOut += float.Parse(dr["CREGWL_REGISTER_OUT"].ToString());
                UpgradeOut += float.Parse(dr["CREGWL_UPGRADE_OUT"].ToString());
                ExgMP += float.Parse(dr["CREGWL_EXCHANGE_MP"].ToString());
                TransferMember += float.Parse(dr["CREGWL_TRANSFER_MEMBER"].ToString());
                TransferFromCP += float.Parse(dr["CREGWL_TRANSFER_FROM_CP"].ToString());
                TopUpIn += float.Parse(dr["CREGWL_TOPUP_IN"].ToString());
                AdjOut += float.Parse(dr["CREGWL_ADJUSTMENT_OUT"].ToString());
                TotalIn += float.Parse(dr["CREGWL_TOTAL_IN"].ToString());
                TotalOut += float.Parse(dr["CREGWL_TOTAL_OUT"].ToString());
                TotalBalance += float.Parse(dr["CREGWL_TOTAL_BALANCE"].ToString());

                csm.RegisterOut = float.Parse(dr["CREGWL_REGISTER_OUT"].ToString());
                csm.UpgradeOut = float.Parse(dr["CREGWL_UPGRADE_OUT"].ToString());
                csm.ExgMP = float.Parse(dr["CREGWL_EXCHANGE_MP"].ToString());
                csm.TransferMember = float.Parse(dr["CREGWL_TRANSFER_MEMBER"].ToString());
                csm.TransferFromCP = float.Parse(dr["CREGWL_TRANSFER_FROM_CP"].ToString());
                csm.TopUpIn = float.Parse(dr["CREGWL_TOPUP_IN"].ToString());
                csm.AdjOut = float.Parse(dr["CREGWL_ADJUSTMENT_OUT"].ToString());
                csm.TotalIn = float.Parse(dr["CREGWL_TOTAL_IN"].ToString());
                csm.TotalOut = float.Parse(dr["CREGWL_TOTAL_OUT"].ToString());
                csm.TotalBalance = float.Parse(dr["CREGWL_TOTAL_BALANCE"].ToString());               
                
                model.CurrentSalesModel.Add(csm);
            }

            model.TotalRegisterOut = RegisterOut.ToString("n2");
            model.TotalUpgradeOut = UpgradeOut.ToString("n2");
            model.TotalExgMP = ExgMP.ToString("n2");
            model.TotalTransferMember = TransferMember.ToString("n2");
            model.TotalTransferFromCP = TransferFromCP.ToString("n2");
            model.TotalTopUpIn = TopUpIn.ToString("n2");
            model.TotalAdjOut = AdjOut.ToString("n2");
            model.TotalTotalIn = TotalIn.ToString("n2");
            model.TotalTotalOut = TotalOut.ToString("n2");
            model.TotalTotalBalance = TotalBalance.ToString("n2");

            return PartialView("RPReport", model);
        }
        public ActionResult CWReport(int selectedPage = 1, string DateFrom = "", string DateTo = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            int ok;
            string msg;
            int pages;


            if (DateFrom == "")
                DateFrom = "29/01/2018";

            if (DateTo == "")
                DateTo = DateTime.Now.ToString("dd/MM/yyyy");

            DateTime sdate = DateTime.ParseExact(DateFrom, "dd/MM/yyyy", null);
            string SD = sdate.ToString("yyyy-MM-dd");

            DateTime tdate = DateTime.ParseExact(DateTo, "dd/MM/yyyy", null);
            string TD = tdate.ToString("yyyy-MM-dd");


            var model = new PaginationCurrentSalesModel();

            var dsCurrentSales = BonusSettingDB.GetCWReport(selectedPage, SD, TD, out pages, out ok, out msg);
            selectedPage = ConstructPageList(selectedPage, pages, model);
                       
            float TransferMember = 0;         
            float TotalIn = 0;
            float TotalOut = 0;
            float TotalBalance = 0;

            float TotalPairing = 0;
            float TotalAmount = 0;
            //float TotalMatching = 0;
            float TotalSponsor = 0;
            float Withdrawal = 0;
            float TransferRP = 0;
            float TransferMP = 0;
            //add in new register
            foreach (DataRow dr in dsCurrentSales.Tables[0].Rows)
            {
                var csm = new CurrentSalesModel();
                
                csm.SalesDate = Convert.ToDateTime(dr["CCSHWL_DATE"].ToString()).ToString("dd/MM/yyyy");
                TotalSponsor += float.Parse(dr["CCSHWL_SPONSOR_BONUS"].ToString());
                TotalPairing += float.Parse(dr["CCSHWL_PAIRING_BONUS"].ToString());
                Withdrawal += float.Parse(dr["CCSHWL_WITHDRWAL"].ToString());
                TransferRP += float.Parse(dr["CCSHWL_TRANSFER_RP"].ToString());
                TransferMP += float.Parse(dr["CCSHWL_TRANSFER_MP"].ToString());
                TransferMember += float.Parse(dr["CCSHWL_TRANSFER_MEMBER"].ToString());
                TotalIn += float.Parse(dr["CCSHWL_TOTAL_IN"].ToString());
                TotalOut += float.Parse(dr["CCSHWL_TOTAL_OUT"].ToString());
                TotalBalance += float.Parse(dr["CCSHWL_TOTAL_BALANCE"].ToString());
                                
                csm.SponsorBonusAmount = float.Parse(dr["CCSHWL_SPONSOR_BONUS"].ToString());
                csm.PairingBonusAmount = float.Parse(dr["CCSHWL_PAIRING_BONUS"].ToString());
                csm.Withdraw = float.Parse(dr["CCSHWL_WITHDRWAL"].ToString());
                csm.TransferRP = float.Parse(dr["CCSHWL_TRANSFER_RP"].ToString());
                csm.TransferMP = float.Parse(dr["CCSHWL_TRANSFER_MP"].ToString());
                csm.TransferMember = float.Parse(dr["CCSHWL_TRANSFER_MEMBER"].ToString());
                csm.TotalIn = float.Parse(dr["CCSHWL_TOTAL_IN"].ToString());
                csm.TotalOut = float.Parse(dr["CCSHWL_TOTAL_OUT"].ToString());
                csm.TotalBalance = float.Parse(dr["CCSHWL_TOTAL_BALANCE"].ToString());

                csm.TotalBonusAmount = csm.SponsorBonusAmount + csm.PairingBonusAmount; //+ csm.MatchingBonusAmount;
                TotalAmount += csm.TotalBonusAmount;

                model.CurrentSalesModel.Add(csm);
            }

            model.TotalPairing = TotalPairing.ToString("n2");
            model.TotalAmount = TotalAmount.ToString("n2");
            model.TotalSponsor = TotalSponsor.ToString("n2");
            model.TotalWithdrawal = Withdrawal.ToString("n2");
            model.TotalTransferRP = TransferRP.ToString("n2");
            model.TotalTransferMP = TransferMP.ToString("n2");
            model.TotalTransferMember = TransferMember.ToString("n2");
            model.TotalTotalIn = TotalIn.ToString("n2");
            model.TotalTotalOut = TotalOut.ToString("n2");
            model.TotalTotalBalance = TotalBalance.ToString("n2");

            return PartialView("CWReport", model);
        }
        public ActionResult MemberReport(int selectedPage = 1, string DateFrom = "", string DateTo = "", string Rank = "", string Country = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            int ok;
            string msg;
            int pages;


            if (DateFrom == "")
                DateFrom = "29/01/2018";

            if (DateTo == "")
                DateTo = DateTime.Now.ToString("dd/MM/yyyy");

            DateTime sdate = DateTime.ParseExact(DateFrom, "dd/MM/yyyy", null);
            string SD = sdate.ToString("yyyy-MM-dd");

            DateTime tdate = DateTime.ParseExact(DateTo, "dd/MM/yyyy", null);
            string TD = tdate.ToString("yyyy-MM-dd");


            var model = new PaginationCurrentSalesModel();

            #region Country
            //combobox for country

            List<CountrySetupModel> countries = Misc.GetAllCountryfromMemberReport();

            model.Countries = from c in countries
                              select new SelectListItem
                              {
                                  Text = c.CountryName
                              };

            model.SelectedCountry = Convert.ToString(Country);
            #endregion

            #region Rank
            List<RankSetupModel> rank = Misc.GetAllRankfromMemberReport();
            model.RankList = from c in rank
                             select new SelectListItem
                             {
                                 Text = c.RankName
                             };

            model.SelectedRank = Convert.ToString(Rank);
            #endregion

            var dsCurrentSales = BonusSettingDB.GetMemberReport(selectedPage, SD, TD, Country, Rank, out pages, out ok, out msg);
            selectedPage = ConstructPageList(selectedPage, pages, model);
            
            //add in new register
            foreach (DataRow dr in dsCurrentSales.Tables[0].Rows)
            {
                var csm = new CurrentSalesModel();
               
                csm.SalesDate = Convert.ToDateTime(dr["CMEM_DATE"].ToString()).ToString("dd/MM/yyyy");
                csm.Country = dr["CMEM_COUNTRY"].ToString();
                csm.Rank = dr["CMEM_RANK"].ToString();
                csm.Quantity = dr["CMEM_QUANTITY"].ToString();
              

                model.CurrentSalesModel.Add(csm);
            }

            return PartialView("MemberReport", model);
        }

        #endregion

        #region Shared
        private static int ConstructPageList(int selectedPage, int pages, PaginationCurrentSalesModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }
        #endregion


    }
}
