﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECFBase.Models;
using ECFBase.Components;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Text;

namespace ECFBase.Controllers.Admin
{
    public class AdminController : Controller
    {
        #region AdminLogin
        public ActionResult AdminLogin(bool reloadPage = false)
        {

            if (reloadPage)
                ViewBag.ReloadPage = "Y";

            if (Request.Url.AbsoluteUri.Contains("test.convexcapital.co.uk"))
            {

            }
            else if (Request.Url.AbsoluteUri.Contains("efund.convexcapital.co.uk"))
            {

            }
            else
            {
                if (Request.Url.AbsoluteUri.Contains("convexcapital.co.uk"))
                {
                    return Redirect("https://bvi.asia");
                }
                if (Request.Url.AbsoluteUri.Contains("www.convexcapital.co.uk"))
                {
                    return Redirect("https://www.bvi.asia");
                }
            }

            

            DataSet logo = AdminGeneralDB.GetCompanySetup();
            foreach (DataRow dr in logo.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["COM_IMAGEPATH"].ToString();

                ViewBag.Logo = logo.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString();
            }

            //Clear all session.
            Session.RemoveAll();
            Session["LanguageChosen"] = ProjectStaticString.DefaultLanguage;
            return View();
        }
        #endregion

        #region ChangeLanguage

        public ActionResult ChangeLanguage(string languageCode)
        {
            Session["LanguageChosen"] = languageCode;
            return Home();
        }

        #endregion

        #region Home
        public ActionResult Home()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            var ahm = new AdminHomeModel();


            var DS = AdminInfoDeskDB.GetAllImage();
            foreach (DataRow dr in DS.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["CINF_IMAGEPATH"].ToString();

                ahm.ImageList.Add(IFile);
            }

            DataSet logo = AdminGeneralDB.GetCompanySetup();
            foreach (DataRow dr in logo.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["COM_IMAGEPATH"].ToString();

                ViewBag.Logo = logo.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString();
            }

            var languageUsed = Session["LanguageChosen"] == null ? ProjectStaticString.DefaultLanguage : Session["LanguageChosen"].ToString();
            ahm.AdminSelectedLanguage = languageUsed;

            int ok = 0;
            string msg = string.Empty;
            var languageList = Misc.GetLanguageList(ref ok, ref msg);
            ahm.LanguageList = from c in languageList
                               select new SelectListItem
                               {
                                   Selected = (c.Value == languageUsed) ? true : false,
                                   Text = c.Text,
                                   Value = c.Value
                               };

            if (Session["page"] != null)
            {
                ViewBag.page = Session["page"];
                Session["page"] = null;
            }

            DataSet dsUser = LoginDB.GetUserByUserName(Session["Admin"].ToString());
            ViewBag.UserName = dsUser.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
            //ahm.Premember = Convert.ToBoolean(dsUser.Tables[0].Rows[0]["CUSR_PREMEMBER"].ToString());
            return View("Home", ahm);
        }
        #endregion

        #region AdminSearchUser
        public ActionResult AdminSearchUser(string Type)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            if (Type == ProjectStaticString.ChangePassword)
            {
                ViewBag.MainModule = ECFBase.Resources.OneForAll.OneForAll.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.OneForAll.OneForAll.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.OneForAll.OneForAll.mnuChgPassword;
            }
            else if (Type == ProjectStaticString.ChangePin)
            {
                ViewBag.MainModule = ECFBase.Resources.OneForAll.OneForAll.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.OneForAll.OneForAll.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.OneForAll.OneForAll.mnuChgPIN;
            }
            else if (Type == ProjectStaticString.ChangeMemberPermission)
            {
                ViewBag.MainModule = ECFBase.Resources.OneForAll.OneForAll.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.OneForAll.OneForAll.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.OneForAll.OneForAll.mnuChgMemPer;
            }
            else if (Type == ProjectStaticString.RegisterwalletTopUp)
            {
                ViewBag.MainModule = ECFBase.Resources.OneForAll.OneForAll.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.OneForAll.OneForAll.mnuUtility;
                ViewBag.TypeResource = Resources.OneForAll.OneForAll.mnuRegisterTopUp;
            }
            else if (Type == ProjectStaticString.CompanyTopUp)
            {
                ViewBag.MainModule = ECFBase.Resources.OneForAll.OneForAll.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.OneForAll.OneForAll.mnuUtility;
                ViewBag.TypeResource = "CRP Top Up";
            }
            else if (Type == ProjectStaticString.TPTopUp)
            {
                ViewBag.MainModule = ECFBase.Resources.OneForAll.OneForAll.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.OneForAll.OneForAll.mnuUtility;
                ViewBag.TypeResource = "TP Top Up";
            }
            else if (Type == ProjectStaticString.CashwalletTopUp)
            {
                ViewBag.MainModule = ECFBase.Resources.OneForAll.OneForAll.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.OneForAll.OneForAll.mnuUtility;
                ViewBag.TypeResource = Resources.OneForAll.OneForAll.mnuCashTopUp;
            }
            else if (Type == ProjectStaticString.MultiPointTopUp)
            {
                ViewBag.MainModule = ECFBase.Resources.OneForAll.OneForAll.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.OneForAll.OneForAll.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.OneForAll.OneForAll.mnuMPTopUp;
            }
            else if (Type == ProjectStaticString.GoldPointTopUp)
            {
                ViewBag.MainModule = ECFBase.Resources.OneForAll.OneForAll.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.OneForAll.OneForAll.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.OneForAll.OneForAll.mnuGPTopUp;
            }
            else if (Type == ProjectStaticString.RPCTopUp)
            {
                ViewBag.MainModule = ECFBase.Resources.OneForAll.OneForAll.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.OneForAll.OneForAll.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.OneForAll.OneForAll.mnuRPCTopUp;
            }
            else if (Type == ProjectStaticString.RMPTopUp)
            {
                ViewBag.MainModule = ECFBase.Resources.OneForAll.OneForAll.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.OneForAll.OneForAll.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.OneForAll.OneForAll.mnuRMPTopUp;
            }            
            else if (Type == ProjectStaticString.ICOTopUp)
            {
                ViewBag.MainModule = ECFBase.Resources.OneForAll.OneForAll.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.OneForAll.OneForAll.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.OneForAll.OneForAll.mnuICOTopUp;
            }            
            else if (Type == ProjectStaticString.ChangeOwnership)
            {
                ViewBag.MainModule = ECFBase.Resources.OneForAll.OneForAll.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.OneForAll.OneForAll.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.OneForAll.OneForAll.mnuChangeOwnership;
            }
            else if (Type == ProjectStaticString.ChangeRanking)
            {
                ViewBag.MainModule = ECFBase.Resources.OneForAll.OneForAll.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.OneForAll.OneForAll.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.OneForAll.OneForAll.mnuChangeRanking;
            }
            else if (Type == ProjectStaticString.ChangeSponsor)
            {
                ViewBag.MainModule = ECFBase.Resources.OneForAll.OneForAll.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.OneForAll.OneForAll.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.OneForAll.OneForAll.mnuChangeSponsor;
            }
            else if (Type == ProjectStaticString.ChangeUpline)
            {
                ViewBag.MainModule = ECFBase.Resources.OneForAll.OneForAll.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.OneForAll.OneForAll.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.OneForAll.OneForAll.mnuChangeUpline;
            }
            else if (Type == ProjectStaticString.TransferMember)
            {
                ViewBag.MainModule = ECFBase.Resources.OneForAll.OneForAll.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.OneForAll.OneForAll.mnuUtility;
                ViewBag.TypeResource = "Transfer Member";
            }


            ViewBag.Type = Type;
            UsernameSearchModel model = new UsernameSearchModel();
            return PartialView("AdminSearchUser", model);
        }

        public ActionResult SearchUser(string Username, string Type)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            if (Type == ProjectStaticString.ChangePassword)
            {
                //redirect to Controller
                return RedirectToAction("ChangePassword", "AdminMember", new { Username = Username });
            }
            else if (Type == ProjectStaticString.ChangePin)
            {
                return RedirectToAction("ChangePin", "AdminMember", new { Username = Username });
            }
            else if (Type == ProjectStaticString.ChangeMemberPermission)
            {
                return RedirectToAction("ChangeMemberPermission", "AdminMember", new { Username = Username });
            }
            else if (Type == ProjectStaticString.RegisterwalletTopUp || Type == ProjectStaticString.CompanyTopUp || Type == ProjectStaticString.TPTopUp || Type == ProjectStaticString.ICOTopUp || Type == ProjectStaticString.CashwalletTopUp || Type == ProjectStaticString.GoldPointTopUp || Type == ProjectStaticString.MultiPointTopUp || Type == ProjectStaticString.RPCTopUp || Type == ProjectStaticString.RMPTopUp)
            {
                return RedirectToAction("WalletAdjustment", "AdminWallet", new { Username = Username, Type = Type });
            }            
            else if (Type == ProjectStaticString.ChangeOwnership)
            {
                return RedirectToAction("ChangeOwnership", "AdminMember", new { Username = Username });
            }
            else if (Type == ProjectStaticString.ChangeRanking)
            {
                return RedirectToAction("ChangeRanking", "AdminMember", new { Username = Username });
            }
            else if (Type == ProjectStaticString.ChangeSponsor)
            {
                return RedirectToAction("ChangeSponsor", "AdminMember", new { Username = Username });
            }
            else if (Type == ProjectStaticString.ChangeUpline)
            {
                return RedirectToAction("ChangeUpline", "AdminMember", new { Username = Username });
            }
            else if (Type == ProjectStaticString.TransferMember)
            {
                return RedirectToAction("TransferMember", "AdminMember", new { Username = Username });
            }
            return Home();
        }
        #endregion
        
    }
}


  