﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using ECFBase.Models;
using ECFBase.Components;
using System.Data;
using System.Web.Services;
using ECFBase.Resources;
using ECFBase.Helpers;
using ECFBase.ThirdParties;
using System.Text;
using System.Collections.Specialized;
using System.Net;

namespace ECFBase.Controllers.Admin
{
    public class AdminMemberController : Controller
    {

        #region MembersList       
        public ActionResult NextPageMemberList(string selectedPage, string Option , string Text , string Day , string Month , string Year )
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                return RedirectToAction("MembersList", "AdminMember", new { selectedPage = selectedPage, Option = Option , Text = Text , Day = Day , Month  = Month  , Year = Year });
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        
        public ActionResult MembersList(int selectedPage = 1, string Option = "0" , string Text = "" , string Day = "0" , string Month = "0" , string Year = "0" )
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            if (Day == "Days")
            {
                Day = "0";
            }
            if (Month == "Months")
            {
                Month = "0";
            }
            if (Year == "Years")
            {
                Year = "0";
            }

            int status = 0;
            string message = string.Empty;
            int pages = 0;
            PaginationMemberModel model = new PaginationMemberModel();
            model.MemberType = "Member";


            DataSet dsMembers = new DataSet();

            if (Option == "0")
            {
                dsMembers = MemberDB.GetAllMembers( Text, Convert.ToInt32(Day), Convert.ToInt32(Month), Convert.ToInt32(Year), selectedPage, out pages, out status, out message);
            }
            else if (Option == "USERNAME")
            {
                dsMembers = MemberDB.GetAllMemberByUsername( Text, Convert.ToInt32(Day), Convert.ToInt32(Month), Convert.ToInt32(Year), selectedPage, out pages, out status, out message);
            }
            else if (Option == "FULLNAME")
            {
                dsMembers = MemberDB.GetAllMemberByFullName( Text, Convert.ToInt32(Day), Convert.ToInt32(Month), Convert.ToInt32(Year), selectedPage, out pages, out status, out message);
            }
            else if (Option == "RANKING")
            {
                dsMembers = MemberDB.GetAllMemberByRanking( Text, Convert.ToInt32(Day), Convert.ToInt32(Month), Convert.ToInt32(Year), selectedPage, out pages, out status, out message);
            }
            else if (Option == "PACKAGE")
            {
                dsMembers = MemberDB.GetAllMemberByPackage( Text, Convert.ToInt32(Day), Convert.ToInt32(Month), Convert.ToInt32(Year), selectedPage, out pages, out status, out message);
            }
            else if (Option == "SPONSOR")
            {
                dsMembers = MemberDB.GetAllMemberBySponsor( Text, Convert.ToInt32(Day), Convert.ToInt32(Month), Convert.ToInt32(Year), selectedPage, out pages, out status, out message);
            }
            else if (Option == "COUNTRY")
            {
                dsMembers = MemberDB.GetAllMemberByCountry(Text, Convert.ToInt32(Day), Convert.ToInt32(Month), Convert.ToInt32(Year), selectedPage, out pages, out status, out message);
            }
            else
            {
                dsMembers = MemberDB.GetAllMembers(Text, Convert.ToInt32(Day), Convert.ToInt32(Month), Convert.ToInt32(Year), selectedPage, out pages, out status, out message);
            }




            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsMembers.Tables[0].Rows)
            {
                var member = new MemberModel();
                member.Number = dr["rownumber"].ToString();
                member.MemberId = dr["CMEM_ID"].ToString();
                member.Username = dr["CUSR_USERNAME"].ToString();
                member.MemberCountry = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                member.SurName = dr["CUSR_FULLNAME"].ToString();
                member.BonusWallet = float.Parse(dr["CMEM_BONUSWALLET"].ToString());
                member.Intro = dr["CMEM_INTRO"].ToString();
                member.Upline = dr["Upline"].ToString();
                member.RegisteredBy = dr["CMEM_REGISTEREDBY"].ToString();
                member.PackageName = dr["PACKAGE"].ToString();
                member.Rank = Misc.GetMemberRanking(dr["CRANK_CODE"].ToString());
                member.Invest = dr["INVEST"].ToString();
                member.JoinedDate = (DateTime)dr["CMEM_CREATEDON"];
                member.UserActive = (bool)dr["CUSR_ACTIVE"];
                member.IC = dr["CUSRINFO_IC"].ToString();
                model.MemberList.Add(member);
            }

            model.FilteringCriteria = Misc.ConstructsFilteringCriteria().ToList();

            model.Days = Misc.ConstructsDay().ToList();
            model.Months = Misc.ConstructsMonth().ToList();
            model.Years = Misc.ConstructYears(2013).ToList();

            model.SelectedDays = Day;
            model.SelectedYears = Year; 
            model.SelectedMonth = Month;

            int isShow = 0;
            Session["isShow"] = isShow;

            ViewBag.Title = "Members List";
            return PartialView("MembersList", model);
        }

        public ActionResult MemberHome(string username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            try
            {
                EditMemberModel memberMC = new EditMemberModel();

                int ok;
                string msg;
                string Username = Session["Admin"].ToString();
                string languageCode = Session["LanguageChosen"].ToString();

                DataSet dsMember = AdminMemberDB.GetMemberInfoByUserName(username, out ok, out msg);

                memberMC.Member.Username = dsMember.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
                memberMC.Member.FirstName = dsMember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                //memberMC.UserInfo.Nickname = dsMember.Tables[0].Rows[0]["CUSRINFO_NICKNAME"].ToString();

                DataSet dsCountry = AdminGeneralDB.GetCountryByID(Convert.ToInt32(dsMember.Tables[0].Rows[0]["CCOUNTRY_ID"]), out ok, out msg);
                memberMC.SelectedCountry = dsCountry.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();

                memberMC.Member.MemberEmail = dsMember.Tables[0].Rows[0]["CUSR_EMAIL"].ToString();
                memberMC.UserInfo.CellPhone = dsMember.Tables[0].Rows[0]["CUSRINFO_CELLPHONE"].ToString();
                memberMC.UserInfo.IC = dsMember.Tables[0].Rows[0]["CUSRINFO_IC"].ToString();
                
                memberMC.UserInfo.ShipName = dsMember.Tables[0].Rows[0]["CUSRINFO_SHIP_NAME"].ToString();
                var shippingAddress = dsMember.Tables[0].Rows[0]["CUSRINFO_SHIP_ADDRESS"].ToString();
                if (!string.IsNullOrEmpty(shippingAddress.Trim()))
                {
                    var address = shippingAddress.Split(new char[] { '\r' });
                    for (int i = 0; i < address.Length; i++)
                    {
                        if (i == 0) memberMC.UserInfo.ShipAddress = address[0];
                        else if (i == 1) memberMC.UserInfo.ShipAddress1 = address[1];
                        else if (i == 2) memberMC.UserInfo.ShipAddress2 = address[2];
                        else if (i == 3) memberMC.UserInfo.ShipAddress3 = address[3];
                    }
                }

                memberMC.UserInfo.ShipCity = dsMember.Tables[0].Rows[0]["CUSRINFO_SHIP_CITY"].ToString();
                memberMC.UserInfo.ShipPostcode = dsMember.Tables[0].Rows[0]["CUSRINFO_SHIP_POSTCODE"].ToString();
                memberMC.UserInfo.SelectedCountry = dsMember.Tables[0].Rows[0]["CUSRINFO_SHIP_COUNTRY"].ToString();
                memberMC.UserInfo.SelectedProvince = dsMember.Tables[0].Rows[0]["CUSRINFO_SHIP_STATE"].ToString();
                memberMC.UserInfo.SelectedCity = dsMember.Tables[0].Rows[0]["CUSRINFO_SHIP_CITY"].ToString();
                memberMC.UserInfo.SelectedDistrict = dsMember.Tables[0].Rows[0]["CUSRINFO_SHIP_DISTRICT"].ToString();
                memberMC.UserInfo.SelectedStockist = dsMember.Tables[0].Rows[0]["CUSRINFO_STOCKIST"].ToString();
                memberMC.UserInfo.ShipPhoneNo = dsMember.Tables[0].Rows[0]["CUSRINFO_SHIP_PHONENO"].ToString();
                memberMC.UserInfo.BeneficiaryName = dsMember.Tables[0].Rows[0]["CUSRINFO_BENEFICIARYNAME"].ToString();
                memberMC.UserInfo.SelectedRelationship = dsMember.Tables[0].Rows[0]["CUSRINFO_RELATIONSHIP"].ToString();
                memberMC.UserInfo.Address = dsMember.Tables[0].Rows[0]["CUSRINFO_MAIL_ADDRESS"].ToString();
                memberMC.UserInfo.SelectedCity = dsMember.Tables[0].Rows[0]["CUSRINFO_MAIL_CITY"].ToString();
                memberMC.UserInfo.State = dsMember.Tables[0].Rows[0]["CUSRINFO_MAIL_STATE"].ToString();
                memberMC.UserInfo.Postcode = dsMember.Tables[0].Rows[0]["CUSRINFO_MAIL_POSTCODE"].ToString();
                memberMC.UserInfo.SelectedCountry = dsMember.Tables[0].Rows[0]["CUSRINFO_MAIL_COUNTRY"].ToString();
                #region Country
                List<CountrySetupModel> countriess = Misc.GetAllMobileCountryList(languageCode, ref ok, ref msg);
                List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

                memberMC.Countries = from c in countriess
                                     select new SelectListItem
                                     {
                                         Selected = false,
                                         Text = c.CountryName + "(" + c.MobileCode + ")",
                                         Value = c.CountryCode
                                     };

                memberMC.UserInfo.Countries = from c in countries
                                              select new SelectListItem
                                              {
                                                  Selected = false,
                                                  Text = c.CountryName,
                                                  Value = c.CountryCode
                                              };

                memberMC.UserInfo.MobileCountryCode = dsMember.Tables[0].Rows[0]["CUSRINFO_PHONE_COUNTRY"].ToString();

                if (memberMC.UserInfo.SelectedCountry == string.Empty && memberMC.UserInfo.Countries.Any())
                {
                    var firstOption = memberMC.UserInfo.Countries.First();
                    memberMC.UserInfo.SelectedCountry = firstOption != null ? firstOption.Value : string.Empty;
                }

                #endregion

                #region Province
                DataSet dsProvinces = AdminGeneralDB.GetAllProvincesByCountry(Session["LanguageChosen"].ToString(), memberMC.UserInfo.SelectedCountry);
                List<ProvinceSetupModel> provinceList = new List<ProvinceSetupModel>();

                foreach (DataRow dr in dsProvinces.Tables[0].Rows)
                {
                    ProvinceSetupModel proModel = new ProvinceSetupModel();
                    proModel.ProvinceCode = dr["CPROVINCE_CODE"].ToString();
                    proModel.ProvinceName = dr["CMULTILANGPROVINCE_NAME"].ToString();

                    provinceList.Add(proModel);
                }

                memberMC.UserInfo.Province = from c in provinceList
                                             select new SelectListItem
                                             {
                                                 Selected = false,
                                                 Text = c.ProvinceName,
                                                 Value = c.ProvinceCode
                                             };

                if (memberMC.UserInfo.SelectedProvince == string.Empty && memberMC.UserInfo.Province.Any())
                {
                    var firstOption = memberMC.UserInfo.Province.First();
                    memberMC.UserInfo.SelectedProvince = firstOption != null ? firstOption.Value : string.Empty;
                }

                #endregion

                #region City
                DataSet dsCity = AdminGeneralDB.GetAllCitiesByProvince(Session["LanguageChosen"].ToString(), memberMC.UserInfo.SelectedProvince);
                List<CitySetupModel> cityList = new List<CitySetupModel>();

                foreach (DataRow dr in dsCity.Tables[0].Rows)
                {
                    CitySetupModel cityModel = new CitySetupModel();
                    cityModel.CityCode = dr["CCITY_CODE"].ToString();
                    cityModel.CityName = dr["CMULTILANGCITY_NAME"].ToString();

                    cityList.Add(cityModel);
                }

                memberMC.UserInfo.City = from c in cityList
                                         select new SelectListItem
                                         {
                                             Selected = false,
                                             Text = c.CityName,
                                             Value = c.CityCode
                                         };

                if (memberMC.UserInfo.SelectedCity == string.Empty && memberMC.UserInfo.City.Any())
                {
                    var firstOption = memberMC.UserInfo.City.First();
                    memberMC.UserInfo.SelectedCity = firstOption != null ? firstOption.Value : string.Empty;
                }

                #endregion

                #region District
                DataSet dsDistrict = AdminGeneralDB.GetAllDistrictsByCity(Session["LanguageChosen"].ToString(), memberMC.UserInfo.SelectedCity);
                List<DistrictSetupModel> districtList = new List<DistrictSetupModel>();

                foreach (DataRow dr in dsDistrict.Tables[0].Rows)
                {
                    DistrictSetupModel districtModel = new DistrictSetupModel();
                    districtModel.DistrictCode = dr["CDISTRICT_CODE"].ToString();
                    districtModel.DistrictName = dr["CMULTILANGDISTRICT_NAME"].ToString();

                    districtList.Add(districtModel);
                }

                memberMC.UserInfo.District = from c in districtList
                                             select new SelectListItem
                                             {
                                                 Selected = false,
                                                 Text = c.DistrictName,
                                                 Value = c.DistrictCode
                                             };

                if (memberMC.UserInfo.SelectedDistrict == string.Empty && memberMC.UserInfo.District.Any())
                {
                    var firstOption = memberMC.UserInfo.District.First();
                    memberMC.UserInfo.SelectedDistrict = firstOption != null ? firstOption.Value : string.Empty;
                }

                #endregion


                #region DOB
                memberMC.UserInfo.Days = Misc.ConstructsDay().ToList();
                memberMC.UserInfo.Months = Misc.ConstructsMonth().ToList();
                memberMC.UserInfo.Years = Misc.ConstructYears().ToList();
                if (dsMember.Tables[0].Rows[0]["CUSR_DOB"].ToString() != "")
                {
                    DateTime dob = Convert.ToDateTime(dsMember.Tables[0].Rows[0]["CUSR_DOB"]);
                    memberMC.UserInfo.SelectedYears = dob.Year.ToString();
                    memberMC.UserInfo.SelectedMonth = dob.Month.ToString();
                    memberMC.UserInfo.SelectedDay = dob.Day.ToString();
                }
                else
                {
                    memberMC.UserInfo.SelectedYears = "1960";
                    memberMC.UserInfo.SelectedMonth = "1";
                    memberMC.UserInfo.SelectedDay = "1";
                }

                #endregion

                #region Gender

                memberMC.UserInfo.Gender = Misc.ConstructGender(dsMember.Tables[0].Rows[0]["CUSRINFO_GENDER"].ToString());

                #endregion



                return PartialView("ViewMemberDetails", memberMC);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewMemberDetails()
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                EditMemberModel memberMC = new EditMemberModel();

                int ok;
                string msg;
                string Username = Session["Admin"].ToString();
                string languageCode = Session["LanguageChosen"].ToString();

                DataSet dsMember = AdminMemberDB.GetMemberInfoByUserName(Username, out ok, out msg);

                memberMC.Member.Username = dsMember.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
                memberMC.Member.FirstName = dsMember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();

                DataSet dsCountry = AdminGeneralDB.GetCountryByID(Convert.ToInt32(dsMember.Tables[0].Rows[0]["CCOUNTRY_ID"]), out ok, out msg);
                memberMC.SelectedCountry = dsCountry.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                memberMC.Member.MemberEmail = dsMember.Tables[0].Rows[0]["CUSR_EMAIL"].ToString();

                memberMC.UserInfo.MobileCountryCode = dsMember.Tables[0].Rows[0]["CUSRINFO_PHONE_COUNTRY"].ToString();
                


                memberMC.UserInfo.CellPhone = dsMember.Tables[0].Rows[0]["CUSRINFO_CELLPHONE"].ToString();
                
                memberMC.UserInfo.IC = dsMember.Tables[0].Rows[0]["CUSRINFO_IC"].ToString();
               
                memberMC.UserInfo.ShipName = dsMember.Tables[0].Rows[0]["CUSRINFO_SHIP_NAME"].ToString();
                var shippingAddress = dsMember.Tables[0].Rows[0]["CUSRINFO_SHIP_ADDRESS"].ToString();
                if (!string.IsNullOrEmpty(shippingAddress.Trim()))
                {
                    var address = shippingAddress.Split(new char[] { '\r' });
                    for (int i = 0; i < address.Length; i++)
                    {
                        if (i == 0) memberMC.UserInfo.ShipAddress = address[0];
                        else if (i == 1) memberMC.UserInfo.ShipAddress1 = address[1];
                        else if (i == 2) memberMC.UserInfo.ShipAddress2 = address[2];
                        else if (i == 3) memberMC.UserInfo.ShipAddress3 = address[3];
                    }
                }

                memberMC.UserInfo.ShipCity = dsMember.Tables[0].Rows[0]["CUSRINFO_SHIP_CITY"].ToString();
                memberMC.UserInfo.ShipPostcode = dsMember.Tables[0].Rows[0]["CUSRINFO_SHIP_POSTCODE"].ToString();
                memberMC.UserInfo.SelectedCountry = dsMember.Tables[0].Rows[0]["CUSRINFO_SHIP_COUNTRY"].ToString();
                memberMC.UserInfo.SelectedProvince = dsMember.Tables[0].Rows[0]["CUSRINFO_SHIP_STATE"].ToString();
                memberMC.UserInfo.SelectedCity = dsMember.Tables[0].Rows[0]["CUSRINFO_SHIP_CITY"].ToString();
                memberMC.UserInfo.SelectedDistrict = dsMember.Tables[0].Rows[0]["CUSRINFO_SHIP_DISTRICT"].ToString();
                memberMC.UserInfo.SelectedStockist = dsMember.Tables[0].Rows[0]["CUSRINFO_STOCKIST"].ToString();
                memberMC.UserInfo.ShipPhoneNo = dsMember.Tables[0].Rows[0]["CUSRINFO_SHIP_PHONENO"].ToString();
                memberMC.UserInfo.BeneficiaryName = dsMember.Tables[0].Rows[0]["CUSRINFO_BENEFICIARYNAME"].ToString();
                memberMC.UserInfo.SelectedRelationship = dsMember.Tables[0].Rows[0]["CUSRINFO_RELATIONSHIP"].ToString();


                #region Country
                
                List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

                List<CountrySetupModel> countrie = Misc.GetAllMobileCountryList(languageCode, ref ok, ref msg);

                memberMC.Countries = from c in countrie
                                     select new SelectListItem
                                     {
                                         Selected = false,
                                         Text = c.CountryCode + "(" + c.MobileCode+ ")",
                                         Value = c.CountryCode
                                     };

                memberMC.UserInfo.Countries = from c in countries
                                              select new SelectListItem
                                              {
                                                  Selected = false,
                                                  Text = c.CountryName,
                                                  Value = c.CountryCode
                                              };

                foreach (CountrySetupModel temp in countrie)
                {
                    if (temp.CountryCode == memberMC.SelectedMobileCountry)
                    {
                        memberMC.UserInfo.MobileCountryCode = temp.MobileCode;
                        break;
                    }
                }

                if (memberMC.UserInfo.SelectedCountry == string.Empty && memberMC.UserInfo.Countries.Any())
                {
                    var firstOption = memberMC.UserInfo.Countries.First();
                    memberMC.UserInfo.SelectedCountry = firstOption != null ? firstOption.Value : string.Empty;
                }

                #endregion

                #region Province
                DataSet dsProvinces = AdminGeneralDB.GetAllProvincesByCountry(Session["LanguageChosen"].ToString(), memberMC.UserInfo.SelectedCountry);
                List<ProvinceSetupModel> provinceList = new List<ProvinceSetupModel>();

                foreach (DataRow dr in dsProvinces.Tables[0].Rows)
                {
                    ProvinceSetupModel proModel = new ProvinceSetupModel();
                    proModel.ProvinceCode = dr["CPROVINCE_CODE"].ToString();
                    proModel.ProvinceName = dr["CMULTILANGPROVINCE_NAME"].ToString();

                    provinceList.Add(proModel);
                }

                memberMC.UserInfo.Province = from c in provinceList
                                             select new SelectListItem
                                             {
                                                 Selected = false,
                                                 Text = c.ProvinceName,
                                                 Value = c.ProvinceCode
                                             };

                if (memberMC.UserInfo.SelectedProvince == string.Empty && memberMC.UserInfo.Province.Any())
                {
                    var firstOption = memberMC.UserInfo.Province.First();
                    memberMC.UserInfo.SelectedProvince = firstOption != null ? firstOption.Value : string.Empty;
                }

                #endregion

                #region City
                DataSet dsCity = AdminGeneralDB.GetAllCitiesByProvince(Session["LanguageChosen"].ToString(), memberMC.UserInfo.SelectedProvince);
                List<CitySetupModel> cityList = new List<CitySetupModel>();

                foreach (DataRow dr in dsCity.Tables[0].Rows)
                {
                    CitySetupModel cityModel = new CitySetupModel();
                    cityModel.CityCode = dr["CCITY_CODE"].ToString();
                    cityModel.CityName = dr["CMULTILANGCITY_NAME"].ToString();

                    cityList.Add(cityModel);
                }

                memberMC.UserInfo.City = from c in cityList
                                         select new SelectListItem
                                         {
                                             Selected = false,
                                             Text = c.CityName,
                                             Value = c.CityCode
                                         };

                if (memberMC.UserInfo.SelectedCity == string.Empty && memberMC.UserInfo.City.Any())
                {
                    var firstOption = memberMC.UserInfo.City.First();
                    memberMC.UserInfo.SelectedCity = firstOption != null ? firstOption.Value : string.Empty;
                }

                #endregion

                #region District
                DataSet dsDistrict = AdminGeneralDB.GetAllDistrictsByCity(Session["LanguageChosen"].ToString(), memberMC.UserInfo.SelectedCity);
                List<DistrictSetupModel> districtList = new List<DistrictSetupModel>();

                foreach (DataRow dr in dsDistrict.Tables[0].Rows)
                {
                    DistrictSetupModel districtModel = new DistrictSetupModel();
                    districtModel.DistrictCode = dr["CDISTRICT_CODE"].ToString();
                    districtModel.DistrictName = dr["CMULTILANGDISTRICT_NAME"].ToString();

                    districtList.Add(districtModel);
                }

                memberMC.UserInfo.District = from c in districtList
                                             select new SelectListItem
                                             {
                                                 Selected = false,
                                                 Text = c.DistrictName,
                                                 Value = c.DistrictCode
                                             };

                if (memberMC.UserInfo.SelectedDistrict == string.Empty && memberMC.UserInfo.District.Any())
                {
                    var firstOption = memberMC.UserInfo.District.First();
                    memberMC.UserInfo.SelectedDistrict = firstOption != null ? firstOption.Value : string.Empty;
                }

                #endregion

                #region DOB
                memberMC.UserInfo.Days = Misc.ConstructsDay().ToList();
                memberMC.UserInfo.Months = Misc.ConstructsMonth().ToList();
                memberMC.UserInfo.Years = Misc.ConstructYears().ToList();
                if (dsMember.Tables[0].Rows[0]["CUSR_DOB"].ToString() != "")
                {
                    DateTime dob = Convert.ToDateTime(dsMember.Tables[0].Rows[0]["CUSR_DOB"]);
                    memberMC.UserInfo.DOB = Convert.ToDateTime(dsMember.Tables[0].Rows[0]["CUSR_DOB"]);
                    memberMC.UserInfo.SelectedYears = dob.Year.ToString();
                    memberMC.UserInfo.SelectedMonth = dob.Month.ToString();
                    memberMC.UserInfo.SelectedDay = dob.Day.ToString();
                }
                else
                {
                    memberMC.UserInfo.SelectedYears = "1960";
                    memberMC.UserInfo.SelectedMonth = "1";
                    memberMC.UserInfo.SelectedDay = "1";
                }
                
                
                #endregion

                #region Gender

                memberMC.UserInfo.Gender = Misc.ConstructGender(dsMember.Tables[0].Rows[0]["CUSRINFO_GENDER"].ToString());

                #endregion

                return PartialView("ViewMemberDetails", memberMC);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateMemberDetails(EditMemberModel mem)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                int ok = 0;
                string msg = "";

                mem.UserInfo.DOB = new DateTime(int.Parse(mem.UserInfo.SelectedYears), int.Parse(mem.UserInfo.SelectedMonth), int.Parse(mem.UserInfo.SelectedDay));

                DateTime DOB = DateTime.Now;
                var HP = "";
                var email = "";
                var IC = "";

                DataSet dsMember = AdminMemberDB.GetMemberInfoByUserName(mem.Member.Username, out ok, out msg);
                if (dsMember.Tables[0].Rows.Count != 0)
                {
                    if (dsMember.Tables[0].Rows[0]["CUSR_DOB"].ToString() != "")
                    {
                        DOB = Convert.ToDateTime(dsMember.Tables[0].Rows[0]["CUSR_DOB"]);
                    }
                    else
                    {
                        DOB = new DateTime(1960, 1, 1);
                    }
                    
                    HP = dsMember.Tables[0].Rows[0]["CUSRINFO_CELLPHONE"].ToString();
                    email = dsMember.Tables[0].Rows[0]["CUSR_EMAIL"].ToString();
                    IC = dsMember.Tables[0].Rows[0]["CUSRINFO_IC"].ToString();
                }

                if (mem.UserInfo.Address == "" || mem.UserInfo.Address == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningFillinMailingAddress);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                if (mem.UserInfo.SelectedCity == "" || mem.UserInfo.SelectedCity == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningFillinCity);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.UserInfo.State == "" || mem.UserInfo.State == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningFillinState);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.UserInfo.Postcode == "" || mem.UserInfo.Postcode == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningFillInPostCode);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.SelectedCountry == "" || mem.SelectedCountry == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningSelectCountry);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.UserInfo.MobileCountryCode == Resources.OneForAll.OneForAll.dropboxSelect)
                {
                    Response.Write("Please Select a Mobile Country");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                MemberProfileDB.UpdateMember(mem, out ok, out msg);

                if (mem.UserInfo.IC != IC)
                {                   
                    AdminDB.InsertUserTrackingLog("Admin", mem.Member.Username, HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), "Update Profile", IC, mem.UserInfo.IC, Session["Admin"].ToString());
                }
                if (mem.UserInfo.DOB != DOB)
                {
                    AdminDB.InsertUserTrackingLog("Admin", mem.Member.Username, HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), "Update Profile", DOB.ToString(), mem.UserInfo.DOB.ToString(), Session["Admin"].ToString());
                }

                if (mem.UserInfo.CellPhone != HP)
                {
                    AdminDB.InsertUserTrackingLog("Admin", mem.Member.Username, HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), "Update Profile", HP, mem.UserInfo.CellPhone, Session["Admin"].ToString());
                }

                if (mem.Member.MemberEmail != email)
                {
                    AdminDB.InsertUserTrackingLog("Admin", mem.Member.Username, HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), "Update Profile", email, mem.Member.MemberEmail, Session["Admin"].ToString());
                }


                //return ViewMemberDetails();
                //return MemberHome(mem.Member.Username);
                return MembersList();

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult CheckFileExtension(string file)
        {
            var isValid = Misc.IsFileExtensionValid(file);

            return Json(isValid.ToString(), JsonRequestBehavior.AllowGet);
        }

        public void ExportMemberList()
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=FullMemberList.csv");
            Response.ContentType = "application/octet-stream";
            StreamWriter sw = new StreamWriter(Response.OutputStream, Encoding.UTF8);
            sw.Write(PrintMemberList());
            sw.Close();
            Response.End();
        }

        private string PrintMemberList()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("No,Member Id,Full Name, Country , IC, Phone Number,EMail ,Package Code,Package Amount,Ranking,Sponsor,Registered By,Join Date Time"));
            string message = string.Empty;
            DataSet dsMemberlist = new DataSet();
            dsMemberlist = MemberDB.GetAllMembersExport();

            foreach (DataRow dr in dsMemberlist.Tables[0].Rows)
            {
                
                sb.AppendLine(dr["rownumber"].ToString() + "," +
                              dr["CUSR_USERNAME"].ToString() + "," +
                              dr["CUSR_FULLNAME"].ToString() + "," +
                              dr["CMULTILANGCOUNTRY_NAME"].ToString() + "," +
                              dr["CUSRINFO_IC"].ToString() + "\t," +
                              dr["CUSRINFO_CELLPHONE"].ToString() + "\t," +
                              dr["CUSR_EMAIL"].ToString() + "," +
                              dr["PACKAGE"].ToString() + "," +
                              dr["INVEST"].ToString() + "," +
                              Misc.GetMemberRanking(dr["CRANK_CODE"].ToString()) + "," +
                              dr["CMEM_INTRO"].ToString() + "," +
                              dr["CMEM_REGISTEREDBY"].ToString() + "," +
                              Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt"));
            }

            return sb.ToString();
        }

        #endregion

        #region Member Upgrade Package Listing
        public ActionResult NextPageMemberUpgradeList(string selectedPage, string Option, string Text, string Day, string Month, string Year)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                return RedirectToAction("MembersUpgradeList", "AdminMember", new { selectedPage = selectedPage, Option = Option, Text = Text, Day = Day, Month = Month, Year = Year });
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult MembersUpgradeList(int selectedPage = 1, string Option = "0", string Text = "", string Day = "0", string Month = "0", string Year = "0")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            if (Day == "Days")
            {
                Day = "0";
            }
            if (Month == "Months")
            {
                Month = "0";
            }
            if (Year == "Years")
            {
                Year = "0";
            }

            int status = 0;
            string message = string.Empty;
            int pages = 0;
            PaginationMemberModel model = new PaginationMemberModel();
            model.MemberType = "Member";
            DataSet dsMembers = MemberDB.GetAllMembersUpgrade(Option, Text, Convert.ToInt32(Day), Convert.ToInt32(Month), Convert.ToInt32(Year), selectedPage, out pages, out status, out message);

            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsMembers.Tables[0].Rows)
            {
                var member = new MemberModel();
                member.Number = dr["rownumber"].ToString();
                member.Username = dr["CUSR_USERNAME"].ToString();
                member.SurName = dr["CUSR_FULLNAME"].ToString();
                member.IC = dr["CUSRINFO_IC"].ToString();
                member.PackageName = dr["CREGWL_APPOTHER"].ToString();
                member.Invest = dr["INVEST"].ToString();
                member.Rank = dr["CRANKSET_NAME"].ToString();
                member.Intro = dr["CMEM_INTRO"].ToString();
                member.RegisteredBy = dr["CMEM_REGISTEREDBY"].ToString();
                member.JoinedDate = (DateTime)dr["CMEM_CREATEDON"];
                member.UpgradeDate = (DateTime)dr["CREGWL_CREATEDON"];
                model.MemberList.Add(member);
            }

            model.FilteringCriteria = Misc.ConstructsFilteringCriteria().ToList();

            model.Days = Misc.ConstructsDay().ToList();
            model.Months = Misc.ConstructsMonth().ToList();
            model.Years = Misc.ConstructYears(2013).ToList();

            model.SelectedDays = Day;
            model.SelectedYears = Year;
            model.SelectedMonth = Month;

            int isShow = 0;
            Session["isShow"] = isShow;

            ViewBag.Title = "Members Upgrade List";
            return PartialView("MembersUpgradeList", model);
        }

        public void ExportMemberUpgraddeList()
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=FullMemberUpgradeList.csv");
            Response.ContentType = "application/octet-stream";
            StreamWriter sw = new StreamWriter(Response.OutputStream, Encoding.UTF8);
            sw.Write(PrintMemberUpgraddeList());
            sw.Close();
            Response.End();
        }

        private string PrintMemberUpgraddeList()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("No,Member Id,Full Name,IC, Package Code,Package Amount,Ranking,Sponsor,Registered By,Join Date Time ,Upgrade Date"));
            string message = string.Empty;
            DataSet dsMemberlist = new DataSet();
            dsMemberlist = MemberDB.GetAllMembersUpgradeExport();

            foreach (DataRow dr in dsMemberlist.Tables[0].Rows)
            {

                sb.AppendLine(dr["rownumber"].ToString() + "," +
                              dr["CUSR_USERNAME"].ToString() + "," +
                              dr["CUSR_FULLNAME"].ToString() + "," +
                              dr["CUSRINFO_IC"].ToString() + "\t," +
                              dr["CREGWL_APPOTHER"].ToString() + "," +
                              dr["INVEST"].ToString() + "," +
                              dr["CRANKSET_NAME"].ToString() + "," +
                              dr["CMEM_INTRO"].ToString() + "," +
                              dr["CMEM_REGISTEREDBY"].ToString() + "," +
                              Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt") + "," +
                               Convert.ToDateTime(dr["CREGWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt")
                              );
            }

            return sb.ToString();
        }
        #endregion

        #region Special Register New Member

        #region Market Tree View
        [HttpPost]
        public ActionResult MemberMarketTree(int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("Adminlogin", "Admin", new {  });
            }


            int ok = 0, pages = 0;
            string msg = "";
            var member = MemberDB.GetMemberByUsername("BVA", out ok, out msg);
            string TopMember = member.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();


            MarketTreeModel memberMarketTree = GetAllMarketTreeMember(TopMember);

            memberMarketTree.country = member.Tables[0].Rows[0]["CCOUNTRY_ID"].ToString();

            var Stockist = MemberDB.GetMemberByUsername("BVA", out ok, out msg);
            memberMarketTree.Stockist = Stockist.Tables[0].Rows[0]["CMEM_MEMBER_TYPE"].ToString();


            DataSet logo = AdminGeneralDB.GetRankIcon();
            foreach (DataRow dr in logo.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["CRANKSET_ICON"].ToString();
                RankModel RankList = new RankModel();
                RankList.RankIconPath = dr["CRANKSET_ICON"].ToString();
                //RankList.RankIconName = float.Parse(dr["AMOUNT"].ToString());
                //RankList.RankAmount = float.Parse(dr["AMOUNT"].ToString());
                ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();
                RankList.RankName = dr["CRANKSET_NAME"].ToString();

                memberMarketTree.RankList.Add(RankList);
            }


            string keke = Session["LanguageChosen"].ToString();
            if (Session["LanguageChosen"].ToString().Equals("zh-cn", StringComparison.InvariantCultureIgnoreCase))
            {
                memberMarketTree.Picture = "RegisterEn.png";
            }
            else if (Session["LanguageChosen"].ToString().Equals("en-US", StringComparison.InvariantCultureIgnoreCase))
            {
                memberMarketTree.Picture = "RegisterEn.png";
            }
            else if (Session["LanguageChosen"].ToString().Equals("zh-tw", StringComparison.InvariantCultureIgnoreCase))
            {
                memberMarketTree.Picture = "RegisterEn.png";
            }
            else if (Session["LanguageChosen"].ToString().Equals("ja-JP", StringComparison.InvariantCultureIgnoreCase))
            {
                memberMarketTree.Picture = "RegisterEn.png";
            }           

            
            return PartialView("SpecialRegisterMarketTree", memberMarketTree);
        }

        private MarketTreeModel GetAllMarketTreeMember(string TopMember)
        {
            MarketTreeModel treeMC = new MarketTreeModel();

            int nCount = 0;
            string fullName = "";

            DataSet dsFollow = MemberDB.GetFollowID(TopMember);

            //Loop for 15 times
            while (nCount <= 14)
            {
                if (dsFollow.Tables[nCount].Rows.Count == 0)
                {
                    treeMC.MemberList.Add("0");
                    treeMC.LogoList.Add("~");
                    treeMC.TooltipList.Add(ConstructTooltipTree("", "", "0", "0", "0", "0", "", ""));
                    treeMC.PackageList.Add("");
                    treeMC.DateList.Add("");
                    treeMC.AccYJLeftList.Add("");
                    treeMC.AccYJRightList.Add("");
                    treeMC.CFBalLeftList.Add("");
                    treeMC.CFBalRightList.Add("");
                    treeMC.SalesLeftList.Add("");
                    treeMC.SalesRightList.Add("");
                    treeMC.TotalDownlineLeft.Add("");
                    treeMC.TotalDownlineRight.Add("");

                    if (nCount == 0)
                    {
                        treeMC.MainLeftYJ = "0";
                        treeMC.MainLeftBalance = "0";
                        treeMC.MainRightYJ = "0";
                        treeMC.MainRightBalance = "0";
                    }
                }
                else
                {
                    DataRow dr = dsFollow.Tables[nCount].Rows[0];
                    DateTime joinedDate = DateTime.Parse(dr["CUSR_CREATEDON"].ToString());
                    fullName = string.Format("{0} {1}", dr["CUSR_FULLNAME"].ToString(), dr["CUSR_FULLNAME"].ToString());
                    treeMC.MemberList.Add(dr["CUSR_USERNAME"].ToString());

                    int ok = 0;
                    string msg = "";
                    string imagePath = "";
                    var member = MemberDB.GetMemberByUsername(dr["CUSR_USERNAME"].ToString(), out ok, out msg);


                    //imagePath = Misc.GetCodeNameImageInt(dr["CRANKSET_NAME"].ToString());
                    imagePath = member.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

                    treeMC.LogoList.Add(imagePath);
                    treeMC.TooltipList.Add(ConstructTooltipTree(fullName, joinedDate.ToShortDateString(), dr["CMTREE_GROUPAYJ"].ToString(), dr["CMTREE_GROUPBYJ"].ToString(), dr["CMTREE_GROUPAGP"].ToString(), dr["CMTREE_GROUPBGP"].ToString(), dr["CMEM_INTRO"].ToString(), string.Format("人民币{0}", dr["CPKG_AMOUNT"].ToString())));
                    treeMC.PackageList.Add(string.Format("{0}", Helper.NVL(dr["CUSRINFO_NICKNAME"])));
                    treeMC.DateList.Add(string.Format("{0}", joinedDate.ToString("dd/MM/yyyy")));


                    float leftYJ1 = float.Parse(dr["CMTREE_GROUPAYJ"].ToString());
                    float rightYJ1 = float.Parse(dr["CMTREE_GROUPBYJ"].ToString());
                    float leftGP1 = float.Parse(dr["CMTREE_GROUPAGP"].ToString());
                    float rightGP1 = float.Parse(dr["CMTREE_GROUPBGP"].ToString());
                    float leftsales1 = float.Parse(dr["LEFTSALES"].ToString());
                    float rightsales1 = float.Parse(dr["RIGHTSALES"].ToString());
                    int lefttotaldownline1 = Convert.ToInt32(dr["CMTREE_TGROUPAMEMBER"].ToString());
                    int righttotaldownline1 = Convert.ToInt32(dr["CMTREE_TGROUPBMEMBER"].ToString());

                    treeMC.AccYJLeftList.Add(string.Format("{0}", Helper.NVL(leftYJ1.ToString("###,##0"))));
                    treeMC.AccYJRightList.Add(string.Format("{0}", Helper.NVL(rightYJ1.ToString("###,##0"))));
                    treeMC.CFBalLeftList.Add(string.Format("{0}", Helper.NVL(leftGP1.ToString("###,##0"))));
                    treeMC.CFBalRightList.Add(string.Format("{0}", Helper.NVL(rightGP1.ToString("###,##0"))));
                    treeMC.SalesLeftList.Add(string.Format("{0}", Helper.NVL(leftsales1.ToString("###,##0"))));
                    treeMC.SalesRightList.Add(string.Format("{0}", Helper.NVL(rightsales1.ToString("###,##0"))));
                    treeMC.TotalDownlineLeft.Add(string.Format("{0}", Helper.NVL(lefttotaldownline1.ToString())));
                    treeMC.TotalDownlineRight.Add(string.Format("{0}", Helper.NVL(righttotaldownline1.ToString())));


                    if (nCount == 0)
                    {
                        float leftYJ = float.Parse(dr["CMTREE_GROUPAYJ"].ToString());
                        float rightYJ = float.Parse(dr["CMTREE_GROUPBYJ"].ToString());
                        float leftGP = float.Parse(dr["CMTREE_GROUPAGP"].ToString());
                        float rightGP = float.Parse(dr["CMTREE_GROUPBGP"].ToString());
                        float leftsales = float.Parse(dr["LEFTSALES"].ToString());
                        float rightsales = float.Parse(dr["RIGHTSALES"].ToString());

                        treeMC.MainLeftYJ = leftYJ.ToString("###,##0");
                        treeMC.MainRightYJ = rightYJ.ToString("###,##0");

                        treeMC.MainLeftBalance = leftGP.ToString("###,##0");
                        treeMC.MainRightBalance = rightGP.ToString("###,##0");

                        treeMC.MainLeftSales = leftsales.ToString("###,##0");
                        treeMC.MainRightSales = rightsales.ToString("###,##0");
                    }
                    else if (nCount == 1)
                    {
                        float leftYJ = float.Parse(dr["CMTREE_GROUPAYJ"].ToString());
                        float rightYJ = float.Parse(dr["CMTREE_GROUPBYJ"].ToString());
                        float leftGP = float.Parse(dr["CMTREE_GROUPAGP"].ToString());
                        float rightGP = float.Parse(dr["CMTREE_GROUPBGP"].ToString());
                        float leftsales = float.Parse(dr["LEFTSALES"].ToString());
                        float rightsales = float.Parse(dr["RIGHTSALES"].ToString());

                        treeMC.FirstLeftYJ = leftYJ.ToString("###,##0");
                        treeMC.FirstRightYJ = rightYJ.ToString("###,##0");

                        treeMC.FirstLeftBalance = leftGP.ToString("###,##0");
                        treeMC.FirstRightBalance = rightGP.ToString("###,##0");

                        treeMC.FirstLeftSales = leftsales.ToString("###,##0");
                        treeMC.FirstRightSales = rightsales.ToString("###,##0");
                    }
                    else if (nCount == 2)
                    {
                        float leftYJ = float.Parse(dr["CMTREE_GROUPAYJ"].ToString());
                        float rightYJ = float.Parse(dr["CMTREE_GROUPBYJ"].ToString());
                        float leftGP = float.Parse(dr["CMTREE_GROUPAGP"].ToString());
                        float rightGP = float.Parse(dr["CMTREE_GROUPBGP"].ToString());
                        float leftsales = float.Parse(dr["LEFTSALES"].ToString());
                        float rightsales = float.Parse(dr["RIGHTSALES"].ToString());
                        int lefttotaldownline = Convert.ToInt32(dr["CMTREE_TGROUPAMEMBER"].ToString());
                        int righttotaldownline = Convert.ToInt32(dr["CMTREE_TGROUPBMEMBER"].ToString());

                        treeMC.SecondLeftYJ = leftYJ.ToString("###,##0");
                        treeMC.SecondRightYJ = rightYJ.ToString("###,##0");

                        treeMC.SecondLeftBalance = leftGP.ToString("###,##0");
                        treeMC.SecondRightBalance = rightGP.ToString("###,##0");

                        treeMC.SecondLeftSales = leftsales.ToString("###,##0");
                        treeMC.SecondRightSales = rightsales.ToString("###,##0");

                        treeMC.SecondLeftTotalDownline = lefttotaldownline.ToString("###,##0");
                        treeMC.SecondRightTotalDownline = righttotaldownline.ToString("###,##0");
                    }
                }

                nCount++;
            }

            return treeMC;
        }

        public string ConstructTooltipTree(string fullName, string joinedDate, string strLeftYJ, string strRightYJ, string LeftGP, string RightGP, string sponsor, string investStar)
        {
            float fLeftYJ = float.Parse(strLeftYJ);
            float fRightYJ = float.Parse(strRightYJ);
            float fLeftGP = float.Parse(LeftGP);
            float fRightGP = float.Parse(RightGP);
            string leftSales = Resources.OneForAll.OneForAll.lblLeftSale;
            string rightSales = Resources.OneForAll.OneForAll.lblRightSale;
            string sponsorLabel = Resources.OneForAll.OneForAll.lblSponsor;
            string tooltip = string.Format("{0}({1})\n{2}\n{3} : {4}\n{5}     :{6}\t\t{7}     ", fullName, joinedDate, sponsorLabel, sponsor, leftSales, strLeftYJ, rightSales, strRightYJ);
            return tooltip;
        }

        public ActionResult GetMarketTreeMostLeft(string Username)
        {

            DataSet dsFollow = MemberDB.GetMarketTreeMostLeft(Username);
            DataRow dr = dsFollow.Tables[0].Rows[0];
            string upMember = dr["CUSR_USERNAME"].ToString();


            return MarketTreeByUsername(upMember);
        }

        public ActionResult GetMarketTreeMostRight(string Username)
        {

            DataSet dsFollow = MemberDB.GetMarketTreeMostRight(Username);
            DataRow dr = dsFollow.Tables[0].Rows[0];
            string upMember = dr["CUSR_USERNAME"].ToString();



            return MarketTreeByUsername(upMember);
        }


        [HttpPost]
        public ActionResult MarketTreeByUsername(string TopMember)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("Adminlogin", "Admin", new {  });
            }
            
            if (TopMember == "Main")
            {
                TopMember ="BVA";
            }

            try
            {
                MarketTreeModel memberMarketTree = GetAllMarketTreeMember(TopMember);



                int ok = 0;
                string msg = "";
                var member = MemberDB.GetMemberByUsername(TopMember, out ok, out msg);
                memberMarketTree.country = member.Tables[0].Rows[0]["CCOUNTRY_ID"].ToString();
                var Stockist = MemberDB.GetMemberByUsername("BVA", out ok, out msg);
                memberMarketTree.Stockist = Stockist.Tables[0].Rows[0]["CMEM_MEMBER_TYPE"].ToString();
                if (Session["LanguageChosen"].ToString().Equals("zh-cn", StringComparison.InvariantCultureIgnoreCase))
                {
                    memberMarketTree.Picture = "RegisterEn.png";
                }
                else if (Session["LanguageChosen"].ToString().Equals("en-US", StringComparison.InvariantCultureIgnoreCase))
                {
                    memberMarketTree.Picture = "RegisterEn.png";
                }
                else if (Session["LanguageChosen"].ToString().Equals("zh-tw", StringComparison.InvariantCultureIgnoreCase))
                {
                    memberMarketTree.Picture = "RegisterEn.png";
                }
                else if (Session["LanguageChosen"].ToString().Equals("zh-JP", StringComparison.InvariantCultureIgnoreCase))
                {
                    memberMarketTree.Picture = "RegisterEn.png";
                }


                DataSet logo = AdminGeneralDB.GetRankIcon();
                foreach (DataRow dr2 in logo.Tables[0].Rows)
                {
                    var IFile = new ImageFile();
                    IFile.FileName = dr2["CRANKSET_ICON"].ToString();
                    RankModel RankList = new RankModel();
                    RankList.RankIconPath = dr2["CRANKSET_ICON"].ToString();
                    //RankList.RankIconName = dr2["AMOUNT"].ToString();
                    //RankList.RankAmount = float.Parse(dr2["AMOUNT"].ToString());
                    RankList.RankName = dr2["CRANKSET_NAME"].ToString();
                    ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

                    memberMarketTree.RankList.Add(RankList);
                }

                


                return PartialView("SpecialRegisterMarketTree", memberMarketTree);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult FindMemberTree(string Username)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("Adminlogin", "Admin", new {  });
                }


                MarketTreeModel memberMarketTree;


                    memberMarketTree = GetAllMarketTreeMember(Username);

                    int ok = 0;
                    string msg = "";
                    var member = MemberDB.GetMemberByUsername(Username, out ok, out msg);
                    memberMarketTree.country = member.Tables[0].Rows[0]["CCOUNTRY_ID"].ToString();
                    var Stockist = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
                    memberMarketTree.Stockist = Stockist.Tables[0].Rows[0]["CMEM_MEMBER_TYPE"].ToString();
                    if (Session["LanguageChosen"].ToString().Equals("zh-cn", StringComparison.InvariantCultureIgnoreCase))
                    {
                        memberMarketTree.Picture = "RegisterEn.png";
                    }
                    else if (Session["LanguageChosen"].ToString().Equals("en-US", StringComparison.InvariantCultureIgnoreCase))
                    {
                        memberMarketTree.Picture = "RegisterEn.png";
                    }
                    else if (Session["LanguageChosen"].ToString().Equals("zh-tw", StringComparison.InvariantCultureIgnoreCase))
                    {
                        memberMarketTree.Picture = "RegisterEn.png";
                    }
                    else if (Session["LanguageChosen"].ToString().Equals("zh-JP", StringComparison.InvariantCultureIgnoreCase))
                    {
                        memberMarketTree.Picture = "RegisterEn.png";
                    }

                    DataSet logo = AdminGeneralDB.GetRankIcon();
                    foreach (DataRow dr2 in logo.Tables[0].Rows)
                    {
                        var IFile = new ImageFile();
                        IFile.FileName = dr2["CRANKSET_ICON"].ToString();
                        RankModel RankList = new RankModel();
                        RankList.RankIconPath = dr2["CRANKSET_ICON"].ToString();
                        //RankList.RankIconName = dr2["AMOUNT"].ToString();
                        RankList.RankAmount = float.Parse(dr2["AMOUNT"].ToString());
                        ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

                        memberMarketTree.RankList.Add(RankList);
                    }

                

                return PartialView("SpecialRegisterMarketTree", memberMarketTree);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GoUpOneMember(string Username)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("Adminlogin", "Admin", new {  });
                }

                DataSet dsFollow = MemberDB.GetMarketTreeByUsername(Username);
                DataRow dr = dsFollow.Tables[0].Rows[0];
                string upMember = dr["CMTREE_UPMEMBER"].ToString();

                if (upMember == "0" )
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgTopMost);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return MarketTreeByUsername(upMember);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }




        //public ActionResult MemberMarketTree(string TopMember = "")
        //{
        //    if (Session["Admin"] == null || Session["Admin"].ToString() == "")
        //    {
        //        return RedirectToAction("AdminLogin", "Admin", new {  });
        //    }


        //    int ok = 0;
        //    string msg = "";



        //    var member = MemberDB.GetMemberByUsername("BVA", out ok, out msg);

        //    if (TopMember == "")
        //    {
        //        TopMember = member.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
        //    }


        //    MarketTreeModel memberMarketTree = GetAllMarketTreeMember(TopMember);

        //    memberMarketTree.country = member.Tables[0].Rows[0]["CCOUNTRY_ID"].ToString();

        //    var Stockist = MemberDB.GetMemberByUsername(TopMember, out ok, out msg);
        //    memberMarketTree.Stockist = Stockist.Tables[0].Rows[0]["CMEM_MEMBER_TYPE"].ToString();


        //    DataSet logo = AdminGeneralDB.GetRankIcon();
        //    foreach (DataRow dr in logo.Tables[0].Rows)
        //    {
        //        var IFile = new ImageFile();
        //        IFile.FileName = dr["CRANKSET_ICON"].ToString();
        //        RankModel RankList = new RankModel();
        //        RankList.RankIconPath = dr["CRANKSET_ICON"].ToString();
        //        //RankList.RankIconName = float.Parse(dr["AMOUNT"].ToString());
        //        RankList.RankName = dr["CRANKSET_NAME"].ToString();
        //        ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

        //        memberMarketTree.RankList.Add(RankList);
        //    }


        //    string keke = Session["LanguageChosen"].ToString();
        //    if (Session["LanguageChosen"].ToString().Equals("zh-cn", StringComparison.InvariantCultureIgnoreCase))
        //    {
        //        memberMarketTree.Picture = "RegisterEn.png";
        //    }
        //    else if (Session["LanguageChosen"].ToString().Equals("en-US", StringComparison.InvariantCultureIgnoreCase))
        //    {
        //        memberMarketTree.Picture = "RegisterEn.png";
        //    }
        //    else if (Session["LanguageChosen"].ToString().Equals("zh-tw", StringComparison.InvariantCultureIgnoreCase))
        //    {
        //        memberMarketTree.Picture = "RegisterEn.png";
        //    }
        //    else if (Session["LanguageChosen"].ToString().Equals("ja-JP", StringComparison.InvariantCultureIgnoreCase))
        //    {
        //        memberMarketTree.Picture = "RegisterEn.png";
        //    }

        //    return PartialView("SpecialRegisterMarketTree", memberMarketTree);
        //}

        //private MarketTreeModel GetAllMarketTreeMember(string TopMember)
        //{
        //    MarketTreeModel treeMC = new MarketTreeModel();

        //    int nCount = 0;
        //    string fullName = "";

        //    DataSet dsFollow = MemberDB.GetFollowID(TopMember);

        //    //Loop for 15 times
        //    while (nCount <= 14)
        //    {
        //        if (dsFollow.Tables[nCount].Rows.Count == 0)
        //        {
        //            treeMC.MemberList.Add("0");
        //            treeMC.LogoList.Add("~");
        //            treeMC.TooltipList.Add(ConstructTooltipTree("", "", "0", "0", "0", "0", "", ""));
        //            treeMC.PackageList.Add("");
        //            treeMC.DateList.Add("");

        //            if (nCount == 0)
        //            {
        //                treeMC.MainLeftYJ = "0";
        //                treeMC.MainLeftBalance = "0";
        //                treeMC.MainRightYJ = "0";
        //                treeMC.MainRightBalance = "0";
        //            }
        //        }
        //        else
        //        {
        //            DataRow dr = dsFollow.Tables[nCount].Rows[0];
        //            DateTime joinedDate = DateTime.Parse(dr["CUSR_CREATEDON"].ToString());
        //            fullName = string.Format("{0} {1}", dr["CUSR_FULLNAME"].ToString(), dr["CUSR_FULLNAME"].ToString());
        //            treeMC.MemberList.Add(dr["CUSR_USERNAME"].ToString());

        //            int ok = 0;
        //            string msg = "";
        //            string imagePath = "";
        //            var member = MemberDB.GetMemberByUsername(dr["CUSR_USERNAME"].ToString(), out ok, out msg);


        //            //imagePath = Misc.GetCodeNameImageInt(dr["CRANKSET_NAME"].ToString());
        //            imagePath = member.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

        //            treeMC.LogoList.Add(imagePath);
        //            treeMC.TooltipList.Add(ConstructTooltipTree(fullName, joinedDate.ToShortDateString(), dr["CMTREE_GROUPAYJ"].ToString(), dr["CMTREE_GROUPBYJ"].ToString(), dr["CMTREE_GROUPAGP"].ToString(), dr["CMTREE_GROUPBGP"].ToString(), dr["CMEM_INTRO"].ToString(), string.Format("人民币{0}", dr["CPKG_AMOUNT"].ToString())));
        //            treeMC.PackageList.Add(string.Format("{0}", Helper.NVL(dr["CUSRINFO_NICKNAME"])));
        //            treeMC.DateList.Add(string.Format("{0}", joinedDate.ToString("dd/MM/yyyy")));

        //            if (nCount == 0)
        //            {
        //                float leftYJ = float.Parse(dr["CMTREE_GROUPAYJ"].ToString());
        //                float rightYJ = float.Parse(dr["CMTREE_GROUPBYJ"].ToString());
        //                float leftGP = float.Parse(dr["CMTREE_GROUPAGP"].ToString());
        //                float rightGP = float.Parse(dr["CMTREE_GROUPBGP"].ToString());

        //                treeMC.MainLeftYJ = leftYJ.ToString("###,##0");
        //                treeMC.MainRightYJ = rightYJ.ToString("###,##0");

        //                treeMC.MainLeftBalance = leftGP.ToString("###,##0");
        //                treeMC.MainRightBalance = rightGP.ToString("###,##0");
        //            }
        //            else if (nCount == 1)
        //            {
        //                float leftYJ = float.Parse(dr["CMTREE_GROUPAYJ"].ToString());
        //                float rightYJ = float.Parse(dr["CMTREE_GROUPBYJ"].ToString());
        //                float leftGP = float.Parse(dr["CMTREE_GROUPAGP"].ToString());
        //                float rightGP = float.Parse(dr["CMTREE_GROUPBGP"].ToString());

        //                treeMC.FirstLeftYJ = leftYJ.ToString("###,##0");
        //                treeMC.FirstRightYJ = rightYJ.ToString("###,##0");

        //                treeMC.FirstLeftBalance = leftGP.ToString("###,##0");
        //                treeMC.FirstRightBalance = rightGP.ToString("###,##0");
        //            }
        //            else if (nCount == 2)
        //            {
        //                float leftYJ = float.Parse(dr["CMTREE_GROUPAYJ"].ToString());
        //                float rightYJ = float.Parse(dr["CMTREE_GROUPBYJ"].ToString());
        //                float leftGP = float.Parse(dr["CMTREE_GROUPAGP"].ToString());
        //                float rightGP = float.Parse(dr["CMTREE_GROUPBGP"].ToString());

        //                treeMC.SecondLeftYJ = leftYJ.ToString("###,##0");
        //                treeMC.SecondRightYJ = rightYJ.ToString("###,##0");

        //                treeMC.SecondLeftBalance = leftGP.ToString("###,##0");
        //                treeMC.SecondRightBalance = rightGP.ToString("###,##0");
        //            }
        //        }

        //        nCount++;
        //    }

        //    return treeMC;
        //}

        //public string ConstructTooltipTree(string fullName, string joinedDate, string strLeftYJ, string strRightYJ, string LeftGP, string RightGP, string sponsor, string investStar)
        //{
        //    float fLeftYJ = float.Parse(strLeftYJ);
        //    float fRightYJ = float.Parse(strRightYJ);
        //    float fLeftGP = float.Parse(LeftGP);
        //    float fRightGP = float.Parse(RightGP);
        //    string leftSales = Resources.OneForAll.OneForAll.lblLeftSale;
        //    string rightSales = Resources.OneForAll.OneForAll.lblRightSale;
        //    string sponsorLabel = Resources.OneForAll.OneForAll.lblSponsor;
        //    string tooltip = string.Format("{0}({1})\n{2}\n{3} : {4}\n{5}     :{6}\t\t{7}     ", fullName, joinedDate, sponsorLabel, sponsor, leftSales, strLeftYJ, rightSales, strRightYJ);
        //    return tooltip;
        //}

        //public ActionResult GetMarketTreeMostLeft(string Username)
        //{
        //    DataSet dsFollow = MemberDB.GetMarketTreeMostLeft(Username);
        //    DataRow dr = dsFollow.Tables[0].Rows[0];
        //    string upMember = dr["CUSR_USERNAME"].ToString();


        //    return MarketTreeByUsername(upMember);
        //}

        //public ActionResult GetMarketTreeMostRight(string Username)
        //{
        //    DataSet dsFollow = MemberDB.GetMarketTreeMostRight(Username);
        //    DataRow dr = dsFollow.Tables[0].Rows[0];
        //    string upMember = dr["CUSR_USERNAME"].ToString();



        //    return MarketTreeByUsername(upMember);
        //}

        //[HttpPost]
        //public ActionResult MarketTreeByUsername(string TopMember)
        //{
        //    if (Session["Admin"] == null || Session["Admin"].ToString() == "")
        //    {
        //        return RedirectToAction("AdminLogin", "Admin", new {  });
        //    }

        //    if (TopMember == "Main")
        //    {
        //        TopMember = "BVA";
        //    }

        //    try
        //    {
        //        MarketTreeModel memberMarketTree = GetAllMarketTreeMember(TopMember);



        //        int ok = 0;
        //        string msg = "";
        //        var member = MemberDB.GetMemberByUsername(TopMember, out ok, out msg);
        //        memberMarketTree.country = member.Tables[0].Rows[0]["CCOUNTRY_ID"].ToString();
        //        var Stockist = MemberDB.GetMemberByUsername("BVA", out ok, out msg);
        //        memberMarketTree.Stockist = Stockist.Tables[0].Rows[0]["CMEM_MEMBER_TYPE"].ToString();
        //        if (Session["LanguageChosen"].ToString().Equals("zh-cn", StringComparison.InvariantCultureIgnoreCase))
        //        {
        //            memberMarketTree.Picture = "RegisterEn.png";
        //        }
        //        else if (Session["LanguageChosen"].ToString().Equals("en-US", StringComparison.InvariantCultureIgnoreCase))
        //        {
        //            memberMarketTree.Picture = "RegisterEn.png";
        //        }
        //        else if (Session["LanguageChosen"].ToString().Equals("zh-tw", StringComparison.InvariantCultureIgnoreCase))
        //        {
        //            memberMarketTree.Picture = "RegisterEn.png";
        //        }
        //        else if (Session["LanguageChosen"].ToString().Equals("zh-JP", StringComparison.InvariantCultureIgnoreCase))
        //        {
        //            memberMarketTree.Picture = "RegisterEn.png";
        //        }


        //        DataSet logo = AdminGeneralDB.GetRankIcon();
        //        foreach (DataRow dr2 in logo.Tables[0].Rows)
        //        {
        //            var IFile = new ImageFile();
        //            IFile.FileName = dr2["CRANKSET_ICON"].ToString();
        //            RankModel RankList = new RankModel();
        //            RankList.RankIconPath = dr2["CRANKSET_ICON"].ToString();
        //            //RankList.RankIconName = dr2["AMOUNT"].ToString();
        //            RankList.RankName = dr2["CRANKSET_NAME"].ToString();
        //            ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

        //            memberMarketTree.RankList.Add(RankList);
        //        }


        //        return PartialView("SpecialRegisterMarketTree", memberMarketTree);
        //    }
        //    catch (Exception e)
        //    {

        //        Response.Write(e.Message);
        //        return Json(string.Empty, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //public ActionResult FindMemberTree(string Username)
        //{
        //    try
        //    {
        //        if (Session["Admin"] == null || Session["Admin"].ToString() == "")
        //        {
        //            return RedirectToAction("AdminLogin", "Admin", new {  });
        //        }

        //        int isBelongTo;

        //        MemberDB.MemberIsInMarketTreeByUsername(Username, "BVA", out isBelongTo);
        //        MarketTreeModel memberMarketTree;

        //        if (isBelongTo == 0)//not belong to
        //        {
        //            Response.Write(string.Format(Resources.OneForAll.OneForAll.msgNotInTree, Username));
        //            return Json(string.Empty, JsonRequestBehavior.AllowGet);
        //        }
        //        else
        //        {
        //            memberMarketTree = GetAllMarketTreeMember(Username);

        //            int ok = 0;
        //            string msg = "";
        //            var member = MemberDB.GetMemberByUsername(Username, out ok, out msg);
        //            memberMarketTree.country = member.Tables[0].Rows[0]["CCOUNTRY_ID"].ToString();
        //            var Stockist = MemberDB.GetMemberByUsername("BVA", out ok, out msg);
        //            memberMarketTree.Stockist = Stockist.Tables[0].Rows[0]["CMEM_MEMBER_TYPE"].ToString();
        //            if (Session["LanguageChosen"].ToString().Equals("zh-cn", StringComparison.InvariantCultureIgnoreCase))
        //            {
        //                memberMarketTree.Picture = "RegisterEn.png";
        //            }
        //            else if (Session["LanguageChosen"].ToString().Equals("en-US", StringComparison.InvariantCultureIgnoreCase))
        //            {
        //                memberMarketTree.Picture = "RegisterEn.png";
        //            }
        //            else if (Session["LanguageChosen"].ToString().Equals("zh-tw", StringComparison.InvariantCultureIgnoreCase))
        //            {
        //                memberMarketTree.Picture = "RegisterEn.png";
        //            }
        //            else if (Session["LanguageChosen"].ToString().Equals("zh-JP", StringComparison.InvariantCultureIgnoreCase))
        //            {
        //                memberMarketTree.Picture = "RegisterEn.png";
        //            }

        //            DataSet logo = AdminGeneralDB.GetRankIcon();
        //            foreach (DataRow dr2 in logo.Tables[0].Rows)
        //            {
        //                var IFile = new ImageFile();
        //                IFile.FileName = dr2["CRANKSET_ICON"].ToString();
        //                RankModel RankList = new RankModel();
        //                RankList.RankIconPath = dr2["CRANKSET_ICON"].ToString();
        //                //RankList.RankIconName = dr2["AMOUNT"].ToString();
        //                //RankList.RankAmount = float.Parse(dr2["AMOUNT"].ToString());
        //                RankList.RankName = dr2["CRANKSET_NAME"].ToString();
        //                ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

        //                memberMarketTree.RankList.Add(RankList);
        //            }

        //        }

        //        return PartialView("SpecialRegisterMarketTree", memberMarketTree);
        //    }
        //    catch (Exception e)
        //    {

        //        Response.Write(e.Message);
        //        return Json(string.Empty, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //public ActionResult GoUpOneMember(string Username)
        //{
        //    try
        //    {
        //        if (Session["Admin"] == null || Session["Admin"].ToString() == "")
        //        {
        //            return RedirectToAction("AdminLogin", "Admin", new {  });
        //        }

        //        DataSet dsFollow = MemberDB.GetMarketTreeByUsername(Username);
        //        DataRow dr = dsFollow.Tables[0].Rows[0];
        //        string upMember = dr["CMTREE_UPMEMBER"].ToString();

        //        if (upMember == "0" || string.Equals(Username, "BVA", StringComparison.CurrentCultureIgnoreCase))
        //        {
        //            Response.Write(Resources.OneForAll.OneForAll.msgTopMost);
        //            return Json(string.Empty, JsonRequestBehavior.AllowGet);
        //        }

        //        return MarketTreeByUsername(upMember);
        //    }
        //    catch (Exception e)
        //    {

        //        Response.Write(e.Message);
        //        return Json(string.Empty, JsonRequestBehavior.AllowGet);
        //    }
        //}
        #endregion

        #region FirstPage-Ranking
        public ActionResult RegisterNewMemberPage1(string RefferUser = "", string upMember = "", int position = 0, bool startFresh = true, string Rank = "", string MBintro = "", string CountryCode = "")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }


                int ok = 0;
                string msg = "";
                string languageCode = "en-us";
                string Status = "";
                MemberDB.GetTime(out Status);

                if (Status == "1")
                {
                    Response.Write("New Registration is not allow from 00:00:00 to 01:00:00 for bonus calculation");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (startFresh)
                {
                    Session["regMember"] = new RegisterNewMemberModel();
                    ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry = "MY";
                    ((RegisterNewMemberModel)Session["regMember"]).PaymentOption = "1";
                    ((RegisterNewMemberModel)Session["regMember"]).Member.Intro = MBintro;
                    ((RegisterNewMemberModel)Session["regMember"]).Member.RegisterPaymentWallet = 100;
                    ((RegisterNewMemberModel)Session["regMember"]).Member.BonusPaymentWallet = 0;
                    ((RegisterNewMemberModel)Session["regMember"]).Member.CompanyPaymentWallet = 0;
                    if (upMember == "")
                    {
                        ((RegisterNewMemberModel)Session["regMember"]).UsernameID = null;

                    }
                    else
                    {
                        var ds = MemberDB.GetMemberByUsername(upMember, out ok, out msg);
                        DataRow rows = ds.Tables[0].Rows[0];
                        string upuser = rows["CUSR_USERNAME"].ToString();

                        ((RegisterNewMemberModel)Session["regMember"]).UpUsername = upuser;
                        ((RegisterNewMemberModel)Session["regMember"]).UsernameID = upMember;


                    }

                    #region Location
                    SelectListItem loc = new SelectListItem();
                    loc.Selected = (position == 0);
                    loc.Text = Resources.OneForAll.OneForAll.lblLeft;
                    loc.Value = "0";
                    ((RegisterNewMemberModel)Session["regMember"]).Locations.Add(loc);
                    loc = new SelectListItem();
                    loc.Selected = (position == 1);
                    loc.Text = Resources.OneForAll.OneForAll.lblRight;
                    loc.Value = "1";
                    ((RegisterNewMemberModel)Session["regMember"]).Locations.Add(loc);

                    ((RegisterNewMemberModel)Session["regMember"]).SelectedLocation = position.ToString();

                    if (loc.Selected = (position == 0))
                    {
                        ((RegisterNewMemberModel)Session["regMember"]).SelectedLocationString = Resources.OneForAll.OneForAll.lblLeft;
                    }
                    else if (loc.Selected = (position == 1))
                    {
                        ((RegisterNewMemberModel)Session["regMember"]).SelectedLocationString = Resources.OneForAll.OneForAll.lblRight;
                    }

                    #endregion

                    #region Country
                    //combobox for country
                    List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

                    ((RegisterNewMemberModel)Session["regMember"]).Countries = from c in countries
                                                                               select new SelectListItem
                                                                               {
                                                                                   Text = c.CountryName,
                                                                                   Value = c.CountryCode
                                                                               };
                    if (((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode == null)
                    {
                        ((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode = countries[0].MobileCode;
                    }
                    #endregion

                    ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry = CountryCode;
                }
                else if (!startFresh)
                {
                    if (string.IsNullOrEmpty(RefferUser))
                    {
                        Response.Write("Please Key In Referral Username");
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                    var dataset = MemberDB.GetMemberByUsername(RefferUser, out ok, out msg);

                    if (dataset.Tables[0].Rows.Count == 0)
                    {
                        Response.Write("Invalid Referral Username");
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }


                    DataRow row = dataset.Tables[0].Rows[0];
                    string user = row["CUSR_USERNAME"].ToString();


                    ((RegisterNewMemberModel)Session["regMember"]).Member.RegisterWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
                    ((RegisterNewMemberModel)Session["regMember"]).Member.BonusWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_BONUSWALLET"].ToString());
                    ((RegisterNewMemberModel)Session["regMember"]).Member.CompanyWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_COMPANYWALLET"].ToString());
                    ((RegisterNewMemberModel)Session["regMember"]).CurrentUserCountry = dataset.Tables[0].Rows[0]["CCOUNTRY_ID"].ToString();
                    ((RegisterNewMemberModel)Session["regMember"]).ReferralUsername = RefferUser;
                }

                return PartialView("SpecialRegisterNewMemberPage1", ((RegisterNewMemberModel)Session["regMember"]));

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        //Go To Next Page        
        public ActionResult RegisterNewMemberPage1Method(RegisterNewMemberModel mem, string Country = "")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry = Country;

                if (string.IsNullOrEmpty(((RegisterNewMemberModel)Session["regMember"]).ReferralUsername))
                {
                    Response.Write("Please Key In Referral Username");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                return RegisterNewMemberPage2();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region SecondPage-Option&Package
        //Load Page
        public ActionResult RegisterNewMemberPage2(bool fresh = true, string SelectedPackage = "", string Rank = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            string languageCode = languageCode = Session["LanguageChosen"].ToString();

            int ok = 0;
            string msg = "";
            List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

            if (fresh)
            {
                #region Package

                ((RegisterNewMemberModel)Session["regMember"]).LanguageCode = languageCode;
                DataSet dsPack = MemberShopCenterDB.GetPackageAndDescription(languageCode, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);
                ((RegisterNewMemberModel)Session["regMember"]).Packages.Clear();
                foreach (DataRow dr in dsPack.Tables[0].Rows)
                {
                    if (dr["CPKG_TYPE"].ToString() == "R")
                    {
                        if (Convert.ToInt32(dr["CPKG_ACCQTY"].ToString()) >= 3)
                        {

                            PackageModel package = new PackageModel();
                            package.PackageID = int.Parse(dr["CPKG_ID"].ToString());
                            package.PackageCode = dr["CPKG_CODE"].ToString();
                            package.PackageDescription = dr["CMULTILANGPACKAGE_DESCRIPTION"] != null ? dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString() : string.Empty;
                            int extensionIndex = dr["CPRG_IMAGEPATH"].ToString().LastIndexOf('\\');
                            string imageName = dr["CPRG_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
                            package.Image = "http://" + Request.Url.Authority + "/Packages/" + package.PackageCode + "/" + imageName;
                            package.Rp = float.Parse(dr["CPKG_AMOUNT"].ToString()).ToString("n2");
                            package.Delivery = float.Parse(dr["CPKG_BV"].ToString());


                            package.PackageName = dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString();


                            ((RegisterNewMemberModel)Session["regMember"]).Countries = from c in countries
                                                                                       select new SelectListItem
                                                                                       {
                                                                                           Text = c.CountryName,
                                                                                           Value = c.CountryCode
                                                                                       };
                            package.Country = countries.Find(item => item.CountryCode == dr["CCOUNTRY_CODE"].ToString()).CountryName;

                            int extensionIndexs = Misc.GetCountryImageByCountryCode(dr["CCOUNTRY_CODE"].ToString()).LastIndexOf('\\');
                            string imageNameCountry = Misc.GetCountryImageByCountryCode(dr["CCOUNTRY_CODE"].ToString()).Substring(extensionIndexs + 1);
                            package.CountryImage = "http://" + Request.Url.Authority + "/Country/" + dr["CCOUNTRY_CODE"].ToString() + "/" + imageNameCountry;

                            ((RegisterNewMemberModel)Session["regMember"]).Packages.Add(package);
                        }
                    }
                }
                if (dsPack.Tables[0].Rows.Count == 0)
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = false;

                }
                else
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = true;
                }

                if (((RegisterNewMemberModel)Session["regMember"]).Packages.Count == 0)
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = false;
                    //Response.Write(Resources.OneForAll.OneForAllmsgNoPackage);
                    //return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                #endregion
            }
            else
            {
                #region Package
                ((RegisterNewMemberModel)Session["regMember"]).SelectedRank = Rank;
                ((RegisterNewMemberModel)Session["regMember"]).LanguageCode = languageCode;
                DataSet dsPack = MemberShopCenterDB.GetPackageAndDescription(languageCode, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);
                ((RegisterNewMemberModel)Session["regMember"]).Packages.Clear();

                foreach (DataRow dr in dsPack.Tables[0].Rows)
                {
                    if (dr["CPKG_TYPE"].ToString() == "R")
                    {
                        if (Convert.ToInt32(dr["CPKG_ACCQTY"].ToString()) >= 3)
                        {

                            PackageModel package = new PackageModel();
                            package.PackageID = int.Parse(dr["CPKG_ID"].ToString());
                            package.PackageCode = dr["CPKG_CODE"].ToString();
                            package.PackageDescription = dr["CMULTILANGPACKAGE_DESCRIPTION"] != null ? dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString() : string.Empty;
                            int extensionIndex = dr["CPRG_IMAGEPATH"].ToString().LastIndexOf('\\');
                            string imageName = dr["CPRG_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
                            package.Image = "http://" + Request.Url.Authority + "/Packages/" + package.PackageCode + "/" + imageName;
                            package.Rp = float.Parse(dr["CPKG_AMOUNT"].ToString()).ToString("n2");
                            package.Delivery = float.Parse(dr["CPKG_BV"].ToString());



                            package.PackageName = dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString();


                            ((RegisterNewMemberModel)Session["regMember"]).Countries = from c in countries
                                                                                       select new SelectListItem
                                                                                       {
                                                                                           Text = c.CountryName,
                                                                                           Value = c.CountryCode
                                                                                       };
                            package.Country = countries.Find(item => item.CountryCode == dr["CCOUNTRY_CODE"].ToString()).CountryName;
                            int extensionIndexs = Misc.GetCountryImageByCountryCode(dr["CCOUNTRY_CODE"].ToString()).LastIndexOf('\\');
                            string imageNameCountry = Misc.GetCountryImageByCountryCode(dr["CCOUNTRY_CODE"].ToString()).Substring(extensionIndexs + 1);
                            package.CountryImage = "http://" + Request.Url.Authority + "/Country/" + dr["CCOUNTRY_CODE"].ToString() + "/" + imageNameCountry;


                            ((RegisterNewMemberModel)Session["regMember"]).Packages.Add(package);
                        }
                    }
                }
                if (dsPack.Tables[0].Rows.Count == 0)
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = false;

                }
                else
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = true;
                }

                if (((RegisterNewMemberModel)Session["regMember"]).Packages.Count == 0)
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = false;
                    //Response.Write(Resources.OneForAll.OneForAllmsgNoPackage);
                    //return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                ((RegisterNewMemberModel)Session["regMember"]).SelectedPackage = SelectedPackage;
                #endregion
            }


            return PartialView("SpecialRegisterNewMemberPage2", ((RegisterNewMemberModel)Session["regMember"]));
        }

        public ActionResult RegisterNewMemberPage2Method(RegisterNewMemberModel mem)
        {
            ((RegisterNewMemberModel)Session["regMember"]).SelectedPackage = mem.SelectedPackage;
            if (mem.SelectedPackage == null)
            {

                if (Session["LanguageChosen"].ToString() == "en-US")
                {
                    Response.Write("Sorry ! Please kindly Choose a Package");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (Session["LanguageChosen"].ToString() == "zh-CN")
                {
                    Response.Write("温馨提示 : 请选择配套 !");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                int ok = 0;
                string msg = string.Empty;
                float RegisterWallet = 0;

                DataSet dsPack = MemberShopCenterDB.GetPackageAndDescription(Session["LanguageChosen"].ToString(), ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);
                var dataset = MemberDB.GetMemberByUsername(((RegisterNewMemberModel)Session["regMember"]).ReferralUsername, out ok, out msg);

                RegisterWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());

                foreach (DataRow dr in dsPack.Tables[0].Rows)
                {
                    if (dr["CPKG_TYPE"].ToString() == "R")
                    {
                        if (mem.SelectedPackage == dr["CPKG_CODE"].ToString())
                        {
                            if (float.Parse(dr["CPKG_AMOUNT"].ToString()) > RegisterWallet)
                            {

                                Response.Write(Resources.OneForAll.OneForAll.msgRWalletInsufficient);
                                return Json(string.Empty, JsonRequestBehavior.AllowGet);
                            }

                        }

                    }
                }
            }
            return RegisterNewMemberPage3();
        }

        public ActionResult BackToRegisterNewMemberPage1()
        {
            return RegisterNewMemberPage1(((RegisterNewMemberModel)Session["regMember"]).ReferralUsername, ((RegisterNewMemberModel)Session["regMember"]).UsernameID, Convert.ToInt32(((RegisterNewMemberModel)Session["regMember"]).SelectedLocation), false, ((RegisterNewMemberModel)Session["regMember"]).SelectedRank, ((RegisterNewMemberModel)Session["regMember"]).Member.Intro, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);
        }

        #endregion

        #region ThirdPage-Upline&Sponser

        public ActionResult RegisterNewMemberPage3(string upMember = "", int position = 0, bool startFresh = true, string Rank = "", string MBintro = "", string CountryCode = "")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Status == "1")
                {
                    Response.Write("New Registration is not allow from 00:00:00 to 01:00:00 for bonus calculation");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                int ok = 0;
                string msg = "";
                string languageCode = languageCode = Session["LanguageChosen"].ToString();
                var dataset = MemberDB.GetMemberByUsername(((RegisterNewMemberModel)Session["regMember"]).ReferralUsername, out ok, out msg);
                DataRow row = dataset.Tables[0].Rows[0];
                string user = row["CUSR_USERNAME"].ToString();


                if (startFresh)
                {

                }
                else if (!startFresh)
                {

                    int Result = 0;

                    DataSet dsUpline = MemberShopCenterDB.SponsorUplineChecking(((RegisterNewMemberModel)Session["regMember"]).UsernameID, MBintro, out Result);

                    if (Result == 0)
                    {
                        Response.Write(Resources.OneForAll.OneForAll.msginvalidupline);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }


                    ((RegisterNewMemberModel)Session["regMember"]).Member.Intro = MBintro;



                    var CheckRank = MemberDB.GetMemberByUsername(MBintro, out ok, out msg);

                    if (CheckRank.Tables[0].Rows.Count == 0)
                    {
                        Response.Write("No This Sponsor Name, Pleaase Try It Again");
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                    ((RegisterNewMemberModel)Session["regMember"]).Member.IntroFullName = CheckRank.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();



                }

                if (((RegisterNewMemberModel)Session["regMember"]).UsernameID == "")
                {
                    ((RegisterNewMemberModel)Session["regMember"]).UsernameID = null;

                }
                else
                {
                    var ds = MemberDB.GetMemberByUsername(((RegisterNewMemberModel)Session["regMember"]).UsernameID, out ok, out msg);
                    DataRow rows = ds.Tables[0].Rows[0];
                    string upuser = rows["CUSR_USERNAME"].ToString();

                    ((RegisterNewMemberModel)Session["regMember"]).UpUsername = upuser;

                    ((RegisterNewMemberModel)Session["regMember"]).UpLineFullName = rows["CUSR_FULLNAME"].ToString();
                }

                #region Location

                if (((RegisterNewMemberModel)Session["regMember"]).SelectedLocation == "0")
                {
                    ((RegisterNewMemberModel)Session["regMember"]).SelectedLocationString = Resources.OneForAll.OneForAll.lblLeft;
                }
                else if (((RegisterNewMemberModel)Session["regMember"]).SelectedLocation == "1")
                {
                    ((RegisterNewMemberModel)Session["regMember"]).SelectedLocationString = Resources.OneForAll.OneForAll.lblRight;
                }

                #endregion

                return PartialView("SpecialRegisterNewMemberPage3", ((RegisterNewMemberModel)Session["regMember"]));

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        //Btn Next        
        public ActionResult RegisterNewMemberPage3Method(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                int ok = 0;
                string msg = "";


                var dataset = MemberDB.GetMemberByUsername(mem.UsernameID, out ok, out msg);
                DataRow row = dataset.Tables[0].Rows[0];
                string user = row["CUSR_USERNAME"].ToString();


                int Result = 0;

                DataSet Upline = MemberShopCenterDB.SponsorUplineChecking(mem.UsernameID, mem.Member.Intro, out Result);

                if (Result == 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msginvalidupline);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                ((RegisterNewMemberModel)Session["regMember"]).Member.Intro = mem.Member.Intro;


                DataSet das = LoginDB.GetUserByUserName(mem.Member.Intro);
                int tablecount = das.Tables[0].Rows.Count;
                if (tablecount == 0)
                {
                    Response.Write("Invalid Sponser ID: " + mem.Member.Intro);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                DataSet dsUpline = LoginDB.GetUserByUserName(mem.UsernameID);
                string existLeft = dsUpline.Tables[0].Rows[0]["CMTREE_FOLLOWGROUPA"].ToString();
                string existRight = dsUpline.Tables[0].Rows[0]["CMTREE_FOLLOWGROUPB"].ToString();


                if (mem.SelectedLocation == "0")
                {
                    if (existLeft != "0")
                    {
                        Response.Write(Resources.OneForAll.OneForAll.msgLeftOccupied);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    if (existRight != "0")
                    {
                        Response.Write(Resources.OneForAll.OneForAll.msgRightOccupied);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                var ds = MemberDB.GetMemberByUsername(mem.Member.Intro, out ok, out msg);
                DataRow rows = ds.Tables[0].Rows[0];
                string sp = rows["CUSR_USERNAME"].ToString();
                mem.introid = sp;
                ((RegisterNewMemberModel)Session["regMember"]).introid = sp;



                ((RegisterNewMemberModel)Session["regMember"]).UsernameID = mem.UsernameID;

                DataSet dsSponsor = LoginDB.GetUserByUserName(sp);
                if (dsSponsor.Tables[0].Rows.Count == 0)
                {
                    Response.Write("Invalid username: " + mem.Member.Intro);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                ((RegisterNewMemberModel)Session["regMember"]).Member.Intro = mem.Member.Intro;

                ((RegisterNewMemberModel)Session["regMember"]).SelectedLocation = mem.SelectedLocation;

                string languageCode = Session["LanguageChosen"].ToString();

                DataSet packageInfo = AdminGeneralDB.GetPackageByCode(((RegisterNewMemberModel)Session["regMember"]).SelectedPackage, languageCode, out ok, out msg);
                float investment = float.Parse(packageInfo.Tables[0].Rows[0]["CPKG_AMOUNT"].ToString());
                float total = investment;
                ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageName = packageInfo.Tables[0].Rows[0]["CMULTILANGPACKAGE_NAME"].ToString();
                ((RegisterNewMemberModel)Session["regMember"]).TotalRD = float.Parse(packageInfo.Tables[0].Rows[0]["CPKG_AMOUNT"].ToString()).ToString("n2");

                string testing = ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry;

                return RegisterNewMemberPage4();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult BackToRegisterNewMemberPage2()
        {
            bool fresh = false;
            string SelectedPackage = ((RegisterNewMemberModel)Session["regMember"]).SelectedPackage;
            string Rank = ((RegisterNewMemberModel)Session["regMember"]).SelectedRank;
            return RegisterNewMemberPage2(fresh, SelectedPackage, Rank);
        }



        #endregion

        #region FouthPage-PersonalDetail

        //Load Page
        public ActionResult RegisterNewMemberPage4()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }


            try
            {
                int ok = 0;
                string msg = "";
                string languageCode = Session["LanguageChosen"].ToString();

                #region Country
                List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

                ((RegisterNewMemberModel)Session["regMember"]).Countries = from c in countries
                                                                           select new SelectListItem
                                                                           {
                                                                               Text = c.CountryName,
                                                                               Value = c.CountryCode
                                                                           };

                string selectedcountry = ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry;


                ((RegisterNewMemberModel)Session["regMember"]).SelectedCountryString = countries.Find(item => item.CountryCode == selectedcountry).CountryName;
                DataSet dsCountrySetup = AdminGeneralDB.GetCountryByCountryCode(selectedcountry, out ok, out msg);
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode = dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_MOBILECODE"].ToString();



                #endregion




                return PartialView("SpecialRegisterNewMemberPage4", ((RegisterNewMemberModel)Session["regMember"]));
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        //Btn Next
        public ActionResult RegisterNewMemberPage4Method(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }


                if (mem.LoginUsername == null)
                {
                    Response.Write("The Username Require Minimun 6 Alphanumeric");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (mem.LoginUsername == "")
                {
                    Response.Write("The Username Require Minimun 6 Alphanumeric");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.LoginUsername.Length < 6)
                {
                    Response.Write("The Username Require Minimun 6 Alphanumeric");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.LoginUsername.Contains(" "))
                {
                    Response.Write("Sorry, you are not allowed to enter any spaces");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.UserInfo.IC.Count() < 5)
                {
                    Response.Write("Please Keyin IC More Than 5 Alphanumeric");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.UserInfo.IC.Count() == 0)
                {
                    Response.Write("Please Key in IC");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                int ok = 0;
                string msg = "";

                var dr = AdminMemberDB.GetMemberByUserName(mem.LoginUsername, out ok, out msg);

                if (dr.Tables[0].Rows.Count != 0)
                {
                    Response.Write("Sorry, This Member ID Already Exixt ");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                ((RegisterNewMemberModel)Session["regMember"]).LoginUsername = mem.LoginUsername;
                ((RegisterNewMemberModel)Session["regMember"]).Member.FirstName = mem.Member.FirstName;
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.IC = mem.UserInfo.IC;
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.Nickname = mem.Member.FirstName;
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode = mem.UserInfo.MobileCountryCode;
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.CellPhone = mem.UserInfo.CellPhone;
                ((RegisterNewMemberModel)Session["regMember"]).Member.MemberEmail = mem.Member.MemberEmail;

                return RegisterNewMemberPage5();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        //Back Button for Page 3
        public ActionResult BackToRegisterNewMemberPage3(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                ((RegisterNewMemberModel)Session["regMember"]).Member.Username = mem.Member.Username;
                ((RegisterNewMemberModel)Session["regMember"]).Member.FirstName = mem.Member.FirstName;
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.IC = mem.UserInfo.IC;
                ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry = mem.SelectedCountry;
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode = mem.UserInfo.MobileCountryCode;
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.CellPhone = mem.UserInfo.CellPhone;
                ((RegisterNewMemberModel)Session["regMember"]).Member.MemberEmail = mem.Member.MemberEmail;
                //string testing = ((RegisterNewMemberModel)Session["regMember"]).SelectedRank;
                int ok = 0;
                string msg = "";

                var dataset = MemberDB.GetMemberByUsername(((RegisterNewMemberModel)Session["regMember"]).ReferralUsername, out ok, out msg);
                ((RegisterNewMemberModel)Session["regMember"]).Member.BonusWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());




                return PartialView("RegisterNewMember", ((RegisterNewMemberModel)Session["regMember"]));

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region FithPage-Review

        public ActionResult RegisterNewMemberPage5()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            try
            {

                string languageCode = Session["LanguageChosen"].ToString();

                var db = MemberShopCenterDB.CheckPackage(((RegisterNewMemberModel)Session["regMember"]).SelectedPackage);

                string Name = string.Empty;


                ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterPaymentWallet = ((RegisterNewMemberModel)Session["regMember"]).Member.RegisterPaymentWallet.ToString("###,###,###");

                string Quantity = db.Tables[0].Rows[0]["CPKG_ACCQTY"].ToString();

                if (Quantity != "1")
                {
                    int Quan = Convert.ToInt32(Quantity);

                    for (int A = 0; A < Quan; A++)
                    {
                        int B = A + 1;
                        Name += ((RegisterNewMemberModel)Session["regMember"]).LoginUsername + "0" + B + ",";
                        if (A == 3)
                        {
                            Name += System.Environment.NewLine;
                        }
                    }


                }
                else
                {
                    Name = ((RegisterNewMemberModel)Session["regMember"]).LoginUsername;
                }

                ((RegisterNewMemberModel)Session["regMember"]).DisplayName = Name;


                return PartialView("SpecialRegisterNewMemberPage5", ((RegisterNewMemberModel)Session["regMember"]));
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RegisterNewMemberPage5Method(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                int ok = 0;
                string msg = "";

                var dataset = MemberDB.GetMemberByUsername(((RegisterNewMemberModel)Session["regMember"]).ReferralUsername, out ok, out msg);
                DataRow row = dataset.Tables[0].Rows[0];
                string refername = row["CUSR_USERNAME"].ToString();

                ((RegisterNewMemberModel)Session["regMember"]).SponsorSecurityPin = mem.SponsorSecurityPin;

                //store existing value before the value get clear out after registration done, important for send sms                
                string selectedCountry = ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry.ToLower();
                string nickname = ((RegisterNewMemberModel)Session["regMember"]).UserInfo.Nickname;
                string username = ((RegisterNewMemberModel)Session["regMember"]).LoginUsername;

                // some logic to validate phone numbers
                string cellPhone = CellPhoneParser.Parse(selectedCountry, ((RegisterNewMemberModel)Session["regMember"]).UserInfo.CellPhone);

                //Encrypt pin and password
                string ic = ((RegisterNewMemberModel)Session["regMember"]).UserInfo.IC;
                string password = ic.Substring(ic.Length - 6);
                ((RegisterNewMemberModel)Session["regMember"]).Member.EncryptedPassword = Authentication.Encrypt(password);
                ((RegisterNewMemberModel)Session["regMember"]).Member.EncryptedPin = Authentication.Encrypt(password);
                ((RegisterNewMemberModel)Session["regMember"]).EncryptedSponsorSecurityPin = Authentication.Encrypt(((RegisterNewMemberModel)Session["regMember"]).SponsorSecurityPin);
                string invoiceNumber = "";


                MemberShopCenterDB.RegisterMultipleAccount(((RegisterNewMemberModel)Session["regMember"]), Session["Admin"].ToString(), out ok, out msg, out invoiceNumber);

                if (ok == 1) //success
                {

                    if (!string.IsNullOrEmpty(cellPhone))
                    {
                        string PackageCode = ((RegisterNewMemberModel)Session["regMember"]).SelectedPackage;

                        var db = MemberShopCenterDB.CheckPackage(PackageCode);
                        string Name = string.Empty;
                        string Quantity = db.Tables[0].Rows[0]["CPKG_ACCQTY"].ToString();
                        if (Quantity == "3")
                        {
                            Name = ((RegisterNewMemberModel)Session["regMember"]).LoginUsername + "01" + " TO " + ((RegisterNewMemberModel)Session["regMember"]).LoginUsername + "03";
                        }
                        else if (Quantity == "7")
                        {
                            Name = ((RegisterNewMemberModel)Session["regMember"]).LoginUsername + "01" + " TO " + ((RegisterNewMemberModel)Session["regMember"]).LoginUsername + "07";
                        }
                        else
                        {
                            Name = ((RegisterNewMemberModel)Session["regMember"]).LoginUsername;
                        }

                        try
                        {
                            string path = Server.MapPath(@"~/Images/MSGLogo.png"); // my logo is placed in images folder

                            string massege = string.Empty;
                            string Title = string.Empty;


                            Title = "BVA";
                            massege = "<img src=cid:myImageID /><br/><br/>{0}<br/><br/>Congratulations and welcome to BVA !<br/>We will do our best toward all the success, Let us work together to create wealth.<br/><br/>恭喜你已经成功注册成为BVA平台的会员！BVA平台欢迎大家的参与，并致力于大家一起创造财富！迎接辉煌！<br/><br/> 会员账号 Member ID         :{1} <br/>登录密码 Login Password ：{2} <br/><br/>Please login to www.bva.com and change your login password and security PIN to prevent unauthorized access.<br/><br/>请登录 www.azdres.com 会员后台后，更改您的登录密码和安全密码，以防被人盗用。";


                            var contentSMS = string.Format(massege, nickname, Name, password);

                            string result = Misc.SendEmailGood("noreply@BVA.com", ((RegisterNewMemberModel)Session["regMember"]).Member.MemberEmail, Title, contentSMS, path);




                        }
                        catch (Exception)
                        {
                            // failed to send sms is ok, but better dont block the registration flow
                        }
                    }

                    string MsgRsuult = Resources.OneForAll.OneForAll.msgRegMemberSucceed1 + "\n" + username + " " + nickname + "\n" + Resources.OneForAll.OneForAll.msgRegMemberSucceed2;



                    return Json(MsgRsuult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (msg == "-1")
                        Response.Write(Resources.OneForAll.OneForAll.msgMemberExist);
                    else if (msg == "-2")
                        Response.Write(Resources.OneForAll.OneForAll.msgPackageNotExists);
                    else if (msg == "-3")
                        Response.Write(Resources.OneForAll.OneForAll.msgRWalletInsufficient);
                    else if (msg == "-4")
                        Response.Write(Resources.OneForAll.OneForAll.msgIntroNotExist);
                    else if (msg == "-5")
                        Response.Write(Resources.OneForAll.OneForAll.msgPINInvalid);
                    else if (msg == "-6")
                        Response.Write(Resources.OneForAll.OneForAll.msgUpNotExist);
                    else if (msg == "-7")
                        Response.Write(Resources.OneForAll.OneForAll.msgLeftOccupied);
                    else if (msg == "-8")
                        Response.Write(Resources.OneForAll.OneForAll.msgRightOccupied);
                    else if (msg == "-9")
                        Response.Write(Resources.OneForAll.OneForAll.msgPositionIncorrect);

                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        //Btn Back from fourth page
        public ActionResult BackToRegisterNewMemberPage4(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                return RegisterNewMemberPage4();

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #endregion

        #region Wallet Balance

        /// <summary>
        /// Retrieve list of members wallet balance
        /// </summary>
        /// <returns></returns>
        public ActionResult WalletsBalance(int selectedPage = 1, string searchMemberList = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            int status = 0;
            string message = string.Empty;
            int pages = 0;
            PaginationMemberWalletModel model = new PaginationMemberWalletModel();
            DataSet dsMemberWallet = MemberDB.GetAllMemberWalletBalance(searchMemberList, selectedPage, out pages, out status, out message);

            selectedPage = ConstructPageList(selectedPage, pages, model);

            float RegisterWallet = 0;
            float CashWallet = 0;
            float GoldPoint = 0;
            float MultiPoint = 0;
            float RPCWallet = 0;
            float RMPWallet = 0;
            float CompanyWallet = 0;

            foreach (DataRow dr in dsMemberWallet.Tables[0].Rows)
            {
                var memberWallet = new MemberWalletModel();
                memberWallet.Number = dr["rownumber"].ToString();
                memberWallet.Username = dr["CUSR_USERNAME"].ToString();
                memberWallet.Fullname = dr["CUSR_FULLNAME"].ToString();
                memberWallet.Rank = dr["CRANKSET_NAME"].ToString();
                memberWallet.RegisterWallet = float.Parse(dr["CMEM_REGISTERWALLET"].ToString());
                memberWallet.CashWallet = float.Parse(dr["CMEM_CASHWALLET"].ToString());
                memberWallet.GoldPoint = float.Parse(dr["CMEM_GOLDPOINT"].ToString());
                memberWallet.MultiPoint = float.Parse(dr["CMEM_MULTIPOINT"].ToString());
                memberWallet.RPCWallet = float.Parse(dr["CMEM_REPURCHASEWALLET"].ToString());
                memberWallet.ICOWallet = float.Parse(dr["CMEM_ICOWALLET"].ToString());
                memberWallet.RMPWallet = float.Parse(dr["CMEM_RMPWALLET"].ToString());
                memberWallet.CompanyWallet = float.Parse(dr["CMEM_COMPANYWALLET"].ToString());
                memberWallet.Total = float.Parse(dr["TOTAL"].ToString());

                CompanyWallet = CompanyWallet + float.Parse(dr["CMEM_COMPANYWALLET"].ToString());
                RegisterWallet = RegisterWallet + float.Parse(dr["CMEM_REGISTERWALLET"].ToString());
                CashWallet = CashWallet + float.Parse(dr["CMEM_CASHWALLET"].ToString());
                GoldPoint = GoldPoint + float.Parse(dr["CMEM_GOLDPOINT"].ToString());
                MultiPoint = MultiPoint + float.Parse(dr["CMEM_MULTIPOINT"].ToString());
                RPCWallet = RPCWallet + float.Parse(dr["CMEM_REPURCHASEWALLET"].ToString());
                RMPWallet = RMPWallet + float.Parse(dr["CMEM_RMPWALLET"].ToString());

                model.MemberWalletList.Add(memberWallet);
            }

            model.FilteringCriteria = Misc.ConstructsFilteringCriteria().ToList();
            model.Months = Misc.ConstructsMonth().ToList();
            model.Years = Misc.ConstructYears(2013).ToList();
            model.SelectedYears = DateTime.Now.Year.ToString();
            model.SelectedMonth = DateTime.Now.Month.ToString();

            model.TotalRegisterWallet = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALREGISTER"].ToString()).ToString("n2");
            model.TotalCashWallet = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALCASH"].ToString()).ToString("n2");
            model.TotalGoldPoint = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALGOLDPOINT"].ToString()).ToString("n2");
            model.TotalMultiPoint = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALMULTIPOINT"].ToString()).ToString("n2");
            model.TotalRPCWallet = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALREPURCHASE"].ToString()).ToString("n2");
            model.TotalRMPWallet = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALRMP"].ToString()).ToString("n2");
            model.TotalCompanyWallet = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALCRP"].ToString()).ToString("n2");


            model.RegisterWallet = RegisterWallet.ToString("n2");
            model.CompanyWallet = CompanyWallet.ToString("n2");
            model.CashWallet = CashWallet.ToString("n2");
            model.GoldPoint = GoldPoint.ToString("n2");
            model.MultiPoint = MultiPoint.ToString("n2");
            model.RPCWallet = RPCWallet.ToString("n2");
            model.RMPWallet = RMPWallet.ToString("n2");
            model.TotalTotal = (float.Parse(model.CompanyWallet) + float.Parse(model.RegisterWallet) + float.Parse(model.CashWallet) + float.Parse(model.GoldPoint) + float.Parse(model.MultiPoint) + float.Parse(model.RPCWallet) + float.Parse(model.RMPWallet)).ToString("n2");

            return PartialView("WalletsBalance", model);
        }

        /// <summary>
        /// Next page of member wallet balance list
        /// </summary>
        /// <param name="selectedPage"></param>
        /// <param name="searchMember"></param>
        /// <returns></returns>
        public ActionResult NextPageMemberWalletBalanceList(string selectedPage, string searchMember)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                return RedirectToAction("WalletsBalance", "AdminMember", new { selectedPage = selectedPage, searchMemberList = searchMember });
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Retrieve list of members wallet balance by joined date
        /// </summary>
        /// <returns></returns>
        public ActionResult SearchMemberWalletBalanceListByJoinedDate(string month, string year, int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            if(month=="Months")
            {
                month = "";
            }

            if (year == "Years")
            {
                year = "";
            }

            int status = 0;
            string message = string.Empty;
            int pages = 0;
            PaginationMemberWalletModel model = new PaginationMemberWalletModel();
            DataSet dsMemberWallet = MemberDB.GetAllMemberWalletBalanceByJoinedDate(month, year, selectedPage, out pages, out status, out message);

            float RegisterWallet = 0;
            float CashWallet = 0;
            float GoldPoint = 0;
            float MultiPoint = 0;
            float RPCWallet = 0;
            float RMPWallet = 0;
            float CompanyWallet = 0;

            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsMemberWallet.Tables[0].Rows)
            {
                var memberWallet = new MemberWalletModel();
                memberWallet.Number = dr["rownumber"].ToString();
                memberWallet.Username = dr["CUSR_USERNAME"].ToString();
                memberWallet.Fullname = dr["CUSR_FULLNAME"].ToString();
                memberWallet.Rank = dr["CRANKSET_NAME"].ToString();
                memberWallet.RegisterWallet = float.Parse(dr["CMEM_REGISTERWALLET"].ToString());
                memberWallet.CashWallet = float.Parse(dr["CMEM_CASHWALLET"].ToString());
                memberWallet.GoldPoint = float.Parse(dr["CMEM_GOLDPOINT"].ToString());
                memberWallet.MultiPoint = float.Parse(dr["CMEM_MULTIPOINT"].ToString());
                memberWallet.RPCWallet = float.Parse(dr["CMEM_REPURCHASEWALLET"].ToString());
                memberWallet.RMPWallet = float.Parse(dr["CMEM_RMPWALLET"].ToString());
                memberWallet.CompanyWallet = float.Parse(dr["CMEM_COMPANYWALLET"].ToString());
                memberWallet.Total = float.Parse(dr["TOTAL"].ToString());

                RegisterWallet = RegisterWallet + float.Parse(dr["CMEM_REGISTERWALLET"].ToString());
                CashWallet = CashWallet + float.Parse(dr["CMEM_CASHWALLET"].ToString());
                GoldPoint = GoldPoint + float.Parse(dr["CMEM_GOLDPOINT"].ToString());
                MultiPoint = MultiPoint + float.Parse(dr["CMEM_MULTIPOINT"].ToString());
                RPCWallet = RPCWallet + float.Parse(dr["CMEM_REPURCHASEWALLET"].ToString());
                RMPWallet = RMPWallet + float.Parse(dr["CMEM_RMPWALLET"].ToString());
                CompanyWallet = CompanyWallet + float.Parse(dr["CMEM_COMPANYWALLET"].ToString());

                model.MemberWalletList.Add(memberWallet);
            }

            model.FilteringCriteria = Misc.ConstructsFilteringCriteria().ToList();
            model.Months = Misc.ConstructsMonth().ToList();
            model.Years = Misc.ConstructYears(2013).ToList();
            model.SelectedYears = year;
            model.SelectedMonth = month;

            model.TotalRegisterWallet = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALREGISTER"].ToString()).ToString("n2");
            model.TotalCashWallet = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALCASH"].ToString()).ToString("n2");
            model.TotalGoldPoint = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALGOLDPOINT"].ToString()).ToString("n2");
            model.TotalMultiPoint = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALMULTIPOINT"].ToString()).ToString("n2");
            model.TotalRPCWallet = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALREPURCHASE"].ToString()).ToString("n2");
            model.TotalRMPWallet = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALRMP"].ToString()).ToString("n2");
            model.TotalCompanyWallet = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALCRP"].ToString()).ToString("n2");

            model.RegisterWallet = RegisterWallet.ToString("n2");
            model.CompanyWallet = CompanyWallet.ToString("n2");
            model.CashWallet = CashWallet.ToString("n2");
            model.GoldPoint = GoldPoint.ToString("n2");
            model.MultiPoint = MultiPoint.ToString("n2");
            model.RPCWallet = RPCWallet.ToString("n2");
            model.RMPWallet = RMPWallet.ToString("n2");
            model.TotalTotal = (float.Parse(model.CompanyWallet) + float.Parse(model.RegisterWallet) + float.Parse(model.CashWallet) + float.Parse(model.GoldPoint) + float.Parse(model.MultiPoint) + float.Parse(model.RPCWallet) + float.Parse(model.RMPWallet)).ToString("n2");

            return PartialView("WalletsBalance", model);
        }

        /// <summary>
        /// Retrieve list of members wallet balance by filtering criteria
        /// </summary>
        /// <returns></returns>
        public ActionResult SearchMemberWalletBalanceListByFC(string fc, string searchFC, int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            int status = 0;
            string message = string.Empty;
            int pages = 0;
            PaginationMemberWalletModel model = new PaginationMemberWalletModel();

            DataSet dsMemberWallet = new DataSet();
            if (fc == "0")
            {
                dsMemberWallet = MemberDB.GetAllMemberWalletBalanceByFilteringCriteria(fc, searchFC, selectedPage, out pages, out status, out message);
            }
            else if (fc == "USERNAME")
            {
                dsMemberWallet = MemberDB.GetAllMemberWalletBalanceByFilteringCriteria(fc, searchFC, selectedPage, out pages, out status, out message);
            }
            else if (fc == "FULLNAME")
            {
                dsMemberWallet = MemberDB.GetAllMemberWalletBalanceByFilteringCriteria(fc, searchFC, selectedPage, out pages, out status, out message);
            }
            else if (fc == "RANKING")
            {
                dsMemberWallet = MemberDB.GetAllMemberWalletBalanceByRanking(fc, searchFC, selectedPage, out pages, out status, out message);
            }
            else if (fc == "PACKAGE")
            {
                dsMemberWallet = MemberDB.GetAllMemberWalletBalanceByPackage(fc, searchFC, selectedPage, out pages, out status, out message);
            }
            else if (fc == "SPONSOR")
            {
                dsMemberWallet = MemberDB.GetAllMemberWalletBalanceBySponsor(fc, searchFC, selectedPage, out pages, out status, out message);
            }
            else if (fc == "COUNTRY")
            {
                dsMemberWallet = MemberDB.GetAllMemberWalletBalanceByCountry(fc, searchFC, selectedPage, out pages, out status, out message);
            }
            float RegisterWallet = 0;
            float CashWallet = 0;
            float GoldPoint = 0;
            float MultiPoint = 0;
            float RPCWallet = 0;
            float RMPWallet = 0;
            float CompanyWallet = 0;

            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsMemberWallet.Tables[0].Rows)
            {
                var memberWallet = new MemberWalletModel();
                memberWallet.Number = dr["rownumber"].ToString();
                memberWallet.Username = dr["CUSR_USERNAME"].ToString();
                memberWallet.Fullname = dr["FULLNAME"].ToString();
                memberWallet.Rank = dr["CRANKSET_NAME"].ToString();
                memberWallet.RegisterWallet = float.Parse(dr["CMEM_REGISTERWALLET"].ToString());
                memberWallet.CashWallet = float.Parse(dr["CMEM_CASHWALLET"].ToString());
                memberWallet.GoldPoint = float.Parse(dr["CMEM_GOLDPOINT"].ToString());
                memberWallet.MultiPoint = float.Parse(dr["CMEM_MULTIPOINT"].ToString());
                memberWallet.RPCWallet = float.Parse(dr["CMEM_REPURCHASEWALLET"].ToString());
                memberWallet.RMPWallet = float.Parse(dr["CMEM_RMPWALLET"].ToString());
                memberWallet.CompanyWallet = float.Parse(dr["CMEM_COMPANYWALLET"].ToString());
                memberWallet.Total = float.Parse(dr["TOTAL"].ToString());

                RegisterWallet = RegisterWallet + float.Parse(dr["CMEM_REGISTERWALLET"].ToString());
                CashWallet = CashWallet + float.Parse(dr["CMEM_CASHWALLET"].ToString());
                GoldPoint = GoldPoint + float.Parse(dr["CMEM_GOLDPOINT"].ToString());
                MultiPoint = MultiPoint + float.Parse(dr["CMEM_MULTIPOINT"].ToString());
                RPCWallet = RPCWallet + float.Parse(dr["CMEM_REPURCHASEWALLET"].ToString());
                RMPWallet = RMPWallet + float.Parse(dr["CMEM_RMPWALLET"].ToString());
                CompanyWallet = CompanyWallet + float.Parse(dr["CMEM_COMPANYWALLET"].ToString());

                model.MemberWalletList.Add(memberWallet);
            }

            model.FilteringCriteria = Misc.ConstructsFilteringCriteria().ToList();
            model.Months = Misc.ConstructsMonth().ToList();
            model.Years = Misc.ConstructYears(2017).ToList();
            model.SelectedYears = DateTime.Now.Year.ToString();
            model.SelectedMonth = DateTime.Now.Month.ToString();

            model.TotalRegisterWallet = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALREGISTER"].ToString()).ToString("n2");
            model.TotalCashWallet = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALCASH"].ToString()).ToString("n2");
            model.TotalGoldPoint = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALGOLDPOINT"].ToString()).ToString("n2");
            model.TotalMultiPoint = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALMULTIPOINT"].ToString()).ToString("n2");
            model.TotalRPCWallet = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALREPURCHASE"].ToString()).ToString("n2");
            model.TotalRMPWallet = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALRMP"].ToString()).ToString("n2");
            model.TotalCompanyWallet = float.Parse(dsMemberWallet.Tables[1].Rows[0]["TOTALCRP"].ToString()).ToString("n2");

            model.RegisterWallet = RegisterWallet.ToString("n2");
            model.CashWallet = CashWallet.ToString("n2");
            model.CompanyWallet = CompanyWallet.ToString("n2");
            model.GoldPoint = GoldPoint.ToString("n2");
            model.MultiPoint = MultiPoint.ToString("n2");
            model.RPCWallet = RPCWallet.ToString("n2");
            model.RMPWallet = RMPWallet.ToString("n2");
            model.TotalTotal = (float.Parse(model.CompanyWallet) + float.Parse(model.RegisterWallet) + float.Parse(model.CashWallet) + float.Parse(model.GoldPoint) + float.Parse(model.MultiPoint) + float.Parse(model.RPCWallet) + float.Parse(model.RMPWallet)).ToString("n2");

            return PartialView("WalletsBalance", model);
        }

        #endregion

        #region Free Registration
        public ActionResult FreeRegistration()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            RegisterNewMemberModel memberMC = new RegisterNewMemberModel();

            #region Make Member ID
            //string userID = "";
            //MemberDB.MakeMemberID(out userID);
            //memberMC.Member.Username = userID;
            #endregion

            memberMC.UpUsername = null;

            #region Location
            SelectListItem loc = new SelectListItem();
            loc.Text = Resources.OneForAll.OneForAll.lblLeft;
            loc.Value = "0";
            memberMC.Locations.Add(loc);
            loc = new SelectListItem();
            loc.Text = Resources.OneForAll.OneForAll.lblRight;
            loc.Value = "1";
            memberMC.Locations.Add(loc);
            #endregion

            try
            {
                int ok = 0;
                string msg = "";
                var message = string.Empty;

                #region Package
                string languageCode = Session["LanguageChosen"].ToString();
                memberMC.LanguageCode = languageCode;

                memberMC.SelectedPackage = "FREE001";
                #endregion

                #region Sales Delivery Mode
                int pages = 0;
                DataSet dsdeliveryMode = AdminGeneralDB.GetAllSalesDeliveryMode(1, out pages);
                List<SalesDeliveryModel> deliList = new List<SalesDeliveryModel>();

                foreach (DataRow dr in dsdeliveryMode.Tables[0].Rows)
                {
                    var salesModel = new SalesDeliveryModel();
                    salesModel.SalesDeliveryID = Convert.ToInt32(dr["CDELIMD_ID"]);
                    salesModel.DeliveryMethod = dr["CDELIMD_METHOD"].ToString();
                    deliList.Add(salesModel);
                }

                memberMC.DeliveryMode = from c in deliList
                                        select new SelectListItem
                                        {
                                            Selected = false,
                                            Text = c.DeliveryMethod,
                                            Value = c.SalesDeliveryID.ToString()
                                        };
                #endregion

                #region Country
                //combobox for country
                List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

                memberMC.Countries = from c in countries
                                     select new SelectListItem
                                     {
                                         Selected = false,
                                         Text = c.CountryName,
                                         Value = c.CountryCode
                                     };

                memberMC.UserInfo.Countries = from c in countries
                                              select new SelectListItem
                                              {
                                                  Selected = false,
                                                  Text = c.CountryName,
                                                  Value = c.CountryCode
                                              };

                if (memberMC.UserInfo.MobileCountryCode == null)
                {
                    memberMC.UserInfo.MobileCountryCode = countries[0].MobileCode;
                }
                #endregion

                #region Province
                DataSet dsProvinces = AdminGeneralDB.GetAllProvincesByCountry(Session["LanguageChosen"].ToString(), countries[0].CountryCode);
                List<ProvinceSetupModel> provinceList = new List<ProvinceSetupModel>();

                foreach (DataRow dr in dsProvinces.Tables[0].Rows)
                {
                    ProvinceSetupModel proModel = new ProvinceSetupModel();
                    proModel.ProvinceCode = dr["CPROVINCE_CODE"].ToString();
                    proModel.ProvinceName = dr["CMULTILANGPROVINCE_NAME"].ToString();

                    provinceList.Add(proModel);
                }

                memberMC.UserInfo.Province = from c in provinceList
                                             select new SelectListItem
                                             {
                                                 Selected = false,
                                                 Text = c.ProvinceName,
                                                 Value = c.ProvinceCode
                                             };

                #endregion

                #region City
                string defaultCity = "";
                if (memberMC.UserInfo.Province.Count() > 0)
                {
                    defaultCity = memberMC.UserInfo.Province.First().Value;
                }
                DataSet dsCity = AdminGeneralDB.GetAllCitiesByProvince(Session["LanguageChosen"].ToString(), defaultCity);
                List<CitySetupModel> cityList = new List<CitySetupModel>();

                foreach (DataRow dr in dsCity.Tables[0].Rows)
                {
                    CitySetupModel cityModel = new CitySetupModel();
                    cityModel.CityCode = dr["CCITY_CODE"].ToString();
                    cityModel.CityName = dr["CMULTILANGCITY_NAME"].ToString();

                    cityList.Add(cityModel);
                }

                memberMC.UserInfo.City = from c in cityList
                                         select new SelectListItem
                                         {
                                             Selected = false,
                                             Text = c.CityName,
                                             Value = c.CityCode
                                         };

                #endregion

                #region Retrieve Sponsor info
                memberMC.ReferralUsername = Session["Admin"].ToString();
                #endregion

                #region DOB
                memberMC.UserInfo.Days = Misc.ConstructsDay().ToList();
                memberMC.UserInfo.Months = Misc.ConstructsMonth().ToList();
                memberMC.UserInfo.Years = Misc.ConstructYears().ToList();
                memberMC.UserInfo.SelectedYears = "1960";
                memberMC.UserInfo.SelectedMonth = "1";
                #endregion

                #region Gender/Sex
                memberMC.UserInfo.Gender = Misc.ConstructGender();
                #endregion



                return PartialView("FreeRegistration", memberMC);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult FreeRegistrationMethod(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                int ok = 0;
                string msg = "";

                //encrypt pin and password
                mem.Member.Password = Authentication.Encrypt(mem.Member.Password);
                mem.Member.Pin = Authentication.Encrypt(mem.Member.Pin);
                mem.SponsorSecurityPin = Authentication.Encrypt(mem.SponsorSecurityPin);

                //mem.Member.Username = string.Format("{0}{1}", mem.SelectedCountry, mem.Member.Username);

                MemberShopCenterDB.FreeRegister(mem, out ok, out msg);

                string nickname = mem.UserInfo.Nickname;
                string username = mem.Member.Username;
                string selectedCountry = mem.SelectedCountry.ToLower();
                string ic = mem.UserInfo.IC;
                string cellPhone = CellPhoneParser.Parse(selectedCountry, (mem.UserInfo.CellPhone));
                string password = Authentication.Decrypt(mem.Member.Password);

                Session["regMember"] = new RegisterNewMemberModel();
                if (!string.IsNullOrEmpty(cellPhone))
                {
                    try
                    {
                        ISms client = new SmsClient();
                        var content = string.Format(Resources.OneForAll.OneForAll.msgGreeting, nickname, username, password);
                        client.SendMessage(cellPhone, content, selectedCountry);
                    }
                    catch (Exception)
                    {
                        // failed to send sms is ok, but better dont block the registration flow
                    }
                }

                if (ok == 1)//success
                {
                    return MembersList();
                }
                else
                {
                    if (msg == "-1")
                        Response.Write(Resources.OneForAll.OneForAll.msgMemberExist);
                    else if (msg == "-2")
                        Response.Write(Resources.OneForAll.OneForAll.msgPackageNotExists);
                    else if (msg == "-3")
                        Response.Write(Resources.OneForAll.OneForAll.msgWalletInsufficient);
                    else if (msg == "-4")
                        Response.Write(Resources.OneForAll.OneForAll.msgIntroNotExist);
                    else if (msg == "-5")
                        Response.Write(Resources.OneForAll.OneForAll.msgAdminPINInvalid);
                    else if (msg == "-6")
                        Response.Write(Resources.OneForAll.OneForAll.msgUpNotExist);
                    else if (msg == "-7")
                        Response.Write(Resources.OneForAll.OneForAll.msgLeftOccupied);
                    else if (msg == "-8")
                        Response.Write(Resources.OneForAll.OneForAll.msgRightOccupied);
                    else if (msg == "-9")
                        Response.Write(Resources.OneForAll.OneForAll.msgPositionIncorrect);

                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Sponsor Chart
        public ActionResult SponsorChartTree(string memberUsername, int? memberLevel)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            string username;
            if (memberUsername == null)
            {
                username = Session["Admin"].ToString();
                ViewBag.MemberLevel = 1;
                ViewBag.NextLevel = 4;
                Session["PreviousView"] = null;
            }
            else
            {
                username = memberUsername;
                ViewBag.MemberLevel = memberLevel;
                ViewBag.NextLevel = memberLevel + 3;
            }

            //Store Previous username and level
            List<String> userNameList = new List<String>();
            if (Session["PreviousView"] == null)
            {
                userNameList.Add(username + ";" + ViewBag.MemberLevel);
                Session["PreviousView"] = userNameList;
            }
            else
            {
                userNameList = (List<string>)Session["PreviousView"];
                userNameList.Add(memberUsername + ";" + memberLevel);
                Session["PreviousView"] = userNameList;
            }

            var resultList = new List<MemberSponsorChartModel>();

            int ok;
            string msg;
            DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChart(username, out ok, out msg);
            ViewBag.MemberCount = 0;
            foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
            {
                var mntm = new MemberSponsorChartModel();
                mntm.Level = dr["LEVEL"].ToString();
                //mntm.FirstLevelMemberCount = dr["FIRSTLEVEL_MEMBERCOUNT"].ToString();
                //mntm.FirstLevelMemberTotalInvest = dr["FIRSTLEVEL_TOTALINVEST"].ToString();

                //mntm.SecondLevelMember = dr["SECONDLEVEL"].ToString();
                //mntm.SecondLevelMemberCount = dr["SECONDLEVEL_MEMBERCOUNT"].ToString();
                //mntm.SecondLevelMemberTotalInvest = dr["SECONDLEVEL_TOTALINVEST"].ToString();

                //mntm.ThirdLevelMember = dr["THIRDLEVEL"].ToString();
                //mntm.ThirdLevelMemberCount = dr["THIRDLEVEL_MEMBERCOUNT"].ToString();
                //mntm.ThirdLevelMemberTotalInvest = dr["THIRDLEVEL_TOTALINVEST"].ToString();

                resultList.Add(mntm);
            }

            return PartialView("SponsorChartTree", resultList);
        }

        public ActionResult BackToPreviousTree()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            string username;
            List<String> userNameList = new List<String>();
            userNameList = (List<string>)Session["PreviousView"];

            if (userNameList.Count == 1)
            {
                string[] PreviousView = userNameList[userNameList.Count - 1].ToString().Split(';');
                username = PreviousView[0];
                ViewBag.MemberLevel = Convert.ToInt32(PreviousView[1]);
                ViewBag.NextLevel = Convert.ToInt32(ViewBag.MemberLevel) + 3;
            }
            else
            {
                string[] PreviousView = userNameList[userNameList.Count - 2].ToString().Split(';');

                username = PreviousView[0];
                ViewBag.MemberLevel = Convert.ToInt32(PreviousView[1]);
                ViewBag.NextLevel = Convert.ToInt32(ViewBag.MemberLevel) + 3;

                userNameList.RemoveAt(userNameList.Count - 1);
                Session["PreviousView"] = userNameList;
            }

            var resultList = new List<MemberSponsorChartModel>();

            int ok;
            string msg;
            DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChart(username, out ok, out msg);
            ViewBag.MemberCount = 0;
            foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
            {
                var mntm = new MemberSponsorChartModel();
                mntm.Level = dr["LEVEL"].ToString();
                //mntm.FirstLevelMemberCount = dr["FIRSTLEVEL_MEMBERCOUNT"].ToString();
                //mntm.FirstLevelMemberTotalInvest = dr["FIRSTLEVEL_TOTALINVEST"].ToString();

                //mntm.SecondLevelMember = dr["SECONDLEVEL"].ToString();
                //mntm.SecondLevelMemberCount = dr["SECONDLEVEL_MEMBERCOUNT"].ToString();
                //mntm.SecondLevelMemberTotalInvest = dr["SECONDLEVEL_TOTALINVEST"].ToString();

                //mntm.ThirdLevelMember = dr["THIRDLEVEL"].ToString();
                //mntm.ThirdLevelMemberCount = dr["THIRDLEVEL_MEMBERCOUNT"].ToString();
                //mntm.ThirdLevelMemberTotalInvest = dr["THIRDLEVEL_TOTALINVEST"].ToString();

                resultList.Add(mntm);
            }

            return PartialView("SponsorChartTree", resultList);
        }

        public ActionResult ModalViewMemberData(string username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            var mm = new MemberModel();

            int ok;
            string msg;
            string languageCode = Session["LanguageChosen"].ToString();
            DataSet dsMember = MemberShopCenterDB.GetSponsorChartMemberByUsername(username, languageCode, out ok, out msg);
            if (dsMember.Tables[0].Rows.Count != 0)
            {
                mm.MemberCountry = dsMember.Tables[0].Rows[0]["CMULTILANGCOUNTRY_NAME"].ToString();
                mm.Username = dsMember.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
                mm.FirstName = dsMember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                mm.JoinedDate = (DateTime)dsMember.Tables[0].Rows[0]["CMEM_CREATEDON"];
                mm.MemberPackage = dsMember.Tables[0].Rows[0]["CPKG_CODE"].ToString();
                mm.Intro = dsMember.Tables[0].Rows[0]["CMEM_INTRO"].ToString();
                mm.Rank = dsMember.Tables[0].Rows[0]["CWPOOLPRM_RANK"].ToString();
            }

            return PartialView("ModalViewMemberData", mm);
        }
        #endregion

        #region View Member Password

        public ActionResult ViewMemberPassword(int selectedPage = 1, string SearchFirstName = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            var model = new PaginationMemberModel();
            int pages = 0;
            string message = string.Empty;
            int status = 0;

            if (selectedPage == 0)
            {
                selectedPage = 1;
            }

            var dsMember = AdminMemberDB.GetMemberPassword(SearchFirstName, selectedPage, out pages, out status, out message);
            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsMember.Tables[0].Rows)
            {
                var member = new MemberModel();
                member.Number = dr["RowNumber"].ToString();
                member.MemberId = dr["CUSR_USERNAME"].ToString();
                member.FirstName = dr["CUSR_FULLNAME"].ToString();
                member.Password = Authentication.Decrypt(dr["CUSR_PASSWORD"].ToString());
                member.Pin = Authentication.Decrypt(dr["CUSR_PIN"].ToString());
                model.MemberList.Add(member);
            }            

            return PartialView("ViewMemberPassword", model);
        }

        #endregion

        #region Direct Sponsor

        public ActionResult CheckTotalSponsor(int selectedPage = 1, double SponsorAmount = 0, DateTime? StartDate = null, DateTime? EndDate = null, string SearchFirstName = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            if (!StartDate.HasValue)
                StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            

            if (!EndDate.HasValue)
                EndDate = StartDate.Value.AddMonths(1).AddDays(-1);

            int status = 0;
            string message = string.Empty;
            int pages = 0;
            PaginationMemberModel model = new PaginationMemberModel();
            model.StartDate = StartDate;
            model.EndDate = EndDate;
            model.SponsorAmount = SponsorAmount;
            model.SearchFirstName = SearchFirstName;

            DataSet dsSponsorAmount = AdminMemberDB.GetMemberTotalSponsor(SponsorAmount, StartDate, EndDate, SearchFirstName, selectedPage, out pages, out status, out message);

            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsSponsorAmount.Tables[0].Rows)
            {
                var member = new MemberModel();
                member.Number = dr["RowNumber"].ToString();
                member.MemberId = dr["CUSR_USERNAME"].ToString();
                member.FirstName = dr["CUSR_FULLNAME"].ToString();
                member.TotalDirectSponsor = string.Format("{0}", Convert.ToInt32(dr["CMEM_TOTAL_DIRECT_SPONSOR"]));
                model.MemberList.Add(member);
            }

            return PartialView("CheckTotalSponsor", model);
        }

        #endregion

        #region ChangePassword
        public ActionResult ChangePassword(string Username, bool noRedirectionRequired = false)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            ChangePasswordModel model = new ChangePasswordModel();

            int ok;
            string msg;
            DataSet dsUser = AdminMemberDB.GetMemberByUserName(Username, out ok, out msg);

            if (dsUser.Tables[0].Rows.Count == 0)
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.msgUserNotExist, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            model.UserName = Username;
            model.UserFullName = dsUser.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            //model.UserFullName = dsUser.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString() + " " + dsUser.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            model.CurrentPassword = Authentication.Decrypt(dsUser.Tables[0].Rows[0]["CUSR_PASSWORD"].ToString());
            model.NewPassword = "";
            model.ConfirmNewPassword = "";
            model.NoRedirectionRequired = noRedirectionRequired;

            model.BonusWallet = float.Parse(dsUser.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
            //model.DateJoined = (DateTime)dsUser.Tables[0].Rows[0]["CMEM_CREATEDON"];
            model.DateJoined = Convert.ToDateTime(dsUser.Tables[0].Rows[0]["CMEM_CREATEDON"]);

            return PartialView("ChangePassword", model);
        }

        [HttpPost]
        public ActionResult ChangePasswordMethod(ChangePasswordModel cpm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                if (cpm.CurrentPassword == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgReqCurrPwd);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (cpm.NewPassword == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgReqNewPwd);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (cpm.ConfirmNewPassword == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgReqConfirmNewPwd);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (cpm.NewPassword != cpm.ConfirmNewPassword)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgPwdNotMatch);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok;
                string msg;
                AdminMemberDB.UpdateChangePassword("SP_ChangePassword", cpm.UserName, Authentication.Encrypt(cpm.CurrentPassword), Authentication.Encrypt(cpm.NewPassword), Session["Admin"].ToString(), out ok, out msg);
                AdminDB.InsertUserTrackingLog("Admin", cpm.UserName, HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), "Change Password", Authentication.Encrypt(cpm.CurrentPassword), Authentication.Encrypt(cpm.NewPassword), Session["Admin"].ToString());
                if (ok == -1)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidCurrPwd);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (ok == -2)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgPasswordAndPinMustDifferent);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (!cpm.NoRedirectionRequired)
                {
                    return RedirectToAction("AdminSearchUser", "Admin", new { Type = ProjectStaticString.ChangePassword });
                }
                else
                {
                    return ViewMemberDetails();
                }
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Transfer Member
        public ActionResult TransferMember(string Username, bool noRedirectionRequired = false)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            ChangePasswordModel model = new ChangePasswordModel();

            int ok;
            string msg;
            DataSet dsUser = AdminMemberDB.GetMemberByUserName(Username, out ok, out msg);

            if (dsUser.Tables[0].Rows.Count == 0)
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.msgUserNotExist, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            model.UserName = Username;
            //model.UserFullName = dsUser.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString() + " " + dsUser.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            model.UserFullName = dsUser.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            model.CurrentPassword = Authentication.Decrypt(dsUser.Tables[0].Rows[0]["CUSR_PASSWORD"].ToString());
            model.NewPassword = "";
            model.ConfirmNewPassword = "";
            model.NoRedirectionRequired = noRedirectionRequired;

            List<CountrySetupModel> countries = Misc.GetAllCountryList("en-US", ref ok, ref msg);

            model.Countries = from c in countries
                                select new SelectListItem
                                {
                                    Text = c.CountryName,
                                    Value = c.CountryId.ToString()
                                };
            model.SelectedCountry = dsUser.Tables[0].Rows[0]["CCOUNTRY_ID"].ToString();
            model.BonusWallet = float.Parse(dsUser.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
            //model.DateJoined = (DateTime)dsUser.Tables[0].Rows[0]["CMEM_CREATEDON"];

            model.DateJoined = Convert.ToDateTime(dsUser.Tables[0].Rows[0]["CMEM_CREATEDON"]);

            return PartialView("TransferMember", model);
        }

        [HttpPost]
        public ActionResult TransferMemberMethod(ChangePasswordModel cpm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }


                if (string.IsNullOrEmpty(cpm.UserFullName))
                {
                    Response.Write("Please Key In Full Name");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(cpm.UserIC))
                {
                    Response.Write("Please Key In IC");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(cpm.UserMobile))
                {
                    Response.Write("Please Key In Mobile Number");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(cpm.UserEmail))
                {
                    Response.Write("Please Key In Email");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                cpm.DateJoined = Convert.ToDateTime(cpm.DateJoined);
                int ok;
                string msg;
                AdminMemberDB.TransferMember("SP_ChangeMember", cpm.UserName, cpm.UserFullName,cpm.UserIC,cpm.UserMobile,cpm.UserEmail, Convert.ToInt32(cpm.SelectedCountry) ,out ok, out msg);

                MemberDB.InsertUserOperation(Session["Admin"].ToString(), "Transfer Member", HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), 0);

                if (!cpm.NoRedirectionRequired)
                {
                    return RedirectToAction("AdminSearchUser", "Admin", new { Type = ProjectStaticString.ChangePassword });
                }
                else
                {
                    return ViewMemberDetails();
                }
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Member Statistic
        public ActionResult MemberStatistic()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            ChangePasswordModel model = new ChangePasswordModel();

            DataSet dsUser = AdminMemberDB.GetMemberStatistic();

            model.TotalUserID = Convert.ToInt32(dsUser.Tables[0].Rows[0]["TOTALUSERID"].ToString()).ToString("###,##0");
            model.TotalMembers = Convert.ToInt32(dsUser.Tables[1].Rows[0]["TOTALMEMBERS"].ToString()).ToString("###,##0");

            foreach (DataRow dr in dsUser.Tables[2].Rows)
            {
                if(dr["CRANKSET_NAME"].ToString() == "RESERVED")
                {
                    model.TotalReserved = Convert.ToInt32(dr["TOTAL"].ToString()).ToString("###,##0");
                }
                else if (dr["CRANKSET_NAME"].ToString() == "BASIC")
                {
                    model.TotalBasic = Convert.ToInt32(dr["TOTAL"].ToString()).ToString("###,##0");
                }
                else if (dr["CRANKSET_NAME"].ToString() == "ADVANCE")
                {
                    model.TotalAdvance = Convert.ToInt32(dr["TOTAL"].ToString()).ToString("###,##0");
                }
                else if (dr["CRANKSET_NAME"].ToString() == "PREMIUM")
                {
                    model.TotalPremium = Convert.ToInt32(dr["TOTAL"].ToString()).ToString("###,##0");
                }
                else if (dr["CRANKSET_NAME"].ToString() == "ELITE")
                {
                    model.TotalElite = Convert.ToInt32(dr["TOTAL"].ToString()).ToString("###,##0");
                }
                else if (dr["CRANKSET_NAME"].ToString() == "ENTERPRISE")
                {
                    model.TotalEnterprise = Convert.ToInt32(dr["TOTAL"].ToString()).ToString("###,##0");
                }

            }



                return PartialView("MemberStatistic", model);
        }
        #endregion

        #region Member Income
        public ActionResult MemberIncome()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            PaginationMemberModel model = new PaginationMemberModel();

            DataSet dsUser = AdminMemberDB.GetMemberIncome();

            foreach (DataRow dr in dsUser.Tables[0].Rows)
            {
                var member = new MemberModel();
                member.Number = dr["rownumber"].ToString();
                member.MemberId = dr["CUSR_USERNAME"].ToString();
                member.SurName = dr["CUSR_FULLNAME"].ToString();
                member.Rank = dr["CRANKSET_NAME"].ToString();
                member.PackageAmount = float.Parse(dr["CPKG_AMOUNT"].ToString()).ToString("n2");
                member.DirectBonus = float.Parse(dr["SponsorBonus"].ToString()).ToString("n2");
                member.DirectRollUp = float.Parse(dr["DirectRollUpBonus"].ToString()).ToString("n2");
                member.GroupBonus = float.Parse(dr["PairingBonus"].ToString()).ToString("n2");
                member.SellEFund = float.Parse(dr["SellEFund"].ToString()).ToString("n2");
                member.TradingBonus = float.Parse(dr["TradingBonus"].ToString()).ToString("n2");
                member.GrandTotal = float.Parse(dr["TOTAL"].ToString()).ToString("n2");
                member.Withdrawal = float.Parse(dr["Withdrawal"].ToString()).ToString("n2");
                member.CPTORP = float.Parse(dr["CPTORP"].ToString()).ToString("n2");
                member.CPTOMP = float.Parse(dr["CPTOMP"].ToString()).ToString("n2");
                member.SecondGrandTotal = float.Parse(dr["GRANDTOTAL"].ToString()).ToString("n2");
                model.MemberList.Add(member);
            }

            return PartialView("MemberIncome", model);
        }

        public void ExportMemberIncome()
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=MemberIncome.csv");
            Response.ContentType = "application/octet-stream";
            StreamWriter sw = new StreamWriter(Response.OutputStream, Encoding.UTF8);
            sw.Write(PrintMemberIncome());
            sw.Close();
            Response.End();
        }

        private string PrintMemberIncome()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("No,Member Id,Full Name, Rank ,Package Amount, Direct Bonus, Direct Roll Up,Group Bonus , Sell eFund Amount,Trading Bonus,Grand Total,Withdrawal,CP TO RP, CP TO MP,Grand Total"));
            string message = string.Empty;
            DataSet dsMemberlist = new DataSet();
            dsMemberlist = AdminMemberDB.GetMemberIncomeAll();

            foreach (DataRow dr in dsMemberlist.Tables[0].Rows)
            {

                sb.AppendLine(dr["rownumber"].ToString() + "," +
                              dr["CUSR_USERNAME"].ToString() + "," +
                              dr["CUSR_FULLNAME"].ToString() + "," +
                              dr["CRANKSET_NAME"].ToString() + "," +
                              float.Parse(dr["CPKG_AMOUNT"].ToString()).ToString("n2").Replace(",", "") + "\t," +
                              float.Parse(dr["SponsorBonus"].ToString()).ToString("n2").Replace(",", "") + "\t," +                              
                              float.Parse(dr["DirectRollUpBonus"].ToString()).ToString("n2").Replace(",", "") + "\t," +
                              float.Parse(dr["PairingBonus"].ToString()).ToString("n2").Replace(",", "") + "\t," +
                              float.Parse(dr["SellEFund"].ToString()).ToString("n2").Replace(",", "") + "\t," +
                              float.Parse(dr["TradingBonus"].ToString()).ToString("n2").Replace(",", "") + "\t," +
                              float.Parse(dr["TOTAL"].ToString()).ToString("n2").Replace(",", "") + "\t," +
                              float.Parse(dr["Withdrawal"].ToString()).ToString("n2").Replace(",", "") + "\t," +
                              float.Parse(dr["CPTORP"].ToString()).ToString("n2").Replace(",", "") + "\t," +
                              float.Parse(dr["CPTOMP"].ToString()).ToString("n2").Replace(",", "") + "\t," +
                              float.Parse(dr["GRANDTOTAL"].ToString()).ToString("n2").Replace(",", "") + "\t"
                              );
            }

            return sb.ToString();
        }
        #endregion

        #region ChangePin
        public ActionResult ChangePin(string Username, bool noRedirectionRequired = false)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            ChangePasswordModel model = new ChangePasswordModel();

            int ok;
            string msg;
            DataSet dsUser = AdminMemberDB.GetMemberByUserName(Username, out ok, out msg);

            if (dsUser.Tables[0].Rows.Count == 0)
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.msgUserNotExist, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            model.UserName = Username;
            model.UserFullName = dsUser.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            model.CurrentPassword = Authentication.Decrypt(dsUser.Tables[0].Rows[0]["CUSR_PIN"].ToString());
            model.NewPassword = "";
            model.ConfirmNewPassword = "";
            model.NoRedirectionRequired = noRedirectionRequired;

            model.BonusWallet = float.Parse(dsUser.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
            model.DateJoined = (DateTime)dsUser.Tables[0].Rows[0]["CMEM_CREATEDON"];


            return PartialView("ChangePin", model);
        }

        [HttpPost]
        public ActionResult ChangePinMethod(ChangePasswordModel cpm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                if (cpm.CurrentPassword == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgReqCurrPwd);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (cpm.NewPassword == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgReqNewPwd);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (cpm.ConfirmNewPassword == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgReqConfirmNewPwd);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (cpm.NewPassword != cpm.ConfirmNewPassword)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgPwdNotMatch);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok;
                string msg;
                AdminMemberDB.UpdateChangePassword("SP_ChangePin", cpm.UserName, Authentication.Encrypt(cpm.CurrentPassword), Authentication.Encrypt(cpm.NewPassword), Session["Admin"].ToString(), out ok, out msg);
                AdminDB.InsertUserTrackingLog("Admin", cpm.UserName, HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), "Change PIN", Authentication.Encrypt(cpm.CurrentPassword), Authentication.Encrypt(cpm.NewPassword), Session["Admin"].ToString());
                if (ok == -1)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidCurrPwd);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (!cpm.NoRedirectionRequired)
                {
                    return RedirectToAction("AdminSearchUser", "Admin", new { Type = ProjectStaticString.ChangePin });
                }
                else
                {
                    return ViewMemberDetails();
                }
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region ChangeMemberPermission
        public ActionResult ChangeMemberPermission(string Username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            MemberPermissionModel model = new MemberPermissionModel();

            int ok;
            string msg;
            DataSet dsUser = AdminMemberDB.GetMemberByUserName(Username, out ok, out msg);

            if (dsUser.Tables[0].Rows.Count == 0)
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.msgUserNotExist, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            model.UserName = Username;
            model.UserFullName = dsUser.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString() ;
            model.IsBlocked = !bool.Parse(dsUser.Tables[0].Rows[0]["CUSR_ACTIVE"].ToString());

            model.BonusWallet = float.Parse(dsUser.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
            model.DateJoined = (DateTime)dsUser.Tables[0].Rows[0]["CMEM_CREATEDON"];
            //if (!string.IsNullOrEmpty(dsUser.Tables[0].Rows[0]["CMEM_IMAGEPATH"].ToString()))
            //{
            //    var index = dsUser.Tables[0].Rows[0]["CMEM_IMAGEPATH"].ToString().LastIndexOf('\\');
            //    var filename = dsUser.Tables[0].Rows[0]["CMEM_IMAGEPATH"].ToString().Substring(index + 1);
            //    model.MemberPhoto = "http://" + Request.Url.Authority + "/MemberProfile/" + Username + "/" + filename;
            //}
            //else
            //{
            //    model.MemberPhoto = "http://" + Request.Url.Authority + "/Images/NoPhoto.png";
            //}

            return PartialView("ChangeMemberPermission", model);
        }

        [HttpPost]
        public ActionResult ChangeMemberPermissionMethod(MemberPermissionModel cpm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                int ok;
                string msg;
                AdminMemberDB.ChangeMemberPermission(cpm.UserName, !cpm.IsBlocked, Session["Admin"].ToString(), out ok, out msg);
                if (ok == -1)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgFailedOperation);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("AdminSearchUser", "Admin", new { Type = ProjectStaticString.ChangeMemberPermission });
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region ChangeOwnership
        public ActionResult ChangeOwnership(string Username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            ChangeOwnershipModel model = new ChangeOwnershipModel();

            int ok;
            string msg;
            DataSet dsUser = AdminMemberDB.GetMemberByUserName(Username, out ok, out msg);

            if (dsUser.Tables[0].Rows.Count == 0)
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.msgUserNotExist, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            model.Username = Username;
            model.FirstName = dsUser.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();

            //store current name
            model.SurName = dsUser.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            //end

            model.BonusWallet = float.Parse(dsUser.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
            model.DateJoined = (DateTime)dsUser.Tables[0].Rows[0]["CMEM_CREATEDON"];
            //if (!string.IsNullOrEmpty(dsUser.Tables[0].Rows[0]["CMEM_IMAGEPATH"].ToString()))
            //{
            //    var index = dsUser.Tables[0].Rows[0]["CMEM_IMAGEPATH"].ToString().LastIndexOf('\\');
            //    var filename = dsUser.Tables[0].Rows[0]["CMEM_IMAGEPATH"].ToString().Substring(index + 1);
            //    model.MemberPhoto = "http://" + Request.Url.Authority + "/MemberProfile/" + Username + "/" + filename;
            //}
            //else
            //{
            //    model.MemberPhoto = "http://" + Request.Url.Authority + "/Images/NoPhoto.png";
            //}

            return PartialView("ChangeOwnership", model);
        }

        [HttpPost]
        public ActionResult ChangeOwnershipMethod(ChangeOwnershipModel cpm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                if (cpm.FirstName == "" || cpm.FirstName == null)
                {
                    Response.Write("Please Key In Full Name");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok;
                string msg;
                AdminMemberDB.UpdateChangeOwnership(cpm.Username, cpm.FirstName, Session["Admin"].ToString(), out ok, out msg);
                AdminDB.InsertUserTrackingLog("Admin", cpm.Username, HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), "Change Name", cpm.SurName, cpm.FirstName, Session["Admin"].ToString());

                if (ok == -1)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgUsernameNotExist);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("AdminSearchUser", "Admin", new { Type = ProjectStaticString.ChangeOwnership });
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region ChangeSponsor
        public ActionResult ChangeSponsor(string Username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            ChangeSponsorModel model = new ChangeSponsorModel();
            int ok = 0;
            string msg = "";

            DataSet dsUser = MemberDB.GetMemberByUsername(Username, out ok, out msg);
            if (dsUser.Tables[0].Rows.Count == 0)
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.msgUserNotExist, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            model.Username = Username;
            model.CurrIntro = dsUser.Tables[0].Rows[0]["CMEM_INTRO"].ToString();
            model.NewIntro = "";

            return PartialView("ChangeSponsor", model);
        }

        [HttpPost]
        public ActionResult ChangeSponsorMethod(ChangeSponsorModel cpm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                if (string.IsNullOrEmpty(cpm.NewIntro))
                {
                    Response.Write("Please Key In New Sponsor");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok;
                string msg;
                AdminMemberDB.UpdateChangeSponsor(cpm.Username, cpm.CurrIntro, cpm.NewIntro, Session["Admin"].ToString(), out ok, out msg);
                AdminDB.InsertUserTrackingLog("Admin", cpm.Username, HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), "Change Sponsor", cpm.CurrIntro, cpm.NewIntro, Session["Admin"].ToString());
                if (ok == -1)
                {
                    Response.Write(string.Format(Resources.OneForAll.OneForAll.msgUserNotExist, cpm.NewIntro));
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("AdminSearchUser", "Admin", new { Type = ProjectStaticString.ChangeSponsor });
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region ChangeUpline
        public ActionResult ChangeUpline(string Username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            ChangeUplineModel model = new ChangeUplineModel();
            int ok = 0;
            string msg = "";

            DataSet dsUser = MemberDB.GetMemberMarketTreeByUsername(Username, out ok, out msg);
            if (dsUser.Tables[0].Rows.Count == 0)
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.msgUserNotExist, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            model.Username = Username;
            model.CurrentUpline = dsUser.Tables[0].Rows[0]["CMTREE_UPMEMBER"].ToString();

            var accounts = new List<SelectListItem>();
            var LeftPosition = new SelectListItem();
            LeftPosition.Text = "Left";
            LeftPosition.Value = "A";
            accounts.Add(LeftPosition);

            var RightPosition = new SelectListItem();
            RightPosition.Text = "Right";
            RightPosition.Value = "B";
            accounts.Add(RightPosition);

            model.SelectedPosition = "A";

            model.Position = accounts;

            return PartialView("ChangeUpline", model);
        }

        [HttpPost]
        public ActionResult ChangeUplineMethod(ChangeUplineModel CUM)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { });
                }

                if (string.IsNullOrEmpty(CUM.NewUpline))
                {
                    Response.Write("Please Key In New Upline");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok = 0;
                string msg = "";

                DataSet dsUser = MemberDB.GetMemberMarketTreeByUsername(CUM.NewUpline, out ok, out msg);
                string PositionLeft = dsUser.Tables[0].Rows[0]["CMTREE_FOLLOWGROUPA"].ToString();
                string PositionRight = dsUser.Tables[0].Rows[0]["CMTREE_FOLLOWGROUPB"].ToString();

                if (CUM.SelectedPosition == "A")
                {
                    if (PositionLeft != "0")
                    {
                        Response.Write("Position left are occupied ");
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }
                else if (CUM.SelectedPosition == "B")
                {
                    if (PositionRight != "0")
                    {
                        Response.Write("Position right are occupied ");
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                AdminMemberDB.UpdateChangeUpline(CUM.Username, CUM.NewUpline, CUM.SelectedPosition, Session["Admin"].ToString(), out ok, out msg);

                AdminDB.InsertUserTrackingLog("Admin", CUM.Username, HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), "Change Upline", CUM.NewUpline, CUM.SelectedPosition, Session["Admin"].ToString());

                if (ok == -1)
                {
                    Response.Write(string.Format(Resources.OneForAll.OneForAll.msgUserNotExist, CUM.NewUpline));
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (ok == -2)
                {
                    Response.Write("Position left are occupied");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (ok == -3)
                {
                    Response.Write("Position right are occupied");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("AdminSearchUser", "Admin", new { Type = ProjectStaticString.ChangeSponsor });
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region ChangeRanking
        public ActionResult ChangeRanking(string Username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            var model = new ChangeRankingModel();

            int ok;
            string msg;
            DataSet dsMember = AdminMemberDB.GetMemberByUserName(Username, out ok, out msg);

            if (dsMember.Tables[0].Rows.Count == 0)
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.msgUserNotExist, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            model.UserName = Username;
            model.SelectedRanking = dsMember.Tables[0].Rows[0]["CRANK_CODE"] == null ? string.Empty : dsMember.Tables[0].Rows[0]["CRANK_CODE"].ToString();
            model.CurrentRank = dsMember.Tables[0].Rows[0]["CRANK_CODE"] == null ? string.Empty : dsMember.Tables[0].Rows[0]["CRANK_CODE"].ToString();
            model.RankList = Misc.GetallRankList();

            model.DateJoined = (DateTime)dsMember.Tables[0].Rows[0]["CMEM_CREATEDON"];

            return PartialView("ChangeRanking", model);
        }

        [HttpPost]
        public ActionResult ChangeRankingMethod(ChangeRankingModel crm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                int ok;
                string msg;
                AdminMemberDB.UpdateMemberRanking(crm.UserName, crm.SelectedRanking, Session["Admin"].ToString(), out ok, out msg);


                AdminDB.InsertUserTrackingLog("Admin", crm.UserName, HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), "Change Ranking", crm.CurrentRank, crm.SelectedRanking, Session["Admin"].ToString());

                if (ok == -1)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidRanking);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                return RedirectToAction("AdminSearchUser", "Admin", new { Type = ProjectStaticString.ChangeRanking });
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Shared Function
        #region ConstructPageList

        private int ConstructPageList(int selectedPage, int pages, PaginationMemberModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            if (pages == 0)
            {
                pageList.Add(0);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            
            return selectedPage;
        }

        private int ConstructPageList(int selectedPage, int pages, PaginationMemberAutoMaintainModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        private int ConstructPageList(int selectedPage, int pages, PaginationMemberWalletModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        #endregion

        #endregion

        #region SearchSponsor
        public ActionResult SponsorTree(string Username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }


            var model = new MemberSponsorChartModel();

            if (Username == null)
            {
                int ok;
                string msg;

                DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChart("BVA", out ok, out msg);

                var member = MemberDB.GetMemberByUsername("BVA", out ok, out msg);

                model.FirstLevelMember = dsNetworkTree.Tables[0].Rows[0]["USERNAME"].ToString();
                model.FirstLevelLevel = dsNetworkTree.Tables[0].Rows[0]["LEVEL"].ToString();
                //model.FirstLevelJoinedDate = dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString();

                var Date = dsNetworkTree.Tables[0].Rows[0]["DATE"];
                model.FirstLevelJoinedDate = Convert.ToDateTime(Date).ToString("dd/MM/yyyy");


                model.FirstLevelRanking = Misc.GetMemberRanking(dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString());
                model.FirstLevelIntro = dsNetworkTree.Tables[0].Rows[0]["INTRO"].ToString();
                model.FirstLevelFullname = dsNetworkTree.Tables[0].Rows[0]["FULLNAME"].ToString();
                model.FirstLevelRankIcon = member.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

                foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
                {
                    MemberList resultList = new MemberList();
                    var info = GetMemberSponsorInfo(dr);
                    resultList.Level = dr["LEVEL"].ToString();
                    resultList.Member = dr["USERNAME"].ToString();
                    resultList.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());
                    //resultList.JoinedDate = dr["DATE"].ToString();
                    var joinDate = dr["DATE"];
                    resultList.JoinedDate = Convert.ToDateTime(joinDate).ToString("dd/MM/yyyy");

                    resultList.FullName = dr["FULLNAME"].ToString();
                    resultList.Intro = dr["INTRO"].ToString();
                    resultList.RankIcon = dr["ICON"].ToString();

                    model.resultList.Add(resultList);

                }
            }
            else
            {
                int ok;
                string msg;

                DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChart(Username, out ok, out msg);

                if (dsNetworkTree.Tables[0].Rows.Count == 0)
                {
                    Response.Write("Invalid Username");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                var member = MemberDB.GetMemberByUsername("BVA", out ok, out msg);

                model.FirstLevelMember = dsNetworkTree.Tables[0].Rows[0]["USERNAME"].ToString();
                model.FirstLevelLevel = dsNetworkTree.Tables[0].Rows[0]["LEVEL"].ToString();
                //model.FirstLevelJoinedDate = dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString();
                var Date = dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString();
                model.FirstLevelJoinedDate = Convert.ToDateTime(Date).ToString("dd/mm/yyyy");
                model.FirstLevelRanking = Misc.GetMemberRanking(dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString());
                model.FirstLevelIntro = dsNetworkTree.Tables[0].Rows[0]["INTRO"].ToString();
                model.FirstLevelFullname = dsNetworkTree.Tables[0].Rows[0]["FULLNAME"].ToString();
                model.FirstLevelRankIcon = member.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

                foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
                {
                    MemberList resultList = new MemberList();
                    var info = GetMemberSponsorInfo(dr);
                    resultList.Level = dr["LEVEL"].ToString();
                    resultList.Member = dr["USERNAME"].ToString();
                    resultList.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());
                    //resultList.JoinedDate = dr["DATE"].ToString();
                    var joinDate = dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString();
                    resultList.JoinedDate = Convert.ToDateTime(joinDate).ToString("dd/mm/yyyy");
                    resultList.FullName = dr["FULLNAME"].ToString();
                    resultList.Intro = dr["INTRO"].ToString();
                    resultList.RankIcon = dr["ICON"].ToString();

                    model.resultList.Add(resultList);

                }
            }
            ViewBag.MemberCount = 0;

            return PartialView("SponsorList", model);
        }
        #endregion

        #region Sponsor List
        public ActionResult SponsorListing(int selectedPage = 1)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }


                int pages = 0;
                PaginationIntroListModel model = new PaginationIntroListModel();
                DataSet dsSponsorList = MemberShopCenterDB.GetAllSponsorListByUsername("BVA", selectedPage, out pages);

                selectedPage = SponsorConstructPageList(selectedPage, pages, model);

                foreach (DataRow dr in dsSponsorList.Tables[0].Rows)
                {
                    IntroListModel sponsorlist = new IntroListModel();
                    sponsorlist.No = dr["rownumber"].ToString();
                    sponsorlist.Username = dr["CUSR_MEMBERID"].ToString();
                    sponsorlist.Name = dr["CUSR_FULLNAME"].ToString();
                    sponsorlist.Rank = Misc.RankNumber(dr["CRANK_CODE"].ToString());
                    sponsorlist.Sponsor = dr["CUSR_INTRO"].ToString();
                    sponsorlist.Level = dr["CMTREE_LEVEL"].ToString();
                    sponsorlist.CreatedDate = Convert.ToDateTime(dr["CMEM_DATEJOINED"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    model.SponsorList.Add(sponsorlist);
                }

                return PartialView("SponsorListing", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        private int SponsorConstructPageList(int selectedPage, int pages, PaginationIntroListModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        private MemberSponsorChartModel GetMemberSponsorInfo(DataRow dr)
        {
            var mntm = new MemberSponsorChartModel();

            mntm.Level = dr["LEVEL"].ToString();
            mntm.Member = dr["USERNAME"].ToString();
            mntm.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());
            mntm.JoinedDate = DateTime.Parse(dr["DATE"].ToString()).ToShortDateString();
            mntm.FullName = dr["FULLNAME"].ToString();
            mntm.Intro = dr["INTRO"].ToString();


            return mntm;
        }

        #region SearchUpLine
        public ActionResult SearchUpline(string Username = "0")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            var resultList = new List<MemberSponsorChartModel>();

            if (string.IsNullOrEmpty(Username))
            {
                Response.Write("Please Key In Username");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (Username == "0")
            {

            }
            else
            {
                int ok;
                int count;
                string msg;
                DataSet dsNetworkTree = MemberShopCenterDB.SearchUpline(Username, out ok, out count, out msg);

                for (int i = 0; count > i; i++)
                {
                    var mntm = new MemberSponsorChartModel();
                    mntm.FirstLevelMember = dsNetworkTree.Tables[i].Rows[0]["CUSR_USERNAME"].ToString();
                    mntm.FirstLevelMemberCount = dsNetworkTree.Tables[i].Rows[0]["CUSR_FULLNAME"].ToString();
                    resultList.Add(mntm);
                }
            }
            ViewBag.MemberCount = 0;

            return PartialView("UplineList", resultList);
        }
        #endregion

        #region SearchSponsor
        public ActionResult SearchSponsor(string Username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            var resultList = new List<MemberSponsorChartModel>();

            if (Username == null)
            {

            }
            else
            {
                int ok;
                int count;
                string msg;
                DataSet dsNetworkTree = MemberShopCenterDB.SearchSponsor(Username, out ok, out count, out msg);

                for (int i = 0; count > i; i++)
                {
                    var mntm = new MemberSponsorChartModel();
                    mntm.FirstLevelMember = dsNetworkTree.Tables[i].Rows[0]["CUSR_USERNAME"].ToString();
                    mntm.FirstLevelMemberCount = dsNetworkTree.Tables[i].Rows[0]["CUSR_FULLNAME"].ToString();
                    resultList.Add(mntm);
                }
            }
            ViewBag.MemberCount = 0;

            return PartialView("SearchSponsor", resultList);
        }
        #endregion
    }
}