﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECFBase.Models;
using ECFBase.Components;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Text;

namespace ECFBase.Controllers.Admin
{
    public class AdminShareController : Controller
    {

        #region Company Share
        public ActionResult ComapanyShare()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            var model = new ShareLogModel();
            var MemberPreTrade = MemberDB.GetAllCompanyShare();
            

            foreach (DataRow dr in MemberPreTrade.Tables[0].Rows)
            {
                var Share = new ShareListLogModel();
                Share.ShareValue = float.Parse(dr["CSHARE_RATE"].ToString()).ToString("0.00");
                Share.ShareUnit =  Convert.ToInt32(dr["CSHARE_QUEUE_MEMBER"].ToString()).ToString("###,##0");
                Share.MemberSelling = Convert.ToInt32(dr["CSHARE_SELLING_MEMBER"].ToString()).ToString("###,##0");
                Share.CompanySelling = Convert.ToInt32(dr["CSHARE_SELLING_COMPANY"].ToString()).ToString("###,##0");
                Share.TotalSelling = (Convert.ToInt32(dr["CSHARE_SELLING_MEMBER"].ToString()) + Convert.ToInt32(dr["CSHARE_SELLING_COMPANY"].ToString())).ToString("###,##0");
                Share.CompanyEatUnit = Convert.ToInt32(dr["CompanyUnit"].ToString()).ToString("###,##0");

                model.MemberPreTreadeList.Add(Share);
            }

            return PartialView("CompanyShare", model);
        }
        #endregion

        #region Buy Share Queue
        public ActionResult BuyingQueue()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            var model = new ShareLogModel();
            var MemberPreTrade = MemberDB.GetAllBuyingQueue();

            float TotalAmount = 0;

            int TotalUnit = 0;

            foreach (DataRow dr in MemberPreTrade.Tables[0].Rows)
            {
                var Share = new ShareListLogModel();
                Share.Username = dr["CUSR_USERNAME"].ToString();
                Share.Amount = float.Parse(dr["CSHRBQ_AMOUNT"].ToString()).ToString("n2");
                Share.ShareUnit = Convert.ToInt32(dr["CSHRBQ_UNIT"].ToString()).ToString("###,##0");
                Share.Type = dr["CSHRBQ_TYPE"].ToString();
                Share.Option = dr["CSHRBQ_STATUS"].ToString();
                //Share.PlaceOrderDate = dr["CSHRBQ_CREATEDON"].ToString();
                Share.PlaceOrderDate = Convert.ToDateTime(dr["CSHRBQ_CREATEDON"].ToString()).ToString("dd/MM/yyyy hh:mm:ss tt");

                Share.ID = dr["CSHRBQ_ORDERID"].ToString();

                TotalAmount = TotalAmount + float.Parse(dr["CSHRBQ_AMOUNT"].ToString());
                TotalUnit = TotalUnit + Convert.ToInt32(dr["CSHRBQ_UNIT"].ToString());

                model.MemberPreTreadeList.Add(Share);
            }

            model.Total = TotalAmount.ToString("n2");
            model.TotalUnit = TotalUnit.ToString("###,##0");

            return PartialView("BuyingQueue", model);
        }

        #endregion

        #region Sell Share Queue
        public ActionResult SellingQueue(string Rate = "0")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            if (string.IsNullOrEmpty(Rate))
            {
                Rate = "0";
            }

            float Result = 0;
            if (!float.TryParse(Rate, out Result))
            {
                Response.Write("Invalid Rate");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            var model = new ShareLogModel();

            var MemberPreTrade = MemberDB.GetAllSellingQueue(Rate);
            var ShareDetail = MemberShareDB.GetShareDetail("BVA");

            int TotalOriUnit = 0;
            int TotalBalanceUnit = 0;
            int SoldUnit = 0;         


            foreach (DataRow dr in MemberPreTrade.Tables[0].Rows)
            {
                var Share = new ShareListLogModel();
                Share.Username = dr["CUSR_USERNAME"].ToString();
                Share.OriUnit = Convert.ToInt32(dr["CSHRSQ_ORIUNIT"].ToString()).ToString("###,##0");
                Share.ShareUnit = (Convert.ToInt32(dr["CSHRSQ_UNIT"].ToString())).ToString("###,##0");
                Share.Rate = float.Parse(dr["CSHRSQ_RATE"].ToString()).ToString("n2");
                Share.PlaceOrderDate = Convert.ToDateTime(dr["CSHRSQ_CREATEDON"].ToString()).ToString("dd/MM/yyyy hh:mm:ss tt");
                Share.ID = dr["CSHRSQ_ORDERID"].ToString();
                Share.Status = dr["CSHRSQ_STATUS"].ToString();
                TotalOriUnit = TotalOriUnit + Convert.ToInt32(dr["CSHRSQ_ORIUNIT"].ToString());
                TotalBalanceUnit = TotalBalanceUnit +  Convert.ToInt32(dr["CSHRSQ_UNIT"].ToString());

                model.MemberPreTreadeList.Add(Share);
            }
            
            TotalBalanceUnit = TotalBalanceUnit + SoldUnit;

            model.TotalOriUnit = TotalOriUnit.ToString("###,##0");
            model.TotalBalanceUnit = TotalBalanceUnit.ToString("###,##0");

            return PartialView("SellingQueue", model);
        }

        #endregion

        #region Member Efund Listing
        public ActionResult MemberEfundListing()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            var model = new ShareLogModel();
            var MemberPreTrade = MemberDB.GetMemberEfund();

            foreach (DataRow dr in MemberPreTrade.Tables[0].Rows)
            {
                var Share = new ShareListLogModel();
                Share.Username = dr["CUSR_USERNAME"].ToString();
                Share.Fullname = dr["CUSR_FULLNAME"].ToString();
                Share.Ranking = dr["CRANKSET_NAME"].ToString();
                Share.Package = dr["CPKG_CODE"].ToString();
                Share.TotalIn = Convert.ToInt32(dr["TOTALIN"].ToString()).ToString("###,##0");
                Share.TotalOut = Convert.ToInt32(dr["TOTALOUT"].ToString()).ToString("###,##0");
                Share.MaxUnit = Convert.ToInt32(dr["CSHRM_MAX_EFUND_UNIT"].ToString()).ToString("###,##0");
                Share.ShareUnit = Convert.ToInt32(dr["CSHRM_ASHARE_BALANCE"].ToString()).ToString("###,##0");                       
                Share.PlaceOrderDate = Convert.ToDateTime(dr["CMEM_CREATEDON"].ToString()).ToString("dd/MM/yyyy hh:mm:ss tt");

                model.MemberPreTreadeList.Add(Share);
            }
            

            return PartialView("MemberEfundListing", model);
        }

        public void ExportMemberEfundList()
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=MemberEfundList.csv");
            Response.ContentType = "application/octet-stream";
            StreamWriter sw = new StreamWriter(Response.OutputStream, Encoding.UTF8);
            sw.Write(PrintMemberEfundList());
            sw.Close();
            Response.End();
        }

        private string PrintMemberEfundList()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("Date Joined , Member ID ,Full Name , Ranking , Package ,Max Unit, Efund Units , Total In , Total Out "));
            string message = string.Empty;
            DataSet dsMemberlist = new DataSet();
            dsMemberlist = MemberDB.GetMemberEfund();

            foreach (DataRow dr in dsMemberlist.Tables[0].Rows)
            {

                sb.AppendLine(
                               Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt") + "," +
                              dr["CUSR_USERNAME"].ToString() + "," +
                              dr["CUSR_FULLNAME"].ToString() + "," +
                              dr["CRANKSET_NAME"].ToString() + "," +
                              dr["CPKG_CODE"].ToString() + "," +
                              dr["CSHRM_MAX_EFUND_UNIT"].ToString() + "," +
                              dr["CSHRM_ASHARE_BALANCE"].ToString() + "," +
                              dr["TOTALIN"].ToString() + "," +
                              dr["TOTALOUT"].ToString());
            }

            return sb.ToString();
        }
        #endregion

        #region Share General Settings

        public ActionResult ShareGeneralSettings()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            ParameterModel pm = new ParameterModel();

            int ok;
            string msg;
            DataSet dsParameter = ParameterDB.GetAllShareParameter(out ok, out msg);
            foreach (DataRow dr in dsParameter.Tables[0].Rows)
            {
                var gs = new GeneralSettingModel();
                gs.ParameterName = dr["CSHRST_CODE"].ToString();
                gs.ParameterDescription = dr["CSHRST_NAME"].ToString();
                gs.ParameterUOM = dr["CSHRST_TYPE"].ToString();
                gs.ParameterStringVal = dr["CSHRST_VALUE"].ToString();
                gs.ParameterMoneyVal = dr["CSHRST_AMOUNT"].ToString();
                gs.ParameterIntVal = dr["CSHRST_UNIT"].ToString();
                pm.SettingList.Add(gs);
            }

            return PartialView("ShareGeneralSettings", pm);

        }

        [HttpPost]
        public ActionResult ShareGeneralSettingsMethod(ParameterModel pm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { });
                }

                int ok;
                string msg;
                for (int i = 0; i < pm.SettingList.Count; i++)
                {
                    if (pm.SettingList[i].ParameterName == "paraNameTableRows" && pm.SettingList[i].ParameterFloatVal == 0)

                    {
                        Response.Write(Resources.OneForAll.OneForAll.WarningTableRow);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                    ParameterDB.UpdateParameter(pm.SettingList[i].ParameterName, "", pm.SettingList[i].ParameterFloatVal, Session["Admin"].ToString(), out ok, out msg);
                }
                return ShareGeneralSettings();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Share Rate Selling Detail

        public ActionResult ShareRateSellingDetail(string Rate = "0" )
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

                      

            if (string.IsNullOrEmpty(Rate))
            {
                Rate = "0";
            }

            if (Rate != "0")
            {
                float CheckRate = 0;
                if (!float.TryParse(Rate, out CheckRate))
                {
                    Response.Write("Invalid Rate");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
            }
                  
            if (Rate == "0")
            {
                var ShareDetail = MemberShareDB.GetShareSellingDetail(Rate);

                Rate = float.Parse(ShareDetail.Tables[0].Rows[0]["CSHARE_RATE"].ToString()).ToString("n2");
            }

            var model = new ShareLogModel();

            var MemberPreTrade = MemberDB.GetShareSellingDetailByRate(Rate);

            int Unit = 0;

            foreach (DataRow dr in MemberPreTrade.Tables[0].Rows)
            {
                var Share = new ShareListLogModel();
                Share.Date = dr["CSHRT_CREATEDON"].ToString();

                Share.Username = dr["CUSR_USERNAME"].ToString();
                Share.Name = dr["CUSR_FULLNAME"].ToString();
                Share.Type = dr["CSHRT_TYPE"].ToString();
                Share.Rate = float.Parse(dr["CSHRT_RATE"].ToString()).ToString("0.00");
                Share.ShareUnit = Convert.ToInt32(dr["CSHRT_UNIT"].ToString()).ToString("###,##0");
                Share.Sell = dr["SELLER"].ToString();
                Unit = Unit + Convert.ToInt32(dr["CSHRT_UNIT"].ToString());
                model.MemberPreTreadeList.Add(Share);
            }

            model.TotalUnit = Unit.ToString("###,##0");
            model.ShareRate = Rate;

            return PartialView("ShareRateSellingDetail", model);
        }

        public void ExportShareRateSellingDetail(string Rate = "0", string DateFrom = "0", string DateTo = "0")
        {
            DateTime From = new DateTime();
            DateTime To = new DateTime();
            string Title = string.Empty;
            Title = "Share_Selling_Detail.csv";
            if (string.IsNullOrEmpty(Rate))
            {
                Rate = "0";
            }

            if (DateFrom != "0")
            {
                From = Convert.ToDateTime(DateFrom);
            }
            else
            {
                From = Convert.ToDateTime("01/01/2018");
            }

            if (DateTo != "0")
            {
                To = Convert.ToDateTime(DateTo);
            }
            else
            {
                To = Convert.ToDateTime("01/01/2118");
            }

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=" + Title + "");
            Response.ContentType = "application/octet-stream";
            StreamWriter sw = new StreamWriter(Response.OutputStream, Encoding.UTF8);
            sw.Write(PrintShareRateSellingDetail(Rate, From, To));
            sw.Close();
            Response.End();
        }

        private string PrintShareRateSellingDetail(string Rate , DateTime DateFrom, DateTime DateTo)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("Date,User ID,Name,Type,Rate,Unit "));
            
            DataSet dsShareSellingDetail = new DataSet();

            dsShareSellingDetail = MemberDB.GetShareSellingDetailByRate(Rate);
            foreach (DataRow dr in dsShareSellingDetail.Tables[0].Rows)
                {
                    sb.AppendLine(
                                    Convert.ToDateTime(dr["CSHRMD_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt") + "," +
                                   dr["CUSR_USERNAME"].ToString() + "," +
                                   dr["CUSR_FULLNAME"].ToString() + "," +
                                   dr["CSHRMD_TYPE"].ToString() + "," +
                                   float.Parse(dr["CSHRMD_RATE"].ToString()).ToString("n2") + "," +
                                   Convert.ToInt32(dr["CSHRMD_UNIT"].ToString()).ToString() 
                               
                                );
                }
           

            return sb.ToString();
        }

        #endregion

        #region Manual Buy Share
        public ActionResult ManualBuyShare()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }
            var model = new ShareLogModel();

            var ShareDetail = MemberShareDB.GetAdminManualBuyShareRelatedInformation("BVA");

            model.TodayPrices = Convert.ToDecimal(ShareDetail.Tables[0].Rows[0]["CSHARE_RATE"].ToString()).ToString("#.00");
            model.TotalRateUp = ShareDetail.Tables[5].Rows[0]["TOTALRATEUP"].ToString();
            model.UnitLeft = (Convert.ToInt32(ShareDetail.Tables[4].Rows[0]["CSHRMD_UNIT"].ToString())).ToString("###,###,##0");


            return PartialView("ManualBuyShare", model);
        }

        [HttpPost]
        public ActionResult ManualBuyShareMethod(string Quantity = "0")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                if (string.IsNullOrEmpty(Quantity))
                {
                    Response.Write("Please Key In Quantity To Buy");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (Convert.ToInt32(Quantity) > 50000)
                {
                    Response.Write("Max Quantity are 50,000.");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                
                int TotalRateUp = 0;
                int UnitLeft = 0;
                int Maxunit = 0;
                string Rate = string.Empty;

                var dbs = MemberDB.GetParameterBasedOnName("Share Rate Per Day");
                int ParaShareRatePerDay = 0;

                ParaShareRatePerDay = Convert.ToInt32(dbs.Tables[0].Rows[0]["CPARA_FLOATVALUE"].ToString());

                var ShareDetail = MemberShareDB.GetAdminManualBuyShareRelatedInformation("BVA");

                TotalRateUp = Convert.ToInt32(ShareDetail.Tables[5].Rows[0]["TOTALRATEUP"].ToString());
                UnitLeft = Convert.ToInt32(ShareDetail.Tables[4].Rows[0]["CSHRMD_UNIT"].ToString());

                if (TotalRateUp == ParaShareRatePerDay)
                {
                    Maxunit = 50000 - UnitLeft;

                    if (Convert.ToInt32(Quantity) > Maxunit)
                    {
                        Response.Write("Cant Key in more than" + Maxunit.ToString("###,##0"));
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                    if (UnitLeft >= 50000)
                    {
                        Response.Write("Today already reach "+ ParaShareRatePerDay.ToString() + " rate limit.");
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                }

                Rate = Convert.ToDecimal(ShareDetail.Tables[0].Rows[0]["CSHARE_RATE"].ToString()).ToString("#.00");


                MemberShareDB.BuyEFundAdmin(Session["Admin"].ToString(), Quantity, Rate);

                return ManualBuyShare();
            }
            catch (Exception e)
            {
                Response.Write("1|" + e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Oversubscribed Setting
        public ActionResult OversubscribedSetting()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }
            var model = new ShareLogModel();


            var ShareDetail = MemberShareDB.GetShareDetail("BVA");

            if (ShareDetail.Tables[8].Rows.Count == 0)
            {
                model.Oversubscribed = "0";
            }
            else
            {
                model.Oversubscribed = (Convert.ToInt32(ShareDetail.Tables[8].Rows[0]["CSHARE_AMOUNT"].ToString())).ToString("###,###,##0");
            }


            return PartialView("OversubscribedSetting", model);
        }

        [HttpPost]
        public ActionResult OversubscribedSettingMethod(string Unit = "0")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { });
                }

                if (string.IsNullOrEmpty(Unit))
                {
                    Response.Write("Please Key In Unit");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                MemberShareDB.Oversubscribed(Session["Admin"].ToString(), Unit);

                return OversubscribedSetting();
            }
            catch (Exception e)
            {
                Response.Write("1|" + e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Block Share On User
        public ActionResult BlockShareMemberList(int selectedPage = 1, string searchMemberList = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            int status = 0;
            string message = string.Empty;
            int pages = 0;
            string languageCode = Session["LanguageChosen"].ToString();

            PaginationMobileAgent model = new PaginationMobileAgent();
            DataSet dsAgents = MemberDB.GetAllBlockShareMember(searchMemberList, selectedPage, languageCode, out pages, out status, out message);

            selectedPage = ConstructPageList(selectedPage, pages, model);


            foreach (DataRow dr in dsAgents.Tables[0].Rows)
            {
                var mobileAgent = new MobileAgent();
                mobileAgent.Number = dr["rownumber"].ToString();
                mobileAgent.AgentID = dr["CUSR_USERNAME"].ToString();
                mobileAgent.FullName = dr["CUSR_FULLNAME"].ToString();
                mobileAgent.JoinedDate = Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToString("dd/MM/yyyy");
                mobileAgent.Country = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                mobileAgent.AdminApproved = dr["CMEM_APPROVEBY"].ToString();
                mobileAgent.DateApproved = Convert.ToDateTime(dr["CMEM_UPDATEDON"]).ToString("dd/MM/yyyy");
                model.MobileList.Add(mobileAgent);
            }

            return PartialView("BlockMemberShareList", model);
        }

        public ActionResult DisqualifyBlockShareMember(string username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            MemberDB.DisqualifiedBlockShareMember(username, Session["Admin"].ToString());

            return BlockShareMemberList(1, "");
        }

        public ActionResult AppointBlockShareMember(string username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            int ok = 0;
            string msg = "";

            var ds = MemberDB.GetMemberByUsername(username, out ok, out msg);

            if (ds.Tables[0].Rows.Count == 0)
            {
                Response.Write("Invalid Username");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            var dss = MemberDB.GetMemberShareListBlockByMember(username);

            if (dss.Tables[0].Rows.Count != 0)
            {
                Response.Write("Username already exist in the list");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            

            MemberDB.AppointBlockShareMember(username, Session["Admin"].ToString());

            return BlockShareMemberList();
        }

        private int ConstructPageList(int selectedPage, int pages, PaginationMobileAgent model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }
        #endregion

        #region Company Member Share Listing
        public ActionResult CompanyShareMemberList(int selectedPage = 1, string searchMemberList = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            int status = 0;
            string message = string.Empty;
            int pages = 0;
            string languageCode = Session["LanguageChosen"].ToString();

            PaginationMobileAgent model = new PaginationMobileAgent();
            DataSet dsAgents = MemberDB.GetAllCompanyShareMember(searchMemberList, selectedPage, languageCode, out pages, out status, out message);

            selectedPage = ConstructPageList(selectedPage, pages, model);


            foreach (DataRow dr in dsAgents.Tables[0].Rows)
            {
                var mobileAgent = new MobileAgent();
                mobileAgent.Number = dr["rownumber"].ToString();
                mobileAgent.AgentID = dr["CUSR_USERNAME"].ToString();
                mobileAgent.FullName = dr["CUSR_FULLNAME"].ToString();
                mobileAgent.JoinedDate = Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToString("dd/MM/yyyy");
                mobileAgent.Country = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                mobileAgent.AdminApproved = dr["CMEM_APPROVEBY"].ToString();
                mobileAgent.DateApproved = Convert.ToDateTime(dr["CMEM_UPDATEDON"]).ToString("dd/MM/yyyy");
                model.MobileList.Add(mobileAgent);
            }

            return PartialView("CompanyMemberShareList", model);
        }

        public ActionResult DisqualifyCompanyShareMember(string username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            MemberDB.DisqualifiedCompanyShareMember(username, Session["Admin"].ToString());

            return CompanyShareMemberList(1, "");
        }

        public ActionResult AppointCompanyShareMember(string username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            int ok = 0;
            string msg = "";

            var ds = MemberDB.GetMemberByUsername(username, out ok, out msg);

            if (ds.Tables[0].Rows.Count == 0)
            {
                Response.Write("Invalid Username");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            var dss = MemberDB.GetMemberShareListCompanyByMember(username);

            if (dss.Tables[0].Rows.Count != 0)
            {
                Response.Write("Username already exist in the list");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }


            MemberDB.AppointCompanyShareMember(username, Session["Admin"].ToString());

            return CompanyShareMemberList();
        }
        #endregion

        #region Company Acc Sell Share
        #region AdminSearchUser
        public ActionResult CompanyShareAcc(string Type)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

           

            ViewBag.Type = Type;
            UsernameSearchModel model = new UsernameSearchModel();
            return PartialView("CompanyShareAcc", model);
        }

        public ActionResult CompanyShareAccSearchUser(string Username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            int ok = 0;
            string msg = string.Empty;

            DataSet dsUser = MemberDB.GetMemberByUsername(Username, out ok, out msg);

            if (dsUser.Tables[0].Rows.Count == 0)
            {
                Response.Write("Invalid Member ID");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            var dss = MemberDB.GetMemberShareListCompanyByMember(Username);

            if (dss.Tables[0].Rows.Count == 0)
            {
                Response.Write("Not Appointed Company Account for This Member ID");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            return RedirectToAction("CompanyAccountSellShare", "AdminShare", new { Username = Username });
        }
        #endregion

        public ActionResult CompanyAccountSellShare(string Username = "0")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }
            var model = new ShareLogModel();
                 
            var ShareDetail = MemberShareDB.GetShareDetail(Username);

            model.MemberID = Username;

            #region Trading Market
            model.TodayPrices = Convert.ToDecimal(ShareDetail.Tables[0].Rows[0]["CSHARE_RATE"].ToString()).ToString("#.00");
            model.OpeningPrices = Convert.ToDecimal(ShareDetail.Tables[0].Rows[0]["CSHARE_RATE"].ToString()).ToString("#.00");
            model.HigherPrices = Convert.ToDecimal(ShareDetail.Tables[0].Rows[0]["CSHARE_RATE"].ToString()).ToString("#.00");
            model.LowerPrices = Convert.ToDecimal(ShareDetail.Tables[1].Rows[0]["CSHARE_RATE"].ToString()).ToString("#.00");
            model.UnitLeft = (Convert.ToInt32(ShareDetail.Tables[0].Rows[0]["CSHARE_TOTAL"].ToString())).ToString("###,###,##0");
            model.MemberShare = (Convert.ToInt32(ShareDetail.Tables[2].Rows[0]["CSHRM_ASHARE_BALANCE"].ToString())).ToString("###,###,##0");
            model.TotalUnit = (Convert.ToInt32(ShareDetail.Tables[2].Rows[0]["CSHRM_ASHARE_BALANCE"].ToString())).ToString("###,###,##0");
            #endregion

            #region Sell E-Fund
            List<RateModel> ListRate = Misc.GetTheOpenRate();
            model.TheRate = from c in ListRate
                            select new SelectListItem
                            {
                                Text = c.RateString,
                                Value = c.TheRate
                            };

            model.SelectedRate = Convert.ToDecimal(ShareDetail.Tables[0].Rows[0]["CSHARE_RATE"].ToString()).ToString("#.00");
            #endregion

            #region Sell E-Fund Queue List
            var dsSellingShareQueue = MemberShareDB.GetAllSellingQueueByMember(Username);

            foreach (DataRow dr in dsSellingShareQueue.Tables[0].Rows)
            {
                var ShareLog = new ShareListLogModel();
                ShareLog.ID = dr["CSHRSQ_ORDERID"].ToString();
                ShareLog.PlaceOrderDate = dr["CSHRSQ_CREATEDON"].ToString();
                ShareLog.Quantity = Convert.ToInt32(dr["CSHRSQ_UNIT"].ToString()).ToString("###,##0");
                int BALANCE = Convert.ToInt32(dr["CSHRSQ_ORIUNIT"].ToString()) - Convert.ToInt32(dr["CSHRSQ_UNIT"].ToString());
                ShareLog.SellOrderQuantity = BALANCE.ToString("###,##0");
                ShareLog.ShareValue = float.Parse(dr["CSHRSQ_SELLINGVALUE"].ToString()).ToString("n2");
                ShareLog.DataID = dr["CSHRSQ_ID"].ToString();
                model.MemberPreTreadeList.Add(ShareLog);
            }
            #endregion            

            return PartialView("CompanyAccountSellShare", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CompanyAccountSellShareMethod(string Username , string KeyinUnit, string Rate)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }          

            #region DB
            var ShareDetail = MemberShareDB.GetShareDetail(Username);

            if (ShareDetail.Tables[6].Rows.Count != 0)
            {
                Response.Write("Sell Share Not Available on this moment");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            var RateDetail = MemberShareDB.GetRateDetail(Rate);
         
            #endregion

            #region Check Condition    

            int ShareAvailableUnit = Convert.ToInt32(RateDetail.Tables[0].Rows[0]["CSHARE_QUEUE_MEMBER"].ToString());
            
            int TotalMemberUnit = 0;
            int AvailableUnitToQueue = 0;

            TotalMemberUnit = Convert.ToInt32(KeyinUnit) + ShareAvailableUnit;

            if (TotalMemberUnit > 50000)
            {
                AvailableUnitToQueue = 50000 - ShareAvailableUnit;

                Response.Write("Invalid Trading Unit , Unit For This Rate Left " + AvailableUnitToQueue.ToString("###,##0"));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            #endregion

            #region Sell E-Fund

            MemberShareDB.SellEFundAdmin(Username, KeyinUnit, Rate);

            #endregion
            
            return CompanyAccountSellShare(Username);
        }

        public ActionResult CompanyAccCancelPlaceOrder(string ID , string Username)
        {
            MemberDB.CompanyAccCancelPlaceShare(ID);

            return CompanyAccountSellShare(Username);
        }

        #endregion

    }
}