﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECFBase.Models;
using ECFBase.Components;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using ECFBase.Resources;

namespace ECFBase.Controllers.Admin
{
    public class AdminWalletController : Controller
    {
        #region WalletAdjustment
        public ActionResult WalletAdjustment(string Username, string Type)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }

            int ok = 0;
            string msg = "";
            WalletAdjustmentModel model = new WalletAdjustmentModel();
            model.Username = Username;
            var dataset = MemberDB.GetMemberByUsername(Username, out ok, out msg);
            if (dataset.Tables[0].Rows.Count == 0)
            {
                Response.Write(Resources.OneForAll.OneForAll.msgInvalidUser1);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            DataRow memberDetails = dataset.Tables[0].Rows[0];

            model.Type = Type;

            if (Type == ProjectStaticString.RegisterwalletTopUp)
            {
                model.TypeResource = Resources.OneForAll.OneForAll.mnuRegisterTopUp;
                model.WalletBalanceTypeResource = "Register Point Balance";
                model.Nickname = memberDetails["CUSRINFO_NICKNAME"].ToString();
                model.CurrentWallet = float.Parse(memberDetails["CMEM_REGISTERWALLET"].ToString());
            }
            else if (Type == ProjectStaticString.CashwalletTopUp)
            {
                model.TypeResource = Resources.OneForAll.OneForAll.mnuCashTopUp;
                model.WalletBalanceTypeResource = "Cash Point Balance";
                model.Nickname = memberDetails["CUSRINFO_NICKNAME"].ToString();
                model.CurrentWallet = float.Parse(memberDetails["CMEM_CASHWALLET"].ToString());
            }
            else if (Type == ProjectStaticString.GoldPointTopUp)
            {
                model.TypeResource = "Gold Point TopUp";
                model.WalletBalanceTypeResource = "Gold Point Balance";
                model.Nickname = memberDetails["CUSRINFO_NICKNAME"].ToString();
                model.CurrentWallet = float.Parse(memberDetails["CMEM_GOLDPOINT"].ToString());
            }
            else if (Type == ProjectStaticString.MultiPointTopUp)
            {
                model.TypeResource = "Multi Point TopUp";
                model.WalletBalanceTypeResource = "Multi Point Balance";
                model.Nickname = memberDetails["CUSRINFO_NICKNAME"].ToString();
                model.CurrentWallet = float.Parse(memberDetails["CMEM_MULTIPOINT"].ToString());
            }
            else if (Type == ProjectStaticString.RPCTopUp)
            {
                model.TypeResource = "Repurchase TopUp";
                model.WalletBalanceTypeResource = "Repurchase Point Balance";
                model.Nickname = memberDetails["CUSRINFO_NICKNAME"].ToString();
                model.CurrentWallet = float.Parse(memberDetails["CMEM_REPURCHASEWALLET"].ToString());
            }
            else if (Type == ProjectStaticString.RMPTopUp)
            {
                model.TypeResource = "RMP TopUp";
                model.WalletBalanceTypeResource = "Register MP Balance";
                model.Nickname = memberDetails["CUSRINFO_NICKNAME"].ToString();
                model.CurrentWallet = float.Parse(memberDetails["CMEM_RMPWALLET"].ToString());
            }
            else if (Type == ProjectStaticString.ICOTopUp)
            {
                model.TypeResource = "ICO TopUp";
                model.WalletBalanceTypeResource = "ICO Point Balance";
                model.Nickname = memberDetails["CUSRINFO_NICKNAME"].ToString();
                model.CurrentWallet = float.Parse(memberDetails["CMEM_ICOWALLET"].ToString());
            }
            else if (Type == ProjectStaticString.CompanyTopUp)
            {
                model.TypeResource = "CRP TopUp";
                model.WalletBalanceTypeResource = "Company Register Point Balance";
                model.Nickname = memberDetails["CUSRINFO_NICKNAME"].ToString();
                model.CurrentWallet = float.Parse(memberDetails["CMEM_COMPANYWALLET"].ToString());
            }
            else if (Type == ProjectStaticString.TPTopUp)
            {
                model.TypeResource = "TP TopUp";
                model.WalletBalanceTypeResource = "Travel Point Balance";
                model.Nickname = memberDetails["CUSRINFO_NICKNAME"].ToString();
                model.CurrentWallet = float.Parse(memberDetails["CMEM_TREAVELWALLET"].ToString());
            }

            model.SelectedCountry = memberDetails["CCOUNTRY_CODE"].ToString();

            #region Transaction type
            string add = Resources.OneForAll.OneForAll.btnAdd;
            string deduct = Resources.OneForAll.OneForAll.cntranDeduct;
            List<string> typeList = new List<string>();
            typeList.Add(add);
            typeList.Add(deduct);

            model.Transaction = from c in typeList
                                select new SelectListItem
                                {
                                    Selected = false,
                                    Text = c.ToString(),
                                    Value = (c.ToString() == add) ? "TOPUP" : "DED" //this is for cashname
                                };

            #endregion

            #region PaymentMode
            List<string> modelist = new List<string>();
            var ds = MemberDB.GetAllPaymentMode(out ok, out msg);
            foreach (DataTable table in ds.Tables)
            {
                foreach (DataRow dr in table.Rows)
                {
                    string mode = dr["CPAYMD_TYPE"].ToString();
                    modelist.Add(mode);
                }
            }

            model.paymentmode = from c in modelist
                                select new SelectListItem
                                {
                                    Selected = false,
                                    Text = c.ToString(),
                                    Value = (c.ToString())
                                };
            #endregion

            return PartialView("WalletAdjustment", model);
        }

        [HttpPost]
        public JsonResult FindBank(string CountryCode)
        {
            string LanguageCode = Session["LanguageChosen"].ToString();

            var result = GetBankList(LanguageCode, CountryCode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public static IEnumerable<SelectListItem> GetBankList(string LanguageCode, string CountryCode)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            int ok = 0;
            string message = "";
            List<BankModel> banks = Misc.GetAllBankByCountry(LanguageCode, CountryCode, ref ok, ref message);

            var result = from c in banks
                         select new SelectListItem
                         {
                             Selected = false,
                             Text = c.BankName,
                             Value = c.BankCode
                         };
            return result.OrderBy(m => m.Value).ToList();
        }

        [HttpPost]
        public ActionResult WalletAdjustmentMethod(WalletAdjustmentModel walletMem)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                int ok = 0;
                string msg = "";
                string abc = walletMem.selectedmode;
                if (walletMem.Remark == "")
                {
                    walletMem.Remark = "";
                }
                if (walletMem.TransactionWallet == 0)
                {
                    Response.Write("Please key in amount to be adjusted");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (walletMem.SelectedTransaction == "DED")
                {
                    walletMem.TransactionWallet = 0 - walletMem.TransactionWallet;
                }

                if (walletMem.Type == ProjectStaticString.RegisterwalletTopUp)
                {
                    AdminWalletDB.WalletAdjustment("SP_WalletRegisterAdjustment", walletMem.Username, walletMem.TransactionWallet, walletMem.SelectedTransaction, walletMem.Remark, walletMem.SelectedCountry, walletMem.Type, Session["Admin"].ToString(), walletMem.selectedmode, out ok, out msg);
                }
                else if (walletMem.Type == ProjectStaticString.CashwalletTopUp)
                {
                    AdminWalletDB.WalletAdjustment("SP_WalletCashAdjustment", walletMem.Username, walletMem.TransactionWallet, walletMem.SelectedTransaction, walletMem.Remark, walletMem.SelectedCountry, walletMem.Type, Session["Admin"].ToString(), walletMem.selectedmode, out ok, out msg);
                }
                else if (walletMem.Type == ProjectStaticString.GoldPointTopUp)
                {
                    AdminWalletDB.WalletAdjustment("SP_WalletGoldPointAdjustment", walletMem.Username, walletMem.TransactionWallet, walletMem.SelectedTransaction, walletMem.Remark, walletMem.SelectedCountry, walletMem.Type, Session["Admin"].ToString(), walletMem.selectedmode, out ok, out msg);
                }
                else if (walletMem.Type == ProjectStaticString.MultiPointTopUp)
                {
                    AdminWalletDB.WalletAdjustment("SP_WalletMultiPointAdjustment", walletMem.Username, walletMem.TransactionWallet, walletMem.SelectedTransaction, walletMem.Remark, walletMem.SelectedCountry, walletMem.Type, Session["Admin"].ToString(), walletMem.selectedmode, out ok, out msg);
                }
                else if (walletMem.Type == ProjectStaticString.RPCTopUp)
                {
                    AdminWalletDB.WalletAdjustment("SP_WalletRepurchaseAdjustment", walletMem.Username, walletMem.TransactionWallet, walletMem.SelectedTransaction, walletMem.Remark, walletMem.SelectedCountry, walletMem.Type, Session["Admin"].ToString(), walletMem.selectedmode, out ok, out msg);
                }
                else if (walletMem.Type == ProjectStaticString.RMPTopUp)
                {
                    AdminWalletDB.WalletAdjustment("SP_WalletRMPAdjustment", walletMem.Username, walletMem.TransactionWallet, walletMem.SelectedTransaction, walletMem.Remark, walletMem.SelectedCountry, walletMem.Type, Session["Admin"].ToString(), walletMem.selectedmode, out ok, out msg);
                }
                else if (walletMem.Type == ProjectStaticString.ICOTopUp)
                {
                    AdminWalletDB.WalletAdjustment("SP_WalletICOAdjustment", walletMem.Username, walletMem.TransactionWallet, walletMem.SelectedTransaction, walletMem.Remark, walletMem.SelectedCountry, walletMem.Type, Session["Admin"].ToString(), walletMem.selectedmode, out ok, out msg);
                }
                else if (walletMem.Type == ProjectStaticString.CompanyTopUp)
                {
                    AdminWalletDB.WalletAdjustment("SP_WalletCompanyAdjustment", walletMem.Username, walletMem.TransactionWallet, walletMem.SelectedTransaction, walletMem.Remark, walletMem.SelectedCountry, walletMem.Type, Session["Admin"].ToString(), walletMem.selectedmode, out ok, out msg);
                }
                else if (walletMem.Type == ProjectStaticString.TPTopUp)
                {
                    AdminWalletDB.WalletAdjustment("SP_WalletTravelAdjustment", walletMem.Username, walletMem.TransactionWallet, walletMem.SelectedTransaction, walletMem.Remark, walletMem.SelectedCountry, walletMem.Type, Session["Admin"].ToString(), walletMem.selectedmode, out ok, out msg);
                }

                if (ok == -1)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidBank);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (ok == 1)//success
                {
                    if (walletMem.Type == ProjectStaticString.RegisterwalletTopUp)
                    {
                        return ViewRegisterWallet();
                    }
                    else if (walletMem.Type == ProjectStaticString.CashwalletTopUp)
                    {
                        return ViewCashWallet();
                    }
                    else if (walletMem.Type == ProjectStaticString.GoldPointTopUp)
                    {
                        return ViewGoldPointWallet();
                    }
                    else if (walletMem.Type == ProjectStaticString.MultiPointTopUp)
                    {
                        return ViewMultiPointWallet();
                    }
                    else if (walletMem.Type == ProjectStaticString.RPCTopUp)
                    {
                        return ViewRPCWallet();
                    }
                    else if (walletMem.Type == ProjectStaticString.ICOTopUp)
                    {
                        return ViewICOWallet();
                    }
                    else if (walletMem.Type == ProjectStaticString.RMPTopUp)
                    {
                        return ViewRMPWallet();
                    }
                    else if (walletMem.Type == ProjectStaticString.CompanyTopUp)
                    {
                        return ViewCompanyWallet();
                    }
                    else if (walletMem.Type == ProjectStaticString.TPTopUp)
                    {
                        return ViewTPWallet();
                    }




                    return PartialView("");
                }
                else
                {
                    //throw new System.InvalidOperationException(string.Format("Failed to topup. Error message : [{0}]", msg));
                    throw new System.InvalidOperationException(string.Format(Resources.OneForAll.OneForAll.msgFailedtoTopUp, msg));
                }
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Withdrawal

        #region CashWallet
        public ActionResult WithdrawalCashLog(int selectedPage = 1 , string Username = "0" , string Country = "0")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                int pages = 0;
                var model = new PaginationWalletLogModel();

                var dsCashWalletLog = new DataSet();
                dsCashWalletLog = AdminWalletDB.GetAllCashWithdrawalLog(selectedPage, Username , Country, out pages);

                selectedPage = ConstructPageList(selectedPage, pages, model);

                if (dsCashWalletLog.Tables.Count != 0)
                {
                    foreach (DataRow dr in dsCashWalletLog.Tables[0].Rows)
                    {
                        WalletLogModel walletLog = new WalletLogModel();
                        walletLog.No = dr["rownumber"].ToString();
                        walletLog.ID = int.Parse(dr["CCSHWL_ID"].ToString());
                        walletLog.Username = dr["CUSR_USERNAME"].ToString();
                        walletLog.IsChecked = false;

                        walletLog.CashOut = float.Parse(dr["CCSHWL_CASHOUT"].ToString());
                        walletLog.CountryName = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                        //walletLog.Bank = dr["CBANK_CODE"].ToString();
                        walletLog.Bank = dr["CCSHWL_BANKNAME"].ToString();
                        walletLog.AccNo = dr["CCSHWL_USERBANKACC"].ToString();
                        walletLog.FullName = dr["CUSR_FULLNAME"].ToString();
                        walletLog.TranState = Misc.GetWithdrawalStatus(dr["CCSHWL_TRANSTATE"].ToString());
                        walletLog.CreatedDate = Convert.ToDateTime(dr["CCSHWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                        walletLog.TranState = Convert.ToDateTime(dr["CCSHWL_UPDATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                        walletLog.refference = dr["REF_CONTENT"].ToString();
                        walletLog.refferenceID = dr["REF_ID"].ToString();
                        string Sta = dr["CCSHWL_TRANSTATE"].ToString();

                        if (Sta == "1")
                        {
                            walletLog.status = "Approve";
                        }
                        else if (Sta == "-1")
                        {
                            walletLog.status = "Refund";
                        }
                        
                        #region Mailing Address

                        walletLog.Address = dr["CCSHWL_MAILINGADDRESS"].ToString();
                       
                        #endregion

                        #region Bank Info
                        
                        walletLog.BranchName = dr["CCSHWL_BRANCHNANE"].ToString();
                        walletLog.BranchAdd = dr["CCSHWL_BANKADDRESS"].ToString();
                        walletLog.BranchSwiftCode = dr["CCSHWL_BANKSWIFTCODE"].ToString();
                       
                        walletLog.SelectedBank = dr["CBANK_CODE"].ToString();

                        #endregion



                        model.WalletLogList.Add(walletLog);
                    }
                }

                return PartialView("WithdrawalCashLog", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult WithdrawCashList(int selectedPage = 1,string searchName= "0", string selectedCountry = "0")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                int pages = 0;
                var model = new PaginationWalletLogModel();
                model.TypeOfWallet = "Cash";


                //select the withdrawal date 
                DataSet dsCashWalletLog = new DataSet();

                dsCashWalletLog = AdminWalletDB.GetAllCashWithdrawal(selectedPage, searchName , selectedCountry, out pages);             

                selectedPage = ConstructPageList(selectedPage, pages, model);

                string totalWithdraw = "";
                string totalAdminCharge = "";
                string totalNettWithdraw = "";
                string IC = string.Empty;
                string PreIC = "";
                float TotalAmount = 0;
                int i = 0;

                WalletLogModel walletLog = new WalletLogModel();
                if (dsCashWalletLog.Tables.Count != 0)
                {
                    foreach (DataRow dr in dsCashWalletLog.Tables[0].Rows)
                    {

                        IC = dr["CUSRINFO_IC"].ToString();

                        if (i == 0)
                        {

                        }
                        else
                        {
                            if (PreIC == IC)
                            {
                                walletLog.ShowSubTotal = false;
                                model.WalletLogList.Add(walletLog);
                            }
                            else if (PreIC != IC)
                            {
                                model.WalletLogList.Add(walletLog);
                                WalletLogModel ELog = new WalletLogModel();
                                ELog.TotalAmout = TotalAmount.ToString("n2");
                                ELog.Subtotal = "SubTotal :";
                                ELog.ShowSubTotal = true;
                                model.WalletLogList.Add(ELog);
                                TotalAmount = 0;
                            }
                        }

                        i += 1;

                        walletLog = new WalletLogModel();
                        walletLog.No = dr["rownumber"].ToString();
                        walletLog.ID = int.Parse(dr["CCSHWL_ID"].ToString());
                        walletLog.Username = dr["CUSR_USERNAME"].ToString();
                        walletLog.FullName = dr["CUSR_FULLNAME"].ToString();
                        walletLog.IsChecked = false;
                        walletLog.BankCharges = float.Parse(dr["CCSHWL_APPNUMBER"].ToString());
                        walletLog.NetAmount = float.Parse(dr["CCSHWL_CASHOUT"].ToString());
                        walletLog.CashOut = walletLog.BankCharges + walletLog.NetAmount;
                        string test = dr["SumWithdrawAmt"].ToString();
                        totalWithdraw = double.Parse(dr["SumWithdrawAmt"].ToString()).ToString("n2");
                        walletLog.AdminFee = float.Parse(dr["CCSHWL_APPNUMBER"].ToString());
                        totalAdminCharge = double.Parse(dr["SumAdminFee"].ToString()).ToString("n2");
                        walletLog.NetWithdrawal = walletLog.CashOut - walletLog.AdminFee;
                        totalNettWithdraw = double.Parse(dr["SumNettWithdraw"].ToString()).ToString("n2");
                        walletLog.BranchName = dr["CMEMBANK_BRANCHNAME"] == null ? string.Empty : dr["CMEMBANK_BRANCHNAME"].ToString();
                        walletLog.CountryName = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                        //walletLog.Bank = dr["CBANK_CODE"].ToString();
                        walletLog.Bank = dr["CCSHWL_BANKNAME"].ToString();
                        walletLog.AccNo = dr["CCSHWL_USERBANKACC"].ToString();
                        walletLog.MobileNumber = dr["CUSRINFO_CELLPHONE"].ToString();
                        walletLog.TranState = Misc.GetWithdrawalStatus(dr["CCSHWL_TRANSTATE"].ToString());
                        walletLog.CreatedDate = Convert.ToDateTime(dr["CCSHWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                        walletLog.IC = dr["CUSRINFO_IC"].ToString();
                        TotalAmount += walletLog.BankCharges + walletLog.NetAmount;
                        PreIC = IC;
                        #region Mailing Address

                        walletLog.Address = dr["CCSHWL_MAILINGADDRESS"].ToString();

                        #endregion

                        #region Bank Info

                        walletLog.BranchName = dr["CCSHWL_BRANCHNANE"].ToString();
                        walletLog.BranchAdd = dr["CCSHWL_BANKADDRESS"].ToString();
                        walletLog.BranchSwiftCode = dr["CCSHWL_BANKSWIFTCODE"].ToString();

                        walletLog.SelectedBank = dr["CBANK_CODE"].ToString();

                        #endregion
                        if (i == 50)
                        {

                            model.WalletLogList.Add(walletLog);
                            WalletLogModel ELog = new WalletLogModel();
                            ELog.TotalAmout = TotalAmount.ToString("n2");
                            ELog.Subtotal = "SubTotal :";
                            ELog.ShowSubTotal = true;
                            model.WalletLogList.Add(ELog);
                        }

                        if (dsCashWalletLog.Tables[0].Rows.Count == i)
                        {

                            model.WalletLogList.Add(walletLog);
                            WalletLogModel ELog = new WalletLogModel();
                            ELog.TotalAmout = TotalAmount.ToString("n2");
                            ELog.Subtotal = "SubTotal :";
                            ELog.ShowSubTotal = true;
                            model.WalletLogList.Add(ELog);
                        }
                    }
                }
                ViewBag.totalWithdraw = totalWithdraw;
                ViewBag.totalAdminCharge = totalAdminCharge;
                ViewBag.totalNettWithdraw = totalNettWithdraw;

                //0 Pending
                //1 Approved
                //-1 Refunded
                //-3 Rejected
                List<string> stateList = new List<string>();
                Misc.InsertWithdrawalStatus(ref stateList);
                

                #region Country
                int ok = 0;
                string msg = "";
                string languageCode = Session["LanguageChosen"].ToString();
                List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

                model.Countries = from c in countries
                                  select new SelectListItem
                                  {
                                      Selected = false,
                                      Text = c.CountryName,
                                      Value = c.CountryCode
                                  };
                #endregion

                return PartialView("WithdrawCashList", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ApproveWithdrawal(string ParameterValue)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                if (ParameterValue == "")
                {
                    Response.Write("No Record Found.");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                string[] parameters = ParameterValue.Split(new string[] { "&" }, StringSplitOptions.None);
                List<int> withdrawalListID = new List<int>();

                foreach (string parameter in parameters)
                {
                    string[] value = parameter.Split(new string[] { "=" }, StringSplitOptions.None);

                    //Since i added datelist and statelist, when serialize, this gets to pass in, I want to ignore this
                    if (value[0] == "dateList" || value[0] == "stateList")
                        continue;

                    if (value[0] != "item.IsChecked" && value[0] != "page" && value[0] != "countryList" && value[0] != "dateList" && value[0] != "stateList")
                    {

                        withdrawalListID.Add(int.Parse(value[0]));
                    }
                    //var parameterValue = Convert.ToBoolean(value[1]);
                }

                AdminWalletDB.ApproveBonusWithdrawal(withdrawalListID, Session["Admin"].ToString());

                //if (Request.IsAjaxRequest())
                //{
                //    return Json(new { redirectToUrl = Url.Action("WithdrawBonusList", "AdminWallet") });
                //}

                return WithdrawCashList();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult RejectWithdrawal(string ParameterValue)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                if (ParameterValue == "")
                {
                    Response.Write("No Record Found.");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                string[] parameters = ParameterValue.Split(new string[] { "&" }, StringSplitOptions.None);
                List<int> withdrawalListID = new List<int>();

                foreach (string parameter in parameters)
                {
                    string[] value = parameter.Split(new string[] { "=" }, StringSplitOptions.None);

                    //Since i added datelist and statelist, when serialize, this gets to pass in, I want to ignore this
                    if (value[0] == "dateList" || value[0] == "stateList")
                        continue;

                    if (value[0] != "item.IsChecked" && value[0] != "page" && value[0] != "countryList" && value[0] != "dateList" && value[0] != "stateList")
                    {
                        withdrawalListID.Add(int.Parse(value[0]));
                    }
                    //var parameterValue = Convert.ToBoolean(value[1]);
                }

                AdminWalletDB.RejectBonusWithdrawal(withdrawalListID, Session["Admin"].ToString());

                //if (Request.IsAjaxRequest())
                //{
                //    return Json(new { redirectToUrl = Url.Action("WithdrawBonusList", "AdminWallet") });
                //}

                return WithdrawCashList();
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult RefundWithdrawal(string ParameterValue)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                if (ParameterValue == "")
                {
                    Response.Write("No Record Found.");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                string[] parameters = ParameterValue.Split(new string[] { "&" }, StringSplitOptions.None);
                List<int> withdrawalListID = new List<int>();

                foreach (string parameter in parameters)
                {
                    string[] value = parameter.Split(new string[] { "=" }, StringSplitOptions.None);

                    //Since i added datelist and statelist, when serialize, this gets to pass in, I want to ignore this
                    if (value[0] == "dateList" || value[0] == "stateList")
                        continue;

                    if (value[0] != "item.IsChecked" && value[0] != "page" && value[0] != "countryList" && value[0] != "dateList" && value[0] != "stateList")
                    {
                        withdrawalListID.Add(int.Parse(value[0]));
                    }
                    //var parameterValue = Convert.ToBoolean(value[1]);
                }

                AdminWalletDB.RefundBonusWithdrawal(withdrawalListID, Session["Admin"].ToString());

                //if (Request.IsAjaxRequest())
                //{
                //    return Json(new { redirectToUrl = Url.Action("WithdrawBonusList", "AdminWallet") });
                //}

                return WithdrawCashList();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public void ExportWithdrawalList()
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=CashWithdrawalList.csv");
            Response.ContentType = "application/octet-stream";
            StreamWriter sw = new StreamWriter(Response.OutputStream, Encoding.UTF8);
            string style = @"<style> .text { mso-number-format:\@; } </style> ";
            sw.Write(PrintBonusWithdrawalList());
            sw.Close();
            Response.End();
        }

        private string PrintBonusWithdrawalList()
         {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("Number, Username,Name , Amount(EP),Bank Charges(%) ,	Net Amount(EP) ,	Country of Citizenship, 	Bank Name,  Branch Name, Branch Address, Branch Swift Code,	Bank Account No ,	Bank Account Holder Name, 	Contact Number, 	NRIC / Passport Number, 	Apply Date "));

            DataSet dsCashWalletLog = AdminWalletDB.GetAllWalletCashWithdrawalExport();

            float BankCharges = 0;
            float NetAmount = 0;
            float Total = 0;
            string address = "";
            string Name = "";

            foreach (DataRow dr in dsCashWalletLog.Tables[0].Rows)
            {
                BankCharges =  float.Parse(dr["CCSHWL_APPNUMBER"].ToString());
                NetAmount = float.Parse(dr["CCSHWL_CASHOUT"].ToString());
                Name = dr["CUSR_FULLNAME"].ToString();
                Total = BankCharges + NetAmount;
                address = dr["CCSHWL_BANKADDRESS"].ToString();


                sb.AppendLine(  dr["rownumber"].ToString() + "," +
                                dr["CUSR_USERNAME"].ToString() + ",\t" +
                                Name + ",\t" +
                                Total + "," +
                                BankCharges + "," +
                                NetAmount + "," +
                                dr["CMULTILANGCOUNTRY_NAME"].ToString() + ",\t" +
                                dr["CCSHWL_BANKNAME"].ToString() + ",\t" +
                                dr["CCSHWL_BRANCHNANE"].ToString() + ",\t" +
                                address.Replace(","," ") +  ",\t" +
                                dr["CCSHWL_BANKSWIFTCODE"].ToString() + ",\t" +
                                dr["CCSHWL_USERBANKACC"].ToString() + ",\t" +
                                dr["CUSR_FULLNAME"].ToString() + ",\t" +
                                dr["CUSRINFO_CELLPHONE"].ToString() + ",\t" +
                                dr["CUSRINFO_IC"].ToString() + ",\t" +
                                Convert.ToDateTime(dr["CCSHWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt"));
            }

            return sb.ToString();
        }
        #endregion


        #endregion

        #region Wallet Log

        public ActionResult ViewRegisterWallet(int selectedPage = 1, string cashname = "0", string startdate = "01/01/1990", string enddate = "01/01/3000", string Username = "0" , string Export = "0")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                int pages = 0;
                PaginationWalletLogModel model = new PaginationWalletLogModel();
                model.TypeOfWallet = "Register";
                ViewBag.wallet = model.TypeOfWallet; ;
                model.show = true;

                DateTime sdate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
                //DateTime ENDDATE = edate.AddDays(1);
                string ED = edate.ToString("yyyy-MM-dd");

                List<CashName> CName = Misc.DBGetAllCashNameList("RP");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };

                DataSet dsWallet = AdminWalletDB.GetAllRegisterWalletLog(Username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), selectedPage, out pages);

                selectedPage = ConstructPageList(selectedPage, pages, model);

                float TotalCashIn = 0;
                float TotalCashOut = 0;

                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    WalletLogModel walletLog = new WalletLogModel();
                    walletLog.FlowID = dr["CREGWL_ID"].ToString();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CREGWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CREGWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CREGWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CREGWL_WALLET"].ToString());
                    walletLog.AppUser = dr["CREGWL_APPUSER"].ToString();
                    walletLog.AppOther = Misc.ConvertToAppOtherBasedOnCashName(dr["CREGWL_APPOTHER"].ToString(), dr["CREGWL_CASHNAME"].ToString());
                    walletLog.paymentmode = dr["CREGWL_PAYMENTMODE"].ToString();
                    walletLog.AppNumber = dr["CREGWL_APPNUMBER"].ToString();
                    walletLog.Bank = dr["CMULTILANGBANK_NAME"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CREGWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    TotalCashIn = TotalCashIn + float.Parse(dr["CREGWL_CASHIN"].ToString());
                    TotalCashOut = TotalCashOut + float.Parse(dr["CREGWL_CASHOUT"].ToString());

                    model.WalletLogList.Add(walletLog);
                }

                model.TotalCashIn = TotalCashIn.ToString("n2");
                model.TotalCashOut = TotalCashOut.ToString("n2");

                model.selectedname = cashname;
                if (startdate != "01/01/1990")
                {
                    model.startdate = startdate;
                }

                if (enddate != "01/01/3000")
                {
                    model.enddate = enddate;
                }

                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        
        public ActionResult ViewCashWallet(int selectedPage = 1, string cashname = "0", string startdate = "01/01/1990", string enddate = "01/01/3000", string Username = "0", string Export = "0")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                int pages = 0;
                PaginationWalletLogModel model = new PaginationWalletLogModel();
                model.TypeOfWallet = "Cash";
                ViewBag.wallet = model.TypeOfWallet; ;
                model.show = true;

                DateTime sdate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
                //DateTime ENDDATE = edate.AddDays(1);
                string ED = edate.ToString("yyyy-MM-dd");

                List<CashName> CName = Misc.DBGetAllCashNameList("CP");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };

                DataSet dsWallet = AdminWalletDB.GetAllCashWalletLog(Username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), selectedPage, out pages);

                selectedPage = ConstructPageList(selectedPage, pages, model);
                float TotalCashIn = 0;
                float TotalCashOut = 0;

                string previous = string.Empty;
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    WalletLogModel walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CCSHWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CCSHWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CCSHWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CCSHWL_WALLET"].ToString());
                    walletLog.AppUser = dr["CCSHWL_APPUSER"].ToString();
                    walletLog.AppOther = Misc.ConvertToAppOtherBasedOnCashName(dr["CCSHWL_APPOTHER"].ToString(), dr["CCSHWL_CASHNAME"].ToString());
                    walletLog.paymentmode = dr["CCSHWL_PAYMENTMODE"].ToString();
                    walletLog.AppNumber = dr["CCSHWL_APPNUMBER"].ToString();
                    walletLog.Bank = dr["CMULTILANGBANK_NAME"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CCSHWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    TotalCashIn = TotalCashIn + float.Parse(dr["CCSHWL_CASHIN"].ToString());
                    TotalCashOut = TotalCashOut + float.Parse(dr["CCSHWL_CASHOUT"].ToString());

                    model.WalletLogList.Add(walletLog);
                }

                model.TotalCashIn = TotalCashIn.ToString("n2");
                model.TotalCashOut = TotalCashOut.ToString("n2");

                //ViewBag.Title = Reports.lblEWalletTransaction;
                model.selectedname = cashname;
                if (startdate != "01/01/1990")
                {
                    model.startdate = startdate;
                }

                if (enddate != "01/01/3000")
                {
                    model.enddate = enddate;
                }
                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewCompanyWallet(int selectedPage = 1, string cashname = "0", string startdate = "01/01/1990", string enddate = "01/01/3000", string Username = "0", string Export = "0")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { });
                }

                int pages = 0;
                PaginationWalletLogModel model = new PaginationWalletLogModel();
                model.TypeOfWallet = "CRP";
                ViewBag.wallet = model.TypeOfWallet; ;
                model.show = true;

                DateTime sdate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
                //DateTime ENDDATE = edate.AddDays(1);
                string ED = edate.ToString("yyyy-MM-dd");

                List<CashName> CName = Misc.DBGetAllCashNameList("CRP");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };

                DataSet dsWallet = AdminWalletDB.GetAllWalletCompanyLog(Username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), selectedPage, out pages);

                float TotalCashIn = 0;
                float TotalCashOut = 0;

                selectedPage = ConstructPageList(selectedPage, pages, model);
                string previous = string.Empty;
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    WalletLogModel walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CCOMWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CCOMWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CCOMWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CCOMWL_WALLET"].ToString());
                    walletLog.AppUser = dr["CCOMWL_APPUSER"].ToString();
                    walletLog.AppOther = Misc.ConvertToAppOtherBasedOnCashName(dr["CCOMWL_APPOTHER"].ToString(), dr["CCOMWL_CASHNAME"].ToString());
                    walletLog.paymentmode = dr["CCOMWL_PAYMENTMODE"].ToString();
                    walletLog.AppNumber = dr["CCOMWL_APPNUMBER"].ToString();
                    walletLog.Bank = dr["CMULTILANGBANK_NAME"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CCOMWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    TotalCashIn = TotalCashIn + float.Parse(dr["CCOMWL_CASHIN"].ToString());
                    TotalCashOut = TotalCashOut + float.Parse(dr["CCOMWL_CASHOUT"].ToString());

                    model.WalletLogList.Add(walletLog);
                }

                model.TotalCashIn = TotalCashIn.ToString("n2");
                model.TotalCashOut = TotalCashOut.ToString("n2");

                ViewBag.Title = "CRP Log";
                model.selectedname = cashname;
                if (startdate != "1990-01-01")
                {
                    model.startdate = startdate;
                }

                if (enddate != "3000-01-01")
                {
                    model.enddate = enddate;
                }
                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewTPWallet(int selectedPage = 1, string cashname = "0", string startdate = "01/01/1990", string enddate = "01/01/3000", string Username = "0", string Export = "0")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { });
                }

                int pages = 0;
                PaginationWalletLogModel model = new PaginationWalletLogModel();
                model.TypeOfWallet = "TP";
                ViewBag.wallet = model.TypeOfWallet;
                model.show = true;

                DateTime sdate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
                //DateTime ENDDATE = edate.AddDays(1);
                string ED = edate.ToString("yyyy-MM-dd");

                List<CashName> CName = Misc.DBGetAllCashNameList("TP");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };

                DataSet dsWallet = AdminWalletDB.GetAllTPLog(Username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), selectedPage, out pages);

                float TotalCashIn = 0;
                float TotalCashOut = 0;

                selectedPage = ConstructPageList(selectedPage, pages, model);
                string previous = string.Empty;
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    WalletLogModel walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CTRVWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CTRVWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CTRVWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CTRVWL_WALLET"].ToString());
                    walletLog.AppUser = dr["CTRVWL_APPUSER"].ToString();
                    walletLog.AppOther = Misc.ConvertToAppOtherBasedOnCashName(dr["CTRVWL_APPOTHER"].ToString(), dr["CTRVWL_CASHNAME"].ToString());
                    walletLog.paymentmode = dr["CTRVWL_PAYMENTMODE"].ToString();
                    walletLog.AppNumber = dr["CTRVWL_APPNUMBER"].ToString();
                    walletLog.Bank = dr["CMULTILANGBANK_NAME"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CTRVWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    TotalCashIn = TotalCashIn + float.Parse(dr["CTRVWL_CASHIN"].ToString());
                    TotalCashOut = TotalCashOut + float.Parse(dr["CTRVWL_CASHOUT"].ToString());

                    model.WalletLogList.Add(walletLog);
                }

                model.TotalCashIn = TotalCashIn.ToString("n2");
                model.TotalCashOut = TotalCashOut.ToString("n2");

                ViewBag.Title = "TP Log";
                model.selectedname = cashname;
                if (startdate != "1990-01-01")
                {
                    model.startdate = startdate;
                }

                if (enddate != "3000-01-01")
                {
                    model.enddate = enddate;
                }
                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewMultiPointWallet(int selectedPage = 1, string cashname = "0", string startdate = "01/01/1990", string enddate = "01/01/3000", string Username = "0", string Export = "0")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                int pages = 0;
                PaginationWalletLogModel model = new PaginationWalletLogModel();
                model.TypeOfWallet = "Multi";
                ViewBag.wallet = model.TypeOfWallet; ;
                model.show = true;

                DateTime sdate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
                //DateTime ENDDATE = edate.AddDays(1);
                string ED = edate.ToString("yyyy-MM-dd");

                List<CashName> CName = Misc.DBGetAllCashNameList("MP");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };

                DataSet dsWallet = AdminWalletDB.GetAllMultiPointWalletLog(Username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), selectedPage, out pages);

                float TotalCashIn = 0;
                float TotalCashOut = 0;

                selectedPage = ConstructPageList(selectedPage, pages, model);
                string previous = string.Empty;
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    WalletLogModel walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CMPWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CMPWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CMPWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CMPWL_WALLET"].ToString());
                    walletLog.AppUser = dr["CMPWL_APPUSER"].ToString();
                    walletLog.AppOther = Misc.ConvertToAppOtherBasedOnCashName(dr["CMPWL_APPOTHER"].ToString(), dr["CMPWL_CASHNAME"].ToString());
                    walletLog.paymentmode = dr["CMPWL_PAYMENTMODE"].ToString();
                    walletLog.AppNumber = dr["CMPWL_APPNUMBER"].ToString();
                    walletLog.Bank = dr["CMULTILANGBANK_NAME"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CMPWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    TotalCashIn = TotalCashIn + float.Parse(dr["CMPWL_CASHIN"].ToString());
                    TotalCashOut = TotalCashOut + float.Parse(dr["CMPWL_CASHOUT"].ToString());

                    model.WalletLogList.Add(walletLog);
                }

                model.TotalCashIn = TotalCashIn.ToString("n2");
                model.TotalCashOut = TotalCashOut.ToString("n2");

                //ViewBag.Title = Reports.lblEWalletTransaction;
                model.selectedname = cashname;
                if (startdate != "01/01/1990")
                {
                    model.startdate = startdate;
                }

                if (enddate != "01/01/3000")
                {
                    model.enddate = enddate;
                }
                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewRMPWallet(int selectedPage = 1, string cashname = "0", string startdate = "01/01/1990", string enddate = "01/01/3000", string Username = "0", string Export = "0")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { });
                }

                int pages = 0;
                PaginationWalletLogModel model = new PaginationWalletLogModel();
                model.TypeOfWallet = "RMP";
                ViewBag.wallet = model.TypeOfWallet; 
                model.show = true;

                DateTime sdate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
                //DateTime ENDDATE = edate.AddDays(1);
                string ED = edate.ToString("yyyy-MM-dd");

                List<CashName> CName = Misc.DBGetAllCashNameList("MP");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };

                DataSet dsWallet = AdminWalletDB.GetAllRMPLog(Username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), selectedPage, out pages);

                float TotalCashIn = 0;
                float TotalCashOut = 0;

                selectedPage = ConstructPageList(selectedPage, pages, model);
                string previous = string.Empty;
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    WalletLogModel walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CRMPWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CRMPWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CRMPWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CRMPWL_WALLET"].ToString());
                    walletLog.AppUser = dr["CRMPWL_APPUSER"].ToString();
                    walletLog.AppOther = Misc.ConvertToAppOtherBasedOnCashName(dr["CRMPWL_APPOTHER"].ToString(), dr["CRMPWL_CASHNAME"].ToString());
                    walletLog.paymentmode = dr["CRMPWL_PAYMENTMODE"].ToString();
                    walletLog.AppNumber = dr["CRMPWL_APPNUMBER"].ToString();
                    walletLog.Bank = dr["CMULTILANGBANK_NAME"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CRMPWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    TotalCashIn = TotalCashIn + float.Parse(dr["CRMPWL_CASHIN"].ToString());
                    TotalCashOut = TotalCashOut + float.Parse(dr["CRMPWL_CASHOUT"].ToString());

                    model.WalletLogList.Add(walletLog);
                }

                model.TotalCashIn = TotalCashIn.ToString("n2");
                model.TotalCashOut = TotalCashOut.ToString("n2");

                //ViewBag.Title = Reports.lblEWalletTransaction;
                model.selectedname = cashname;
                if (startdate != "01/01/1990")
                {
                    model.startdate = startdate;
                }

                if (enddate != "01/01/3000")
                {
                    model.enddate = enddate;
                }
                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewGoldPointWallet(int selectedPage = 1, string cashname = "0", string startdate = "01/01/1990", string enddate = "01/01/3000", string Username = "0", string Export = "0")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                int pages = 0;
                PaginationWalletLogModel model = new PaginationWalletLogModel();
                model.TypeOfWallet = "Gold";
                ViewBag.wallet = model.TypeOfWallet; ;
                model.show = true;

                DateTime sdate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
                //DateTime ENDDATE = edate.AddDays(1);
                string ED = edate.ToString("yyyy-MM-dd");

                List<CashName> CName = Misc.DBGetAllCashNameList("GP");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };

                DataSet dsWallet = AdminWalletDB.GetAllGoldPointWalletLog(Username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), selectedPage, out pages);

                float TotalCashIn = 0;
                float TotalCashOut = 0;

                selectedPage = ConstructPageList(selectedPage, pages, model);
                string previous = string.Empty;
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    WalletLogModel walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["GLDWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["GLDWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["GLDWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["GLDWL_WALLET"].ToString());
                    walletLog.AppUser = dr["GLDWL_APPUSER"].ToString();
                    walletLog.AppOther = Misc.ConvertToAppOtherBasedOnCashName(dr["GLDWL_APPOTHER"].ToString(), dr["GLDWL_CASHNAME"].ToString());
                    walletLog.paymentmode = dr["GLDWL_PAYMENTMODE"].ToString();
                    walletLog.AppNumber = dr["GLDWL_APPNUMBER"].ToString();
                    walletLog.Bank = dr["CMULTILANGBANK_NAME"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["GLDWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    TotalCashIn = TotalCashIn + float.Parse(dr["GLDWL_CASHIN"].ToString());
                    TotalCashOut = TotalCashOut + float.Parse(dr["GLDWL_CASHOUT"].ToString());

                    model.WalletLogList.Add(walletLog);
                }

                model.TotalCashIn = TotalCashIn.ToString("n2");
                model.TotalCashOut = TotalCashOut.ToString("n2");

                //ViewBag.Title = Reports.lblEWalletTransaction;
                model.selectedname = cashname;
                if (startdate != "01/01/1990")
                {
                    model.startdate = startdate;
                }

                if (enddate != "01/01/3000")
                {
                    model.enddate = enddate;
                }
                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewRPCWallet(int selectedPage = 1, string cashname = "0", string startdate = "01/01/1990", string enddate = "01/01/3000", string Username = "0", string Export = "0")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                int pages = 0;
                PaginationWalletLogModel model = new PaginationWalletLogModel();
                model.TypeOfWallet = "RPC";
                ViewBag.wallet = model.TypeOfWallet; ;
                model.show = true;

                DateTime sdate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
                //DateTime ENDDATE = edate.AddDays(1);
                string ED = edate.ToString("yyyy-MM-dd");

                List<CashName> CName = Misc.DBGetAllCashNameList("RPC");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };

                float TotalCashIn = 0;
                float TotalCashOut = 0;

                DataSet dsWallet = AdminWalletDB.GetAllRPCWalletLog(Username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), selectedPage, out pages);

                selectedPage = ConstructPageList(selectedPage, pages, model);
                string previous = string.Empty;
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    WalletLogModel walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CRPCWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CRPCWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CRPCWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CRPCWL_WALLET"].ToString());
                    walletLog.AppUser = dr["CRPCWL_APPUSER"].ToString();
                    walletLog.AppOther = Misc.ConvertToAppOtherBasedOnCashName(dr["CRPCWL_APPOTHER"].ToString(), dr["CRPCWL_CASHNAME"].ToString());
                    walletLog.paymentmode = dr["CRPCWL_PAYMENTMODE"].ToString();
                    walletLog.AppNumber = dr["CRPCWL_APPNUMBER"].ToString();
                    walletLog.Bank = dr["CMULTILANGBANK_NAME"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CRPCWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    TotalCashIn = TotalCashIn + float.Parse(dr["CRPCWL_CASHIN"].ToString());
                    TotalCashOut = TotalCashOut + float.Parse(dr["CRPCWL_CASHOUT"].ToString());

                    model.WalletLogList.Add(walletLog);
                }

                model.TotalCashIn = TotalCashIn.ToString("n2");
                model.TotalCashOut = TotalCashOut.ToString("n2");

                //ViewBag.Title = Reports.lblEWalletTransaction;
                model.selectedname = cashname;
                if (startdate != "01/01/1990")
                {
                    model.startdate = startdate;
                }

                if (enddate != "01/01/3000")
                {
                    model.enddate = enddate;
                }
                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewICOWallet(int selectedPage = 1, string cashname = "0", string startdate = "01/01/1990", string enddate = "01/01/3000", string Username = "0", string Export = "0")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }

                int pages = 0;
                PaginationWalletLogModel model = new PaginationWalletLogModel();
                model.TypeOfWallet = "ICO";
                ViewBag.wallet = model.TypeOfWallet; ;
                model.show = true;

                DateTime sdate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
                //DateTime ENDDATE = edate.AddDays(1);
                string ED = edate.ToString("yyyy-MM-dd");

                List<CashName> CName = Misc.DBGetAllCashNameList("ICO");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };

                DataSet dsWallet = AdminWalletDB.GetAllICOWalletLog(Username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), selectedPage, out pages);

                float TotalCashIn = 0;
                float TotalCashOut = 0;

                selectedPage = ConstructPageList(selectedPage, pages, model);
                string previous = string.Empty;
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    WalletLogModel walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CICOWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CICOWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CICOWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CICOWL_WALLET"].ToString());
                    walletLog.AppUser = dr["CICOWL_APPUSER"].ToString();
                    walletLog.AppOther = Misc.ConvertToAppOtherBasedOnCashName(dr["CICOWL_APPOTHER"].ToString(), dr["CICOWL_CASHNAME"].ToString());
                    walletLog.paymentmode = dr["CICOWL_PAYMENTMODE"].ToString();
                    walletLog.AppNumber = dr["CICOWL_APPNUMBER"].ToString();
                    walletLog.Bank = dr["CMULTILANGBANK_NAME"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CICOWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    TotalCashIn = TotalCashIn + float.Parse(dr["CICOWL_CASHIN"].ToString());
                    TotalCashOut = TotalCashOut + float.Parse(dr["CICOWL_CASHOUT"].ToString());

                    model.WalletLogList.Add(walletLog);
                }

                model.TotalCashIn = TotalCashIn.ToString("n2");
                model.TotalCashOut = TotalCashOut.ToString("n2");

                //ViewBag.Title = Reports.lblEWalletTransaction;
                model.selectedname = cashname;
                if (startdate != "01/01/1990")
                {
                    model.startdate = startdate;
                }

                if (enddate != "01/01/3000")
                {
                    model.enddate = enddate;
                }
                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public void Exportwallet(string WalletType = "",string cashname = "0", string username = "0", string startdate = "01/01/1990", string enddate = "01/01/3000", string Export = "1")
        {

            string Title = string.Empty;

            if (WalletType == "Register")
            {
                Title = "RP-Log.csv";
            }
            //if (WalletType == "Bonus")
            //{
            //    Title = "PP-Log.csv";
            //}
            if (WalletType == "Cash")
            {
                Title = "CP-Log.csv";
            }
            if (WalletType == "Gold")
            {
                Title = "GP-Log.csv";
            }

            if (WalletType == "Multi")
            {
                Title = "MP-Log.csv";
            }
            if (WalletType == "CRP")
            {
                Title = "CRP-Log.csv";
            }
            if (WalletType == "TP")
            {
                Title = "TP-Log.csv";
            }
            if (WalletType == "RPC")
            {
                Title = "RPC-Log.csv";
            }
            if (WalletType == "ICO")
            {
                Title = "ICO-Log.csv";
            }
            if (WalletType == "RMP")
            {
                Title = "RMP-Log.csv";
            }
            //if (WalletType == "Company")
            //{
            //    Title = "GP-Log.csv";
            //}
            //if (WalletType == "Share")
            //{
            //    Title = "EP-Log.csv";
            //}
            //if (WalletType == "Shop")
            //{
            //    Title = "MP-Log.csv";
            //}


            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=" + Title + "");
            Response.ContentType = "application/octet-stream";
            StreamWriter sw = new StreamWriter(Response.OutputStream, Encoding.UTF8);
            sw.Write(Printwallet(WalletType, cashname, username, startdate, enddate,Export));
            sw.Close();
            Response.End();
        }

        private string Printwallet(string WalletType, string cashname, string username, string startdate, string enddate, string Export )
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("Username,Fullname,Remark,Credit,Debit,Balance,App User,App User Fullame,Extra Info,Payment,Transaction Date "));
       
            int pages = 0;
            string message = string.Empty;
            DataSet dsWallet = new DataSet();

            DateTime sdate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
            string SD = sdate.ToString("yyyy-MM-dd");

            DateTime edate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
            //DateTime ENDDATE = edate.AddDays(0);
            string ED = edate.ToString("yyyy-MM-dd");

            if (WalletType == "TP")
            {
                dsWallet = AdminWalletDB.GetAllTPLog(username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), 1, out pages);
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    sb.AppendLine(
                                  dr["CUSR_USERNAME"].ToString() + "," +
                                  dr["Member_Fullname"].ToString() + "," +
                                  Misc.GetReadableCashName(dr["CTRVWL_CASHNAME"].ToString()) + "," +
                                  float.Parse(dr["CTRVWL_CASHIN"].ToString()) + "," +
                                  float.Parse(dr["CTRVWL_CASHOUT"].ToString()) + "," +
                                  float.Parse(dr["CTRVWL_WALLET"].ToString()) + "," +
                                  dr["CTRVWL_APPUSER"].ToString() + "," +
                                  dr["APPUSER_Fulname"].ToString() + "," +
                                  Misc.ConvertToAppOtherBasedOnCashName(dr["CTRVWL_APPOTHER"].ToString(), dr["CTRVWL_CASHNAME"].ToString()) + "," +
                                  dr["CTRVWL_PAYMENTMODE"].ToString() + "," +
                    Convert.ToDateTime(dr["CTRVWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt"));
                }
            }

            if (WalletType == "CRP")
            {
                dsWallet = AdminWalletDB.GetAllWalletCompanyLog(username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), 1, out pages);
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    sb.AppendLine(
                                  dr["CUSR_USERNAME"].ToString() + "," +
                                  dr["Member_Fullname"].ToString() + "," +
                                  Misc.GetReadableCashName(dr["CCOMWL_CASHNAME"].ToString()) + "," +
                                  float.Parse(dr["CCOMWL_CASHIN"].ToString()) + "," +
                                  float.Parse(dr["CCOMWL_CASHOUT"].ToString()) + "," +
                                  float.Parse(dr["CCOMWL_WALLET"].ToString()) + "," +
                                  dr["CCOMWL_APPUSER"].ToString() + "," +
                                  dr["APPUSER_Fulname"].ToString() + "," +
                                  Misc.ConvertToAppOtherBasedOnCashName(dr["CCOMWL_APPOTHER"].ToString(), dr["CCOMWL_CASHNAME"].ToString()) + "," +
                                  dr["CCOMWL_PAYMENTMODE"].ToString() + "," +
                    Convert.ToDateTime(dr["CCOMWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt"));
                }
            }

            if (WalletType == "Register")
            {
                dsWallet = AdminWalletDB.GetAllRegisterWalletLog(username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), 1, out pages);
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    sb.AppendLine(
                                  dr["CUSR_USERNAME"].ToString() + "," +
                                  dr["Member_Fullname"].ToString() + "," +
                                  Misc.GetReadableCashName(dr["CREGWL_CASHNAME"].ToString()) + "," +
                                  float.Parse(dr["CREGWL_CASHIN"].ToString()) + "," +
                                  float.Parse(dr["CREGWL_CASHOUT"].ToString()) + "," +
                                  float.Parse(dr["CREGWL_WALLET"].ToString()) + "," +
                                  dr["CREGWL_APPUSER"].ToString() + "," +
                                  dr["APPUSER_Fulname"].ToString() + "," +
                                  Misc.ConvertToAppOtherBasedOnCashName(dr["CREGWL_APPOTHER"].ToString(), dr["CREGWL_CASHNAME"].ToString()) + "," +
                                  dr["CREGWL_PAYMENTMODE"].ToString() + "," +
                    Convert.ToDateTime(dr["CREGWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt"));
                }
            }
           
            if (WalletType == "Cash")
            {
                dsWallet = AdminWalletDB.GetAllCashWalletLog(username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), 1, out pages);
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    sb.AppendLine(
                                  dr["CUSR_USERNAME"].ToString() + "," +
                                  dr["Member_Fullname"].ToString() + "," +
                                  Misc.GetReadableCashName(dr["CCSHWL_CASHNAME"].ToString()) + "," +
                                  float.Parse(dr["CCSHWL_CASHIN"].ToString()) + "," +
                                  float.Parse(dr["CCSHWL_CASHOUT"].ToString()) + "," +
                                  float.Parse(dr["CCSHWL_WALLET"].ToString()) + "," +
                                  dr["CCSHWL_APPUSER"].ToString() + "," +
                                  dr["APPUSER_Fulname"].ToString() + "," +
                                  Misc.ConvertToAppOtherBasedOnCashName(dr["CCSHWL_APPOTHER"].ToString(), dr["CCSHWL_CASHNAME"].ToString()) + "," +
                                   dr["CCSHWL_PAYMENTMODE"].ToString() + "," +
                    Convert.ToDateTime(dr["CCSHWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt"));
                }
            }
            if (WalletType == "Gold")
            {
                dsWallet = AdminWalletDB.GetAllGoldPointWalletLog(username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), 1, out pages);
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    sb.AppendLine(
                                  dr["CUSR_USERNAME"].ToString() + "," +
                                  dr["Member_Fullname"].ToString() + "," +
                                  Misc.GetReadableCashName(dr["GLDWL_CASHNAME"].ToString()) + "," +
                                  float.Parse(dr["GLDWL_CASHIN"].ToString()) + "," +
                                  float.Parse(dr["GLDWL_CASHOUT"].ToString()) + "," +
                                  float.Parse(dr["GLDWL_WALLET"].ToString()) + "," +
                                  dr["GLDWL_APPUSER"].ToString() + "," +
                                  dr["APPUSER_Fulname"].ToString() + "," +
                                  Misc.ConvertToAppOtherBasedOnCashName(dr["GLDWL_APPOTHER"].ToString(), dr["GLDWL_CASHNAME"].ToString()) + "," +
                                  dr["GLDWL_PAYMENTMODE"].ToString() + "," +
                    Convert.ToDateTime(dr["GLDWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt"));
                }
            }
            if (WalletType == "Multi")
            {
                dsWallet = AdminWalletDB.GetAllMultiPointWalletLog(username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), 1, out pages);
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    sb.AppendLine(
                                  dr["CUSR_USERNAME"].ToString() + "," +
                                  dr["Member_Fullname"].ToString() + "," +
                                  Misc.GetReadableCashName(dr["CMPWL_CASHNAME"].ToString()) + "," +
                                  float.Parse(dr["CMPWL_CASHIN"].ToString()) + "," +
                                  float.Parse(dr["CMPWL_CASHOUT"].ToString()) + "," +
                                  float.Parse(dr["CMPWL_WALLET"].ToString()) + "," +
                                  dr["CMPWL_APPUSER"].ToString() + "," +
                                  dr["APPUSER_Fulname"].ToString() + "," +
                                  Misc.ConvertToAppOtherBasedOnCashName(dr["CMPWL_APPOTHER"].ToString(), dr["CMPWL_CASHNAME"].ToString()) + "," +
                                  dr["CMPWL_PAYMENTMODE"].ToString() + "," +
                    Convert.ToDateTime(dr["CMPWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt"));
                }
            }
            if (WalletType == "RMP")
            {
                dsWallet = AdminWalletDB.GetAllRMPLog(username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), 1, out pages);
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    sb.AppendLine(
                                  dr["CUSR_USERNAME"].ToString() + "," +
                                  dr["Member_Fullname"].ToString() + "," +
                                  Misc.GetReadableCashName(dr["CRMPWL_CASHNAME"].ToString()) + "," +
                                  float.Parse(dr["CRMPWL_CASHIN"].ToString()) + "," +
                                  float.Parse(dr["CRMPWL_CASHOUT"].ToString()) + "," +
                                  float.Parse(dr["CRMPWL_WALLET"].ToString()) + "," +
                                  dr["CRMPWL_APPUSER"].ToString() + "," +
                                  dr["APPUSER_Fulname"].ToString() + "," +
                                  Misc.ConvertToAppOtherBasedOnCashName(dr["CRMPWL_APPOTHER"].ToString(), dr["CRMPWL_CASHNAME"].ToString()) + "," +
                                  dr["CRMPWL_PAYMENTMODE"].ToString() + "," +
                    Convert.ToDateTime(dr["CRMPWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt"));
                }
            }
            if (WalletType == "RPC")
            {
                dsWallet = AdminWalletDB.GetAllRPCWalletLog(username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), 1, out pages);
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    sb.AppendLine(
                                  dr["CUSR_USERNAME"].ToString() + "," +
                                  dr["Member_Fullname"].ToString() + "," +
                                  Misc.GetReadableCashName(dr["CRPCWL_CASHNAME"].ToString()) + "," +
                                  float.Parse(dr["CRPCWL_CASHIN"].ToString()) + "," +
                                  float.Parse(dr["CRPCWL_CASHOUT"].ToString()) + "," +
                                  float.Parse(dr["CRPCWL_WALLET"].ToString()) + "," +
                                  dr["CRPCWL_APPUSER"].ToString() + "," +
                                  dr["APPUSER_Fulname"].ToString() + "," +
                                  Misc.ConvertToAppOtherBasedOnCashName(dr["CRPCWL_APPOTHER"].ToString(), dr["CRPCWL_CASHNAME"].ToString()) + "," +
                                  dr["CRPCWL_PAYMENTMODE"].ToString() + "," +
                    Convert.ToDateTime(dr["CRPCWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt"));
                }
            }
            if (WalletType == "ICO")
            {
                dsWallet = AdminWalletDB.GetAllICOWalletLog(username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), 1, out pages);
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    sb.AppendLine(
                                  dr["CUSR_USERNAME"].ToString() + "," +
                                  dr["Member_Fullname"].ToString() + "," +
                                  Misc.GetReadableCashName(dr["CICOWL_CASHNAME"].ToString()) + "," +
                                  float.Parse(dr["CICOWL_CASHIN"].ToString()) + "," +
                                  float.Parse(dr["CICOWL_CASHOUT"].ToString()) + "," +
                                  float.Parse(dr["CICOWL_WALLET"].ToString()) + "," +
                                  dr["CICOWL_APPUSER"].ToString() + "," +
                                  dr["APPUSER_Fulname"].ToString() + "," +
                                  Misc.ConvertToAppOtherBasedOnCashName(dr["CICOWL_APPOTHER"].ToString(), dr["CICOWL_CASHNAME"].ToString()) + "," +
                                  Convert.ToDateTime(dr["CICOWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt"));
                }
            }

            return sb.ToString();
        }
        #endregion
        
        #region General for View Next Page Log
        public ActionResult NextWalletTransaction(string selectedPage, string username, string walletType, string cashname, string datetime,string startdate , string enddate)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new {  });
                }
                if (walletType == "Register")
                {
                    return RedirectToAction("ViewRegisterWallet", "AdminWallet", new { selectedPage = selectedPage, username = username, cashname = cashname, startdate = startdate, enddate = enddate });
                }
                //if (walletType == "Bonus")
                //{
                //    return RedirectToAction("ViewBonusWallet", "AdminWallet", new { selectedPage = selectedPage, cashname = cashname, startdate = startdate, enddate = enddate });
                //}
                if (walletType == "Cash")
                {
                    return RedirectToAction("ViewCashWallet", "AdminWallet", new { selectedPage = selectedPage, username = username, cashname = cashname, startdate = startdate, enddate = enddate });
                }
                if (walletType == "Gold")
                {
                    return RedirectToAction("ViewGoldPointWallet", "AdminWallet", new { selectedPage = selectedPage, username = username, cashname = cashname, startdate = startdate, enddate = enddate });
                }
                if (walletType == "Multi")
                {
                    return RedirectToAction("ViewMultiPointWallet", "AdminWallet", new { selectedPage = selectedPage, username = username, cashname = cashname, startdate = startdate, enddate = enddate });
                }
                if (walletType == "RPC")
                {
                    return RedirectToAction("ViewRPCWallet", "AdminWallet", new { selectedPage = selectedPage, username = username, cashname = cashname, startdate = startdate, enddate = enddate });
                }
                if (walletType == "ICO")
                {
                    return RedirectToAction("ViewICOWallet", "AdminWallet", new { selectedPage = selectedPage, username = username, cashname = cashname, startdate = startdate, enddate = enddate });
                }
                if (walletType == "RMP")
                {
                    return RedirectToAction("ViewRMPWallet", "AdminWallet", new { selectedPage = selectedPage, username = username, cashname = cashname, startdate = startdate, enddate = enddate });
                }
                if (walletType == "CRP")
                {
                    return RedirectToAction("ViewCompanyWallet", "AdminWallet", new { selectedPage = selectedPage, username = username, cashname = cashname, startdate = startdate, enddate = enddate });
                }
                if (walletType == "TP")
                {
                    return RedirectToAction("ViewTPWallet", "AdminWallet", new { selectedPage = selectedPage, username = username, cashname = cashname, startdate = startdate, enddate = enddate });
                }

                //purposely set to unknown, so that we know there is some wallet that is not yet add in the if condition
                return RedirectToAction("UnknownWalletTransaction", "AdminWallet", new { selectedPage = selectedPage });

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region SharedMethod
        private int ConstructPageList(int selectedPage, int pages, PaginationWalletLogModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        private int ConstructPageList(int selectedPage, int pages, PaginationShareCertPurchaseModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }
        #endregion

        #region Purchase RP       
        public ActionResult PurchaseRP(int selectedPage = 1)

        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { });
                }

                PaginationMobileAgent model = new PaginationMobileAgent();

                int ok;
                string msg;
                int pages = 0;

                DataSet dsWallet = AdminGeneralDB.GetAllPendingPurchaseRP(selectedPage, out pages, out ok, out msg);

                List<int> pageList = new List<int>();
                for (int z = 1; z <= pages; z++)
                {
                    pageList.Add(z);
                }

                model.Pages = from c in pageList
                              select new SelectListItem
                              {
                                  Selected = (c.ToString() == selectedPage.ToString()),
                                  Text = c.ToString(),
                                  Value = c.ToString()
                              };

                string previous = string.Empty;
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    MobileRequestList Request = new MobileRequestList();
                    Request.PinID = dr["CTUR_ID"].ToString();
                    //Request.Date = dr["CTUR_CREATEDON"].ToString();
                    Request.Date = Convert.ToDateTime(dr["CTUR_CREATEDON"]).ToString("dd/MM/yyyy");
                    Request.ReferenceOrder = dr["CTUR_REFFERALID"].ToString();
                    Request.MemberID = dr["CUSR_USERNAME"].ToString();
                    Request.FullName = dr["CTUR_FULLNAME"].ToString();
                    Request.TopUpFor = dr["CTUR_FROM"].ToString();
                    Request.Currency = dr["CTUR_CURRENCY"].ToString();
                    Request.LocalAmount = float.Parse(dr["CTUR_LOCAL_AMOUNT"].ToString()).ToString("n2");
                    Request.Amount = float.Parse(dr["CTUR_AMOUNT"].ToString()).ToString("n2");
                    Request.Remark = dr["CTUR_REMARK"].ToString();
                    Request.AdminRemark = dr["CTUR_ADMIN_REMARK"].ToString();
                    Request.VerifyBy = dr["CTUR_APPROVEBY"].ToString();

                    Request.ApprovedBy = dr["CTUR_GENERATEBY"].ToString();
                    Request.Province = dr["CMULTILANGPROVINCE_NAME"].ToString();
                    Request.BranchName = dr["CTUR_BRANCH"].ToString();
                    Request.AccHolderName = dr["CTUR_ACCOUNT_HOLDER_NAME"].ToString();
                    Request.AccNo = dr["CTUR_ACCOUNT_NUMBER"].ToString();
                    Request.Bank = Misc.GetBankCountryProvincebyID(dr["CTUR_BANK"].ToString(), Session["LanguageChosen"].ToString());


                    if (dr["CTUR_STATUS"].ToString() == "Pending")
                    {
                        Request.VerifyOn = "";
                        Request.ApprovedOn = "";
                    }
                    else
                    {
                        if (dr["CTUR_STATUS"].ToString() == "Approve" || dr["CTUR_STATUS"].ToString() == "Reject")
                        {
                            //Request.VerifyOn = dr["CTUR_APPROVETIME"].ToString();
                            Request.VerifyOn = Convert.ToDateTime(dr["CTUR_APPROVETIME"]).ToString("dd/MM/yyyy");
                            Request.ApprovedOn = "";
                        }
                        else
                        {
                            //Request.VerifyOn = dr["CTUR_APPROVETIME"].ToString();
                            //Request.ApprovedOn = dr["CTUR_GENERATEON"].ToString();
                            Request.VerifyOn = Convert.ToDateTime(dr["CTUR_APPROVETIME"]).ToString("dd/MM/yyyy");
                            Request.ApprovedOn = Convert.ToDateTime(dr["CTUR_GENERATEON"]).ToString("dd/MM/yyyy");
                        }

                    }

                    Request.Status = dr["CTUR_STATUS"].ToString();

                    model.MobileTopUpList.Add(Request);
                }

                return PartialView("PurchaseRP", model);
                //Session["page"] = "PurchaseRP";
                //ViewBag.page = "PurchaseRP";
                //return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult VeryfyPurchaseRP(string ID)
        {
            int ok = 0;
            string msg = string.Empty;

            PaginationMobileAgent model = new PaginationMobileAgent();


            DataSet dr = AdminGeneralDB.GetRequestPurchaseRPByID(Convert.ToInt32(ID));
            model.PinID = dr.Tables[0].Rows[0]["CTUR_ID"].ToString();
            model.Date = Convert.ToDateTime(dr.Tables[0].Rows[0]["CTUR_CREATEDON"]).ToString("dd/MM/yyyy");
            model.Username = dr.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
            var ds = MemberDB.GetMemberByUsername(model.Username, out ok, out msg);
            model.Name = ds.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            model.USDAmount = float.Parse(dr.Tables[0].Rows[0]["CTUR_AMOUNT"].ToString()).ToString("n2");
            model.LocalAmount = float.Parse(dr.Tables[0].Rows[0]["CTUR_LOCAL_AMOUNT"].ToString()).ToString("n2");
            model.Country = Misc.GetCountryNameByCountryCode(ds.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString());
            model.Status = dr.Tables[0].Rows[0]["CTUR_STATUS"].ToString();
            int extensionIndex = dr.Tables[0].Rows[0]["CTUR_IMAGE"].ToString().LastIndexOf('\\');
            string imageName = dr.Tables[0].Rows[0]["CTUR_IMAGE"].ToString().Substring(extensionIndex + 1);
            model.ImagePath = "http://" + Request.Url.Authority + "/Images/Payment" + "/" + imageName;
            model.PaymentMethod = dr.Tables[0].Rows[0]["CTUR_PAYMENT_METHOD"].ToString();
            model.Remark = dr.Tables[0].Rows[0]["CTUR_REMARK"].ToString();
            model.Province = dr.Tables[0].Rows[0]["CMULTILANGPROVINCE_NAME"].ToString();
            model.Bank = Misc.GetBankCountryProvincebyID(dr.Tables[0].Rows[0]["CTUR_BANK"].ToString(), Session["LanguageChosen"].ToString());
            model.BranchName = dr.Tables[0].Rows[0]["CTUR_BRANCH"].ToString();
            model.AccountHolerName = dr.Tables[0].Rows[0]["CTUR_ACCOUNT_HOLDER_NAME"].ToString();
            model.AccountNumber = dr.Tables[0].Rows[0]["CTUR_ACCOUNT_NUMBER"].ToString();


            return PartialView("PurchaseRPDataByID", model);

        }

        public ActionResult GeneratePurchaseRP(string ID)
        {
            int ok = 0;
            string msg = string.Empty;

            PaginationMobileAgent model = new PaginationMobileAgent();


            DataSet dr = AdminGeneralDB.GetRequestPurchaseRPByID(Convert.ToInt32(ID));
            model.PinID = dr.Tables[0].Rows[0]["CTUR_ID"].ToString();
            //model.Date = dr.Tables[0].Rows[0]["CTUR_CREATEDON"].ToString();
            model.Date = Convert.ToDateTime(dr.Tables[0].Rows[0]["CTUR_CREATEDON"]).ToString("dd/MM/yyyy");
            model.Username = dr.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
            var ds = MemberDB.GetMemberByUsername(model.Username, out ok, out msg);
            model.Name = ds.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            model.USDAmount = float.Parse(dr.Tables[0].Rows[0]["CTUR_AMOUNT"].ToString()).ToString("n2");
            model.LocalAmount = float.Parse(dr.Tables[0].Rows[0]["CTUR_LOCAL_AMOUNT"].ToString()).ToString("n2");
            model.Country = Misc.GetCountryNameByCountryCode(ds.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString());
            model.Status = dr.Tables[0].Rows[0]["CTUR_STATUS"].ToString();
            int extensionIndex = dr.Tables[0].Rows[0]["CTUR_IMAGE"].ToString().LastIndexOf('\\');
            string imageName = dr.Tables[0].Rows[0]["CTUR_IMAGE"].ToString().Substring(extensionIndex + 1);
            model.ImagePath = "http://" + Request.Url.Authority + "/Images/Payment" + "/" + imageName;
            model.PaymentMethod = dr.Tables[0].Rows[0]["CTUR_PAYMENT_METHOD"].ToString();
            model.Remark = dr.Tables[0].Rows[0]["CTUR_REMARK"].ToString();
            model.ApproveBy = dr.Tables[0].Rows[0]["CTUR_APPROVEBY"].ToString();
            //model.ApproveOn = dr.Tables[0].Rows[0]["CTUR_APPROVETIME"].ToString();
            model.ApproveOn = Convert.ToDateTime(dr.Tables[0].Rows[0]["CTUR_APPROVETIME"]).ToString("dd/MM/yyyy");
            model.Province = dr.Tables[0].Rows[0]["CMULTILANGPROVINCE_NAME"].ToString();
            model.Bank = Misc.GetBankCountryProvincebyID(dr.Tables[0].Rows[0]["CTUR_BANK"].ToString(), Session["LanguageChosen"].ToString());
            model.BranchName = dr.Tables[0].Rows[0]["CTUR_BRANCH"].ToString();
            model.AccountHolerName = dr.Tables[0].Rows[0]["CTUR_ACCOUNT_HOLDER_NAME"].ToString();
            model.AccountNumber = dr.Tables[0].Rows[0]["CTUR_ACCOUNT_NUMBER"].ToString();

            return PartialView("GenerateRPDataByID", model);

        }

        public ActionResult ApprovePurchaseRP(string ID, string AdminRemark)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { });
                }

                if (string.IsNullOrEmpty(AdminRemark))
                {
                    AdminRemark = " ";
                }


                AdminStockistDB.ApprovePurchaseRP(ID, AdminRemark, Session["Admin"].ToString());
                //return RedirectToAction("PurchaseRP", "AdminWallet");
                Session["page"] = "PurchaseRP";
                ViewBag.page = "PurchaseRP";
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //return PurchaseRP();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RejectPurchaseRP(string ID, string AdminRemark)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { });
                }

                AdminStockistDB.RejectPurchaseRP(ID, AdminRemark, Session["Admin"].ToString());

                //return RedirectToAction("PurchaseRP", "AdminWallet");
                Session["page"] = "PurchaseRP";
                ViewBag.page = "PurchaseRP";
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //return PurchaseRP();
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PurchaseRPHistory(int selectedPage = 1)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { });
                }

                int pages = 0;
                int ok = 0;
                string msg = string.Empty;

                PaginationMobileAgent model = new PaginationMobileAgent();


                DataSet dsWallet = AdminGeneralDB.GetAllPurchaseRP(selectedPage, out pages, out ok, out msg);

                selectedPage = ConstructPageList(selectedPage, pages, model);
                string previous = string.Empty;
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    MobileRequestList Request = new MobileRequestList();
                    //Request.Date = dr["CTUR_CREATEDON"].ToString();
                    Request.Date = Convert.ToDateTime(dr["CTUR_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                    Request.ReferenceOrder = dr["CTUR_REFFERALID"].ToString();
                    Request.Status = dr["CTUR_STATUS"].ToString();
                    Request.MemberID = dr["CUSR_USERNAME"].ToString();
                    Request.FullName = dr["CTUR_FULLNAME"].ToString();
                    Request.TopUpFor = dr["CTUR_FROM"].ToString();
                    Request.Currency = dr["CTUR_CURRENCY"].ToString();
                    Request.LocalAmount = float.Parse(dr["CTUR_LOCAL_AMOUNT"].ToString()).ToString("n2");
                    Request.Amount = float.Parse(dr["CTUR_AMOUNT"].ToString()).ToString("n2");
                    //Request.GeneratedBy = dr["CTUR_GENERATEBY"].ToString();
                    //Request.GeneratedOn = dr["CTUR_GENERATEON"].ToString();
                    if (Request.Status == "Reject")
                    {
                        Request.GeneratedOn = Convert.ToDateTime(dr["CTUR_APPROVETIME"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                        Request.GeneratedBy = dr["CTUR_APPROVEBY"].ToString();
                    }
                    else
                    {
                        Request.GeneratedOn = Convert.ToDateTime(dr["CTUR_GENERATEON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                        if (Convert.ToDateTime(Request.GeneratedOn).ToString("dd/MM/yyyy") == "01/01/1900")
                        {
                            Request.GeneratedOn = "";
                        }
                        Request.GeneratedBy = dr["CTUR_GENERATEBY"].ToString();
                    }
                    
                    Request.AdminID = dr["CTUR_APPROVEBY"].ToString();
                    //Request.ProcessedDate = dr["CTUR_APPROVETIME"].ToString();
                    Request.ProcessedDate = Convert.ToDateTime(dr["CTUR_APPROVETIME"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                    Request.Remark = dr["CTUR_REMARK"].ToString();
                    Request.ImagePath = dr["CTUR_IMAGE"].ToString();




                    model.MobileTopUpList.Add(Request);
                }

                return PartialView("PurchaseRPHistory", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public void DownloadPaymentSlip(string fileName, string refOrder)
        {
            //because the filename is fullpath, we just need the file name
            string fileNameRemoved = fileName.Substring(fileName.LastIndexOf('\\') + 1);
            refOrder = string.Format("{0}.jpg", refOrder.Trim());
            Response.Buffer = false; //transmitfile self buffers
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            //Response.AddHeader("Content-Disposition", string.Format("attachment; filename=myfile.pdf"));
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", System.Web.HttpUtility.UrlEncode(refOrder, System.Text.Encoding.UTF8)));
            Response.TransmitFile(Request.MapPath("~/Images/Payment/") + fileNameRemoved); //transmitfile keeps entire file from loading into memory
            Response.End();
        }

        public ActionResult GenerateRP(string ID)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { });
                }

                AdminStockistDB.GeneratePurchaseRP(ID, Session["Admin"].ToString());

                //return PurchaseRP();
                //return RedirectToAction("PurchaseRP", "AdminWallet");
                Session["page"] = "PurchaseRP";
                ViewBag.page = "PurchaseRP";
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        private int ConstructPageList(int selectedPage, int pages, PaginationMobileAgent model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }
        #endregion      

    }
}
