﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECFBase.Controllers.Default
{
    public class DefaultController : Controller
    {
        //
        // GET: /Default/
        public ActionResult Index()
        {
            if (Request.Url.AbsoluteUri.Contains("test.convexcapital.co.uk"))
            {
                return RedirectToAction("MemberLogin", "Member", new { });
            }
            if (Request.Url.AbsoluteUri.Contains("efund.convexcapital.co.uk"))
            {
                return RedirectToAction("MemberLogin", "Member", new { });
            }
            if (Request.Url.AbsoluteUri.Contains("login.convexcapital.co.uk"))
            {
                return Redirect("https://bvi.asia");
            }
            if (Request.Url.AbsoluteUri.Contains("convexcapital.co.uk"))
            {
                return Redirect("https://bvi.asia");
            }
            if (Request.Url.AbsoluteUri.Contains("www.convexcapital.co.uk"))
            {
                return Redirect("https://www.bvi.asia");
            }
            if (Request.Url.AbsoluteUri.Contains("sys.convexcapital.co.uk"))
            {
                return RedirectToAction("AdminLogin", "Admin", new {  });
            }            
            if (Request.Url.AbsoluteUri.Contains("sys.superccnet.com"))
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }
            if (Request.Url.AbsoluteUri.Contains("bvi.asia"))
            {
                return RedirectToAction("MemberLogin", "Member", new { });
            }
            if (Request.Url.AbsoluteUri.Contains("www.bvi.asia"))
            {
                return RedirectToAction("MemberLogin", "Member", new { });
            }
            return View();
        }
    }
}
