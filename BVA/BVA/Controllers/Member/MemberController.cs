﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ECFBase.Models;
using ECFBase.Components;
using System.Data;
using System.Globalization;
using System.IO;
using iTextSharp.text.pdf;
using System.Net;
using ECFBase.Helpers;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Specialized;
using System.Web.Script.Serialization;
using ECFBase.ThirdParties;
using Newtonsoft.Json;

namespace ECFBase.Controllers.Member
{
    public class MemberController : Controller
    {
        #region Home
        public ActionResult Home(string username, int selectedPage = -1, bool fresh = true , string RefreshFromLogo = "No")
        {
            int ok = 0;
            string msg = "";
                       
            if (!string.IsNullOrEmpty(username) && Session["Admin"] != null)
            {
                Session["Username"] = username;
            }

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ExpiredSession = "Yes" });
            }

            string Status = "";
            MemberDB.GetTime(out Status);

            if (Session["Admin"] == null)
            {
                if (Status == "1")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

            }

            string languageUsed = string.Empty;
            var dsMember = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

           
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["LanguageChosen"].ToString());
            System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["LanguageChosen"].ToString());

            
            MemberModel model = new MemberModel();
            
            var DS = AdminInfoDeskDB.GetAllImage();
            foreach (DataRow dr in DS.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["CINF_IMAGEPATH"].ToString();

                model.ImageList.Add(IFile);
            }

            int pages = 0;

            DataSet logo = AdminGeneralDB.GetCompanySetup();
            foreach (DataRow dr in logo.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["COM_IMAGEPATH"].ToString();

                ViewBag.Logo = logo.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString();
            }


            var dsLog = MemberDB.GetMemberOperationLog(Session["Username"].ToString());

            var BTC = MemberDB.GetParameterBasedOnName("BTCWalletAddress");
            var ETH = MemberDB.GetParameterBasedOnName("ETHWalletAddress");

            var ETHRate = MemberDB.GetParameterBasedOnName("ETH");
            var BTCRate = MemberDB.GetParameterBasedOnName("BTC");

            model.BTC = BTC.Tables[0].Rows[0]["CPARA_STRINGVALUE"].ToString();
            model.ETH = ETH.Tables[0].Rows[0]["CPARA_STRINGVALUE"].ToString();

            model.BTCRate = float.Parse(BTCRate.Tables[0].Rows[0]["CPARA_FLOATVALUE"].ToString()).ToString("n2");
            model.ETHRate = float.Parse(ETHRate.Tables[0].Rows[0]["CPARA_FLOATVALUE"].ToString()).ToString("n2");


            string RankCode = dsMember.Tables[0].Rows[0]["CRANK_CODE"].ToString();
            string Package = dsMember.Tables[0].Rows[0]["CPKG_CODE"].ToString();

            if (dsLog.Tables[0].Rows.Count == 0)
            {
                model.OperationLogShow = false;
            }
            else
            {
                model.OperationLogShow = true;
                string Name = Session["Username"].ToString();
                var ActiveDate = dsLog.Tables[0].Rows[0]["CUSEROPLOG_CREATEDON"];
                string ConvertDate = Convert.ToDateTime(ActiveDate).ToString("dd/MM/yyyy hh:MM:ss tt");
                string FullName = dsMember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                model.OperationLog = string.Format(Resources.OneForAll.OneForAll.msgWELCOME, Name, ConvertDate);

            }

            model.FirstName = dsMember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            model.JoinedDateString = dsMember.Tables[0].Rows[0]["CMEM_CREATEDON"].ToString();
            
            model.JoinedDate = Convert.ToDateTime(dsMember.Tables[0].Rows[0]["CMEM_CREATEDON"]);
            model.Rank = Misc.GetMemberRanking(dsMember.Tables[0].Rows[0]["CRANK_CODE"].ToString());
         
            

            model.Username = Session["Username"].ToString();
            model.MemberSelectedLanguage = languageUsed;
            model.InvoiceID = dsMember.Tables[0].Rows[0]["CINVNO_ID"].ToString();

            var languageList = Misc.GetLanguageList(ref ok, ref msg);
            model.LanguageList = from c in languageList
                                 select new SelectListItem
                                 {
                                     Selected = (c.Value == languageUsed) ? true : false,
                                     Text = c.Text,
                                     Value = c.Value
                                 };

            if (dsMember.Tables[0].Rows.Count != 0)
            {
                Session["CountryCode"] = dsMember.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                Session["MemberCurrency"] = dsMember.Tables[0].Rows[0]["CCURRENCY_CODE"].ToString();   
                model.MemberCurrency = dsMember.Tables[0].Rows[0]["CCURRENCY_NAME"].ToString();
                model.RegisterWallet = float.Parse(dsMember.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
                model.CashWallet = float.Parse(dsMember.Tables[0].Rows[0]["CMEM_CASHWALLET"].ToString());
                model.GoldPointWallet = float.Parse(dsMember.Tables[0].Rows[0]["CMEM_GOLDPOINT"].ToString());
                model.GoldPointOnly = float.Parse(dsMember.Tables[0].Rows[0]["CROI_ORI_AMOUNT"].ToString());
                model.MultiPointWallet = float.Parse(dsMember.Tables[0].Rows[0]["CMEM_MULTIPOINT"].ToString());
                model.RPCWallet = float.Parse(dsMember.Tables[0].Rows[0]["CMEM_REPURCHASEWALLET"].ToString());
                model.CompanyWallet = float.Parse(dsMember.Tables[0].Rows[0]["CMEM_COMPANYWALLET"].ToString());
                model.RMPWallet = float.Parse(dsMember.Tables[0].Rows[0]["CMEM_RMPWALLET"].ToString());
                model.JoinedDate = (DateTime)dsMember.Tables[0].Rows[0]["CMEM_CREATEDON"];
                model.FirstName = dsMember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                model.MemberType = dsMember.Tables[0].Rows[0]["CMEM_MEMBER_TYPE"].ToString();
                model.Rank = Misc.GetMemberRanking(dsMember.Tables[0].Rows[0]["CRANK_CODE"].ToString());
                model.PreviousRank = Misc.GetMemberRanking(dsMember.Tables[0].Rows[0]["CMEM_PREV_RANK_CODE"].ToString());
                Session["IsActivated"] = (dsMember.Tables[0].Rows[0]["CPKG_CODE"].ToString() == "FREE001") ? 0 : 1;   
                model.ResetPasswordPin = dsMember.Tables[0].Rows[0]["CMEM_RESET_PASSPIN"].ToString();
            }

            model.NewMessage = 1;
            model.TotalMessages = 100;

            #region Share
            float CurrentRate = 0;

            var ShareDetail = MemberShareDB.GetShareDetail(Session["Username"].ToString());

            model.ShareUnitBalance = (Convert.ToInt32(ShareDetail.Tables[2].Rows[0]["CSHRM_TOTAL_UNIT"].ToString())).ToString("###,###,##0");

            CurrentRate = float.Parse(ShareDetail.Tables[0].Rows[0]["CSHARE_RATE"].ToString());

            model.ShareValue = (Convert.ToInt32(ShareDetail.Tables[2].Rows[0]["CSHRM_TOTAL_UNIT"].ToString()) * CurrentRate).ToString("n2");
            #endregion

            #region InfoDesk


            DataSet dsEventsPromo = AdminInfoDeskDB.GetAllInfoDesk("E", Session["LanguageChosen"].ToString(), 1, out pages, out ok, out msg);
            foreach (DataRow dr in dsEventsPromo.Tables[0].Rows)
            {
                var item = new InfoDeskModel();
                item.InfoDeskTitle = dr["CMULTILANGINFODESK_TITLE"].ToString();
                item.InfoDeskID = int.Parse(dr["CINF_ID"].ToString());
                item.IsNewInfo = Convert.ToDateTime(dr["CINF_CREATEDON"]).AddDays(14) > DateTime.Now;
                item.Created = Convert.ToDateTime(dr["CINF_CREATEDON"]);
                model.EventAndPromotions.Add(item);
            }
            string bB = string.Empty;

            DataSet dsCorporateNews = AdminInfoDeskDB.GetAllInfoDesk("C", Session["LanguageChosen"].ToString(), 1, out pages, out ok, out msg);

            foreach (DataRow dr in dsCorporateNews.Tables[0].Rows)
            {
                bB = dr["CINF_CODE"].ToString();
                var item = new InfoDeskModel();

                item.InfoDeskTitle = dr["CMULTILANGINFODESK_TITLE"].ToString();
                item.InfoDeskID = int.Parse(dr["CINF_ID"].ToString());
                item.InfoDeskContent = dr["CMULTILANGINFODESK_CONTENT"].ToString();
                item.IsNewInfo = Convert.ToDateTime(dr["CINF_CREATEDON"]).AddDays(14) > DateTime.Now;
                item.Created = Convert.ToDateTime(dr["CINF_CREATEDON"]);
                item.A = dr["CINF_CODE"].ToString();

                List<SelectListItem> languages = Misc.GetLanguageList(ref ok, ref msg);
                DataSet dsMultiLanguageInfoDesk = AdminInfoDeskDB.GetMultiLanguageInfoDeskByID(item.InfoDeskID, out ok, out msg);


                foreach (SelectListItem language in languages)
                {
                    DataRow[] drs = dsMultiLanguageInfoDesk.Tables[0].Select("CINF_ID='" + item.InfoDeskID + "' AND CLANG_CODE='" + language.Value + "'");
                    int extensionIndex = drs[0]["CINF_IMAGEPATH"].ToString().LastIndexOf('\\');

                    string imageName = drs[0]["CINF_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
                    item.Image = "http://" + Request.Url.Authority + "/Products/" +bB + "/" + imageName;

                    if (imageName == "")
                    {
                        item.Image = "";
                    }
                }

                model.CorporateNews.Add(item);
            }
            ViewBag.Count = model.CorporateNews.Count();

            DataSet dsMyNews = AdminHelpDeskDB.GetAllNoticeByUsername(Session["Username"].ToString(), Session["LanguageChosen"].ToString(), 1, out pages, out ok, out msg);
            foreach (DataRow dr in dsMyNews.Tables[0].Rows)
            {
                var item = new InfoDeskModel();
                item.InfoDeskTitle = dr["CMULTILANGNOT_TITLE"].ToString();
                item.InfoDeskID = int.Parse(dr["CNOT_ID"].ToString());
                item.IsNewInfo = Convert.ToDateTime(dr["CNOT_CREATEDON"]).AddDays(14) > DateTime.Now;
                item.Created = Convert.ToDateTime(dr["CNOT_CREATEDON"]);
                model.MyNews.Add(item);
            }


            #endregion

            if (Session["page"] != null)
            {
                ViewBag.page = Session["page"];
                Session["page"] = null;
            }

            if (Session["selectedPage"] != null)
            {
                ViewBag.selectedPage = Session["selectedPage"];
                Session["selectedPage"] = null;
            }

            if (Session["walletType"] != null)
            {
                ViewBag.walletType = Session["walletType"];
                Session["walletType"] = null;
            }

            if (Session["date"] != null)
            {
                ViewBag.date = Session["date"];
                Session["date"] = null;
            }

            if (Session["searchUsername"] != null)
            {
                ViewBag.searchUsername = Session["searchUsername"];
                Session["searchUsername"] = null;
            }

            if (model.ResetPasswordPin == "False" && Session["Admin"] == null)
            {
                return View("ResetPasswordAndPin", model);
            }

            if (Session["isShow"] != null)
            {
                string news = Session["isShow"].ToString();
                if (news == "0")
                {
                    ViewBag.Show = "1";

                    int isShow = 1;
                    Session["isShow"] = isShow;
                }
            }

            if (RefreshFromLogo == "Yes")
            {
                ViewBag.Show = "1";
                int isShow = 1;
                Session["isShow"] = isShow;
            }

            return View("Home", model);
        }
        #endregion
        public ActionResult Currency(int selectedPage = -1, bool fresh = true)
        {

            string Status = "";
            MemberDB.GetTime(out Status);

            if (Session["Admin"] == null)
            {
                if (Status == "1")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

            }

            int ok = 0;
            string msg = "";
            int pages = 0;

            MemberModel model = new MemberModel();

           

            DataSet dsCurrency = AdminGeneralDB.GetAllCurrency(1, Session["LanguageChosen"].ToString(), out pages, out ok, out msg);

            foreach (DataRow dr in dsCurrency.Tables[0].Rows)
            {
                var item = new CurrencySetupModel();
                item.CountryName = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                item.CurrencyId = Convert.ToInt32(dr["CCURRENCY_ID"]);
                item.CurrencyCode = dr["CCURRENCY_CODE"].ToString();
                item.CurrencyName = dr["CCURRENCY_NAME"].ToString();
                item.CurrencyBuy = Convert.ToDouble(dr["CCURRENCY_BUY"]);
                item.CurrencySell = Convert.ToDouble(dr["CCURRENCY_SELL"]);
                item.CountryCode = dr["CCOUNTRY_CODE"].ToString();
                var img = dr["CCOUNTRY_IMAGEPATH"].ToString();
                var filename = Path.GetFileName(img);
                item.CountryImage = "http://" + Request.Url.Authority + "/Country/" + item.CountryCode + "/" + filename;
                model.Currency.Add(item);
            }

            
            return View("Currency", model);
        }

        #region ChangeLanguage

        public ActionResult ChangeLanguage(string languageCode)
        {
            Session["LanguageChosen"] = languageCode;
            var username = Session["Username"] == null ? string.Empty : Session["Username"].ToString();
            return Home(username,-1,false);
        }

        #endregion

        #region MemberLogin
        public ActionResult MemberLogin(string selectedLanguage = null , string ReloadPage = "No" , string Expired = "No" )
        {

            if (Request.Url.AbsoluteUri.Contains("test.convexcapital.co.uk"))
            {

            }
            else if (Request.Url.AbsoluteUri.Contains("efund.convexcapital.co.uk"))
            {

            }
            else
            {
                if (Request.Url.AbsoluteUri.Contains("convexcapital.co.uk"))
                {
                    return Redirect("https://bvi.asia");
                }
                if (Request.Url.AbsoluteUri.Contains("www.convexcapital.co.uk"))
                {
                    return Redirect("https://www.bvi.asia");
                }
            }
           
            if (ReloadPage == "Yes")
            {
                ViewBag.ReloadPage = "Yes";
            }

            if (Expired == "Yes")
            {
                ViewBag.Expired = "Yes";
            }
            else
            {
                ViewBag.Expired = "No";
            }

            if (!string.IsNullOrEmpty(selectedLanguage))
            {
                Session["LanguageChosen"] = selectedLanguage;
                System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["LanguageChosen"].ToString());
                System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["LanguageChosen"].ToString());
                return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "No"});
            }

            DataSet logo = AdminGeneralDB.GetCompanySetup();
            foreach (DataRow dr in logo.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["COM_IMAGEPATH"].ToString();

                ViewBag.Logo = logo.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString();
            }
            
            //Clear all session except selected language.
            string language = Session["LanguageChosen"].ToString();
            Session.RemoveAll();
            Session["LanguageChosen"] = language;
            return View();
        }
        #endregion

        #region PINValidation
        public ActionResult PINValidation(string Type)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new {  ExpiredSession = "Yes" });
            }

            if (Type == ProjectStaticString.EditProfile)
            {
                ViewBag.MainModule = ECFBase.Resources.OneForAll.OneForAll.mnuMember;
                ViewBag.SubModule = ECFBase.Resources.OneForAll.OneForAll.mnuProfile;
                ViewBag.TypeResource = ECFBase.Resources.OneForAll.OneForAll.lblEditProfile;
            }
            else if (Type == ProjectStaticString.BankAccInfo)
            {
                ViewBag.MainModule = ECFBase.Resources.OneForAll.OneForAll.mnuMember;
                ViewBag.SubModule = ECFBase.Resources.OneForAll.OneForAll.mnuProfile;
                ViewBag.TypeResource = ECFBase.Resources.OneForAll.OneForAll.lblChgBankInfo;
            }

            ViewBag.Type = Type;
            PINValidationModel model = new PINValidationModel();
            return PartialView("PINValidation", model);
        }

        public ActionResult ValidatePIN(string PIN, string Type)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ExpiredSession = "Yes" });
            }

            int ok;
            string msg;
            MemberDB.ValidateUserPIN(Session["Username"].ToString(), Authentication.Encrypt(PIN), out ok, out msg);
            if (ok == 0)
            {
                Response.Write(Resources.OneForAll.OneForAll.msgInvalidPIN);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (Type == ProjectStaticString.EditProfile)
            {
                //redirect to Controller
                return RedirectToAction("EditProfile", "MemberProfile");
            }
            else if (Type == ProjectStaticString.BankAccInfo)
            {
                return RedirectToAction("BankAccInfo", "MemberProfile");
            }

            return Home(Session["Username"].ToString());
        }
        #endregion
        private int BinaryConstructPageList(int selectedPage, int pages, MarketTreeModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        #region MarketTree
        [HttpPost]
        public ActionResult MarketTree(int selectedPage = 1)
        {
            Session["RegisterPage"] = "RP";
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" ,  Expired = "Yes"});
            }

            string Status = "";
            MemberDB.GetTime(out Status);

            if (Session["Admin"] == null)
            {
                if (Status == "1")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

            }

            int ok = 0, pages = 0;
            string msg = "";
            var member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
            string TopMember = member.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();


            MarketTreeModel memberMarketTree = GetAllMarketTreeMember(TopMember);
            memberMarketTree.country = member.Tables[0].Rows[0]["CCOUNTRY_ID"].ToString();


            DataSet logo = AdminGeneralDB.GetRankIcon();
            foreach (DataRow dr in logo.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["CRANKSET_ICON"].ToString();
                RankModel RankList = new RankModel();
                RankList.RankIconPath = dr["CRANKSET_ICON"].ToString();
                ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();
                RankList.RankName = dr["CRANKSET_NAME"].ToString();
                memberMarketTree.RankList.Add(RankList);
            }


            string keke = Session["LanguageChosen"].ToString();
            if (Session["LanguageChosen"].ToString().Equals("zh-cn", StringComparison.InvariantCultureIgnoreCase))
            {
                memberMarketTree.Picture = "RegisterEn.png";
            }
            else if (Session["LanguageChosen"].ToString().Equals("en-US", StringComparison.InvariantCultureIgnoreCase))
            {
                memberMarketTree.Picture = "RegisterEn.png";
            }
            else if (Session["LanguageChosen"].ToString().Equals("zh-tw", StringComparison.InvariantCultureIgnoreCase))
            {
                memberMarketTree.Picture = "RegisterEn.png";
            }
            else if (Session["LanguageChosen"].ToString().Equals("ja-JP", StringComparison.InvariantCultureIgnoreCase))
            {
                memberMarketTree.Picture = "RegisterEn.png";
            }


            return PartialView("MarketTree", memberMarketTree);
        }

        public ActionResult GroupListing(int selectedPage = 1)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ExpiredSession = "Yes" });
            }


            //int ok = 0, 
            //string msg = "";
            //var member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
            //string TopMember = member.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();

            int pages = 0;
            MarketTreeModel memberMarketTree = new MarketTreeModel();

            DataSet dsBinaryList = MemberShopCenterDB.GetAllBinaryListByUsername(Session["Username"].ToString(), selectedPage, out pages);

            selectedPage = BinaryConstructPageList(selectedPage, pages, memberMarketTree);

            //if (dsBinaryList.Tables[0].Rows.Count != 0)
            //{
            //    memberMarketTree.FirstLevelUsername = dsBinaryList.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
            //    memberMarketTree.FirstLevelFullName = dsBinaryList.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            //    //memberMarketTree.FirstLevelSponsor = dsBinaryList.Tables[0].Rows[0]["CMTREE_UNDER"].ToString();
            //    memberMarketTree.FirstLevelUpline = dsBinaryList.Tables[0].Rows[0]["CMEM_INTRO"].ToString();
            //    memberMarketTree.FirstLevelLevel = dsBinaryList.Tables[0].Rows[0]["CMTREE_LEVEL"].ToString();
            //    memberMarketTree.FirstLevelRank = Misc.GetMemberRanking(dsBinaryList.Tables[0].Rows[0]["CRANK_CODE"].ToString());
            //    memberMarketTree.FirstLevelCreatedDate = Convert.ToDateTime(dsBinaryList.Tables[0].Rows[0]["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
            //    memberMarketTree.FirstLevelRankIcon = dsBinaryList.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

            //}

            foreach (DataRow dr in dsBinaryList.Tables[0].Rows)
            {              
                IntroListModel sponsorlist = new IntroListModel();
                sponsorlist.No = dr["rownumber"].ToString();
                sponsorlist.Username = dr["CUSR_USERNAME"].ToString();
                sponsorlist.FullName = dr["CUSR_FULLNAME"].ToString();

                string firstmember = dr["CUSR_USERNAME"].ToString();
                if (firstmember == Session["Username"].ToString())
                {

                    sponsorlist.Upline = "";
                }
                else
                {
                    sponsorlist.Upline = dr["CMEM_INTRO"].ToString();
                }

                sponsorlist.Level = dr["CMTREE_LEVEL"].ToString();
                sponsorlist.Rank = Misc.GetMemberRanking(dr["CRANK_CODE"].ToString());
                sponsorlist.CreatedDate = Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                sponsorlist.RankIcon = dr["CRANKSET_ICON"].ToString();

                if (dr["CMTREE_LEVEL"].ToString() != "0")
                {
                    memberMarketTree.SponsorList.Add(sponsorlist);
                }

                    
            }
           


            return PartialView("GroupListing", memberMarketTree);
        }

        public ActionResult GroupChart(int selectedPage = 1)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ExpiredSession = "Yes" });
            }
            
            int pages = 0;

            MarketTreeModel memberMarketTree = new MarketTreeModel();
            DataSet dsBinaryList = MemberShopCenterDB.GetAllBinaryListByUsername(Session["Username"].ToString(), selectedPage, out pages);

            if (dsBinaryList.Tables[0].Rows.Count != 0)
            {
                memberMarketTree.FirstLevelUsername = dsBinaryList.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
                memberMarketTree.FirstLevelFullName = dsBinaryList.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                memberMarketTree.FirstLevelUpline = dsBinaryList.Tables[0].Rows[0]["CMEM_INTRO"].ToString();
                memberMarketTree.FirstLevelLevel = dsBinaryList.Tables[0].Rows[0]["CMTREE_LEVEL"].ToString();
                memberMarketTree.FirstLevelRank = Misc.GetMemberRanking(dsBinaryList.Tables[0].Rows[0]["CRANK_CODE"].ToString());
                memberMarketTree.FirstLevelCreatedDate = Convert.ToDateTime(dsBinaryList.Tables[0].Rows[0]["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                memberMarketTree.FirstLevelRankIcon = dsBinaryList.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

            }

            DataSet logo = AdminGeneralDB.GetRankIcon();
            foreach (DataRow dr in logo.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["CRANKSET_ICON"].ToString();
                RankModel RankList = new RankModel();
                RankList.RankIconPath = dr["CRANKSET_ICON"].ToString();
                ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();
                RankList.RankName = dr["CRANKSET_NAME"].ToString();

                memberMarketTree.RankList.Add(RankList);
            }
            
            return PartialView("GroupChart", memberMarketTree);
        }

        [HttpPost]
        public JsonResult FindNextGroupChart(string username )
        {
            int ok = 0;
            string msg = "";

            DataSet dsNetworkTree = MemberShopCenterDB.GetBinaryChartLevel2(username, out ok, out msg);
            ViewBag.MemberCount = 0;

            var member = MemberDB.GetMemberByUsername(username, out ok, out msg);
            List<MemberList> list = new List<MemberList>();

            foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
            {
                var resultList = new MemberList();
                //resultList.No = dr["rownumber"].ToString();
                resultList.Username = dr["CUSR_USERNAME"].ToString();
                resultList.FullName = dr["CUSR_FULLNAME"].ToString();

                string firstmember = dr["CUSR_USERNAME"].ToString();

                if (firstmember == Session["Username"].ToString())
                {

                    resultList.Upline = "";
                }
                else
                {
                    resultList.Upline = dr["CMEM_INTRO"].ToString();
                }

                resultList.Level = dr["CMTREE_LEVEL"].ToString();
                resultList.Rank = Misc.GetMemberRanking(dr["CRANK_CODE"].ToString());
                resultList.CreatedDate = Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                resultList.RankIcon = dr["CRANKSET_ICON"].ToString();              

                if (dr["CMTREE_LEVEL"].ToString() == "1")
                {
                    list.Add(resultList);
                }
            }

            return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        }

        private MarketTreeModel GetBinaryListing(string TopMember, int selectedPage = 1)
        {

            int pages = 0, ok = 0;
            string msg = "";

            MarketTreeModel memberMarketTree = new MarketTreeModel();

            var member = MemberDB.GetMemberByUsername(TopMember, out ok, out msg);
            TopMember = member.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
            DataSet dsBinaryList = MemberShopCenterDB.GetAllBinaryListByUsername(TopMember, selectedPage, out pages);

            if (dsBinaryList.Tables[0].Rows.Count != 0)
            {
                memberMarketTree.FirstLevelUsername = dsBinaryList.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
                memberMarketTree.FirstLevelFullName = dsBinaryList.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                memberMarketTree.FirstLevelUpline = dsBinaryList.Tables[0].Rows[0]["CMEM_INTRO"].ToString();
                memberMarketTree.FirstLevelLevel = dsBinaryList.Tables[0].Rows[0]["CMTREE_LEVEL"].ToString();
                memberMarketTree.FirstLevelRank = Misc.GetMemberRanking(dsBinaryList.Tables[0].Rows[0]["CRANK_CODE"].ToString());
                memberMarketTree.FirstLevelCreatedDate = Convert.ToDateTime(dsBinaryList.Tables[0].Rows[0]["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                memberMarketTree.FirstLevelRankIcon = member.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

            }

            selectedPage = BinaryConstructPageList(selectedPage, pages, memberMarketTree);

            foreach (DataRow dr in dsBinaryList.Tables[0].Rows)
            {
                IntroListModel sponsorlist = new IntroListModel();
                sponsorlist.No = dr["rownumber"].ToString();
                sponsorlist.Username = dr["CUSR_USERNAME"].ToString();
                sponsorlist.FullName = dr["CUSR_FULLNAME"].ToString();
                //sponsorlist.Sponsor = dr["CMTREE_UNDER"].ToString();

                string firstmember = dr["CUSR_USERNAME"].ToString();
                if (firstmember == Session["Username"].ToString())
                {

                    sponsorlist.Upline = "";
                }
                else
                {
                    sponsorlist.Upline = dr["CMEM_INTRO"].ToString();
                }

                sponsorlist.Level = dr["CMTREE_LEVEL"].ToString();
                sponsorlist.Rank = Misc.GetMemberRanking(dr["CRANK_CODE"].ToString());
                sponsorlist.CreatedDate = Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                //sponsorlist.RankIcon = member.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();
                sponsorlist.RankIcon = dr["CRANKSET_ICON"].ToString();

                if (dr["CMTREE_LEVEL"].ToString() != "0")
                {
                    memberMarketTree.SponsorList.Add(sponsorlist);
                }

                
            }
            return memberMarketTree;

        }

        //public ActionResult MarketTree(int selectedPage = 1)
        //{
        //    if (Session["Username"] == null || Session["Username"].ToString() == "")
        //    {
        //         return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
        //    }


        //    int ok = 0;
        //    string msg = "";
        //    var member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
        //    string TopMember = member.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();


        //    MarketTreeModel memberMarketTree = GetAllMarketTreeMember(TopMember);

        //    memberMarketTree.country = member.Tables[0].Rows[0]["CCOUNTRY_ID"].ToString();

        //    var Stockist = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
        //    memberMarketTree.Stockist = Stockist.Tables[0].Rows[0]["CMEM_MEMBER_TYPE"].ToString();


        //    DataSet logo = AdminGeneralDB.GetRankIcon();
        //    foreach (DataRow dr in logo.Tables[0].Rows)
        //    {
        //        var IFile = new ImageFile();
        //        IFile.FileName = dr["CRANKSET_ICON"].ToString();
        //        RankModel RankList = new RankModel();
        //        RankList.RankIconPath = dr["CRANKSET_ICON"].ToString();
        //        //RankList.RankIconName = float.Parse(dr["AMOUNT"].ToString());
        //        //RankList.RankAmount = float.Parse(dr["AMOUNT"].ToString());
        //        ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();
        //        RankList.RankName = dr["CRANKSET_NAME"].ToString();

        //        memberMarketTree.RankList.Add(RankList);
        //    }


        //    string keke = Session["LanguageChosen"].ToString();
        //    if (Session["LanguageChosen"].ToString().Equals("zh-cn", StringComparison.InvariantCultureIgnoreCase))
        //    {
        //        memberMarketTree.Picture = "RegisterEn.png";
        //    }
        //    else if (Session["LanguageChosen"].ToString().Equals("en-US", StringComparison.InvariantCultureIgnoreCase))
        //    {
        //        memberMarketTree.Picture = "RegisterEn.png";
        //    }
        //    else if (Session["LanguageChosen"].ToString().Equals("zh-tw", StringComparison.InvariantCultureIgnoreCase))
        //    {
        //        memberMarketTree.Picture = "RegisterEn.png";
        //    }
        //    else if (Session["LanguageChosen"].ToString().Equals("ja-JP", StringComparison.InvariantCultureIgnoreCase))
        //    {
        //        memberMarketTree.Picture = "RegisterEn.png";
        //    }


        //    int pages = 0;
        //    //PaginationIntroListModel model = new PaginationIntroListModel();
        //    DataSet dsBinaryList = MemberShopCenterDB.GetAllBinaryListByUsername(Session["Username"].ToString(), selectedPage, out pages);

        //    if (dsBinaryList.Tables[0].Rows.Count != 0)
        //    {
        //        memberMarketTree.FirstLevelUsername = dsBinaryList.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
        //        memberMarketTree.FirstLevelFullName = dsBinaryList.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
        //        //memberMarketTree.FirstLevelSponsor = dsBinaryList.Tables[0].Rows[0]["CMTREE_UNDER"].ToString();
        //        memberMarketTree.FirstLevelUpline = dsBinaryList.Tables[0].Rows[0]["CMEM_INTRO"].ToString();
        //        memberMarketTree.FirstLevelLevel = dsBinaryList.Tables[0].Rows[0]["CMTREE_LEVEL"].ToString();
        //        memberMarketTree.FirstLevelRank = Misc.RankNumber(dsBinaryList.Tables[0].Rows[0]["CRANK_CODE"].ToString());
        //        memberMarketTree.FirstLevelCreatedDate = Convert.ToDateTime(dsBinaryList.Tables[0].Rows[0]["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
        //        memberMarketTree.FirstLevelRankIcon = member.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();
        //    }

        //    selectedPage = BinaryConstructPageList(selectedPage, pages, memberMarketTree);

        //    foreach (DataRow dr in dsBinaryList.Tables[0].Rows)
        //    {
        //        IntroListModel sponsorlist = new IntroListModel();
        //        sponsorlist.No = dr["rownumber"].ToString();
        //        sponsorlist.Username = dr["CUSR_USERNAME"].ToString();
        //        sponsorlist.FullName = dr["CUSR_FULLNAME"].ToString();
        //        //sponsorlist.Sponsor = dr["CMTREE_UNDER"].ToString();

        //        string firstmember= dr["CUSR_USERNAME"].ToString();
        //        if (firstmember == Session["Username"].ToString())
        //        {

        //            sponsorlist.Upline = "";
        //        }
        //        else
        //        {
        //            sponsorlist.Upline = dr["CMEM_INTRO"].ToString();
        //        }

        //        sponsorlist.Level = dr["CMTREE_LEVEL"].ToString();
        //        sponsorlist.Rank = Misc.RankNumber(dr["CRANK_CODE"].ToString());
        //        sponsorlist.CreatedDate = Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
        //        sponsorlist.RankIcon = member.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

        //        memberMarketTree.SponsorList.Add(sponsorlist);
        //    }


        //    return PartialView("MarketTree", memberMarketTree);
        //}

        private MarketTreeModel GetAllMarketTreeMember(string TopMember)
        {
            MarketTreeModel treeMC = new MarketTreeModel();

            int nCount = 0;
            string fullName = "";

            DataSet dsFollow = MemberDB.GetFollowID(TopMember);

            //Loop for 15 times
            while (nCount <= 14)
            {
                if (dsFollow.Tables[nCount].Rows.Count == 0)
                {
                    treeMC.MemberList.Add("0");
                    treeMC.LogoList.Add("~");
                    treeMC.TooltipList.Add(ConstructTooltipTree("", "", "0", "0", "0", "0", "", ""));
                    treeMC.PackageList.Add("");
                    treeMC.DateList.Add("");
                    treeMC.AccYJLeftList.Add("");
                    treeMC.AccYJRightList.Add("");
                    treeMC.CFBalLeftList.Add("");
                    treeMC.CFBalRightList.Add("");
                    treeMC.SalesLeftList.Add("");
                    treeMC.SalesRightList.Add("");
                    treeMC.TotalDownlineLeft.Add("");
                    treeMC.TotalDownlineRight.Add("");

                    if (nCount == 0)
                    {
                        treeMC.MainLeftYJ = "0";
                        treeMC.MainLeftBalance = "0";
                        treeMC.MainRightYJ = "0";
                        treeMC.MainRightBalance = "0";
                    }
                }
                else
                {
                    DataRow dr = dsFollow.Tables[nCount].Rows[0];
                    DateTime joinedDate = DateTime.Parse(dr["CUSR_CREATEDON"].ToString());
                    fullName = string.Format("{0} {1}", dr["CUSR_FULLNAME"].ToString(), dr["CUSR_FULLNAME"].ToString());
                    treeMC.MemberList.Add(dr["CUSR_USERNAME"].ToString());

                    //int ok = 0;
                    //string msg = "";
                    //string  imagePath = "";
                    //var member = MemberDB.GetMemberByUsername(dr["CUSR_USERNAME"].ToString(), out ok, out msg);
                    //imagePath = member.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

                    treeMC.LogoList.Add(dr["CRANKSET_ICON"].ToString());
                    treeMC.TooltipList.Add(ConstructTooltipTree(fullName, joinedDate.ToShortDateString(), dr["CMTREE_GROUPAYJ"].ToString(), dr["CMTREE_GROUPBYJ"].ToString(), dr["CMTREE_GROUPAGP"].ToString(), dr["CMTREE_GROUPBGP"].ToString(), dr["CMEM_INTRO"].ToString(), string.Format("人民币{0}", dr["CPKG_AMOUNT"].ToString())));
                    treeMC.PackageList.Add(string.Format("{0}", Helper.NVL(dr["CUSRINFO_NICKNAME"])));
                    treeMC.DateList.Add(string.Format("{0}", joinedDate.ToString("dd/MM/yyyy")));


                    float leftYJ1 = float.Parse(dr["CMTREE_GROUPAYJ"].ToString());
                    float rightYJ1 = float.Parse(dr["CMTREE_GROUPBYJ"].ToString());
                    float leftGP1 = float.Parse(dr["CMTREE_GROUPAGP"].ToString());
                    float rightGP1 = float.Parse(dr["CMTREE_GROUPBGP"].ToString());
                    float leftsales1 = float.Parse(dr["LEFTSALES"].ToString());
                    float rightsales1 = float.Parse(dr["RIGHTSALES"].ToString());
                    int lefttotaldownline1 = Convert.ToInt32(dr["CMTREE_TGROUPAMEMBER"].ToString());
                    int righttotaldownline1 = Convert.ToInt32(dr["CMTREE_TGROUPBMEMBER"].ToString());

                    treeMC.AccYJLeftList.Add(string.Format("{0}", Helper.NVL(leftYJ1.ToString("###,##0"))));
                    treeMC.AccYJRightList.Add(string.Format("{0}", Helper.NVL(rightYJ1.ToString("###,##0"))));
                    treeMC.CFBalLeftList.Add(string.Format("{0}", Helper.NVL(leftGP1.ToString("###,##0"))));
                    treeMC.CFBalRightList.Add(string.Format("{0}", Helper.NVL(rightGP1.ToString("###,##0"))));
                    treeMC.SalesLeftList.Add(string.Format("{0}", Helper.NVL(leftsales1.ToString("###,##0"))));
                    treeMC.SalesRightList.Add(string.Format("{0}", Helper.NVL(rightsales1.ToString("###,##0"))));
                    treeMC.TotalDownlineLeft.Add(string.Format("{0}", Helper.NVL(lefttotaldownline1.ToString())));
                    treeMC.TotalDownlineRight.Add(string.Format("{0}", Helper.NVL(righttotaldownline1.ToString())));


                    if (nCount == 0)
                    {
                        float leftYJ = float.Parse(dr["CMTREE_GROUPAYJ"].ToString());
                        float rightYJ = float.Parse(dr["CMTREE_GROUPBYJ"].ToString());
                        float leftGP = float.Parse(dr["CMTREE_GROUPAGP"].ToString());
                        float rightGP = float.Parse(dr["CMTREE_GROUPBGP"].ToString());
                        float leftsales = float.Parse(dr["LEFTSALES"].ToString());
                        float rightsales = float.Parse(dr["RIGHTSALES"].ToString());

                        treeMC.MainLeftYJ = leftYJ.ToString("###,##0") ;
                        treeMC.MainRightYJ = rightYJ.ToString("###,##0");

                        treeMC.MainLeftBalance =  leftGP.ToString("###,##0");
                        treeMC.MainRightBalance = rightGP.ToString("###,##0");

                        treeMC.MainLeftSales = leftsales.ToString("###,##0");
                        treeMC.MainRightSales = rightsales.ToString("###,##0");
                    }
                    else if (nCount == 1)
                    {
                        float leftYJ = float.Parse(dr["CMTREE_GROUPAYJ"].ToString());
                        float rightYJ = float.Parse(dr["CMTREE_GROUPBYJ"].ToString());
                        float leftGP = float.Parse(dr["CMTREE_GROUPAGP"].ToString());
                        float rightGP = float.Parse(dr["CMTREE_GROUPBGP"].ToString());
                        float leftsales = float.Parse(dr["LEFTSALES"].ToString());
                        float rightsales = float.Parse(dr["RIGHTSALES"].ToString());

                        treeMC.FirstLeftYJ = leftYJ.ToString("###,##0");
                        treeMC.FirstRightYJ = rightYJ.ToString("###,##0");

                        treeMC.FirstLeftBalance = leftGP.ToString("###,##0");
                        treeMC.FirstRightBalance = rightGP.ToString("###,##0");

                        treeMC.FirstLeftSales = leftsales.ToString("###,##0");
                        treeMC.FirstRightSales = rightsales.ToString("###,##0");
                    }
                    else if (nCount == 2)
                    {
                        float leftYJ = float.Parse(dr["CMTREE_GROUPAYJ"].ToString());
                        float rightYJ = float.Parse(dr["CMTREE_GROUPBYJ"].ToString());
                        float leftGP = float.Parse(dr["CMTREE_GROUPAGP"].ToString());
                        float rightGP = float.Parse(dr["CMTREE_GROUPBGP"].ToString());
                        float leftsales = float.Parse(dr["LEFTSALES"].ToString());
                        float rightsales = float.Parse(dr["RIGHTSALES"].ToString());
                        int lefttotaldownline = Convert.ToInt32(dr["CMTREE_TGROUPAMEMBER"].ToString());
                        int righttotaldownline = Convert.ToInt32(dr["CMTREE_TGROUPBMEMBER"].ToString());

                        treeMC.SecondLeftYJ = leftYJ.ToString("###,##0");
                        treeMC.SecondRightYJ = rightYJ.ToString("###,##0");

                        treeMC.SecondLeftBalance = leftGP.ToString("###,##0");
                        treeMC.SecondRightBalance = rightGP.ToString("###,##0");

                        treeMC.SecondLeftSales = leftsales.ToString("###,##0");
                        treeMC.SecondRightSales = rightsales.ToString("###,##0");

                        treeMC.SecondLeftTotalDownline = lefttotaldownline.ToString("###,##0");
                        treeMC.SecondRightTotalDownline = righttotaldownline.ToString("###,##0");
                    }
                }

                nCount++;
            }

            return treeMC;
        }

        public string ConstructTooltipTree(string fullName, string joinedDate, string strLeftYJ, string strRightYJ, string LeftGP, string RightGP, string sponsor, string investStar)
        {
            float fLeftYJ = float.Parse(strLeftYJ);
            float fRightYJ = float.Parse(strRightYJ);
            float fLeftGP = float.Parse(LeftGP);
            float fRightGP = float.Parse(RightGP);
            string leftSales = Resources.OneForAll.OneForAll.lblLeftSale;
            string rightSales = Resources.OneForAll.OneForAll.lblRightSale;
            string sponsorLabel = Resources.OneForAll.OneForAll.lblSponsor;
            string tooltip = string.Format("{0}({1})\n{2}\n{3} : {4}\n{5}     :{6}\t\t{7}     ", fullName, joinedDate,  sponsorLabel, sponsor, leftSales, strLeftYJ, rightSales, strRightYJ);
            return tooltip;
        }

        public ActionResult GetMarketTreeMostLeft(string Username)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ExpiredSession = "Yes" });
            }

            int isBelongTo;
            MemberDB.MemberIsInMarketTreeByUsername(Username, Session["Username"].ToString(), out isBelongTo);

            if (isBelongTo == 0)//not belong to
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.msgNotInTree, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            DataSet dsFollow = MemberDB.GetMarketTreeMostLeft(Username);
            DataRow dr = dsFollow.Tables[0].Rows[0];
            string upMember = dr["CUSR_USERNAME"].ToString();


            return MarketTreeByUsername(upMember);
        }

        public ActionResult GetMarketTreeMostRight(string Username)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ExpiredSession = "Yes" });
            }
            int isBelongTo;
            MemberDB.MemberIsInMarketTreeByUsername(Username, Session["Username"].ToString(), out isBelongTo);

            if (isBelongTo == 0)//not belong to
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.msgNotInTree, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            DataSet dsFollow = MemberDB.GetMarketTreeMostRight(Username);
            DataRow dr = dsFollow.Tables[0].Rows[0];
            string upMember = dr["CUSR_USERNAME"].ToString();



            return MarketTreeByUsername(upMember);
        }


        [HttpPost]
        public ActionResult MarketTreeByUsername(string TopMember)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ExpiredSession = "Yes" });
            }

            int isBelongTo;
            MemberDB.MemberIsInMarketTreeByUsername(TopMember, Session["Username"].ToString(), out isBelongTo);

            if (isBelongTo == 0)//not belong to
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.msgNotInTree, TopMember));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (TopMember == "Main")
            {
                TopMember = Session["Username"].ToString();
            }

            try
            {
                MarketTreeModel memberMarketTree = GetAllMarketTreeMember(TopMember);

                

                int ok = 0;
                string msg = "";
                var member = MemberDB.GetMemberByUsername(TopMember, out ok, out msg);
                memberMarketTree.country = member.Tables[0].Rows[0]["CCOUNTRY_ID"].ToString();
                var Stockist = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
                memberMarketTree.Stockist = Stockist.Tables[0].Rows[0]["CMEM_MEMBER_TYPE"].ToString();
                if (Session["LanguageChosen"].ToString().Equals("zh-cn", StringComparison.InvariantCultureIgnoreCase))
                {
                    memberMarketTree.Picture = "RegisterEn.png";
                }
                else if (Session["LanguageChosen"].ToString().Equals("en-US", StringComparison.InvariantCultureIgnoreCase))
                {
                    memberMarketTree.Picture = "RegisterEn.png";
                }
                else if (Session["LanguageChosen"].ToString().Equals("zh-tw", StringComparison.InvariantCultureIgnoreCase))
                {
                    memberMarketTree.Picture = "RegisterEn.png";
                }
                else if (Session["LanguageChosen"].ToString().Equals("zh-JP", StringComparison.InvariantCultureIgnoreCase))
                {
                    memberMarketTree.Picture = "RegisterEn.png";
                }


                DataSet logo = AdminGeneralDB.GetRankIcon();
                foreach (DataRow dr2 in logo.Tables[0].Rows)
                {
                    var IFile = new ImageFile();
                    IFile.FileName = dr2["CRANKSET_ICON"].ToString();
                    RankModel RankList = new RankModel();
                    RankList.RankIconPath = dr2["CRANKSET_ICON"].ToString();
                    //RankList.RankIconName = dr2["AMOUNT"].ToString();
                    //RankList.RankAmount = float.Parse(dr2["AMOUNT"].ToString());
                    RankList.RankName = dr2["CRANKSET_NAME"].ToString();
                    ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

                    memberMarketTree.RankList.Add(RankList);
                }



                return PartialView("MarketTree", memberMarketTree);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult FindMemberTree(string Username)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ExpiredSession = "Yes" });
                }

                int isBelongTo;

                MemberDB.MemberIsInMarketTreeByUsername(Username, Session["Username"].ToString(), out isBelongTo);

                

                MarketTreeModel memberMarketTree;

                if (isBelongTo == 0)//not belong to
                {
                    Response.Write(string.Format(Resources.OneForAll.OneForAll.msgNotInTree, Username));
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    memberMarketTree = GetAllMarketTreeMember(Username);

                    int ok = 0;
                    string msg = "";
                    var member = MemberDB.GetMemberByUsername(Username, out ok, out msg);
                    memberMarketTree.country = member.Tables[0].Rows[0]["CCOUNTRY_ID"].ToString();
                    var Stockist = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
                    memberMarketTree.Stockist = Stockist.Tables[0].Rows[0]["CMEM_MEMBER_TYPE"].ToString();
                    if (Session["LanguageChosen"].ToString().Equals("zh-cn", StringComparison.InvariantCultureIgnoreCase))
                    {
                        memberMarketTree.Picture = "RegisterEn.png";
                    }
                    else if (Session["LanguageChosen"].ToString().Equals("en-US", StringComparison.InvariantCultureIgnoreCase))
                    {
                        memberMarketTree.Picture = "RegisterEn.png";
                    }
                    else if (Session["LanguageChosen"].ToString().Equals("zh-tw", StringComparison.InvariantCultureIgnoreCase))
                    {
                        memberMarketTree.Picture = "RegisterEn.png";
                    }
                    else if (Session["LanguageChosen"].ToString().Equals("zh-JP", StringComparison.InvariantCultureIgnoreCase))
                    {
                        memberMarketTree.Picture = "RegisterEn.png";
                    }

                    DataSet logo = AdminGeneralDB.GetRankIcon();
                    foreach (DataRow dr2 in logo.Tables[0].Rows)
                    {
                        var IFile = new ImageFile();
                        IFile.FileName = dr2["CRANKSET_ICON"].ToString();
                        RankModel RankList = new RankModel();
                        RankList.RankIconPath = dr2["CRANKSET_ICON"].ToString();
                        //RankList.RankIconName = dr2["AMOUNT"].ToString();
                        RankList.RankAmount = float.Parse(dr2["AMOUNT"].ToString());
                        ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

                        memberMarketTree.RankList.Add(RankList);
                    }

                }

                return PartialView("MarketTree", memberMarketTree);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GoUpOneMember(string Username)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ExpiredSession = "Yes" });
                }

                DataSet dsFollow = MemberDB.GetMarketTreeByUsername(Username);
                DataRow dr = dsFollow.Tables[0].Rows[0];
                string upMember = dr["CMTREE_UPMEMBER"].ToString();

                if (upMember == "0" || string.Equals(Username.Replace(" ", ""), Session["Username"].ToString(), StringComparison.CurrentCultureIgnoreCase))
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgTopMost);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return MarketTreeByUsername(upMember);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region ResetPassword

        public ActionResult ResetPasswordAndPinMethod(string password, string newpassword, string pin, string newpin)
        {
            if (password.Length < 6 || newpassword.Length < 6)
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.WarningPassSixChar));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (pin.Length < 6 || newpin.Length < 6)
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.WarningPinSixChar));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (password != newpassword)
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.WarningPasswordMustBeSame));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (pin != newpin)
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.WarningPinMustBeSame));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (password == pin)
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.WarningPassAndPinMustBeDifferent));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (!Misc.IsAlphaNum(password))
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.WarningPassMustBeAlphanumeric));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (!Misc.IsAlphaNum(pin))
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.WarningPinMustBeAlphanumeric));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            int ok = 0;

            MemberDB.UpdatePassAndPin(Session["Username"].ToString(), Authentication.Encrypt(Helpers.Helper.NVL(password)), Authentication.Encrypt(Helpers.Helper.NVL(pin)), out ok);

            if (ok == -1)
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.WarningPassMustBeDifferentFromCurrentPass));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            else if (ok == -2)
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.WarningPinMustBeDifferentFromCurrentPin));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            return RedirectToAction("Home", "Member");
            //return Json(string.Empty, JsonRequestBehavior.AllowGet);
        }


        #endregion
    }
}