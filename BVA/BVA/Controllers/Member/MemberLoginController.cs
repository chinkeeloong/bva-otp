﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECFBase.Helpers;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web.SessionState;
using System.Web.UI.HtmlControls;
using System.IO;
using ECFBase.Components;
using System.Text;
using System.Net.Mail;
using System.Configuration;
using ECFBase.Models;
using System.Globalization;

namespace ECFBase.Controllers.Member
{
    public class MemberLoginController : Controller
    {
        public ActionResult CaptchaImage(string prefix, bool noisy = true)
        {
            Session["CaptchaImageText"] = LoginDB.GenerateRandomCode();
            CaptchaImage ci = new CaptchaImage(Session["CaptchaImageText"].ToString(), 280, 50, "Miriam");
            
            FileContentResult img = null;
            var mem = new MemoryStream();

            ci.Image.Save(mem, System.Drawing.Imaging.ImageFormat.Jpeg);
            img = this.File(mem.GetBuffer(), "image/Jpeg");

            return img;
        }

        [HttpPost]
        public ActionResult ValidateLogin(string userName, string password, string captcha, string selectedLanguage = null)
        {
            try
            {
                int ok = 0;
                string msg = "";

                if (captcha != Session["CaptchaImageText"].ToString())
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidCaptcha);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    //return RedirectToAction("MemberLogin", "Member", new { msg = Resources.OneForAll.OneForAll.msgInvalidCaptcha });
                }

                if (!string.IsNullOrEmpty(selectedLanguage))
                {
                    Session["LanguageChosen"] = selectedLanguage;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["LanguageChosen"].ToString());
                    System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["LanguageChosen"].ToString());
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    //return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Status == "1")
                {
                    Response.Write("Login is not allow from 00:00:00 to 03:00:00 for bonus calculation.");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                DataSet dsUser = MemberDB.GetMemberByUsername(userName, out ok, out msg);

                if (dsUser.Tables[0].Rows.Count == 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidLogin);
                    //return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", msg = Resources.OneForAll.OneForAll.msgInvalidLogin });
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                
               
                //string abc = Authentication.Decrypt("");

                string userPassword = dsUser.Tables[0].Rows[0]["CUSR_PASSWORD"].ToString();
                if (Authentication.Encrypt(password) != userPassword)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidLogin);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    //return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", msg = Resources.OneForAll.OneForAll.msgInvalidLogin });
                }

                var isBlocked = !Convert.ToBoolean(dsUser.Tables[0].Rows[0]["CUSR_ACTIVE"]);
                if (isBlocked)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgUserBlocked);
                    //return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", msg = Resources.OneForAll.OneForAll.msgUserBlocked });
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                //if (dsUser.Tables[0].Rows[0]["CUSR_ACTIVE"].ToString() == "False")
                //{

                //    Response.Write(Resources.OneForAll.OneForAll.msgAccountInactive);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                //bool isActivated = bool.Parse(dsUser.Tables[0].Rows[0]["CMEM_ISACTIVATED"].ToString());

                //if (isActivated == false)
                //{
                //    Response.Write(Resources.OneForAll.OneForAll.msgUserNotActivated);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                Session["CountryCode"] = dsUser.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                Session["Username"] = userName;
   
                //track member IP during login
                MemberDB.InsertUserOperation(userName, "Login", HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), 0);

                //Login Successful
                //TODO: access right for the login user.
                //TODO: validate if login user is a member.
                string languageUsed = string.Empty;
                //var dsMember = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
                //if (dsMember.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString() == "CN")
                //{
                //    languageUsed = "zh-CN";
                //}
                //else
                //{
                //    languageUsed = "en-US";
                //}
                //Session["LanguageChosen"] = languageUsed;

                languageUsed = Session["LanguageChosen"].ToString();
                Session["LanguageChosen"] = languageUsed;
                int isShow = 0;
                Session["isShow"] = isShow;
                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                ModelState.AddModelError("Error", e.Message);
                return Json(new { success = false });
            }
        }

        [HttpPost]
        public ActionResult SendPassword(string userName, string userEmail)
        {
            try
            {
                DataSet dsUser = LoginDB.GetUserByUserName(userName);
                if (dsUser.Tables[0].Rows.Count == 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidMemberID);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                string email = dsUser.Tables[0].Rows[0]["CUSR_EMAIL"].ToString();
                if (email != userEmail)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgEmailNotMatch);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                string userFirstname = dsUser.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                string userPassword = dsUser.Tables[0].Rows[0]["CUSR_PASSWORD"].ToString();
                string userPin = dsUser.Tables[0].Rows[0]["CUSR_PIN"].ToString();

                string mailSubject = "BVI";

                string welcomeContent = string.Format("<html>" +
                                                        "<head>" +
                                                        "   <title></title>" +
                                                        "</head>" +
                                                        "<body>" +
                                                        "    <table>" +
                                                        //"        <tr>" +
                                                        //"            <td><img alt=\"\" src=cid:myImageID style=\"height: 113px; width: 274px\" /></td>" +
                                                        //"        </tr>" +
                                                        "        <tr>" +
                                                        "            <td></td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td>{0}</td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td></td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td>{1}</td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td></td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td>{2}</td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td></td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td>{3}</td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td></td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td>{4}</td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td></td>" +
                                                        "        </tr>" +
                                                         "        <tr>" +
                                                        "            <td>{5}</td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td></td>" +
                                                        "        </tr>" +
                                                        "    </table>" +
                                                        "</body>" +
                                                        "</html>",
                                                        Resources.OneForAll.OneForAll.EmailContent1,
                                                        Resources.OneForAll.OneForAll.EmailContent2, 
                                                        Resources.OneForAll.OneForAll.EmailContent3 + " " + userName,
                                                        Resources.OneForAll.OneForAll.EmailContent4 + " " + Authentication.Decrypt(userPassword),
                                                        Resources.OneForAll.OneForAll.EmailContent6 + " " + Authentication.Decrypt(userPin),
                                                        Resources.OneForAll.OneForAll.EmailContent5);

                string imagePath = string.Empty; //Path.Combine(Server.MapPath("~/Images/"), "Logo.jpg");
                string result = Misc.SendEmail("noreply@bvi.asia", email, mailSubject, welcomeContent, imagePath);
                if (result == "Successfully")
                {
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Response.Write(result);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception e)
            {
                ModelState.AddModelError("Error", e.Message);
                return Json(new { success = false });
            }
        }

        public ActionResult ForgetPassword()
        {

            DataSet logo = AdminGeneralDB.GetCompanySetup();
            foreach (DataRow dr in logo.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["COM_IMAGEPATH"].ToString();

                ViewBag.Logo = logo.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString();
            }

            return PartialView("ForgetPassword");
        }
        

    }
}