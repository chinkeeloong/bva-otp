﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ECFBase.Models;
using ECFBase.Components;
using System.Data;
using ECFBase.Helpers;

namespace ECFBase.Controllers.Member
{
    public class MemberProfileController : Controller
    {

        #region Edit Profile

        [HttpPost]
        public JsonResult FindProvince(string CountryCode)
        {
            string LanguageCode = Session["LanguageChosen"].ToString();

            var result = GetProvinceList(LanguageCode, CountryCode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public static IEnumerable<SelectListItem> GetProvinceList(string LanguageCode, string CountryCode)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            int ok = 0;
            string message = "";
            List<ProvinceModel> provinces = Misc.GetAllProvinceByCountry(LanguageCode, CountryCode, ref ok, ref message);

            var result = from c in provinces
                         select new SelectListItem
                         {
                             Selected = false,
                             Text = c.ProvinceName,
                             Value = c.ProvinceCode
                         };
            return result.OrderBy(m => m.Value).ToList();
        }

        public ActionResult EditProfile(string countrycode ="")
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                 return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
            }

            string Status = "";
            MemberDB.GetTime(out Status);

            if (Session["Admin"] == null)
            {
                if (Status == "1")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

            }

            EditMemberModel memberMC = new EditMemberModel();

            int ok;
            string msg;
            string Username = Session["Username"].ToString();
            string languageCode = Session["LanguageChosen"].ToString();

            DataSet dsMember = AdminMemberDB.GetMemberInfoByUserName(Username, out ok, out msg);

            memberMC.Member.Username = dsMember.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
            memberMC.Member.FirstName = dsMember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            memberMC.UserInfo.Nickname = dsMember.Tables[0].Rows[0]["CUSRINFO_NICKNAME"].ToString();            
            DataSet dsCountry = AdminGeneralDB.GetCountryByID(Convert.ToInt32(dsMember.Tables[0].Rows[0]["CCOUNTRY_ID"]), out ok, out msg);
            memberMC.SelectedCountry = dsCountry.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
            memberMC.Member.MemberEmail = dsMember.Tables[0].Rows[0]["CUSR_EMAIL"].ToString();
            memberMC.UserInfo.CellPhone = dsMember.Tables[0].Rows[0]["CUSRINFO_CELLPHONE"].ToString();
            memberMC.UserInfo.MobileCountryCode = dsMember.Tables[0].Rows[0]["CUSRINFO_PHONE_COUNTRY"].ToString();
            //memberMC.UserInfo.IC = String.Concat(dsMember.Tables[0].Rows[0]["CUSRINFO_IC"].ToString().Substring(dsMember.Tables[0].Rows[0]["CUSRINFO_IC"].ToString().Length - 2).PadRight(4),"XXXX");
            memberMC.UserInfo.IC = dsMember.Tables[0].Rows[0]["CUSRINFO_IC"].ToString();
            memberMC.UserInfo.BeneficiaryName = dsMember.Tables[0].Rows[0]["CUSRINFO_BENEFICIARYNAME"].ToString();
            memberMC.UserInfo.SelectedRelationship = dsMember.Tables[0].Rows[0]["CUSRINFO_RELATIONSHIP"].ToString();
            memberMC.UserInfo.Address = dsMember.Tables[0].Rows[0]["CUSRINFO_MAIL_ADDRESS"].ToString();
            memberMC.UserInfo.SelectedCity = dsMember.Tables[0].Rows[0]["CUSRINFO_MAIL_CITY"].ToString();
            memberMC.UserInfo.State = dsMember.Tables[0].Rows[0]["CUSRINFO_MAIL_STATE"].ToString();
            memberMC.UserInfo.Postcode = dsMember.Tables[0].Rows[0]["CUSRINFO_MAIL_POSTCODE"].ToString();
            memberMC.UserInfo.SelectedCountry = dsMember.Tables[0].Rows[0]["CUSRINFO_MAIL_COUNTRY"].ToString();

            #region Country
            List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

            List<CountrySetupModel> countrie = Misc.GetAllMobileCountryList(languageCode, ref ok, ref msg);

            memberMC.Countries = from c in countrie
                                 select new SelectListItem
                                 {
                                     Selected = false,
                                     Text = c.CountryName + "(" + c.MobileCode + ")",
                                     Value = c.CountryCode
                                 };

            memberMC.UserInfo.Countries = from c in countries
                                          select new SelectListItem
                                          {
                                              Selected = false,
                                              Text = c.CountryName,
                                              Value = c.CountryCode
                                          };

            foreach (CountrySetupModel temp in countrie)
            {
                if (temp.CountryCode == memberMC.SelectedMobileCountry)
                {
                    memberMC.UserInfo.MobileCountryCode = temp.MobileCode;
                    break;
                }
            }


            if (memberMC.UserInfo.SelectedCountry == string.Empty && memberMC.UserInfo.Countries.Any())
            {               
                memberMC.UserInfo.SelectedCountry = "0";
            }

            #endregion


            #region DOB
            memberMC.UserInfo.Days = Misc.ConstructsDay().ToList();
            memberMC.UserInfo.Months = Misc.ConstructsMonth().ToList();
            memberMC.UserInfo.Years = Misc.ConstructYears().ToList();
            if (dsMember.Tables[0].Rows[0]["CUSR_DOB"].ToString() != "")
            {
                DateTime dob = Convert.ToDateTime(dsMember.Tables[0].Rows[0]["CUSR_DOB"]);
                memberMC.UserInfo.SelectedYears = dob.Year.ToString();
                memberMC.UserInfo.SelectedMonth = dob.Month.ToString();
                memberMC.UserInfo.SelectedDay = dob.Day.ToString();
            }
            else
            {
                memberMC.UserInfo.SelectedYears = "1960";
                memberMC.UserInfo.SelectedMonth = "1";
                memberMC.UserInfo.SelectedDay = "1";
            }

            #endregion

            #region Gender

            memberMC.UserInfo.Gender = Misc.ConstructGender(dsMember.Tables[0].Rows[0]["CUSRINFO_GENDER"].ToString());
            
            #endregion

            

            return PartialView("EditProfile", memberMC);
        }

        [HttpPost]
        public ActionResult EditProfileMethod(EditMemberModel mem)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                int ok = 0;
                string msg = "";

                if (mem.UserInfo.Address == "" || mem.UserInfo.Address == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningFillinMailingAddress);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                if (mem.UserInfo.SelectedCity == "" || mem.UserInfo.SelectedCity == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningFillinCity);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.UserInfo.State == "" || mem.UserInfo.State == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningFillinState);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.UserInfo.Postcode == "" || mem.UserInfo.Postcode == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningFillInPostCode);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                //if (mem.UserInfo.SelectedCountry == "" || mem.UserInfo.SelectedCountry == null)
                //{
                //    Response.Write(Resources.OneForAll.OneForAll.warningSelectCountry);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                if (mem.UserInfo.IC == "" || mem.UserInfo.IC == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningPlsKeyInIC);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.Member.FirstName == "" || mem.Member.FirstName == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningFullname);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                mem.UserInfo.DOB = new DateTime(int.Parse(mem.UserInfo.SelectedYears), int.Parse(mem.UserInfo.SelectedMonth), int.Parse(mem.UserInfo.SelectedDay));

                if (mem.Member.MemberImage != null)
                {
                    if (Misc.IsFileExtensionValid(mem.Member.MemberImage.FileName))
                    {
                        string basePath = Server.MapPath("~/MemberProfile/" + mem.Member.Username);
                        if (Directory.Exists(basePath) == false)
                        {
                            Directory.CreateDirectory(basePath);
                        }
                        var filename = Path.GetFileName(mem.Member.MemberImage.FileName);
                        mem.Member.MemberImagePath = basePath + "\\" + filename;
                        mem.Member.MemberImage.SaveAs(mem.Member.MemberImagePath);
                    }
                }

                mem.Member.Username = Session["Username"].ToString();

                DateTime DOB = DateTime.Now;
                var HP = "";
                var email = "";

                DataSet dsMember = AdminMemberDB.GetMemberInfoByUserName(mem.Member.Username, out ok, out msg);
                if (dsMember.Tables[0].Rows.Count != 0)
                {
                    if (dsMember.Tables[0].Rows[0]["CUSR_DOB"].ToString() != "")
                    {
                        DOB = Convert.ToDateTime(dsMember.Tables[0].Rows[0]["CUSR_DOB"]);
                    }
                    else
                    {
                        DOB = new DateTime(1960, 1, 1);
                    }
                    HP = dsMember.Tables[0].Rows[0]["CUSRINFO_CELLPHONE"].ToString();
                    email = dsMember.Tables[0].Rows[0]["CUSR_EMAIL"].ToString();

                }


                MemberProfileDB.UpdateMember(mem, out ok, out msg);


                if (mem.UserInfo.DOB != DOB)
                {
                    AdminDB.InsertUserTrackingLog("Member", mem.Member.Username, HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), "Update Profile", DOB.ToString(), mem.UserInfo.DOB.ToString(), Session["Username"].ToString());
                }

                if (mem.UserInfo.CellPhone != HP)
                {
                    AdminDB.InsertUserTrackingLog("Member", mem.Member.Username, HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), "Update Profile", HP, mem.UserInfo.CellPhone, Session["Username"].ToString());
                }

                if (mem.Member.MemberEmail != email)
                {
                    AdminDB.InsertUserTrackingLog("Member", mem.Member.Username, HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), "Update Profile", email, mem.Member.MemberEmail, Session["Username"].ToString());
                }


                Session["page"] = "EditProfile";
                ViewBag.page = "EditProfile";
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CheckFileExtension(string file)
        {
            var isValid = Misc.IsFileExtensionValid(file);

            return Json(isValid.ToString(), JsonRequestBehavior.AllowGet);
        }
        
        #endregion

        #region Change Password

        public ActionResult ChangePassword()
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                 return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
            }

            string Status = "";
            MemberDB.GetTime(out Status);

            if (Session["Admin"] == null)
            {
                if (Status == "1")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

            }

            MemberPwdPINModel model = new MemberPwdPINModel();
            model.Username = Session["Username"].ToString();

            return PartialView("ChangePassword", model);
        }

        [HttpPost]
        public ActionResult ChangePasswordMethod(MemberPwdPINModel mpm)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                if (mpm.NewPassword != mpm.ConfirmNewPassword)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgPwdNotMatch);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (mpm.NewPassword == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgReqNewPwd);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (mpm.ConfirmNewPassword == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningConfirmNewPwd);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok;
                string msg;
                mpm.Username = Session["Username"].ToString();
                MemberProfileDB.ChangePassword(mpm.Username, Authentication.Encrypt(mpm.CurrPassword), Authentication.Encrypt(mpm.NewPassword), mpm.Username, out ok, out msg);
                AdminDB.InsertUserTrackingLog("Member", mpm.Username, HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), "Change Password", Authentication.Encrypt(mpm.CurrPassword), Authentication.Encrypt(mpm.NewPassword), mpm.Username);
                if (ok == -1)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidCurrPwd);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (ok == -2)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgPasswordAndPinMustDifferent);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return ChangePassword();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Change PIN

        public ActionResult ChangeEPIN()
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                 return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
            }

            string Status = "";
            MemberDB.GetTime(out Status);

            if (Session["Admin"] == null)
            {
                if (Status == "1")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

            }

            MemberPwdPINModel model = new MemberPwdPINModel();
            model.Username = Session["Username"].ToString();

            return PartialView("ChangeEPIN", model);
        }

        [HttpPost]
        public ActionResult ChangeEPINMethod(MemberPwdPINModel mpm)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                if (mpm.NewPIN != mpm.ConfirmNewPIN)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgPINNotMatch);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (mpm.NewPIN == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgReqNewPwd);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (mpm.ConfirmNewPIN == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningConfirmNewPwd);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                int ok;
                string msg;
                mpm.Username = Session["Username"].ToString();
                MemberProfileDB.ChangePIN(mpm.Username, Authentication.Encrypt(mpm.CurrPIN), Authentication.Encrypt(mpm.NewPIN), mpm.Username, out ok, out msg);
                AdminDB.InsertUserTrackingLog("Member", mpm.Username, HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), "Change PIN", Authentication.Encrypt(mpm.CurrPIN), Authentication.Encrypt(mpm.NewPIN), mpm.Username);
                if (ok == -1)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidCurrPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                //else if (ok == -2)
                //{
                //    Response.Write(Resources.MemberProfile.Profile.msgPasswordAndPinMustDifferent);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                return ChangeEPIN();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Bank Account Info

        [HttpPost]
        public JsonResult FindBank(string CountryCode)
        {
            string LanguageCode = Session["LanguageChosen"].ToString();

            var result = GetBankList(LanguageCode, CountryCode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public static IEnumerable<SelectListItem> GetBankList(string LanguageCode, string CountryCode)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            int ok = 0;
            string message = "";
            List<BankModel> banks = Misc.GetAllBankByCountry(LanguageCode, CountryCode, ref ok, ref message);

            var result = from c in banks
                         select new SelectListItem
                         {
                             Selected = false,
                             Text = c.BankName,
                             Value = c.BankCode
                         };
            return result.OrderBy(m => m.Value).ToList();
        }

        public ActionResult BankAccInfo()
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                 return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
            }

            string Status = "";
            MemberDB.GetTime(out Status);

            if (Session["Admin"] == null)
            {
                if (Status == "1")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

            }

            int ok = 0;
            string msg = "";
            string language = Session["LanguageChosen"].ToString();

            MemberBankModel model = new MemberBankModel();
            model.Username = Session["Username"].ToString();

            //combobox for country
            List<CountrySetupModel> countries = Misc.GetAllCountryList(language, ref ok, ref msg);
            model.CountryList = from c in countries
                                select new SelectListItem
                                {
                                    Selected = false,
                                    Text = c.CountryName,
                                    Value = c.CountryCode
                                };

            DataSet dsMemberBank = MemberProfileDB.GetMemberBankByUsername(model.Username, out ok, out msg);

            DataSet dsmember = AdminMemberDB.GetMemberByUserName(model.Username, out ok, out msg);

            if (dsMemberBank.Tables[0].Rows.Count == 0)
            {
                string selectedcountry = dsmember.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                model.SelectedCountry = dsmember.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                model.SelectedCountryStraing = countries.Find(item => item.CountryCode == selectedcountry).CountryName;    

                //combobox for bank
                //string selectedcountry = dsMemberBank.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                //model.SelectedCountryStraing = countries.First().CountryName;                  
                // model.SelectedCountry = dsMemberBank.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                string countryCode = model.SelectedCountry;
                //model.MemberBankID = Convert.ToInt32(dsMemberBank.Tables[0].Rows[0]["CMEMBANK_ID"]);
                List<BankModel> banks = Misc.GetAllBankByCountry("en-US", model.SelectedCountry, ref ok, ref msg);
                model.BankList = from c in banks
                                 select new SelectListItem
                                 {
                                     Selected = false,
                                     Text = c.BankName,
                                     Value = c.BankCode
                                 };
                model.SelectedBank = "null";
                model.MemberBankID = 0;
                model.BranchName = "";
                model.AccountNumber = "";
                model.BeneficiaryIC = dsmember.Tables[0].Rows[0]["CUSRINFO_IC"].ToString();
                model.BeneficiaryName = dsmember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                model.EnableEditAll = true;
                model.BranchAdd1 = "";
                model.BranchAdd2 = "";
                model.BranchSwiftCode = "";
                model.Address = "";
                model.SelectedCity = "";
                model.State = "";
                model.Postcode = "";
                model.SelectedMailingCountry = "";
                model.ChinaBank = "";
                if (dsmember.Tables[0].Rows.Count != 0)
                {
                    model.Address = dsmember.Tables[0].Rows[0]["CUSRINFO_MAIL_ADDRESS"].ToString();
                    model.SelectedCity = dsmember.Tables[0].Rows[0]["CUSRINFO_MAIL_CITY"].ToString();
                    model.State = dsmember.Tables[0].Rows[0]["CUSRINFO_MAIL_STATE"].ToString();
                    model.Postcode = dsmember.Tables[0].Rows[0]["CUSRINFO_MAIL_POSTCODE"].ToString();
                    string SelectedMailingCountry = dsmember.Tables[0].Rows[0]["CUSRINFO_MAIL_COUNTRY"].ToString();
                    if (SelectedMailingCountry!="")
                    {
                        model.SelectedMailingCountry = countries.Find(item => item.CountryCode == SelectedMailingCountry).CountryName;
                    }
                   
                }
            }
            else
            {
                string selectedcountry = dsMemberBank.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                model.SelectedCountry = dsMemberBank.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                model.SelectedCountryStraing = countries.Find(item => item.CountryCode == selectedcountry).CountryName;               
                model.MemberBankID = Convert.ToInt32(dsMemberBank.Tables[0].Rows[0]["CMEMBANK_ID"]);
               // model.SelectedCountry = dsMemberBank.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                model.SelectedBank = dsMemberBank.Tables[0].Rows[0]["CBANK_CODE"].ToString();
                model.ChinaBank = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_NAME"].ToString();
                //combobox for bank
                List<BankModel> banks = Misc.GetAllBankByCountry("en-us", model.SelectedCountry, ref ok, ref msg);
                model.BankList = from c in banks
                                 select new SelectListItem
                                 {
                                     Selected = c.BankCode == model.SelectedBank ? true : false,
                                     Text = c.BankName,
                                     Value = c.BankCode
                                 };

                model.Username = dsMemberBank.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
                model.BranchName = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_BRANCHNAME"].ToString();
                model.AccountNumber = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_ACCOUNTNUMBER"].ToString();
                model.BeneficiaryName = dsmember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                model.BeneficiaryIC = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_BENEFICIARYIC"].ToString();
                model.BeneficiaryPhone = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_BENEFICIARYPHONE"].ToString();
                model.BeneficiaryRelationship = dsmember.Tables[0].Rows[0]["CUSRINFO_RELATIONSHIP"].ToString();
                model.BranchAdd1 = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_ADDRESS1"].ToString();
                model.BranchAdd2 = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_ADDRESS2"].ToString();
                model.BranchSwiftCode = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_SWIFTCODE"].ToString();
                model.Address = dsmember.Tables[0].Rows[0]["CUSRINFO_MAIL_ADDRESS"].ToString();
                model.SelectedCity = dsmember.Tables[0].Rows[0]["CUSRINFO_MAIL_CITY"].ToString();
                model.State = dsmember.Tables[0].Rows[0]["CUSRINFO_MAIL_STATE"].ToString();
                model.Postcode = dsmember.Tables[0].Rows[0]["CUSRINFO_MAIL_POSTCODE"].ToString();
                string SelectedMailingCountry = dsmember.Tables[0].Rows[0]["CUSRINFO_MAIL_COUNTRY"].ToString();
                if (SelectedMailingCountry != "")
                {
                    model.SelectedMailingCountry = countries.Find(item => item.CountryCode == SelectedMailingCountry).CountryName;
                }

                if (model.BranchName == "" || model.AccountNumber == "" || model.BeneficiaryName == "")
                {
                    model.EnableEdit = true;
                }
            }

            return PartialView("BankAccInfo", model);
        }

        [HttpPost]
        public ActionResult BankAccInfoMethod(MemberBankModel mbm)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                int ok = 0;
                string msg = "";
                var BankID = "";
                var BranchName = "";
                var AccountNo = "";
                var BranchAdd1 = "";
                var BranchAdd2 = "";
                var SwiftCode = "";
               
                DataSet dsMemberBank = MemberProfileDB.GetMemberBankByUsername(Session["Username"].ToString(), out ok, out msg);
                if (dsMemberBank.Tables[0].Rows.Count != 0)
                {
                    BankID = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_ID"].ToString(); 
                    BranchName = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_BRANCHNAME"].ToString();
                    AccountNo = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_ACCOUNTNUMBER"].ToString();
                    BranchAdd1 = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_ADDRESS1"].ToString();
                    BranchAdd2 = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_ADDRESS2"].ToString();
                    SwiftCode = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_SWIFTCODE"].ToString();
                }

                if (mbm.ChinaBank == "" || mbm.ChinaBank == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningPlsKeyInBank);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (mbm.BranchAdd1 == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningBranchAddress);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (mbm.BranchSwiftCode == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningSwiftCode);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (mbm.BranchName == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningPlsEnterBranchName);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mbm.AccountNumber == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningPlsEnterAccNumber);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (mbm.BeneficiaryName == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningPlsEnterAccName);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (mbm.BeneficiaryIC == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningPlsEnterNRIC);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                //if (mbm.SelectedBank == "null")
                //{
                //    Response.Write(Resources.OneForAll.OneForAll.warningPlsSelectBank);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}
                if (mbm.SelectedBank == null)
                {
                    mbm.SelectedBank = "NA";
                }

                if (dsMemberBank.Tables[0].Rows.Count == 0)
                {
                    //Create New Record
    
                        MemberProfileDB.CreateNewMemberBank(mbm.SelectedCountry, mbm.SelectedBank, mbm.ChinaBank, mbm.Username, Helper.NVL(mbm.BranchAdd1), Helper.NVL(mbm.BranchAdd2), Helper.NVL(mbm.BranchSwiftCode), Helper.NVL(mbm.BranchName), Helper.NVL(mbm.AccountNumber), Helper.NVL(mbm.BeneficiaryName), Helper.NVL(mbm.BeneficiaryIC), Session["Username"].ToString(), Session["Username"].ToString(), out ok, out msg);
                }
                else
                {
                    //Update Existing Record

                        MemberProfileDB.UpdateMemberBank(mbm.SelectedCountry, mbm.SelectedBank, mbm.ChinaBank, mbm.Username, Helper.NVL(mbm.BranchAdd1), Helper.NVL(mbm.BranchAdd2), Helper.NVL(mbm.BranchSwiftCode), Helper.NVL(mbm.BranchName), Helper.NVL(mbm.AccountNumber), Helper.NVL(mbm.BeneficiaryName), Helper.NVL(mbm.BeneficiaryIC), Helper.NVL(mbm.BeneficiaryPhone), Helper.NVL(mbm.BeneficiaryRelationship), Helper.NVL(mbm.Username), out ok, out msg);
                }


                if (mbm.SelectedBank != BankID)
                {                   
                    AdminDB.InsertUserTrackingLog("Member", mbm.Username, HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), "Update Bank", BankID, mbm.SelectedBank, mbm.Username);
                }
                if (mbm.BranchName != BranchName)
                { 
                    AdminDB.InsertUserTrackingLog("Member", mbm.Username, HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), "Update Bank", BranchName, mbm.BranchName, mbm.Username);
                }

                if (mbm.AccountNumber != AccountNo)
                { 
                    AdminDB.InsertUserTrackingLog("Member", mbm.Username, HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), "Update Bank", AccountNo, mbm.AccountNumber, mbm.Username);
                }


                return BankAccInfo();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region
        
        public ActionResult SwitchAccount()
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                 return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
            }

            string Status = "";
            MemberDB.GetTime(out Status);

            if (Session["Admin"] == null)
            {
                if (Status == "1")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

            }


            int ok = 0;
            string msg = string.Empty;
            var ds = MemberDB.GetMemberByUsername(Session["Username"].ToString() , out ok, out msg);

            RegisterNewMemberModel model = new RegisterNewMemberModel();

            model.Member.OwnAccount = Misc.GetAllOwnAccount(Session["Username"].ToString());
            model.Member.FirstName = ds.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();


            return PartialView("SwitchAccount", model);
        }

        [HttpPost]
        public ActionResult SwitchAccountMethod(string Username , string Password)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                int ok = 0;
                string msg = string.Empty;

                DataSet dsUser = MemberDB.GetMemberByUsername(Username, out ok, out msg);

                if (dsUser.Tables[0].Rows.Count == 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidLogin);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                
                string userPassword = dsUser.Tables[0].Rows[0]["CUSR_PASSWORD"].ToString();
                if (Authentication.Encrypt(Password) != userPassword)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidLogin);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                var isBlocked = !Convert.ToBoolean(dsUser.Tables[0].Rows[0]["CUSR_ACTIVE"]);
                if (isBlocked)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgUserBlocked);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                Session["CountryCode"] = dsUser.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                Session["Username"] = Username;

                //track member IP during login
                MemberDB.InsertUserOperation(Username, "Switch Login", HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), 0);

                //Login Successful
                //TODO: access right for the login user.
                //TODO: validate if login user is a member.
                string languageUsed = string.Empty;
                var dsMember = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
                if (dsMember.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString() == "CN")
                {
                    languageUsed = "zh-CN";
                }
                else
                {
                    languageUsed = "en-US";
                }
                Session["LanguageChosen"] = languageUsed;
                int isShow = 0;
                Session["isShow"] = isShow;

                return SwitchAccount();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}