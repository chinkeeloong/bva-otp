﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ECFBase.Models;
using ECFBase.Components;
using System.Data;
using System.Globalization;
using System.IO;
using iTextSharp.text.pdf;
using System.Net;
using ECFBase.Helpers;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Specialized;
using System.Web.Script.Serialization;
using ECFBase.ThirdParties;
using Newtonsoft.Json;

namespace ECFBase.Controllers.Member
{
    public class MemberShareController : Controller
    {

        #region Sell Share Page
        public ActionResult MemberSellShare(string TheRate = "0")
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                 return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
            }


            Response.Write(Resources.OneForAll.OneForAll.WarningSplitShare);
            return Json(string.Empty, JsonRequestBehavior.AllowGet);

            string Status = "";
            MemberDB.GetTime(out Status);

            if (Session["Admin"] == null)
            {
                if (Status == "1")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

            }

            var model = new ShareLogModel();

            var ShareDetail = MemberShareDB.GetShareDetail(Session["Username"].ToString());


            //int CompanyShareUnit = Convert.ToInt32(ShareDetail.Tables[6].Rows[0]["TOTALCOMPANYSHARE"].ToString());

            //if (CompanyShareUnit > 50000)
            //{
            //    Response.Write(Resources.OneForAll.OneForAll.WarningSplitShare);
            //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
            //}

            #region Trading Market
            model.TodayPrices = Convert.ToDecimal(ShareDetail.Tables[0].Rows[0]["CSHARE_RATE"].ToString()).ToString("#.00");
            model.OpeningPrices = Convert.ToDecimal(ShareDetail.Tables[0].Rows[0]["CSHARE_RATE"].ToString()).ToString("#.00");
            model.HigherPrices = Convert.ToDecimal(ShareDetail.Tables[0].Rows[0]["CSHARE_RATE"].ToString()).ToString("#.00");
            model.LowerPrices = Convert.ToDecimal(ShareDetail.Tables[1].Rows[0]["CSHARE_RATE"].ToString()).ToString("#.00");
            model.UnitLeft = (Convert.ToInt32(ShareDetail.Tables[0].Rows[0]["CSHARE_TOTAL"].ToString())).ToString("###,###,##0");
            if (ShareDetail.Tables[8].Rows.Count == 0)
            {
                model.Oversubscribed = "0";
            }
            else
            {
                model.Oversubscribed = (Convert.ToInt32(ShareDetail.Tables[8].Rows[0]["CSHARE_AMOUNT"].ToString())).ToString("###,###,##0");
            }            
            model.MemberShare = (Convert.ToInt32(ShareDetail.Tables[2].Rows[0]["CSHRM_ASHARE_BALANCE"].ToString())).ToString("###,###,##0");
            model.TotalUnit = (Convert.ToInt32(ShareDetail.Tables[2].Rows[0]["CSHRM_ASHARE_BALANCE"].ToString())).ToString("###,###,##0");
            #endregion

            #region Sell E-Fund

            int CurrentSellingUnit = 0;

            CurrentSellingUnit = Convert.ToInt32(ShareDetail.Tables[2].Rows[0]["CSHRM_QUEUE_UNIT"].ToString());


            model.CurrentUnit = (Convert.ToInt32(ShareDetail.Tables[2].Rows[0]["CSHRM_ASHARE_BALANCE"].ToString())).ToString("###,###,##0");
            model.PackageAmount = ShareDetail.Tables[2].Rows[0]["CSHRM_PACKAGE_AMOUNT"].ToString();
            model.TradingOrder = ShareDetail.Tables[5].Rows[0]["CSHRST_VALUE"].ToString();
            int MaxAvailableUnit = Convert.ToInt32((float.Parse(model.PackageAmount) / float.Parse(model.TodayPrices)) / 100 * float.Parse(model.TradingOrder));

            if (Convert.ToInt32(ShareDetail.Tables[2].Rows[0]["CSHRM_SPLIT_COUNT"].ToString()) == 0)
            {
                MaxAvailableUnit = Convert.ToInt32((float.Parse(model.PackageAmount) / float.Parse(model.TodayPrices)) / 100 * 10);
            }

            MaxAvailableUnit = MaxAvailableUnit - CurrentSellingUnit;

            if (Convert.ToInt32(ShareDetail.Tables[2].Rows[0]["CSHRM_ASHARE_BALANCE"].ToString()) > MaxAvailableUnit)
            {
                model.AvailableUnitString = MaxAvailableUnit.ToString("###,###,##0");
                model.AvailableUnit = MaxAvailableUnit.ToString();
                model.KeyinUnit = MaxAvailableUnit.ToString();
            }
            else
            {
                model.AvailableUnitString = Convert.ToInt32(ShareDetail.Tables[2].Rows[0]["CSHRM_ASHARE_BALANCE"].ToString()).ToString("###,###,##0");
                model.AvailableUnit = ShareDetail.Tables[2].Rows[0]["CSHRM_ASHARE_BALANCE"].ToString();
                model.KeyinUnit = ShareDetail.Tables[2].Rows[0]["CSHRM_ASHARE_BALANCE"].ToString();
            }
            
            
            List<RateModel> ListRate = Misc.GetTheOpenRate();
            model.TheRate  = from c in ListRate
                            select new SelectListItem
                            {
                                Text = c.RateString,
                                Value = c.TheRate
                            };

            model.SelectedRate = Convert.ToDecimal(ShareDetail.Tables[0].Rows[0]["CSHARE_RATE"].ToString()).ToString("#.00");

            model.Total = (Convert.ToInt32(model.KeyinUnit) * float.Parse(model.TodayPrices)).ToString("n2");
            #endregion

            #region Sell E-Fund Queue List
            var dsSellingShareQueue = MemberShareDB.GetAllSellingQueueByMember(Session["Username"].ToString());

            foreach (DataRow dr in dsSellingShareQueue.Tables[0].Rows)
            {
                var ShareLog = new ShareListLogModel();
                ShareLog.ID = dr["CSHRSQ_ORDERID"].ToString();
                ShareLog.Rate = float.Parse(dr["CSHRSQ_RATE"].ToString()).ToString("n2");
                ShareLog.PlaceOrderDate = dr["CSHRSQ_CREATEDON"].ToString();
                ShareLog.Quantity = Convert.ToInt32(dr["CSHRSQ_ORIUNIT"].ToString()).ToString("###,##0") ;
                int BALANCE = Convert.ToInt32(dr["CSHRSQ_ORIUNIT"].ToString()) - Convert.ToInt32(dr["CSHRSQ_UNIT"].ToString());
                ShareLog.SellOrderQuantity = BALANCE.ToString("###,##0");
                ShareLog.ShareValue = float.Parse(dr["CSHRSQ_SELLINGVALUE"].ToString()).ToString("n2");
                ShareLog.DataID = dr["CSHRSQ_ID"].ToString();
                model.MemberPreTreadeList.Add(ShareLog);
            }
            #endregion
            
            return PartialView("MemberSellShare", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SellEFund(string KeyinUnit, string Rate, string EPin, string Captcha)
        {
            Response.Write(Resources.OneForAll.OneForAll.WarningSplitShare);
            return Json(string.Empty, JsonRequestBehavior.AllowGet);

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
            }

           

            if (Captcha != Session["CaptchaImageText"].ToString())
            {
                Response.Write(Resources.OneForAll.OneForAll.msgInvalidCaptcha);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            string Status = "";
            MemberDB.GetTime(out Status);

            if (Session["Admin"] == null)
            {
                if (Status == "1")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }
                
            }

            #region DB
            int ok = 0;
            string msg = string.Empty;

            var ShareDetail = MemberShareDB.GetShareDetail(Session["Username"].ToString());

            if (ShareDetail.Tables[6].Rows.Count != 0)
            {
                Response.Write(Resources.OneForAll.OneForAll.WarningBlockByCompany);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }


            if (ShareDetail.Tables[7].Rows.Count != 0)
            { 
                float LastRate = float.Parse(ShareDetail.Tables[7].Rows[0]["CSHRSQ_RATE"].ToString());
                float ChoosingRate = float.Parse(Rate);
                int Section = 0;
                int SellSection = 0;

                // Member Choosen
                if (ChoosingRate < 1.20)
                {
                    SellSection = 1;
                }
                else if (ChoosingRate < 1.40)
                {
                    SellSection = 2;
                }
                else if (ChoosingRate < 1.60)
                {
                    SellSection = 3;
                }
                else if (ChoosingRate < 1.80)
                {
                    SellSection = 4;
                }
                else if (ChoosingRate <= 2.00)
                {
                    SellSection = 5;
                }

                // Member Last Sell
                if (LastRate < 1.20)
                {
                    Section = 1;
                }
                else if (LastRate < 1.40)
                {
                    Section = 2;
                }
                else if (LastRate < 1.60)
                {
                    Section = 3;
                }
                else if (LastRate < 1.80)
                {
                    Section = 4;
                }
                else if (LastRate <= 2.00)
                {
                    Section = 5;
                }

                if (Section >= SellSection)
                {

                    Response.Write(Resources.OneForAll.OneForAll.WarningSellSharePart1 + LastRate.ToString("n2") + Resources.OneForAll.OneForAll.WarningSellSharePart2);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                //float ResultRate = LastRate + float.Parse("0.19");

                //if (ResultRate >= float.Parse(Rate))
                //{
                //    Response.Write(Resources.OneForAll.OneForAll.WarningSellSharePart1 + LastRate.ToString("n2") + Resources.OneForAll.OneForAll.WarningSellSharePart2);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}
            }
            var RateDetail = MemberShareDB.GetRateDetail(Rate);

            DataSet dsUser = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
            #endregion

            #region Check Condition    

            string UserPin = dsUser.Tables[0].Rows[0]["CUSR_PIN"].ToString();
            if (Authentication.Encrypt(EPin) != UserPin)
            {
                Response.Write(Resources.OneForAll.OneForAll.msgInvalidPIN);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            float CurrentRate = float.Parse(ShareDetail.Tables[0].Rows[0]["CSHARE_RATE"].ToString());

            float LastOrderRate = CurrentRate + float.Parse("0.18");

            if (float.Parse(Rate) > LastOrderRate)
            {
                if (ShareDetail.Tables[7].Rows.Count != 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.WarningNewRatePart1 + Rate + Resources.OneForAll.OneForAll.WarningNewRatePart2);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        
                }
            }


            int CurrentSellingUnit = Convert.ToInt32(ShareDetail.Tables[2].Rows[0]["CSHRM_QUEUE_UNIT"].ToString());

            if (CurrentSellingUnit != 0)
            {
                Response.Write("One Member only can trade one time until the pending selling is clear off.");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            float PackageAmount = float.Parse(ShareDetail.Tables[2].Rows[0]["CSHRM_PACKAGE_AMOUNT"].ToString());

            float TradingOrder = float.Parse(ShareDetail.Tables[5].Rows[0]["CSHRST_VALUE"].ToString());
            
            

            int MaxAvailableUnit = Convert.ToInt32((PackageAmount / float.Parse(Rate)) / 100 * TradingOrder);

            if (Convert.ToInt32(ShareDetail.Tables[2].Rows[0]["CSHRM_SPLIT_COUNT"].ToString()) == 0)
            {
                MaxAvailableUnit = Convert.ToInt32((PackageAmount / float.Parse(Rate)) / 100 * 10);
            }

            MaxAvailableUnit = MaxAvailableUnit - CurrentSellingUnit;

            

            int ShareAvailableUnit = Convert.ToInt32(RateDetail.Tables[0].Rows[0]["CSHARE_QUEUE_MEMBER"].ToString());

            int CurrentUnit = Convert.ToInt32(ShareDetail.Tables[2].Rows[0]["CSHRM_ASHARE_BALANCE"].ToString());

            if (Convert.ToInt32(KeyinUnit) > CurrentUnit)
            {
                Response.Write(Resources.OneForAll.OneForAll.WarningMoreThenCurrentUnit);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (CurrentUnit > MaxAvailableUnit)
            {
                if (Convert.ToInt32(KeyinUnit) > MaxAvailableUnit)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningexceededlimit);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
            }

            

            int TotalMemberUnit = 0;
            int AvailableUnitToQueue = 0;

            TotalMemberUnit = Convert.ToInt32(KeyinUnit) + ShareAvailableUnit;

            if (TotalMemberUnit > 50000)
            {
                AvailableUnitToQueue = 50000 - ShareAvailableUnit;

                Response.Write(Resources.OneForAll.OneForAll.warningRateUnitLeft + AvailableUnitToQueue.ToString("###,##0"));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            #endregion

            #region Sell E-Fund
            string ErrorMSG = string.Empty;

            MemberShareDB.SellEFund(Session["Username"].ToString(), KeyinUnit, Rate, out ErrorMSG);

            if (ErrorMSG == "-1")
            {
                var LatestDetail = MemberShareDB.GetRateDetail(Rate);

                ShareAvailableUnit = Convert.ToInt32(LatestDetail.Tables[0].Rows[0]["CSHARE_QUEUE_MEMBER"].ToString());

                AvailableUnitToQueue = 50000 - ShareAvailableUnit;

                Response.Write(Resources.OneForAll.OneForAll.warningRateUnitLeft + AvailableUnitToQueue.ToString("###,##0"));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            else if (ErrorMSG == "-2")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
            }

            #endregion

            Session["page"] = "SharePage";
            ViewBag.page = "SharePage";
            return Json(string.Empty, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CancelPlaceOrder(string ID)
        {

            Response.Write(Resources.OneForAll.OneForAll.WarningSplitShare);
            return Json(string.Empty, JsonRequestBehavior.AllowGet);

            string Status = "";
            MemberDB.GetTime(out Status);

            if (Session["Admin"] == null)
            {
                if (Status == "1")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

            }

            string MSG = string.Empty;

            MemberDB.CancelPlaceShare(ID, out MSG);

            if (MSG == "-1")
            {
                Response.Write(Resources.OneForAll.OneForAll.WarningCantCancelDuringCurrentRate);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            return MemberSellShare();
        }

        public ActionResult GoToSellingShareListing()
        {
            Response.Write(Resources.OneForAll.OneForAll.WarningSplitShare);
            return Json(string.Empty, JsonRequestBehavior.AllowGet);

            string Status = "";
            MemberDB.GetTime(out Status);

            if (Session["Admin"] == null)
            {
                if (Status == "1")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

            }

            try
            {
                ViewBag.page = "SellingUnitListing";
                Session["page"] = "SellingUnitListing";
                return RedirectToAction("Home", "Member");
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SellingUnitListing(string Rate = "0")
        {

            Response.Write(Resources.OneForAll.OneForAll.WarningSplitShare);
            return Json(string.Empty, JsonRequestBehavior.AllowGet);

            try
            {
                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

                var model = new PaginationWalletLogModel();
                var ShareDetail = MemberShareDB.GetShareSellingDetail(Rate);

                DataSet dsBonusWalletLog = MemberShareDB.SellingQueueByMember(Session["Username"].ToString(), Rate);

                int TotalUnit = 0;
                int TotalSold = 0;
                int SoldUnit = 0;

                //var walletLog = new WalletLogModel();
                //walletLog.FlowID = "BVISeller";
                //walletLog.Rate = float.Parse(ShareDetail.Tables[0].Rows[0]["CSHARE_RATE"].ToString()).ToString("n2");
                //walletLog.OriUnit = "50,000";
                //SoldUnit = 50000 - Convert.ToInt32(ShareDetail.Tables[0].Rows[0]["CSHARE_COMPANY"].ToString());
                //walletLog.UnitSold = SoldUnit.ToString("###,##0");
                //model.WalletLogList.Add(walletLog);

                //TotalUnit = TotalUnit + 50000;
                //TotalSold = TotalSold + SoldUnit;
                  
                foreach (DataRow dr in dsBonusWalletLog.Tables[0].Rows)
                {
                    var walletLog = new WalletLogModel();

                    walletLog.FlowID = dr["CSHRSQ_ORDERID"].ToString();

                    walletLog.Rate = float.Parse(dr["CSHRSQ_RATE"].ToString()).ToString("n2"); ;

                    walletLog.OriUnit = Convert.ToInt32(dr["CSHRSQ_ORIUNIT"].ToString()).ToString("###,##0");

                    SoldUnit = Convert.ToInt32(dr["CSHRSQ_ORIUNIT"].ToString()) - Convert.ToInt32(dr["CSHRSQ_UNIT"].ToString());

                    walletLog.UnitSold = SoldUnit.ToString("###,##0");

                    TotalUnit = TotalUnit + Convert.ToInt32(dr["CSHRSQ_ORIUNIT"].ToString());

                    TotalSold = TotalSold + SoldUnit;

                    model.WalletLogList.Add(walletLog);
                }

                List<RateModel> ListRate = Misc.GetTheOpenRate();
                model.TheRate = from c in ListRate
                                select new SelectListItem
                                {
                                    Text = c.RateString,
                                    Value = c.TheRate
                                };

                if (Rate == "0")
                {
                    model.SelectedRate = Convert.ToDecimal(ShareDetail.Tables[0].Rows[0]["CSHARE_RATE"].ToString()).ToString("#.00");
                }
                else
                {
                    model.SelectedRate = Rate;
                }


                model.TotalSold = TotalSold.ToString("###,##0");
                model.TotalUnit = TotalUnit.ToString("###,##0");

                return PartialView("SellingUnitListing", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Buy Share Page

        public ActionResult MemberBuyShare(string TheRate = "0")
        {

            Response.Write(Resources.OneForAll.OneForAll.WarningSplitShare);
            return Json(string.Empty, JsonRequestBehavior.AllowGet);

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
            }

            string Status = "";
            MemberDB.GetTime(out Status);

            if (Session["Admin"] == null)
            {
                if (Status == "1")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

            }

            var model = new ShareLogModel();

            var ShareDetail = MemberShareDB.GetShareDetail(Session["Username"].ToString());

           
            #region Buy E-Fund
            model.RPCPointBalance = float.Parse(ShareDetail.Tables[3].Rows[0]["CMEM_REPURCHASEWALLET"].ToString()).ToString("0.00");
            model.GoldPointBalance = float.Parse(ShareDetail.Tables[3].Rows[0]["CMEM_GOLDPOINT"].ToString()).ToString("0.00");
            model.HiddenRPCPointBalance = float.Parse(ShareDetail.Tables[3].Rows[0]["CMEM_REPURCHASEWALLET"].ToString()).ToString("0.00");
            model.HiddenGoldPointBalance = float.Parse(ShareDetail.Tables[3].Rows[0]["CMEM_GOLDPOINT"].ToString()).ToString("0.00");
            model.BuyTheRate = Convert.ToDecimal(ShareDetail.Tables[0].Rows[0]["CSHARE_RATE"].ToString()).ToString("#.00");
            model.RATEMAX = ShareDetail.Tables[4].Rows[0]["TOTALRATEUP"].ToString();
            #endregion

            #region Buy E-Fund Queue List
            var dsBuyingShareQueue = MemberShareDB.GetAllBuyingQueueByMember(Session["Username"].ToString());

            foreach (DataRow dr in dsBuyingShareQueue.Tables[0].Rows)
            {
                var ShareLog = new ShareListLogModel();
                ShareLog.ID = dr["CSHRBQ_ORDERID"].ToString();
                ShareLog.Amount = float.Parse(dr["CSHRBQ_AMOUNT"].ToString()).ToString("n2");
                ShareLog.Type = dr["CSHRBQ_TYPE"].ToString();
                ShareLog.PlaceOrderDate = dr["CSHRBQ_CREATEDON"].ToString();

                model.MemberPreBuyList.Add(ShareLog);
            }
            #endregion

            return PartialView("MemberBuyShare", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BuyEFund(string WalletType, string Quantity, string EPin)
        {
            Response.Write(Resources.OneForAll.OneForAll.WarningSplitShare);
            return Json(string.Empty, JsonRequestBehavior.AllowGet);

            // string TheRate,
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                 return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
            }

            string Status = "";
            MemberDB.GetTime(out Status);

            if (Session["Admin"] == null)
            {
                if (Status == "1")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

            }

            int ok = 0;
            string msg = string.Empty;


            if (Quantity == "0")
            {
                Response.Write(Resources.OneForAll.OneForAll.WarningQuantity);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            float CheckQuantity = 0;

            if (!float.TryParse(Quantity, out CheckQuantity))
            {
                Response.Write("Invalid Quantity");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            #region Warning Empty Value

            if (WalletType == "0")
            {
                Response.Write(Resources.OneForAll.OneForAll.WarningWalletType);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrEmpty(Quantity))
            {
                Response.Write(Resources.OneForAll.OneForAll.WarningQuantity);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            //if (string.IsNullOrEmpty(TheRate))
            //{
            //    Response.Write(Resources.OneForAll.OneForAll.WarningRate);
            //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
            //}

            if (string.IsNullOrEmpty(EPin))
            {
                Response.Write(Resources.OneForAll.OneForAll.warningPIN);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            #endregion

            #region DB
            DataSet dsUser = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
            var ds = MemberShareDB.GetShareDetail(Session["Username"].ToString());

            var dbs = MemberDB.GetParameterBasedOnName("Share Rate Per Day");
            #endregion

            #region E-Pin Validation
            string UserEpin = dsUser.Tables[0].Rows[0]["CUSR_PIN"].ToString();

            if (Authentication.Encrypt(EPin) != UserEpin)
            {
                Response.Write(Resources.OneForAll.OneForAll.msgInvalidPIN);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            #endregion

            #region Information

            float GP = float.Parse(ds.Tables[3].Rows[0]["CMEM_GOLDPOINT"].ToString());
            float RPC = float.Parse(ds.Tables[3].Rows[0]["CMEM_REPURCHASEWALLET"].ToString());
            float TodayPrices = float.Parse(ds.Tables[0].Rows[0]["CSHARE_RATE"].ToString());
            #endregion  

            #region First Condition (Check Point)

            if (WalletType == "RPC")
            {
                if (float.Parse(Quantity) > RPC)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningRPCNotEnough);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
            }
            else if (WalletType == "GP")
            {
                if (float.Parse(Quantity) > GP)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningGPNotEnough);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                Response.Write(Resources.OneForAll.OneForAll.warningPlsSelectPointType);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }


            if (TodayPrices > float.Parse(Quantity))
            {
                Response.Write("Cant Be Lower Than Current Rate");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            #endregion


            int TotalRateUp = 0;
            int ParaShareRatePerDay = 0;
            ParaShareRatePerDay = Convert.ToInt32(dbs.Tables[0].Rows[0]["CPARA_FLOATVALUE"].ToString());
            TotalRateUp = Convert.ToInt32(ds.Tables[4].Rows[0]["TOTALRATEUP"].ToString());

            #region Second Condition (Cehck Unit Valid For That Rate)

            //if (Convert.ToInt32(Quantity) > UnitBalance)
            //{
            //    Response.Write(Resources.OneForAll.OneForAll.msgQuantityMoreThanUnitBal + TheRate +"("+UnitBalance.ToString("###,###")+")" );
            //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
            //}

            #endregion

            #region Buy E-Fund

            MemberShareDB.BuyEFund(Session["Username"].ToString(), WalletType, Quantity,"0" , out ok);

            if (ok == -1)
            {
                Response.Write(Resources.OneForAll.OneForAll.warningGPNotEnough);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            else if (ok == -2)
            {
                Response.Write(Resources.OneForAll.OneForAll.warningRPCNotEnough); 
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            #endregion

            //Session["page"] = "SharePage";
            //ViewBag.page = "SharePage";
            //return Json(string.Empty, JsonRequestBehavior.AllowGet);
            return MemberBuyShare();
        }

        #endregion

        #region Share Unit Statement Page        
        public ActionResult ShareUnitStatement(int selectedPage = -1)
        {
            Response.Write(Resources.OneForAll.OneForAll.WarningSplitShare);
            return Json(string.Empty, JsonRequestBehavior.AllowGet);

            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                var ShareDetail = MemberShareDB.GetShareDetail(Session["Username"].ToString());


                //int CompanyShareUnit = Convert.ToInt32(ShareDetail.Tables[6].Rows[0]["TOTALCOMPANYSHARE"].ToString());

                //if (CompanyShareUnit > 50000)
                //{
                //    Response.Write(Resources.OneForAll.OneForAll.WarningSplitShare);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                var Summary = MemberShareDB.GetTotalInOutUnit(Session["Username"].ToString());

                

                

                int pages = 0;
                var model = new PaginationWalletLogModel();

                model.TotalCashIn = Convert.ToInt32(Summary.Tables[0].Rows[0]["TOTALIN"].ToString()).ToString("###,##0");
                model.TotalCashOut = Convert.ToInt32(Summary.Tables[1].Rows[0]["TOTALOUT"].ToString()).ToString("###,##0");  

                DataSet dsBonusWalletLog = MemberWalletDB.GetAllShareUnitLogByUsername( Session["Username"].ToString(), selectedPage, out pages);
                selectedPage = ConstructPageList(selectedPage, pages, model);

                foreach (DataRow dr in dsBonusWalletLog.Tables[0].Rows)
                {
                    var walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CSHRA_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CSHRA_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CSHRA_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CSHRA_BALANACE"].ToString());
                    walletLog.AppOther = dr["CSHRA_OTHER"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CSHRA_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                    model.WalletLogList.Add(walletLog);
                }
                
                return PartialView("ShareUnitStatement", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Share Wallet Statement Page
        public ActionResult ShareWalletStatement(int selectedPage = -1, string cashname = "0")
        {

            Response.Write(Resources.OneForAll.OneForAll.WarningSplitShare);
            return Json(string.Empty, JsonRequestBehavior.AllowGet);

            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                var ShareDetail = MemberShareDB.GetShareDetail(Session["Username"].ToString());
                //int CompanyShareUnit = Convert.ToInt32(ShareDetail.Tables[6].Rows[0]["TOTALCOMPANYSHARE"].ToString());

                //if (CompanyShareUnit > 50000)
                //{
                //    Response.Write(Resources.OneForAll.OneForAll.WarningSplitShare);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                int pages = 0;
                var model = new PaginationWalletLogModel();

                DataSet dsBonusWalletLog = MemberWalletDB.GetAllShareWalletLogByUsername(cashname, Session["Username"].ToString(), selectedPage, out pages);
                selectedPage = ConstructPageList(selectedPage, pages, model);

                foreach (DataRow dr in dsBonusWalletLog.Tables[0].Rows)
                {
                    var walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CSHRWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CSHRWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CSHRWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CSHRWL_WALLET"].ToString());
                    walletLog.AppUser = dr["CSHRWL_APPUSER"].ToString();
                    var remark = Misc.GetReadableCashName(dr["CSHRWL_APPOTHER"].ToString());
                    walletLog.AppOther = remark.Length > 40 ? remark.Substring(0, 40) + "..." : remark;
                    walletLog.AppNumber = dr["CSHRWL_APPNUMBER"].ToString();
                    walletLog.AppRate = dr["CSHRWL_APPRATE"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CSHRWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    model.WalletLogList.Add(walletLog);
                }
                
                return PartialView("ShareWalletStatment", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Sharing Method
        private int ConstructPageList(int selectedPage, int pages, PaginationWalletLogModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 0; z < pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = (c + 1).ToString(),
                              Value = c.ToString()
                          };

            //if the selected page is -1, then set the first selected page
            if (model.Pages.Count() != 0 && selectedPage == -1)
            {
                model.Pages.First().Selected = true;
                selectedPage = int.Parse(model.Pages.First().Value);
            }
            return selectedPage;
        }

        #endregion

    }
}
