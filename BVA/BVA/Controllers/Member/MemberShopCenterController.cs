﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ECFBase.Models;
using System.Data;
using ECFBase.Components;
using System.Text.RegularExpressions;
using System.IO;
using ECFBase.Helpers;
using ECFBase.ThirdParties;
using System.Net;
using System.Web.Services;
using System.Web.Script.Services;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Globalization;

namespace ECFBase.Controllers.Member
{
    public class MemberShopCenterController : Controller
    {

        #region Old Sponsor Chart

        //[WebMethod]
        //[ScriptMethod(UseHttpGet = true)]

        //public ActionResult SearchSponsor(string Username)
        //{
        //    try { 
        //    if (Session["Username"] == null || Session["Username"].ToString() == "")
        //    {
        //         return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
        //    }

        //    if (Username == null || Username == "")
        //    {
        //        Username = Session["Username"].ToString();
        //    }


        //    var model = new MemberSponsorChartModel();

        //        int ok;
        //        string msg;

        //        DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChart(Username, out ok, out msg);

        //    if (dsNetworkTree.Tables[0].Rows.Count > 0)
        //    {
        //        model.FirstLevelMember = dsNetworkTree.Tables[0].Rows[0]["USERNAME"].ToString();
        //        model.FirstLevelLevel = dsNetworkTree.Tables[0].Rows[0]["LEVEL"].ToString();
        //        model.FirstLevelJoinedDate = dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString();
        //        model.FirstLevelRanking = dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString();
        //        model.FirstLevelIntro = dsNetworkTree.Tables[0].Rows[0]["INTRO"].ToString();
        //        model.FirstLevelFullname = dsNetworkTree.Tables[0].Rows[0]["FULLNAME"].ToString();

        //        foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
        //        {
        //            MemberList resultList = new MemberList();
        //            var info = GetMemberSponsorInfo(dr);
        //            resultList.Level = dr["LEVEL"].ToString();
        //            resultList.Member = dr["USERNAME"].ToString();
        //            resultList.Ranking = dr["RANK"].ToString();
        //            resultList.JoinedDate = dr["DATE"].ToString();
        //            resultList.FullName = dr["FULLNAME"].ToString();
        //            resultList.Intro = dr["INTRO"].ToString();

        //            model.resultList.Add(resultList);

        //        }

        //        ViewBag.MemberCount = 0;
        //    }
        //    else
        //    {
        //            Response.Write(Resources.OneForAll.OneForAll.warningPlsKeyInSearchID);
        //            return Json(string.Empty, JsonRequestBehavior.AllowGet);
        //    }

        //        return PartialView("SponsorChartTree", model);
        //    }
        //    catch (Exception e)
        //    {
        //        Response.Write(e.Message);
        //        return Json(string.Empty, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //public ActionResult SponsorChartTree(string memberUsername, int? memberLevel)
        //{
        //    if (Session["Username"] == null || Session["Username"].ToString() == "")
        //    {
        //         return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
        //    }

        //    string username;
        //    if (memberUsername == null)
        //    {
        //        username = Session["Username"].ToString();
        //        ViewBag.MemberLevel = 1;
        //        ViewBag.NextLevel = 4;
        //        Session["PreviousView"] = null;
        //    }
        //    else
        //    {
        //        username = memberUsername;
        //        ViewBag.MemberLevel = memberLevel;
        //        ViewBag.NextLevel = memberLevel + 3;
        //    }

        //    //Store Previous username and level
        //    List<String> userNameList = new List<String>();
        //    if (Session["PreviousView"] == null)
        //    {
        //        userNameList.Add(username + ";" + ViewBag.MemberLevel);
        //        Session["PreviousView"] = userNameList;
        //    }
        //    else
        //    {
        //        userNameList = (List<string>)Session["PreviousView"];
        //        userNameList.Add(memberUsername + ";" + memberLevel);
        //        Session["PreviousView"] = userNameList;
        //    }

        //    var model = new MemberSponsorChartModel();

        //    int ok;
        //    string msg;
        //    DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChart(username, out ok, out msg);
        //    ViewBag.MemberCount = 0;

        //    var member = MemberDB.GetMemberByUsername(username, out ok, out msg);

        //    model.FirstLevelMember = dsNetworkTree.Tables[0].Rows[0]["USERNAME"].ToString();
        //    model.FirstLevelLevel = dsNetworkTree.Tables[0].Rows[0]["LEVEL"].ToString();
        //    model.FirstLevelJoinedDate = dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString();
        //    model.FirstLevelRanking = Misc.GetMemberRanking(dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString());
        //    //model.FirstLevelRanking = dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString();
        //    model.FirstLevelIntro = dsNetworkTree.Tables[0].Rows[0]["INTRO"].ToString();
        //    model.FirstLevelFullname = dsNetworkTree.Tables[0].Rows[0]["FULLNAME"].ToString();
        //    model.FirstLevelRankIcon = member.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

        //    foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
        //    {
        //        MemberList resultList = new MemberList();
        //        var info = GetMemberSponsorInfo(dr);
        //        resultList.Level = dr["LEVEL"].ToString();
        //        resultList.Member = dr["USERNAME"].ToString();
        //        resultList.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());
        //        //resultList.Ranking = dr["RANK"].ToString();
        //        resultList.JoinedDate = dr["DATE"].ToString();
        //        resultList.FullName = dr["FULLNAME"].ToString();
        //        resultList.Intro = dr["INTRO"].ToString();
        //        resultList.RankIcon = dr["ICON"].ToString();

        //        model.resultList.Add(resultList);

        //    }

        //    DataSet logo = AdminGeneralDB.GetRankIcon();
        //    foreach (DataRow dr in logo.Tables[0].Rows)
        //    {
        //        var IFile = new ImageFile();
        //        IFile.FileName = dr["CRANKSET_ICON"].ToString();
        //        RankModel RankList = new RankModel();
        //        RankList.RankIconPath = dr["CRANKSET_ICON"].ToString();
        //        //RankList.RankIconName = dr["AMOUNT"].ToString();
        //        RankList.RankName = dr["CRANKSET_NAME"].ToString();
        //        ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

        //        model.RankList.Add(RankList);
        //    }

        //              return PartialView("SponsorChartTree", model);
        //}

        //private MemberSponsorChartModel GetMemberSponsorInfo(DataRow dr)
        //{
        //    var mntm = new MemberSponsorChartModel();

        //    mntm.Level = dr["LEVEL"].ToString();
        //    mntm.Member = dr["USERNAME"].ToString();
        //    mntm.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());
        //    mntm.JoinedDate = DateTime.Parse(dr["DATE"].ToString()).ToShortDateString();
        //    mntm.FullName = dr["FULLNAME"].ToString();
        //    mntm.Intro = dr["INTRO"].ToString();
        //    mntm.RankIcon = dr["ICON"].ToString();

        //    return mntm;
        //}
        #endregion

        #region SponsorChart

        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ActionResult SearchSponsor(string Username)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                if (Username == null || Username == "")
                {
                    Username = Session["Username"].ToString();
                }

                var model = new MemberSponsorChartModel();

                int ok;
                string msg;
                DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChart(Username, out ok, out msg);

                if (dsNetworkTree.Tables[0].Rows.Count > 0)
                {
                    model.FirstLevelMember = dsNetworkTree.Tables[0].Rows[0]["USERNAME"].ToString();
                    model.FirstLevelLevel = dsNetworkTree.Tables[0].Rows[0]["LEVEL"].ToString();
                    //model.FirstLevelJoinedDate = dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString();

                    var Date = dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString();
                    model.FirstLevelJoinedDate = Convert.ToDateTime(Date).ToString("dd/mm/yyyy");



                    model.FirstLevelRanking = dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString();
                    model.FirstLevelIntro = dsNetworkTree.Tables[0].Rows[0]["INTRO"].ToString();
                    model.FirstLevelFullname = dsNetworkTree.Tables[0].Rows[0]["FULLNAME"].ToString();

                    foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
                    {
                        MemberList resultList = new MemberList();
                        var info = GetMemberSponsorInfo(dr);
                        resultList.Level = dr["LEVEL"].ToString();
                        resultList.Member = dr["USERNAME"].ToString();
                        resultList.Ranking = dr["RANK"].ToString();
                        //resultList.JoinedDate = dr["DATE"].ToString();
                        var joinDate = dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString();
                        resultList.JoinedDate = Convert.ToDateTime(joinDate).ToString("dd/mm/yyyy");

                        resultList.FullName = dr["FULLNAME"].ToString();
                        resultList.Intro = dr["INTRO"].ToString();
                        model.resultList.Add(resultList);
                    }

                    ViewBag.MemberCount = 0;
                }
                else
                {
                    Response.Write("Invalid Sponsor");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return PartialView("SponsorChartTree", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        private int SponsorListConstructPageList(int selectedPage, int pages, MemberSponsorChartModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            if (pages == 0)
            {
                pageList.Add(1);
            }
            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        public ActionResult SponsorChartTree(string memberUsername, int? memberLevel, int selectedpage = 1)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                 return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
            }

            string Status = "";
            MemberDB.GetTime(out Status);

            if (Session["Admin"] == null)
            {
                if (Status == "1")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

            }

            string username;
            if (memberUsername == null)
            {
                username = Session["Username"].ToString();
                ViewBag.MemberLevel = 1;
                ViewBag.NextLevel = 4;
                Session["PreviousView"] = null;
            }
            else
            {
                username = memberUsername;
                ViewBag.MemberLevel = memberLevel;
                ViewBag.NextLevel = memberLevel + 3;
            }

            //Store Previous username and level
            var userNameList = new List<String>();
            if (Session["PreviousView"] == null)
            {
                userNameList.Add(username + ";" + ViewBag.MemberLevel);
                Session["PreviousView"] = userNameList;
            }
            else
            {
                userNameList = (List<string>)Session["PreviousView"];
                userNameList.Add(memberUsername + ";" + memberLevel);
                Session["PreviousView"] = userNameList;
            }

            var model = new MemberSponsorChartModel();
            int ok;
            string msg;
            DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChartVer1(username, out ok, out msg);
            ViewBag.MemberCount = 0;

            var member = MemberDB.GetMemberByUsername(username, out ok, out msg);
            model.FirstLevelMember = dsNetworkTree.Tables[0].Rows[0]["USERNAME"].ToString();
            model.FirstLevelLevel = dsNetworkTree.Tables[0].Rows[0]["LEVEL"].ToString();
            //model.FirstLevelJoinedDate = DateTime.Parse(dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString()).ToShortDateString();

            //var Date = dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString();
            //model.FirstLevelJoinedDate = Convert.ToDateTime(Date).ToString("dd/mm/yyyy");
            model.FirstLevelJoinedDate = Convert.ToDateTime(dsNetworkTree.Tables[0].Rows[0]["DATE"]).ToString("dd/MM/yyyy");

            model.FirstLevelRanking = Misc.GetMemberRanking(dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString());

            if (dsNetworkTree.Tables[0].Rows[0]["UPGRADEDATE"].ToString() != "")
            {
                model.FirstLevelRanking += string.Format(" (Upgrade:{0})", DateTime.Parse(dsNetworkTree.Tables[0].Rows[0]["UPGRADEDATE"].ToString()).ToShortDateString());
            }

            model.FirstLevelIntro = dsNetworkTree.Tables[0].Rows[0]["INTRO"].ToString();

            model.FirstLevelIntro = string.Format("{0} ({1})", model.FirstLevelIntro, dsNetworkTree.Tables[0].Rows[0]["GROUPSALES"].ToString());

            model.FirstLevelFullname = dsNetworkTree.Tables[0].Rows[0]["FULLNAME"].ToString();
            model.FirstLevelRankIcon = Misc.GetMemberRankingIcon(dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString());

            foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
            {
                //var resultList = new MemberList();
                MemberList resultList = new MemberList();
                resultList.Level = dr["LEVEL"].ToString();
                resultList.Member = dr["USERNAME"].ToString();
                resultList.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());

                if (dr["UPGRADEDATE"].ToString() != "")
                {
                    model.Ranking += string.Format(" (Upgrade:{0})", DateTime.Parse(dr["UPGRADEDATE"].ToString()).ToShortDateString());
                }

                //resultList.JoinedDate = DateTime.Parse(dr["DATE"].ToString()).ToShortDateString();

                var JoinedDate = dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString();
                resultList.JoinedDate = Convert.ToDateTime(JoinedDate).ToString("dd/mm/yyyy");

                resultList.FullName = dr["FULLNAME"].ToString();
                resultList.Intro = dr["INTRO"].ToString();

                model.Intro = string.Format("{0} ({1})", model.Intro, dr["GROUPSALES"].ToString());

                resultList.RankIcon = Misc.GetMemberRankingIcon(dr["RANK"].ToString());
                resultList.MemberCount = dr["MEMBERCOUNT"].ToString();
                resultList.GoingToExpended = int.Parse(dr["LEVEL"].ToString()) % 3 == 0;
                model.resultList.Add(resultList);
            }

            DataSet logo = AdminGeneralDB.GetRankIcon();
            foreach (DataRow dr in logo.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["CRANKSET_ICON"].ToString();
                RankModel RankList = new RankModel();
                RankList.RankIconPath = dr["CRANKSET_ICON"].ToString();
                RankList.RankName = dr["CRANKSET_NAME"].ToString();
                ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

                model.RankList.Add(RankList);
            }

            /*** For Referral Listing***/
            //int pages = 0;
            //DataSet dsSponsorList = MemberShopCenterDB.GetAllSponsorListByUsername(Session["Username"].ToString(), selectedpage, out pages);

            //selectedpage = SponsorListConstructPageList(selectedpage, pages, model);

            //foreach (DataRow dr in dsSponsorList.Tables[0].Rows)
            //{
            //    IntroListModel sponsorlist = new IntroListModel();
            //    sponsorlist.No = dr["rownumber"].ToString();
            //    sponsorlist.Username = dr["CUSR_MEMBERID"].ToString();
            //    sponsorlist.Name = dr["CUSR_FULLNAME"].ToString();
            //    sponsorlist.Rank = Misc.GetMemberRanking(dr["CRANK_CODE"].ToString());
            //    //sponsorlist.Sponsor = dr["CUSR_INTRO"].ToString();

            //    string firstupline = dr["CUSR_INTRO"].ToString();
            //    if (sponsorlist.Username == Session["Username"].ToString())
            //    {
            //        sponsorlist.Sponsor = "";
            //    }
            //    else
            //    {
            //        sponsorlist.Sponsor = firstupline;
            //    }


            //    sponsorlist.Level = dr["CMTREE_LEVEL"].ToString();
            //    sponsorlist.CreatedDate = Convert.ToDateTime(dr["CMEM_DATEJOINED"]).ToString("dd/MM/yyyy hh:mm:ss tt");

            //    model.SponsorList.Add(sponsorlist);
            //}


            return PartialView("SponsorChartTree", model);
        }


        //public ActionResult FindSponsorDownline(string Username)
        //{
        //    if (Session["Username"] == null || Session["Username"].ToString() == "")
        //    {
        //         return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
        //    }

        //    int isBelongTo;
        //    MemberDB.MemberIsInDownlineByUsername(Username, Session["Username"].ToString(), out isBelongTo);
        //    if (isBelongTo == 0)//not belong to
        //    {
        //        Response.Write(string.Format(Resources.MemberShopCenter.ShopCenter.msgNotInTree, Username));
        //        return Json(string.Empty, JsonRequestBehavior.AllowGet);
        //    }

        //    return SponsorChartTreeVer1(Username, null);
        //}

        //public ActionResult SponsorChartTree(string memberUsername, int? memberLevel)
        //{
        //    if (Session["Username"] == null || Session["Username"].ToString() == "")
        //    {
        //         return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
        //    }

        //    string username;
        //    if (memberUsername == null)
        //    {
        //        username = Session["Username"].ToString();
        //        ViewBag.MemberLevel = 1;
        //        ViewBag.NextLevel = 4;
        //        Session["PreviousView"] = null;
        //    }
        //    else
        //    {
        //        username = memberUsername;
        //        ViewBag.MemberLevel = memberLevel;
        //        ViewBag.NextLevel = memberLevel + 3;
        //    }

        //    //Store Previous username and level
        //    var userNameList = new List<String>();
        //    if (Session["PreviousView"] == null)
        //    {
        //        userNameList.Add(username + ";" + ViewBag.MemberLevel);
        //        Session["PreviousView"] = userNameList;
        //    }
        //    else
        //    {
        //        userNameList = (List<string>)Session["PreviousView"];
        //        userNameList.Add(memberUsername + ";" + memberLevel);
        //        Session["PreviousView"] = userNameList;
        //    }

        //    var model = new MemberSponsorChartModel();
        //    int ok;
        //    string msg;
        //    DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChartVer1(username, out ok, out msg);
        //    ViewBag.MemberCount = 0;

        //    var member = MemberDB.GetMemberByUsername(username, out ok, out msg);
        //    model.FirstLevelMember = dsNetworkTree.Tables[0].Rows[0]["USERNAME"].ToString();
        //    model.FirstLevelLevel = dsNetworkTree.Tables[0].Rows[0]["LEVEL"].ToString();
        //    model.FirstLevelJoinedDate = DateTime.Parse(dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString()).ToShortDateString();
        //    model.FirstLevelRanking = Misc.GetMemberRanking(dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString());

        //    if (dsNetworkTree.Tables[0].Rows[0]["UPGRADEDATE"].ToString() != "")
        //    {
        //        model.FirstLevelRanking += string.Format(" (Upgrade:{0})", DateTime.Parse(dsNetworkTree.Tables[0].Rows[0]["UPGRADEDATE"].ToString()).ToShortDateString());
        //    }

        //    model.FirstLevelIntro = dsNetworkTree.Tables[0].Rows[0]["INTRO"].ToString();

        //    model.FirstLevelIntro = string.Format("{0} ({1})", model.FirstLevelIntro, dsNetworkTree.Tables[0].Rows[0]["GROUPSALES"].ToString());

        //    model.FirstLevelFullname = dsNetworkTree.Tables[0].Rows[0]["FULLNAME"].ToString();
        //    model.FirstLevelRankIcon = Misc.GetMemberRankingIcon(dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString());

        //    foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
        //    {
        //        var resultList = new MemberList();
        //        resultList.Level = dr["LEVEL"].ToString();
        //        resultList.Member = dr["USERNAME"].ToString();
        //        resultList.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());

        //        if (dr["UPGRADEDATE"].ToString() != "")
        //        {
        //            model.Ranking += string.Format(" (Upgrade:{0})", DateTime.Parse(dr["UPGRADEDATE"].ToString()).ToShortDateString());
        //        }

        //        resultList.JoinedDate = DateTime.Parse(dr["DATE"].ToString()).ToShortDateString();
        //        resultList.FullName = dr["FULLNAME"].ToString();
        //        resultList.Intro = dr["INTRO"].ToString();

        //        model.Intro = string.Format("{0} ({1})", model.Intro, dr["GROUPSALES"].ToString());

        //        resultList.RankIcon = Misc.GetMemberRankingIcon(dr["RANK"].ToString());
        //        resultList.MemberCount = dr["MEMBERCOUNT"].ToString();
        //        resultList.GoingToExpended = int.Parse(dr["LEVEL"].ToString()) % 3 == 0;
        //        model.resultList.Add(resultList);
        //    }

        //    return PartialView("SponsorChartTree", model);
        //}

        private MemberSponsorChartModel GetMemberSponsorInfoVer2(DataRow dr)
        {
            var mntm = new MemberSponsorChartModel();
            mntm.Level = dr["LEVEL"].ToString();
            mntm.Member = dr["USERNAME"].ToString();
            mntm.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());
            mntm.JoinedDate = DateTime.Parse(dr["DATE"].ToString()).ToShortDateString();
            mntm.FullName = dr["FULLNAME"].ToString();
            mntm.Intro = dr["INTRO"].ToString();
            mntm.RankIcon = dr["ICON"].ToString();
            return mntm;
        }

        [HttpPost]
        public JsonResult FindNextSponsorList(string username, int level)
        {
            int ok = 0;
            string msg = "";
            

            DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChartVer1Fixed2Level(username, out ok, out msg);
            ViewBag.MemberCount = 0;

            var member = MemberDB.GetMemberByUsername(username, out ok, out msg);
            List<MemberList> list = new List<MemberList>();

            foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
            {
                var resultList = new MemberList();
                //resultList.Level = dr["LEVEL"].ToString();
                resultList.Level = string.Format("{0}", level + 1);
                resultList.Member = dr["USERNAME"].ToString();
                resultList.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());

                if (dr["UPGRADEDATE"].ToString() != "")
                {
                    resultList.Ranking += string.Format(" (Upgrade:{0})", DateTime.Parse(dr["UPGRADEDATE"].ToString()).ToShortDateString());
                }

                resultList.JoinedDate = DateTime.Parse(dr["DATE"].ToString()).ToShortDateString();


                var Date = dr["DATE"].ToString();
                //resultList.JoinedDate = Convert.ToDateTime(Date).ToString("dd/mm/yyyy");
                resultList.JoinedDate = Convert.ToDateTime(dr["DATE"]).ToString("dd/MM/yyyy");


                resultList.FullName = dr["FULLNAME"].ToString();
                resultList.Intro = dr["INTRO"].ToString();

                resultList.Intro = string.Format("{0} ({1})", resultList.Intro, dr["GROUPSALES"].ToString());

                resultList.RankIcon = Misc.GetMemberRankingIcon(dr["RANK"].ToString());
                resultList.MemberCount = dr["MEMBERCOUNT"].ToString();
                resultList.GoingToExpended = int.Parse(dr["LEVEL"].ToString()) % 3 == 0;

                if (dr["LEVEL"].ToString() == "2")
                {
                    list.Add(resultList);
                }
            }

            return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        }

        private MemberSponsorChartModel GetMemberSponsorInfo(DataRow dr)
        {
            var mntm = new MemberSponsorChartModel();

            mntm.Level = dr["LEVEL"].ToString();
            mntm.Member = dr["USERNAME"].ToString();
            mntm.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());
            mntm.JoinedDate = DateTime.Parse(dr["DATE"].ToString()).ToShortDateString();
            mntm.FullName = dr["FULLNAME"].ToString();
            mntm.Intro = dr["INTRO"].ToString();
            mntm.RankIcon = dr["ICON"].ToString();

            return mntm;
        }

        #endregion

        #region CheckEmail
        [HttpPost]
        public JsonResult CheckEmail(string Email)
        {
            var result = false;
            Regex reg = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

            if (reg.IsMatch(Email))
            {
                result = true;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion
        
        #region Invoice Listing

        public ActionResult PrintInvoice(int InvoiceID)
        {
            ViewBag.InvoiceID = InvoiceID;
            return View("PrintInvoice");
        }

        public ActionResult InvoiceListing(int selectedPage = 1)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                 return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
            }

            int ok;
            string msg;
            int pages = 0;
            var model = new PaginationInvoiceListingModel();

            var dsInvoice = AdminSalesOrderDB.GetAllInvoicesByUsername(Session["Username"].ToString(), Session["LanguageChosen"].ToString(),
                                                                        selectedPage, out pages, out ok, out msg);

            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsInvoice.Tables[0].Rows)
            {
                var invoice = new InvoiceModel();

                //invoice info
                invoice.RowNumber = dr["rownumber"].ToString();
                invoice.Id = Convert.ToInt32(dr["CINVNO_ID"]);
                invoice.InvoiceNo = dr["CINVNO_NO"].ToString();
                invoice.InvoiceCashName = Misc.GetReadableCashName(dr["CINVNO_CASHNAME"].ToString());
                invoice.Username = dr["CUSR_USERNAME"].ToString();
                invoice.Date = (DateTime)dr["CINVNO_CREATEDON"];
                invoice.Status = dr["DOSTATUS"].ToString();
                invoice.Investment = float.Parse(dr["PKGINVEST"].ToString());
                invoice.PackageQuantity = 1; //currently only 1 Pkg is sold during register
                invoice.TotalAmount = (invoice.PackageQuantity * invoice.Investment);

                model.InvoiceLists.Add(invoice);
            }

            return PartialView("InvoiceListing", model);
        }

        public ActionResult InvoiceView(int Id)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                 return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
            }

            int ok;
            string msg;
            var dsInvoice = AdminSalesOrderDB.GetInvoiceRelatedInfoById(Id, Session["LanguageChosen"].ToString(),
                                                                        out ok, out msg);
            var invoice = new InvoiceModel();

            //invoice info
            invoice.RowNumber = dsInvoice.Tables[0].Rows[0]["rownumber"].ToString();
            invoice.Id = Convert.ToInt32(dsInvoice.Tables[0].Rows[0]["CINVNO_ID"]);
            invoice.InvoiceNo = dsInvoice.Tables[0].Rows[0]["CINVNO_NO"].ToString();
            invoice.InvoiceCashName = Misc.GetReadableCashName(dsInvoice.Tables[0].Rows[0]["CINVNO_CASHNAME"].ToString());
            invoice.CountryCode = dsInvoice.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
            invoice.Date = (DateTime)dsInvoice.Tables[0].Rows[0]["CINVNO_CREATEDON"];
            var shippingAddress = dsInvoice.Tables[0].Rows[0]["CINVNO_ADDRESS"].ToString();
            if (!string.IsNullOrEmpty(shippingAddress.Trim()))
            {

                var address = shippingAddress.Split(new char[] { '\r' });
                for (int i = 0; i < address.Length; i++)
                {
                    if (i == 0) invoice.ShipAddress = address[0];
                    else if (i == 1) invoice.ShipAddress1 = address[1];
                    else if (i == 2) invoice.ShipAddress2 = address[2];
                    else if (i == 3) invoice.ShipAddress3 = address[3];
                }
            }

            //user and member info
            invoice.Username = dsInvoice.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
            invoice.CellPhone = dsInvoice.Tables[0].Rows[0]["CELLPHONE"].ToString();
            invoice.Phone = dsInvoice.Tables[0].Rows[0]["PHONE"].ToString();
            invoice.IntroPerson = dsInvoice.Tables[0].Rows[0]["INTRO"].ToString();

            //package info
            invoice.PackageCode = dsInvoice.Tables[0].Rows[0]["CPKG_CODE"].ToString();
            invoice.Investment = float.Parse(dsInvoice.Tables[0].Rows[0]["PKGINVEST"].ToString());
            invoice.PackageQuantity = 1; //currently only 1 Pkg is sold during register
            invoice.PackageDescription = dsInvoice.Tables[0].Rows[0]["PKGDESC"].ToString();
            invoice.TotalAmount = (invoice.PackageQuantity * invoice.Investment);

            return PartialView("InvoiceView", invoice);
        }

        #endregion

        #region Sponsor List
        public ActionResult SponsorListing(int selectedPage = 1)
        {
            try
            {
                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                int pages = 0;
                MemberSponsorChartModel model = new MemberSponsorChartModel();
              
                DataSet dsSponsorList = MemberShopCenterDB.GetAllSponsorListByUsername(Session["Username"].ToString(), selectedPage, out pages);

                selectedPage = SponsorListConstructPageList(selectedPage, pages, model);

                foreach (DataRow dr in dsSponsorList.Tables[0].Rows)
                {
                    IntroListModel sponsorlist = new IntroListModel();
                    sponsorlist.No = dr["rownumber"].ToString();
                    sponsorlist.Username = dr["CUSR_MEMBERID"].ToString();
                    sponsorlist.Name = dr["CUSR_FULLNAME"].ToString();
                    sponsorlist.Rank = Misc.GetMemberRanking(dr["CRANK_CODE"].ToString());
                    //sponsorlist.Sponsor = dr["CUSR_INTRO"].ToString();

                    string firstupline = dr["CUSR_INTRO"].ToString();
                    if (sponsorlist.Username == Session["Username"].ToString())
                    {
                        sponsorlist.Sponsor = "";
                    }
                    else
                    {
                        sponsorlist.Sponsor = firstupline;
                    }


                    sponsorlist.Level = dr["CMTREE_LEVEL"].ToString();
                    sponsorlist.CreatedDate = Convert.ToDateTime(dr["CMEM_DATEJOINED"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    model.SponsorList.Add(sponsorlist);
                }

                return PartialView("SponsorListing", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Binary List

        public ActionResult BinaryListing(int selectedPage = 1)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                int pages = 0;
                PaginationIntroListModel model = new PaginationIntroListModel();
                DataSet dsBinaryList = MemberShopCenterDB.GetAllBinaryListByUsername(Session["Username"].ToString(), selectedPage, out pages);

                selectedPage = BinaryConstructPageList(selectedPage, pages, model);

                foreach (DataRow dr in dsBinaryList.Tables[0].Rows)
                {
                    IntroListModel sponsorlist = new IntroListModel();
                    sponsorlist.No = dr["rownumber"].ToString();
                    sponsorlist.Username = dr["CUSR_USERNAME"].ToString();
                    sponsorlist.FullName = dr["CUSR_FIRSTNAME"].ToString();
                    sponsorlist.Sponsor = dr["CMTREE_UNDER"].ToString();
                    sponsorlist.Upline = dr["CMTREE_UPMEMBER"].ToString();
                    sponsorlist.Level = dr["CMTREE_LEVEL"].ToString();
                    sponsorlist.Rank = Misc.RankNumber(dr["CRANK_CODE"].ToString());
                    sponsorlist.CreatedDate = Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                    model.SponsorList.Add(sponsorlist);
                }

                return PartialView("BinaryListing", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
        
        #region Shared

        private int BinaryConstructPageList(int selectedPage, int pages, PaginationIntroListModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        private int SponsorConstructPageList(int selectedPage, int pages, PaginationIntroListModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        private int ConstructPageList(int selectedPage, int pages, PaginationInvoiceListingModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        [HttpPost]
        public JsonResult FindCountryCode(string CountryCode)
        {
            int ok = 0;
            string msg = "";
            DataSet dsCountrySetup = AdminGeneralDB.GetCountryByCountryCode(CountryCode, out ok, out msg);

            return Json(dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_MOBILECODE"].ToString(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FindCity(string ProvinceCode)
        {
            string LanguageCode = Session["LanguageChosen"].ToString();

            var result = GetCityList(LanguageCode, ProvinceCode);
            ((RegisterNewMemberModel)Session["regMember"]).UserInfo.City = result;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public static IEnumerable<SelectListItem> GetCityList(string LanguageCode, string ProvinceCode)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            int ok = 0;
            string message = "";
            List<CityModel> cities = Misc.GetAllCityByProvince(LanguageCode, ProvinceCode, ref ok, ref message);

            var result = new[] { new SelectListItem { Text = "" } }.Concat(from c in cities
                                                                           select new SelectListItem
                                                                           {
                                                                               Selected = false,
                                                                               Text = c.CityName,
                                                                               Value = c.CityCode
                                                                           });
            return result.OrderBy(m => m.Value).ToList();
        }

        [HttpPost]
        public JsonResult FindProvince(string CountryCode)
        {
            string LanguageCode = Session["LanguageChosen"].ToString();

            var result = GetProvinceList(LanguageCode, CountryCode);
            ((RegisterNewMemberModel)Session["regMember"]).UserInfo.Province = result;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public static IEnumerable<SelectListItem> GetProvinceList(string LanguageCode, string CountryCode)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            int ok = 0;
            string message = "";
            List<ProvinceModel> provinces = Misc.GetAllProvinceByCountry(LanguageCode, CountryCode, ref ok, ref message);

            var result = new[] { new SelectListItem { Text = "" } }.Concat(from c in provinces
                                                                           select new SelectListItem
                                                                           {
                                                                               Selected = false,
                                                                               Text = c.ProvinceName,
                                                                               Value = c.ProvinceCode
                                                                           });
            return result.OrderBy(m => m.Value).ToList();
        }

        [HttpPost]
        public JsonResult CheckAvailability(string Username)
        {
            int ok = 0;
            MemberShopCenterDB.CheckUsernameAvailability(Username, out ok);

            return Json(ok.ToString(), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region MarketTree
        [HttpPost]
        public ActionResult MPMarketTree(int selectedPage = 1)
        {
            Session["RegisterPage"] = "MP";
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                 return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
            }


            string Status = "";
            MemberDB.GetTime(out Status);

            if (Session["Admin"] == null)
            {
                if (Status == "1")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

            }

            int ok = 0, pages = 0;
            string msg = "";
            var member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
            string TopMember = member.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();


            MarketTreeModel memberMarketTree = MPGetAllMarketTreeMember(TopMember);

            memberMarketTree.country = member.Tables[0].Rows[0]["CCOUNTRY_ID"].ToString();

            var Stockist = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
            memberMarketTree.Stockist = Stockist.Tables[0].Rows[0]["CMEM_MEMBER_TYPE"].ToString();


            DataSet logo = AdminGeneralDB.GetRankIcon();
            foreach (DataRow dr in logo.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["CRANKSET_ICON"].ToString();
                RankModel RankList = new RankModel();
                RankList.RankIconPath = dr["CRANKSET_ICON"].ToString();
                //RankList.RankIconName = float.Parse(dr["AMOUNT"].ToString());
                //RankList.RankAmount = float.Parse(dr["AMOUNT"].ToString());
                ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();
                RankList.RankName = dr["CRANKSET_NAME"].ToString();

                memberMarketTree.RankList.Add(RankList);
            }


            string keke = Session["LanguageChosen"].ToString();
            if (Session["LanguageChosen"].ToString().Equals("zh-cn", StringComparison.InvariantCultureIgnoreCase))
            {
                memberMarketTree.Picture = "RegisterEn.png";
            }
            else if (Session["LanguageChosen"].ToString().Equals("en-US", StringComparison.InvariantCultureIgnoreCase))
            {
                memberMarketTree.Picture = "RegisterEn.png";
            }
            else if (Session["LanguageChosen"].ToString().Equals("zh-tw", StringComparison.InvariantCultureIgnoreCase))
            {
                memberMarketTree.Picture = "RegisterEn.png";
            }
            else if (Session["LanguageChosen"].ToString().Equals("ja-JP", StringComparison.InvariantCultureIgnoreCase))
            {
                memberMarketTree.Picture = "RegisterEn.png";
            }

            return PartialView("MPMarketTree", memberMarketTree);
        }  

        private MarketTreeModel MPGetAllMarketTreeMember(string TopMember)
        {
            MarketTreeModel treeMC = new MarketTreeModel();

            int nCount = 0;
            string fullName = "";

            DataSet dsFollow = MemberDB.GetFollowID(TopMember);

            //Loop for 15 times
            while (nCount <= 14)
            {
                if (dsFollow.Tables[nCount].Rows.Count == 0)
                {
                    treeMC.MemberList.Add("0");
                    treeMC.LogoList.Add("~");
                    treeMC.TooltipList.Add(ConstructTooltipTree("", "", "0", "0", "0", "0", "", ""));
                    treeMC.PackageList.Add("");
                    treeMC.DateList.Add("");
                    treeMC.AccYJLeftList.Add("");
                    treeMC.AccYJRightList.Add("");
                    treeMC.CFBalLeftList.Add("");
                    treeMC.CFBalRightList.Add("");
                    treeMC.SalesLeftList.Add("");
                    treeMC.SalesRightList.Add("");
                    treeMC.TotalDownlineLeft.Add("");
                    treeMC.TotalDownlineRight.Add("");

                    if (nCount == 0)
                    {
                        treeMC.MainLeftYJ = "0";
                        treeMC.MainLeftBalance = "0";
                        treeMC.MainRightYJ = "0";
                        treeMC.MainRightBalance = "0";
                    }
                }
                else
                {
                    DataRow dr = dsFollow.Tables[nCount].Rows[0];
                    DateTime joinedDate = DateTime.Parse(dr["CUSR_CREATEDON"].ToString());
                    fullName = string.Format("{0} {1}", dr["CUSR_FULLNAME"].ToString(), dr["CUSR_FULLNAME"].ToString());
                    treeMC.MemberList.Add(dr["CUSR_USERNAME"].ToString());

                    int ok = 0;
                    string msg = "";
                    string imagePath = "";
                    var member = MemberDB.GetMemberByUsername(dr["CUSR_USERNAME"].ToString(), out ok, out msg);


                    //imagePath = Misc.GetCodeNameImageInt(dr["CRANKSET_NAME"].ToString());
                    imagePath = member.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

                    treeMC.LogoList.Add(imagePath);
                    treeMC.TooltipList.Add(ConstructTooltipTree(fullName, joinedDate.ToShortDateString(), dr["CMTREE_GROUPAYJ"].ToString(), dr["CMTREE_GROUPBYJ"].ToString(), dr["CMTREE_GROUPAGP"].ToString(), dr["CMTREE_GROUPBGP"].ToString(), dr["CMEM_INTRO"].ToString(), string.Format("人民币{0}", dr["CPKG_AMOUNT"].ToString())));
                    treeMC.PackageList.Add(string.Format("{0}", Helper.NVL(dr["CUSRINFO_NICKNAME"])));
                    treeMC.DateList.Add(string.Format("{0}", joinedDate.ToString("dd/MM/yyyy")));


                    float leftYJ1 = float.Parse(dr["CMTREE_GROUPAYJ"].ToString());
                    float rightYJ1 = float.Parse(dr["CMTREE_GROUPBYJ"].ToString());
                    float leftGP1 = float.Parse(dr["CMTREE_GROUPAGP"].ToString());
                    float rightGP1 = float.Parse(dr["CMTREE_GROUPBGP"].ToString());
                    float leftsales1 = float.Parse(dr["LEFTSALES"].ToString());
                    float rightsales1 = float.Parse(dr["RIGHTSALES"].ToString());
                    int lefttotaldownline1 = Convert.ToInt32(dr["CMTREE_TGROUPAMEMBER"].ToString());
                    int righttotaldownline1 = Convert.ToInt32(dr["CMTREE_TGROUPBMEMBER"].ToString());

                    treeMC.AccYJLeftList.Add(string.Format("{0}", Helper.NVL(leftYJ1.ToString("###,##0"))));
                    treeMC.AccYJRightList.Add(string.Format("{0}", Helper.NVL(rightYJ1.ToString("###,##0"))));
                    treeMC.CFBalLeftList.Add(string.Format("{0}", Helper.NVL(leftGP1.ToString("###,##0"))));
                    treeMC.CFBalRightList.Add(string.Format("{0}", Helper.NVL(rightGP1.ToString("###,##0"))));
                    treeMC.SalesLeftList.Add(string.Format("{0}", Helper.NVL(leftsales1.ToString("###,##0"))));
                    treeMC.SalesRightList.Add(string.Format("{0}", Helper.NVL(rightsales1.ToString("###,##0"))));
                    treeMC.TotalDownlineLeft.Add(string.Format("{0}", Helper.NVL(lefttotaldownline1.ToString())));
                    treeMC.TotalDownlineRight.Add(string.Format("{0}", Helper.NVL(righttotaldownline1.ToString())));


                    if (nCount == 0)
                    {
                        float leftYJ = float.Parse(dr["CMTREE_GROUPAYJ"].ToString());
                        float rightYJ = float.Parse(dr["CMTREE_GROUPBYJ"].ToString());
                        float leftGP = float.Parse(dr["CMTREE_GROUPAGP"].ToString());
                        float rightGP = float.Parse(dr["CMTREE_GROUPBGP"].ToString());
                        float leftsales = float.Parse(dr["LEFTSALES"].ToString());
                        float rightsales = float.Parse(dr["RIGHTSALES"].ToString());

                        treeMC.MainLeftYJ = leftYJ.ToString("###,##0");
                        treeMC.MainRightYJ = rightYJ.ToString("###,##0");

                        treeMC.MainLeftBalance = leftGP.ToString("###,##0");
                        treeMC.MainRightBalance = rightGP.ToString("###,##0");

                        treeMC.MainLeftSales = leftsales.ToString("###,##0");
                        treeMC.MainRightSales = rightsales.ToString("###,##0");
                    }
                    else if (nCount == 1)
                    {
                        float leftYJ = float.Parse(dr["CMTREE_GROUPAYJ"].ToString());
                        float rightYJ = float.Parse(dr["CMTREE_GROUPBYJ"].ToString());
                        float leftGP = float.Parse(dr["CMTREE_GROUPAGP"].ToString());
                        float rightGP = float.Parse(dr["CMTREE_GROUPBGP"].ToString());
                        float leftsales = float.Parse(dr["LEFTSALES"].ToString());
                        float rightsales = float.Parse(dr["RIGHTSALES"].ToString());

                        treeMC.FirstLeftYJ = leftYJ.ToString("###,##0");
                        treeMC.FirstRightYJ = rightYJ.ToString("###,##0");

                        treeMC.FirstLeftBalance = leftGP.ToString("###,##0");
                        treeMC.FirstRightBalance = rightGP.ToString("###,##0");

                        treeMC.FirstLeftSales = leftsales.ToString("###,##0");
                        treeMC.FirstRightSales = rightsales.ToString("###,##0");
                    }
                    else if (nCount == 2)
                    {
                        float leftYJ = float.Parse(dr["CMTREE_GROUPAYJ"].ToString());
                        float rightYJ = float.Parse(dr["CMTREE_GROUPBYJ"].ToString());
                        float leftGP = float.Parse(dr["CMTREE_GROUPAGP"].ToString());
                        float rightGP = float.Parse(dr["CMTREE_GROUPBGP"].ToString());
                        float leftsales = float.Parse(dr["LEFTSALES"].ToString());
                        float rightsales = float.Parse(dr["RIGHTSALES"].ToString());
                        int lefttotaldownline = Convert.ToInt32(dr["CMTREE_TGROUPAMEMBER"].ToString());
                        int righttotaldownline = Convert.ToInt32(dr["CMTREE_TGROUPBMEMBER"].ToString());

                        treeMC.SecondLeftYJ = leftYJ.ToString("###,##0");
                        treeMC.SecondRightYJ = rightYJ.ToString("###,##0");

                        treeMC.SecondLeftBalance = leftGP.ToString("###,##0");
                        treeMC.SecondRightBalance = rightGP.ToString("###,##0");

                        treeMC.SecondLeftSales = leftsales.ToString("###,##0");
                        treeMC.SecondRightSales = rightsales.ToString("###,##0");

                        treeMC.SecondLeftTotalDownline = lefttotaldownline.ToString("###,##0");
                        treeMC.SecondRightTotalDownline = righttotaldownline.ToString("###,##0");
                    }
                }

                nCount++;
            }

            return treeMC;
        }

        public string ConstructTooltipTree(string fullName, string joinedDate, string strLeftYJ, string strRightYJ, string LeftGP, string RightGP, string sponsor, string investStar)
        {
            float fLeftYJ = float.Parse(strLeftYJ);
            float fRightYJ = float.Parse(strRightYJ);
            float fLeftGP = float.Parse(LeftGP);
            float fRightGP = float.Parse(RightGP);
            string leftSales = Resources.OneForAll.OneForAll.lblLeftSale;
            string rightSales = Resources.OneForAll.OneForAll.lblRightSale;
            string sponsorLabel = Resources.OneForAll.OneForAll.lblSponsor;
            string tooltip = string.Format("{0}({1})\n{2}\n{3} : {4}\n{5}     :{6}\t\t{7}     ", fullName, joinedDate, sponsorLabel, sponsor, leftSales, strLeftYJ, rightSales, strRightYJ);
            return tooltip;
        }

        public ActionResult MPGetMarketTreeMostLeft(string Username)
        {

            int isBelongTo;
            MemberDB.MemberIsInMarketTreeByUsername(Username, Session["Username"].ToString(), out isBelongTo);

            if (isBelongTo == 0)//not belong to
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.msgNotInTree, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            DataSet dsFollow = MemberDB.GetMarketTreeMostLeft(Username);
            DataRow dr = dsFollow.Tables[0].Rows[0];
            string upMember = dr["CUSR_USERNAME"].ToString();


            return MPMarketTreeByUsername(upMember);
        }

        public ActionResult MPGetMarketTreeMostRight(string Username)
        {
            int isBelongTo;
            MemberDB.MemberIsInMarketTreeByUsername(Username, Session["Username"].ToString(), out isBelongTo);

            if (isBelongTo == 0)//not belong to
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.msgNotInTree, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            DataSet dsFollow = MemberDB.GetMarketTreeMostRight(Username);
            DataRow dr = dsFollow.Tables[0].Rows[0];
            string upMember = dr["CUSR_USERNAME"].ToString();



            return MPMarketTreeByUsername(upMember);
        }


        [HttpPost]
        public ActionResult MPMarketTreeByUsername(string TopMember)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                 return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
            }

            int isBelongTo;
            MemberDB.MemberIsInMarketTreeByUsername(TopMember, Session["Username"].ToString(), out isBelongTo);

            if (isBelongTo == 0)//not belong to
            {
                Response.Write(string.Format(Resources.OneForAll.OneForAll.msgNotInTree, TopMember));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (TopMember == "Main")
            {
                TopMember = Session["Username"].ToString();
            }

            try
            {
                MarketTreeModel memberMarketTree = MPGetAllMarketTreeMember(TopMember);



                int ok = 0;
                string msg = "";
                var member = MemberDB.GetMemberByUsername(TopMember, out ok, out msg);
                memberMarketTree.country = member.Tables[0].Rows[0]["CCOUNTRY_ID"].ToString();
                var Stockist = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
                memberMarketTree.Stockist = Stockist.Tables[0].Rows[0]["CMEM_MEMBER_TYPE"].ToString();
                if (Session["LanguageChosen"].ToString().Equals("zh-cn", StringComparison.InvariantCultureIgnoreCase))
                {
                    memberMarketTree.Picture = "RegisterEn.png";
                }
                else if (Session["LanguageChosen"].ToString().Equals("en-US", StringComparison.InvariantCultureIgnoreCase))
                {
                    memberMarketTree.Picture = "RegisterEn.png";
                }
                else if (Session["LanguageChosen"].ToString().Equals("zh-tw", StringComparison.InvariantCultureIgnoreCase))
                {
                    memberMarketTree.Picture = "RegisterEn.png";
                }
                else if (Session["LanguageChosen"].ToString().Equals("zh-JP", StringComparison.InvariantCultureIgnoreCase))
                {
                    memberMarketTree.Picture = "RegisterEn.png";
                }


                DataSet logo = AdminGeneralDB.GetRankIcon();
                foreach (DataRow dr2 in logo.Tables[0].Rows)
                {
                    var IFile = new ImageFile();
                    IFile.FileName = dr2["CRANKSET_ICON"].ToString();
                    RankModel RankList = new RankModel();
                    RankList.RankIconPath = dr2["CRANKSET_ICON"].ToString();
                    //RankList.RankIconName = dr2["AMOUNT"].ToString();
                    //RankList.RankAmount = float.Parse(dr2["AMOUNT"].ToString());
                    RankList.RankName = dr2["CRANKSET_NAME"].ToString();
                    ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

                    memberMarketTree.RankList.Add(RankList);
                }      


                return PartialView("MPMarketTree", memberMarketTree);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult MPFindMemberTree(string Username)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                int isBelongTo;

                MemberDB.MemberIsInMarketTreeByUsername(Username, Session["Username"].ToString(), out isBelongTo);



                MarketTreeModel memberMarketTree;

                if (isBelongTo == 0)//not belong to
                {
                    Response.Write(string.Format(Resources.OneForAll.OneForAll.msgNotInTree, Username));
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    memberMarketTree = MPGetAllMarketTreeMember(Username);

                    int ok = 0;
                    string msg = "";
                    var member = MemberDB.GetMemberByUsername(Username, out ok, out msg);
                    memberMarketTree.country = member.Tables[0].Rows[0]["CCOUNTRY_ID"].ToString();
                    var Stockist = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
                    memberMarketTree.Stockist = Stockist.Tables[0].Rows[0]["CMEM_MEMBER_TYPE"].ToString();
                    if (Session["LanguageChosen"].ToString().Equals("zh-cn", StringComparison.InvariantCultureIgnoreCase))
                    {
                        memberMarketTree.Picture = "RegisterEn.png";
                    }
                    else if (Session["LanguageChosen"].ToString().Equals("en-US", StringComparison.InvariantCultureIgnoreCase))
                    {
                        memberMarketTree.Picture = "RegisterEn.png";
                    }
                    else if (Session["LanguageChosen"].ToString().Equals("zh-tw", StringComparison.InvariantCultureIgnoreCase))
                    {
                        memberMarketTree.Picture = "RegisterEn.png";
                    }
                    else if (Session["LanguageChosen"].ToString().Equals("zh-JP", StringComparison.InvariantCultureIgnoreCase))
                    {
                        memberMarketTree.Picture = "RegisterEn.png";
                    }

                    DataSet logo = AdminGeneralDB.GetRankIcon();
                    foreach (DataRow dr2 in logo.Tables[0].Rows)
                    {
                        var IFile = new ImageFile();
                        IFile.FileName = dr2["CRANKSET_ICON"].ToString();
                        RankModel RankList = new RankModel();
                        RankList.RankIconPath = dr2["CRANKSET_ICON"].ToString();
                        //RankList.RankIconName = dr2["AMOUNT"].ToString();
                        RankList.RankAmount = float.Parse(dr2["AMOUNT"].ToString());
                        ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

                        memberMarketTree.RankList.Add(RankList);
                    }

                }

                return PartialView("MPMarketTree", memberMarketTree);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult MPGoUpOneMember(string Username)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                DataSet dsFollow = MemberDB.GetMarketTreeByUsername(Username);
                DataRow dr = dsFollow.Tables[0].Rows[0];
                string upMember = dr["CMTREE_UPMEMBER"].ToString();

                if (upMember == "0" || string.Equals(Username.Replace(" ", ""), Session["Username"].ToString(), StringComparison.CurrentCultureIgnoreCase))
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgTopMost);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return MPMarketTreeByUsername(upMember);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        //Done 100% DB side
        #region RegisterNewMember

        #region First Page - Upline & Sponser
        public ActionResult RegisterNewMemberPage1(string upMember = "", int position = 0, bool startFresh = true, string MBintro = "")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                if (Session["CountryCode"] == null || Session["CountryCode"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

                int ok = 0;
                string msg = "";



                if (startFresh)
                {
                    Session["regMember"] = new RegisterNewMemberModel();

                    ((RegisterNewMemberModel)Session["regMember"]).UplineID = upMember;

                    var ds = MemberDB.GetMemberByUsername(((RegisterNewMemberModel)Session["regMember"]).UplineID, out ok, out msg);
                    DataRow rows = ds.Tables[0].Rows[0];
                    string upuser = rows["CUSR_USERNAME"].ToString();
                    string Language = Session["LanguageChosen"].ToString();

                    ((RegisterNewMemberModel)Session["regMember"]).Member.Upline = upuser;

                    ((RegisterNewMemberModel)Session["regMember"]).UpLineFullName = rows["CUSR_FULLNAME"].ToString();

                    ((RegisterNewMemberModel)Session["regMember"]).LanguageCode = Language;

                    #region Location

                    SelectListItem loc = new SelectListItem();
                    loc.Selected = (position == 0);
                    loc.Text = ECFBase.Resources.OneForAll.OneForAll.lblLeft;
                    loc.Value = "0";
                    ((RegisterNewMemberModel)Session["regMember"]).Locations.Add(loc);
                    loc = new SelectListItem();
                    loc.Selected = (position == 1);
                    loc.Text = ECFBase.Resources.OneForAll.OneForAll.lblRight;
                    loc.Value = "1";
                    ((RegisterNewMemberModel)Session["regMember"]).Locations.Add(loc);

                    ((RegisterNewMemberModel)Session["regMember"]).SelectedLocation = position.ToString();

                    if (loc.Selected = (position == 0))
                    {
                        ((RegisterNewMemberModel)Session["regMember"]).SelectedLocationString = ECFBase.Resources.OneForAll.OneForAll.lblLeft;
                    }
                    else if (loc.Selected = (position == 1))
                    {
                        ((RegisterNewMemberModel)Session["regMember"]).SelectedLocationString = ECFBase.Resources.OneForAll.OneForAll.lblRight;
                    }

                    #endregion

                }
                else if (!startFresh)
                {
                    int Result = 0;

                    DataSet dsUpline = MemberShopCenterDB.SponsorUplineCheckingForRegister(Session["Username"].ToString(), MBintro, out Result);

                    if (Result == 0)
                    {
                        Response.Write(Resources.OneForAll.OneForAll.warninginvalidsponsoridtryagain);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                    ((RegisterNewMemberModel)Session["regMember"]).Member.Intro = MBintro;

                    var CheckRank = MemberDB.GetMemberByUsername(MBintro, out ok, out msg);

                    if (CheckRank.Tables[0].Rows.Count == 0)
                    {
                        Response.Write(Resources.OneForAll.OneForAll.warningSponsorNameNotExist);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                    ((RegisterNewMemberModel)Session["regMember"]).Member.IntroFullName = CheckRank.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();

                }

                return PartialView("RegisterNewMemberPage1", ((RegisterNewMemberModel)Session["regMember"]));

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        //Btn Next        
        public ActionResult RegisterNewMemberPage1Method(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                int Result = 0;

                DataSet Upline = MemberShopCenterDB.SponsorUplineCheckingForRegister(Session["Username"].ToString(), mem.Member.Intro, out Result);

                if (Result == 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warninginvalidsponsoridtryagain);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (Result == -1)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgCantMoreThanFiveUpline);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                ((RegisterNewMemberModel)Session["regMember"]).Member.Intro = mem.Member.Intro;

                DataSet dsSponsor = LoginDB.GetUserByUserName(mem.Member.Intro);
                if (dsSponsor.Tables[0].Rows.Count == 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningInvalidSponsorID + mem.Member.Intro);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                DataSet dsUpline = LoginDB.GetUserByUserName(mem.Member.Upline);
                string existLeft = dsUpline.Tables[0].Rows[0]["CMTREE_FOLLOWGROUPA"].ToString();
                string existRight = dsUpline.Tables[0].Rows[0]["CMTREE_FOLLOWGROUPB"].ToString();

                if (mem.SelectedLocation == "0")
                {
                    if (existLeft != "0")
                    {
                        Response.Write(Resources.OneForAll.OneForAll.msgLeftOccupied);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    if (existRight != "0")
                    {
                        Response.Write(Resources.OneForAll.OneForAll.msgRightOccupied);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                ((RegisterNewMemberModel)Session["regMember"]).Member.Upline = mem.Member.Upline;
                ((RegisterNewMemberModel)Session["regMember"]).Member.Intro = mem.Member.Intro;
                ((RegisterNewMemberModel)Session["regMember"]).SelectedLocation = mem.SelectedLocation;


                return RegisterNewMemberPage2();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Second Page - Option & Package
        //Load Page
        public ActionResult RegisterNewMemberPage2(string SelectedCountry = "MY")
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            int ok = 0;
            string msg = "";
            List<CountrySetupModel> countries = Misc.GetAllCountryList(Session["LanguageChosen"].ToString(), ref ok, ref msg);

            ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry = SelectedCountry;


            #region Package
            DataSet dsPack = MemberShopCenterDB.GetPackageAndDescription(((RegisterNewMemberModel)Session["regMember"]).LanguageCode, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);

            if (Session["Username"].ToString().ToLower() == "wma111")
            {
                dsPack = MemberShopCenterDB.GetPackageAndDescriptionZeroBV(((RegisterNewMemberModel)Session["regMember"]).LanguageCode, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);
            }

            ((RegisterNewMemberModel)Session["regMember"]).Packages.Clear();

            foreach (DataRow dr in dsPack.Tables[0].Rows)
            {
                if (dr["CPKG_TYPE"].ToString() == "R")
                {
                    if (Convert.ToInt32(dr["CPKG_ACCQTY"].ToString()) == 1)
                    {

                        PackageModel package = new PackageModel();
                        package.PackageID = int.Parse(dr["CPKG_ID"].ToString());
                        package.PackageCode = dr["CPKG_CODE"].ToString();
                        package.PackageDescription = dr["CMULTILANGPACKAGE_DESCRIPTION"] != null ? dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString() : string.Empty;
                        int extensionIndex = dr["CPRG_IMAGEPATH"].ToString().LastIndexOf('\\');
                        string imageName = dr["CPRG_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
                        package.Image = "http://" + Request.Url.Authority + "/Packages/" + package.PackageCode + "/" + imageName;
                        package.TotalAmount = float.Parse(dr["CPKG_AMOUNT"].ToString()).ToString("n2");
                        package.BV = float.Parse(dr["CPKG_BV"].ToString()).ToString("n2");
                        package.EPoint = float.Parse(dr["CPKG_EUNIT"].ToString()).ToString("n2");
                        package.PackageName = dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString();
                        ((RegisterNewMemberModel)Session["regMember"]).Countries = from c in countries
                                                                                   select new SelectListItem
                                                                                   {
                                                                                       Text = c.CountryName,
                                                                                       Value = c.CountryCode
                                                                                   };
                        package.Country = countries.Find(item => item.CountryCode == dr["CCOUNTRY_CODE"].ToString()).CountryName;
                        int extensionIndexs = Misc.GetCountryImageByCountryCode(dr["CCOUNTRY_CODE"].ToString()).LastIndexOf('\\');
                        string imageNameCountry = Misc.GetCountryImageByCountryCode(dr["CCOUNTRY_CODE"].ToString()).Substring(extensionIndexs + 1);
                        package.CountryImage = "http://" + Request.Url.Authority + "/Country/" + dr["CCOUNTRY_CODE"].ToString() + "/" + imageNameCountry;


                        ((RegisterNewMemberModel)Session["regMember"]).Packages.Add(package);
                    }
                }
            }

            if (((RegisterNewMemberModel)Session["regMember"]).Packages.Count == 0)
            {
                ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = false;
            }
            else
            {
                ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = true;
            }
            #endregion

            #region Country
            //combobox for country
            ((RegisterNewMemberModel)Session["regMember"]).Countries = from c in countries
                                                                       select new SelectListItem
                                                                       {
                                                                           Text = c.CountryName,
                                                                           Value = c.CountryCode
                                                                       };
            if (((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode == null)
            {
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode = countries[0].MobileCode;
            }
            #endregion


            return PartialView("RegisterNewMemberPage2", ((RegisterNewMemberModel)Session["regMember"]));
        }

        public ActionResult RegisterNewMemberPage2Method(RegisterNewMemberModel mem)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            if (mem.SelectedPackage == null)
            {
                Response.Write(Resources.OneForAll.OneForAll.warningPlsChooseAPackage);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            #region Package

            DataSet dsPack = MemberShopCenterDB.GetPackageAndDescription(((RegisterNewMemberModel)Session["regMember"]).LanguageCode, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);

            if (Session["Username"].ToString().ToLower() == "wma111")
            {
                dsPack = MemberShopCenterDB.GetPackageAndDescriptionZeroBV(((RegisterNewMemberModel)Session["regMember"]).LanguageCode, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);
            }

            ((RegisterNewMemberModel)Session["regMember"]).Packages.Clear();

            foreach (DataRow dr in dsPack.Tables[0].Rows)
            {
                if (dr["CPKG_CODE"].ToString() == mem.SelectedPackage)
                {
                    ((RegisterNewMemberModel)Session["regMember"]).TotalRD = float.Parse(dr["CPKG_AMOUNT"].ToString()).ToString("n2");
                    ((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount = dr["CPKG_AMOUNT"].ToString();
                    ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageAmount = float.Parse(dr["CPKG_AMOUNT"].ToString()).ToString("n2");
                    ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageName = dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString();

                }
            }

            #endregion

            ((RegisterNewMemberModel)Session["regMember"]).SelectedPackage = mem.SelectedPackage;
            ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry = mem.SelectedCountry;


            return RegisterNewMemberPage3();
        }

        public ActionResult BackToRegisterNewMemberPage1()
        {
            return RegisterNewMemberPage1(((RegisterNewMemberModel)Session["regMember"]).Member.Upline, Convert.ToInt32(((RegisterNewMemberModel)Session["regMember"]).SelectedLocation), false, ((RegisterNewMemberModel)Session["regMember"]).Member.Intro);
        }

        #endregion

        #region Third Page - Payment Method
        public ActionResult RegisterNewMemberPage3()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                if (Session["CountryCode"] == null || Session["CountryCode"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }


                int ok = 0;
                string msg = "";
                string languageCode = Session["LanguageChosen"].ToString();
                var dataset = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
                DataRow row = dataset.Tables[0].Rows[0];
                string user = row["CUSR_USERNAME"].ToString();

                ((RegisterNewMemberModel)Session["regMember"]).Member.RegisterWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
                ((RegisterNewMemberModel)Session["regMember"]).Member.RMPWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_RMPWALLET"].ToString());
                ((RegisterNewMemberModel)Session["regMember"]).Member.CRPWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_COMPANYWALLET"].ToString());
                float PackageAmount = 0;

                DataSet dsPack = MemberShopCenterDB.GetPackageAndDescription(Session["LanguageChosen"].ToString(), ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);

                if (Session["Username"].ToString().ToLower() == "wma111")
                {
                    dsPack = MemberShopCenterDB.GetPackageAndDescriptionZeroBV(((RegisterNewMemberModel)Session["regMember"]).LanguageCode, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);
                }

                foreach (DataRow dr in dsPack.Tables[0].Rows)
                {
                    if (dr["CPKG_TYPE"].ToString() == "R")
                    {
                        if (((RegisterNewMemberModel)Session["regMember"]).SelectedPackage == dr["CPKG_CODE"].ToString())
                        {
                            PackageAmount = float.Parse(dr["CPKG_AMOUNT"].ToString());
                        }

                    }
                }

                if (string.IsNullOrEmpty(((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterPaymentWallet))
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterPaymentWallet = (PackageAmount * 30 / 100).ToString("n2");
                    ((RegisterNewMemberModel)Session["regMember"]).Member.SCompanyPaymentWallet = (PackageAmount * 40 / 100).ToString("n2");
                    ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterMPPaymentWallet = (PackageAmount * 30 / 100).ToString("n2");
                }

                return PartialView("RegisterNewMemberPage3", ((RegisterNewMemberModel)Session["regMember"]));

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        //Go To Next Page        
        public ActionResult RegisterNewMemberPage3Method(string RegisterPaymentWallet = "", string CompanyPaymentWallet = "" , string RegisterMPPaymentWallet = "")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                if (string.IsNullOrEmpty(RegisterPaymentWallet))
                {
                    RegisterPaymentWallet = "0";
                }
                if (string.IsNullOrEmpty(CompanyPaymentWallet))
                {
                    CompanyPaymentWallet = "0";
                }
                if (string.IsNullOrEmpty(RegisterMPPaymentWallet))
                {
                    RegisterMPPaymentWallet = "0";
                }
                //Other case, should not have empty string PackageAmount [So I remain this as this is a safeguard if somehow the PackageAmount is empty string]
                //Disable the whole if code when this is no longer needed
                if (Session["Username"].ToString().ToLower() == "wma111" && ((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount == null)
                {
                    ((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount = "0";
                }

                float PackageAmount = float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount);

                if ((float.Parse(RegisterPaymentWallet) + float.Parse(CompanyPaymentWallet) + float.Parse(RegisterMPPaymentWallet)) > PackageAmount)
                {
                    Response.Write("(CRP + RP + RMP) Cant Be More Than Package Amount");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if ((float.Parse(RegisterPaymentWallet) + float.Parse(CompanyPaymentWallet) + float.Parse(RegisterMPPaymentWallet)) < PackageAmount)
                {
                    Response.Write("(CRP + RP + RMP) Cant Be Less Than Package Amount");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok = 0;
                string msg = string.Empty;

                var ds = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                float RP = float.Parse(ds.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
                float RMP = float.Parse(ds.Tables[0].Rows[0]["CMEM_RMPWALLET"].ToString());
                float CRP = float.Parse(ds.Tables[0].Rows[0]["CMEM_COMPANYWALLET"].ToString());

                float CRPMin = float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount) * 40 / 100;

                //float RMPMax = float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount) * 20 / 100;

                //if (CRPMin > float.Parse(CompanyPaymentWallet))
                //{
                //    Response.Write("Company Register Point (CRP) Cant Lower Than 40% ");
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                //if (float.Parse(RegisterMPPaymentWallet) > RMPMax)
                //{
                //    Response.Write("Register MP (RMP) Cant More Than 20% ");
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                if (float.Parse(CompanyPaymentWallet) > CRP)
                {
                    Response.Write("Company Register Point (CRP) Insufficient ");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (float.Parse(RegisterPaymentWallet) > RP)
                {
                    Response.Write("Register Point (RP) Insufficient ");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (float.Parse(RegisterMPPaymentWallet) > RMP)
                {
                    Response.Write("Register MP (RMP) Insufficient");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                ((RegisterNewMemberModel)Session["regMember"]).Member.RP = RegisterPaymentWallet;
                ((RegisterNewMemberModel)Session["regMember"]).Member.CRP = CompanyPaymentWallet;
                ((RegisterNewMemberModel)Session["regMember"]).Member.RMP = RegisterMPPaymentWallet;
                ((RegisterNewMemberModel)Session["regMember"]).Member.SCompanyPaymentWallet = float.Parse(CompanyPaymentWallet).ToString("n2");
                ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterMPPaymentWallet = float.Parse(RegisterMPPaymentWallet).ToString("n2");
                ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterPaymentWallet = float.Parse(RegisterPaymentWallet).ToString("n2");
                
                return RegisterNewMemberPage4();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult BackToRegisterNewMemberPage2(RegisterNewMemberModel mem)
        {
            try
            {

                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }




                return RegisterNewMemberPage2(((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Fouth Page - Personal Detail

        //Load Page
        public ActionResult RegisterNewMemberPage4()
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            if (Session["CountryCode"] == null || Session["CountryCode"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
            }

            try
            {
                int ok = 0;
                string msg = "";
                string languageCode = Session["LanguageChosen"].ToString();

                #region Country
                List<CountrySetupModel> countries = Misc.GetAllMobileCountryList(languageCode, ref ok, ref msg);

                ((RegisterNewMemberModel)Session["regMember"]).MobileCountries = from c in countries
                                                                                 select new SelectListItem
                                                                                 {
                                                                                     Text = c.CountryName + "(" + c.MobileCode + ")",
                                                                                     Value = c.CountryCode
                                                                                 };

                string selectedcountry = ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry;


                ((RegisterNewMemberModel)Session["regMember"]).SelectedCountryString = countries.Find(item => item.CountryCode == selectedcountry).CountryName;
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode = selectedcountry;



                #endregion




                return PartialView("RegisterNewMemberPage4", ((RegisterNewMemberModel)Session["regMember"]));
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        //Btn Next
        public ActionResult RegisterNewMemberPage4Method(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }


                if (mem.LoginUsername == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.WarningUsernameSixChar);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (mem.LoginUsername == "")
                {
                    Response.Write(Resources.OneForAll.OneForAll.WarningUsernameSixChar);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.LoginUsername.Length < 6)
                {
                    Response.Write(Resources.OneForAll.OneForAll.WarningUsernameSixChar);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.LoginUsername.Contains(" "))
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningNotAllowSpacing);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.LoginUsername.Contains(" "))
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningNotAllowSpacing);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(mem.UserInfo.IC))
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningPlsKeyInIC);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.UserInfo.IC.Count() == 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningPlsKeyInIC);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.UserInfo.IC.Count() < 5)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningICMoreThan5Alphanumeric);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(mem.UserInfo.CellPhone))
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningPleaseKeyInPhoneNumber);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.UserInfo.MobileCountryCode == Resources.OneForAll.OneForAll.dropboxSelect)
                {
                    Response.Write("Please Select Mobile Country");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(mem.Member.MemberEmail))
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgEmailInvalid);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                Regex reg = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

                if (reg.IsMatch(mem.Member.MemberEmail))
                {

                }
                else
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgEmailInvalid);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                //Regex Chinesereg = new Regex(@"\p{IsCJKUnifiedIdeographs}");

                //if (reg.IsMatch(mem.LoginUsername))
                //{
                //    Response.Write("No chinese characters allow");
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                //string word = mem.LoginUsername;
                //char fo = word[0];
                //UnicodeCategory cat = char.GetUnicodeCategory(fo);
                //if (cat == UnicodeCategory.OtherLetter)
                //{
                //    Response.Write("No chinese characters allow");
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                int ok = 0;
                string msg = "";

                var dr = AdminMemberDB.GetMemberByUserName(mem.LoginUsername, out ok, out msg);

                if (dr.Tables[0].Rows.Count != 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningMemberIDExisted);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                ((RegisterNewMemberModel)Session["regMember"]).LoginUsername = mem.LoginUsername.ToUpper();
                ((RegisterNewMemberModel)Session["regMember"]).Member.FirstName = mem.Member.FirstName.ToUpper();
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.IC = mem.UserInfo.IC;
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.Nickname = mem.Member.FirstName.ToUpper();
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode = mem.UserInfo.MobileCountryCode;
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.CellPhone = mem.UserInfo.CellPhone;
                ((RegisterNewMemberModel)Session["regMember"]).Member.MemberEmail = mem.Member.MemberEmail;

                return RegisterNewMemberPage5();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        //Back Button for Page 3
        public ActionResult BackToRegisterNewMemberPage3(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                if (string.IsNullOrEmpty(mem.LoginUsername))
                {
                    ((RegisterNewMemberModel)Session["regMember"]).LoginUsername = "";
                }
                else
                {
                    ((RegisterNewMemberModel)Session["regMember"]).LoginUsername = mem.LoginUsername.ToUpper();
                }

                if (string.IsNullOrEmpty(mem.Member.FirstName))
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Member.FirstName = "";
                }
                else
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Member.FirstName = mem.Member.FirstName.ToUpper();
                }

                if (string.IsNullOrEmpty(mem.UserInfo.IC))
                {
                    ((RegisterNewMemberModel)Session["regMember"]).UserInfo.IC = "";
                }
                else
                {
                    ((RegisterNewMemberModel)Session["regMember"]).UserInfo.IC = mem.UserInfo.IC;
                }

                if (string.IsNullOrEmpty(mem.UserInfo.CellPhone))
                {
                    ((RegisterNewMemberModel)Session["regMember"]).UserInfo.CellPhone = "";
                }
                else
                {
                    ((RegisterNewMemberModel)Session["regMember"]).UserInfo.CellPhone = mem.UserInfo.CellPhone;
                }

                if (string.IsNullOrEmpty(mem.Member.MemberEmail))
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Member.MemberEmail = "";
                }
                else
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Member.MemberEmail = mem.Member.MemberEmail;
                }

                int ok = 0;
                string msg = "";

                var dataset = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
                ((RegisterNewMemberModel)Session["regMember"]).Member.BonusWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());




                return PartialView("RegisterNewMemberPage3", ((RegisterNewMemberModel)Session["regMember"]));

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Fith Page - Review

        public ActionResult RegisterNewMemberPage5()
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            if (Session["CountryCode"] == null || Session["CountryCode"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
            }

            try
            {

                string languageCode = Session["LanguageChosen"].ToString();

                var db = MemberShopCenterDB.CheckPackage(((RegisterNewMemberModel)Session["regMember"]).SelectedPackage);

                DataSet dsPack = MemberShopCenterDB.GetPackageAndDescription(languageCode, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);

                if (Session["Username"].ToString().ToLower() == "wma111")
                {
                    dsPack = MemberShopCenterDB.GetPackageAndDescriptionZeroBV(((RegisterNewMemberModel)Session["regMember"]).LanguageCode, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);
                }

                foreach (DataRow dr in dsPack.Tables[0].Rows)
                {
                    if (dr["CPKG_TYPE"].ToString() == "R")
                    {
                        if (Convert.ToInt32(dr["CPKG_ACCQTY"].ToString()) == 1)
                        {

                            if ((dr["CPKG_CODE"].ToString() == ((RegisterNewMemberModel)Session["regMember"]).SelectedPackage))
                            {

                                ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageAmount = (float.Parse(dr["CPKG_AMOUNT"].ToString())).ToString("n2");
                                ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageBV = (float.Parse(dr["CPKG_BV"].ToString())).ToString("n2");
                                ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageEPoint = (float.Parse(dr["CPKG_EUNIT"].ToString())).ToString("n2");



                            }
                        }
                    }
                }

                string Name = string.Empty;

                string Quantity = db.Tables[0].Rows[0]["CPKG_ACCQTY"].ToString();

                if (Quantity != "1")
                {
                    int Quan = Convert.ToInt32(Quantity);

                    for (int A = 0; A < Quan; A++)
                    {
                        int B = A + 1;
                        Name += ((RegisterNewMemberModel)Session["regMember"]).LoginUsername + "0" + B + ",";
                        if (A == 3)
                        {
                            Name += System.Environment.NewLine;
                        }
                    }


                }
                else
                {
                    Name = ((RegisterNewMemberModel)Session["regMember"]).LoginUsername;
                }

                ((RegisterNewMemberModel)Session["regMember"]).DisplayName = Name;


                return PartialView("RegisterNewMemberPage5", ((RegisterNewMemberModel)Session["regMember"]));
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RegisterNewMemberPage5Method(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                int ok = 0;
                string msg = "";

                var dataset = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
                DataRow row = dataset.Tables[0].Rows[0];
                string refername = row["CUSR_USERNAME"].ToString();

                ((RegisterNewMemberModel)Session["regMember"]).ReferralUsername = Session["Username"].ToString();
                ((RegisterNewMemberModel)Session["regMember"]).SponsorSecurityPin = mem.SponsorSecurityPin;

                //store existing value before the value get clear out after registration done, important for send sms                
                string selectedCountry = ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry.ToLower();
                string nickname = ((RegisterNewMemberModel)Session["regMember"]).UserInfo.Nickname;
                string username = ((RegisterNewMemberModel)Session["regMember"]).LoginUsername;

                // some logic to validate phone numbers
                string cellPhone = CellPhoneParser.Parse(selectedCountry, ((RegisterNewMemberModel)Session["regMember"]).UserInfo.CellPhone);

                //Encrypt pin and password
                string ic = ((RegisterNewMemberModel)Session["regMember"]).UserInfo.IC;
                string password = ic.Substring(ic.Length - 6);
                ((RegisterNewMemberModel)Session["regMember"]).Member.EncryptedPassword = Authentication.Encrypt(password);
                ((RegisterNewMemberModel)Session["regMember"]).Member.EncryptedPin = Authentication.Encrypt(password);
                ((RegisterNewMemberModel)Session["regMember"]).EncryptedSponsorSecurityPin = Authentication.Encrypt(((RegisterNewMemberModel)Session["regMember"]).SponsorSecurityPin);
                string invoiceNumber = "";


                MemberShopCenterDB.RegisterMember(((RegisterNewMemberModel)Session["regMember"]), out ok, out msg, out invoiceNumber);

                if (ok == 1) //success
                {

                    if (!string.IsNullOrEmpty(cellPhone))
                    {
                        string PackageCode = ((RegisterNewMemberModel)Session["regMember"]).SelectedPackage;

                        var db = MemberShopCenterDB.CheckPackage(PackageCode);
                        string Name = string.Empty;
                        string Quantity = db.Tables[0].Rows[0]["CPKG_ACCQTY"].ToString();
                        if (Quantity == "3")
                        {
                            Name = ((RegisterNewMemberModel)Session["regMember"]).LoginUsername + "01" + " TO " + ((RegisterNewMemberModel)Session["regMember"]).LoginUsername + "03";
                        }
                        else if (Quantity == "7")
                        {
                            Name = ((RegisterNewMemberModel)Session["regMember"]).LoginUsername + "01" + " TO " + ((RegisterNewMemberModel)Session["regMember"]).LoginUsername + "07";
                        }
                        else
                        {
                            Name = ((RegisterNewMemberModel)Session["regMember"]).LoginUsername;
                        }

                        try
                        {
                            string path = string.Empty;
                            DataSet logo = AdminGeneralDB.GetCompanySetup();
                            foreach (DataRow dr in logo.Tables[0].Rows)
                            {
                                var IFile = new ImageFile();
                                IFile.FileName = dr["COM_IMAGEPATH"].ToString();

                                string TempPath = logo.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString().Replace("../..", "~");

                                path = Server.MapPath(TempPath);
                            }



                            string massege = string.Empty;
                            string Title = string.Empty;


                            Title = "Welcome to BVI.ASIA";
                            massege = "<img height=\"200\" width=\"300\" src=cid:myImageID /><br/><br/>{3},{4}<br/><br/>{0}<br/><br/>Congratulations and welcome to BVI.asia !<br/>We will do our best toward all the success, Let us work together to create wealth.<br/><br/>恭喜你已经成功注册成为BVI.asia平台的会员！BVI.asia平台欢迎大家的参与，并致力于大家一起创造财富！迎接辉煌！<br/><br/> 会员账号 Member ID         :{1} <br/>登录密码 Login Password ：{2} <br/><br/>Please login to www.bvi.asia and change your login password and security PIN to prevent unauthorized access.<br/><br/>请登录  www.bvi.asia 会员后台后，更改您的登录密码和安全密码，以防被人盗用。";


                            var contentSMS = string.Format(massege, nickname, Name, password, Name, ((RegisterNewMemberModel)Session["regMember"]).Member.FirstName);

                            string result = Misc.SendEmailGood("noreply@bvi.com", ((RegisterNewMemberModel)Session["regMember"]).Member.MemberEmail, Title, contentSMS, path);




                        }
                        catch (Exception aa)
                        {
                            // failed to send sms is ok, but better dont block the registration flow
                        }
                    }

                    string MsgRsuult = Resources.OneForAll.OneForAll.msgRegMemberSucceed1 + "\n" + username + " " + nickname + "\n" + Resources.OneForAll.OneForAll.msgRegMemberSucceed2;



                    return Json(MsgRsuult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (msg == "-1")
                        Response.Write(Resources.OneForAll.OneForAll.msgMemberExist);
                    else if (msg == "-2")
                        Response.Write(Resources.OneForAll.OneForAll.msgPackageNotExists);
                    else if (msg == "-3")
                        Response.Write(Resources.OneForAll.OneForAll.msgRWalletInsufficient);
                    else if (msg == "-4")
                        Response.Write(Resources.OneForAll.OneForAll.msgIntroNotExist);
                    else if (msg == "-5")
                        Response.Write(Resources.OneForAll.OneForAll.msgPINInvalid);
                    else if (msg == "-6")
                        Response.Write(Resources.OneForAll.OneForAll.msgUpNotExist);
                    else if (msg == "-7")
                        Response.Write(Resources.OneForAll.OneForAll.msgLeftOccupied);
                    else if (msg == "-8")
                        Response.Write(Resources.OneForAll.OneForAll.msgRightOccupied);
                    else if (msg == "-9")
                        Response.Write(Resources.OneForAll.OneForAll.msgPositionIncorrect);

                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        //Btn Back from fourth page
        public ActionResult BackToRegisterNewMemberPage4(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                return RegisterNewMemberPage4();

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #endregion

        //Done 100%
        #region Upgrade Member

        #region First Page - Ranking
        public ActionResult CheckUser(string Username = "")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                int isBelongTo;
                MemberDB.MemberIsInMarketTreeByUsername(Username, Session["Username"].ToString(), out isBelongTo);

                if (isBelongTo == 0)//not belong to
                {
                    Response.Write(Resources.OneForAll.OneForAll.WarningInvalidUseridPlsTryAgain);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok = 0;
                string msg = string.Empty;
                string CurrentPackage = string.Empty;

                var db = MemberDB.GetMemberByUsername(Username, out ok, out msg);
                var ds = MemberDB.CheckRanking();

                CurrentPackage = db.Tables[0].Rows[0]["CPKG_CODE"].ToString();

                DataSet DBSS = MemberShopCenterDB.GetCurrentPackageDetail(CurrentPackage);

                if (DBSS.Tables[0].Rows[0]["CRANKSET_CODE"].ToString() == ds.Tables[0].Rows[0]["CRANKSET_CODE"].ToString())
                {
                    Response.Write(Resources.OneForAll.OneForAll.WarningHigherPackage);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                string Result = "";
                string Name = "";
                string Rank = "";
                string Country = "";

                Name = db.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                Rank = Misc.GetMemberRanking(db.Tables[0].Rows[0]["CRANK_CODE"].ToString());
                Country = db.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();

                Result = Name + "," + Rank + "," + Country;


                return Json(Result, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult UpgradeMemberPage1()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                if (Session["CountryCode"] == null || Session["CountryCode"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

                int ok = 0;
                string msg = "";
                string languageCode = "en-us";
                Session["regMember"] = new RegisterNewMemberModel();


                var dataset = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                string user = dataset.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();

                ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry = dataset.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();


                //((RegisterNewMemberModel)Session["regMember"]).PaymentOption = "1";
                //((RegisterNewMemberModel)Session["regMember"]).Member.RegisterWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
                //((RegisterNewMemberModel)Session["regMember"]).Member.RMPWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_RMPWALLET"].ToString());
                //((RegisterNewMemberModel)Session["regMember"]).Member.RegisterPaymentWallet = 100;
                //((RegisterNewMemberModel)Session["regMember"]).Member.RegisterMPPaymentWallet = 0;


                #region Country
                //combobox for country
                List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

                ((RegisterNewMemberModel)Session["regMember"]).Countries = from c in countries
                                                                           select new SelectListItem
                                                                           {
                                                                               Text = c.CountryName,
                                                                               Value = c.CountryCode
                                                                           };
                if (((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode == null)
                {
                    ((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode = countries[0].MobileCode;
                }
                #endregion



                return PartialView("UpgradeMemberPage1", ((RegisterNewMemberModel)Session["regMember"]));

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        //Go To Next Page        
        public ActionResult UpgradeMemberPage1Method(RegisterNewMemberModel mem, string Username = "")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                ((RegisterNewMemberModel)Session["regMember"]).Member.Username = Username;

                return UpgradeMemberPage2(mem);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region Second Page - Option & Package
        //Load Page
        public ActionResult UpgradeMemberPage2(RegisterNewMemberModel mem)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            string languageCode = languageCode = Session["LanguageChosen"].ToString();

            int ok = 0;
            string msg = "";
            string CurrPackage = "";
            List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);
            var ds = MemberDB.GetMemberByUsername(((RegisterNewMemberModel)Session["regMember"]).Member.Username, out ok, out msg);

            #region Package

            CurrPackage = ds.Tables[0].Rows[0]["CPKG_CODE"].ToString();

            DataSet CheckingRaking = MemberShopCenterDB.GetCurrentPackageDetail(CurrPackage);

            ((RegisterNewMemberModel)Session["regMember"]).SelectedRank = CheckingRaking.Tables[0].Rows[0]["CRANKSET_CODE"].ToString();
            ((RegisterNewMemberModel)Session["regMember"]).LanguageCode = languageCode;
            ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry = ds.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();

            string CurrentPackage = ds.Tables[0].Rows[0]["CPKG_CODE"].ToString();
            float CurrentPackagePrice = 0;
            float CurrentBV = 0;
            float CurrentEPoint = 0;

            DataSet DBSS = MemberShopCenterDB.GetCurrentPackageDetail(CurrentPackage);
            CurrentPackagePrice = float.Parse(DBSS.Tables[0].Rows[0]["CPKG_AMOUNT"].ToString());
            CurrentBV = float.Parse(DBSS.Tables[0].Rows[0]["CPKG_BV"].ToString());
            CurrentEPoint = float.Parse(DBSS.Tables[0].Rows[0]["CPKG_EUNIT"].ToString());

            DataSet dsPack = MemberShopCenterDB.GetPackageAndDescriptionForUpgrade(languageCode, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry, ((RegisterNewMemberModel)Session["regMember"]).SelectedRank);
            ((RegisterNewMemberModel)Session["regMember"]).Packages.Clear();

            foreach (DataRow dr in dsPack.Tables[0].Rows)
            {
                if (dr["CPKG_TYPE"].ToString() == "R")
                {
                    if (Convert.ToInt32(dr["CPKG_ACCQTY"].ToString()) == 1)
                    {

                        PackageModel package = new PackageModel();
                        package.PackageID = int.Parse(dr["CPKG_ID"].ToString());
                        package.PackageCode = dr["CPKG_CODE"].ToString();
                        package.PackageDescription = dr["CMULTILANGPACKAGE_DESCRIPTION"] != null ? dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString() : string.Empty;
                        int extensionIndex = dr["CPRG_IMAGEPATH"].ToString().LastIndexOf('\\');
                        string imageName = dr["CPRG_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
                        package.Image = "http://" + Request.Url.Authority + "/Packages/" + package.PackageCode + "/" + imageName;
                        package.Rp = float.Parse(dr["CPKG_AMOUNT"].ToString()).ToString("n2");
                        package.TotalAmount = (float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString("n2");
                        package.BV = (float.Parse(dr["CPKG_BV"].ToString()) - CurrentBV).ToString("n2");
                        package.EPoint = (float.Parse(dr["CPKG_EUNIT"].ToString()) - CurrentEPoint).ToString("n2");



                        package.PackageName = dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString();


                        ((RegisterNewMemberModel)Session["regMember"]).Countries = from c in countries
                                                                                   select new SelectListItem
                                                                                   {
                                                                                       Text = c.CountryName,
                                                                                       Value = c.CountryCode
                                                                                   };
                        package.Country = countries.Find(item => item.CountryCode == dr["CCOUNTRY_CODE"].ToString()).CountryName;
                        int extensionIndexs = Misc.GetCountryImageByCountryCode(dr["CCOUNTRY_CODE"].ToString()).LastIndexOf('\\');
                        string imageNameCountry = Misc.GetCountryImageByCountryCode(dr["CCOUNTRY_CODE"].ToString()).Substring(extensionIndexs + 1);
                        package.CountryImage = "http://" + Request.Url.Authority + "/Country/" + dr["CCOUNTRY_CODE"].ToString() + "/" + imageNameCountry;


                        ((RegisterNewMemberModel)Session["regMember"]).Packages.Add(package);
                    }
                }
            }
            if (dsPack.Tables[0].Rows.Count == 0)
            {
                ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = false;

            }
            else
            {
                ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = true;
            }

            if (((RegisterNewMemberModel)Session["regMember"]).Packages.Count == 0)
            {
                ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = false;
            }



            #endregion



            return PartialView("UpgradeMemberPage2", ((RegisterNewMemberModel)Session["regMember"]));
        }

        public ActionResult UpgradeMemberPage2Method(RegisterNewMemberModel mem)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            int ok = 0;
            string msg = string.Empty;
            string CurrPackage = string.Empty;

            if (string.IsNullOrEmpty(mem.SelectedPackage))
            {
                Response.Write(Resources.OneForAll.OneForAll.warningPlsChooseAPackage);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            var ds = MemberDB.GetMemberByUsername(((RegisterNewMemberModel)Session["regMember"]).Member.Username, out ok, out msg);
            ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry = ds.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();

            CurrPackage = ds.Tables[0].Rows[0]["CPKG_CODE"].ToString();
            ((RegisterNewMemberModel)Session["regMember"]).SelectedPackage = mem.SelectedPackage;

            DataSet CheckingRaking = MemberShopCenterDB.GetCurrentPackageDetail(CurrPackage);
            ((RegisterNewMemberModel)Session["regMember"]).SelectedRank = CheckingRaking.Tables[0].Rows[0]["CRANKSET_CODE"].ToString();

            string CurrentPackage = ds.Tables[0].Rows[0]["CPKG_CODE"].ToString();
            float CurrentPackagePrice = 0;


            DataSet dssPack = MemberShopCenterDB.GetPackageDetail("en-US", ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry, CurrentPackage);
            foreach (DataRow dr in dssPack.Tables[0].Rows)
            {
                if (dr["CPKG_CODE"].ToString() == CurrentPackage)
                {
                    CurrentPackagePrice = float.Parse(dr["CPKG_AMOUNT"].ToString());

                }
            }

            DataSet packageInfo = AdminGeneralDB.GetPackageByCode(((RegisterNewMemberModel)Session["regMember"]).SelectedPackage, "en-US", out ok, out msg);
            float investment = float.Parse(packageInfo.Tables[0].Rows[0]["CPKG_AMOUNT"].ToString());
            float total = investment;
            ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageName = packageInfo.Tables[0].Rows[0]["CMULTILANGPACKAGE_NAME"].ToString();
            ((RegisterNewMemberModel)Session["regMember"]).TotalRD = (float.Parse(packageInfo.Tables[0].Rows[0]["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString("n2");
            ((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount = (float.Parse(packageInfo.Tables[0].Rows[0]["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString();
            return UpgradeMemberPage3(mem);
        }

        public ActionResult BackToUpgradeMemberPage1()
        {
            return UpgradeMemberPage1();
        }

        #endregion

        #region Third Page - Payment Method
        public ActionResult UpgradeMemberPage3(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                if (Session["CountryCode"] == null || Session["CountryCode"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

                int ok = 0;
                string msg = "";
                var dataset = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                string user = dataset.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();

                ((RegisterNewMemberModel)Session["regMember"]).Member.RegisterWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
                ((RegisterNewMemberModel)Session["regMember"]).Member.RMPWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_RMPWALLET"].ToString());
                ((RegisterNewMemberModel)Session["regMember"]).Member.CRPWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_COMPANYWALLET"].ToString());

                ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterPaymentWallet = (float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount) * 30 / 100).ToString();
                ((RegisterNewMemberModel)Session["regMember"]).Member.SCompanyPaymentWallet = (float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount) * 40 / 100).ToString();
                ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterMPPaymentWallet = (float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount) * 30 / 100).ToString();


                return PartialView("UpgradeMemberPage3", ((RegisterNewMemberModel)Session["regMember"]));

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        //Go To Next Page        
        public ActionResult UpgradeMemberPage3Method(string RegisterPaymentWallet = "", string CompanyPaymentWallet = "" , string RegisterMPPaymentWallet = "")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                if (string.IsNullOrEmpty(RegisterPaymentWallet))
                {
                    RegisterPaymentWallet = "0";
                }
                if (string.IsNullOrEmpty(CompanyPaymentWallet))
                {
                    CompanyPaymentWallet = "0";
                }
                if (string.IsNullOrEmpty(RegisterMPPaymentWallet))
                {
                    RegisterMPPaymentWallet = "0";
                }

                float PackageAmount = float.Parse(((RegisterNewMemberModel)Session["regMember"]).TotalRD);

                if ((float.Parse(RegisterPaymentWallet) + float.Parse(CompanyPaymentWallet) + float.Parse(RegisterMPPaymentWallet)) > PackageAmount)
                {
                    Response.Write("(CRP + RP + RMP) Cant Be More Than Package Amount");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if ((float.Parse(RegisterPaymentWallet) + float.Parse(CompanyPaymentWallet) + float.Parse(RegisterMPPaymentWallet)) < PackageAmount)
                {
                    Response.Write("(CRP + RP + RMP) Cant Be Less Than Package Amount");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok = 0;
                string msg = string.Empty;

                var ds = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                float RP = float.Parse(ds.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
                float RMP = float.Parse(ds.Tables[0].Rows[0]["CMEM_RMPWALLET"].ToString());
                float CRP = float.Parse(ds.Tables[0].Rows[0]["CMEM_COMPANYWALLET"].ToString());

                float CRPMin = float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount) * 40 / 100;

                //float RMPMax = float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount) * 20 / 100;

                //if (CRPMin > float.Parse(CompanyPaymentWallet))
                //{
                //    Response.Write("Company Register Point (CRP) Cant Lower Than 40% ");
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                //if (float.Parse(RegisterMPPaymentWallet) > RMPMax)
                //{
                //    Response.Write("Register MP (RMP) Cant More Than 20% ");
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                if (float.Parse(CompanyPaymentWallet) > CRP)
                {
                    Response.Write("Company Register Point (CRP) Insufficient ");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (float.Parse(RegisterPaymentWallet) > RP)
                {
                    Response.Write("Register Point (RP) Insufficient ");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (float.Parse(RegisterMPPaymentWallet) > RMP)
                {
                    Response.Write("Register MP (RMP) Insufficient");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                ((RegisterNewMemberModel)Session["regMember"]).Member.RP = RegisterPaymentWallet;
                ((RegisterNewMemberModel)Session["regMember"]).Member.CRP = CompanyPaymentWallet;
                ((RegisterNewMemberModel)Session["regMember"]).Member.RMP = RegisterMPPaymentWallet;
                ((RegisterNewMemberModel)Session["regMember"]).Member.SCompanyPaymentWallet = float.Parse(CompanyPaymentWallet).ToString("n2");
                ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterMPPaymentWallet = float.Parse(RegisterMPPaymentWallet).ToString("n2");
                ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterPaymentWallet = float.Parse(RegisterPaymentWallet).ToString("n2");


                return UpgradeMemberPage4();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        //Btn Back from fourth page
        public ActionResult BackToUpgradeMemberPage2(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                return UpgradeMemberPage2(mem);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Fourth Page - Review

        public ActionResult UpgradeMemberPage4()
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            if (Session["CountryCode"] == null || Session["CountryCode"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
            }

            try
            {

                string languageCode = Session["LanguageChosen"].ToString();

                int ok = 0;
                string msg = "";

                var ds = MemberDB.GetMemberByUsername(((RegisterNewMemberModel)Session["regMember"]).Member.Username, out ok, out msg);
                var db = MemberShopCenterDB.CheckPackage(((RegisterNewMemberModel)Session["regMember"]).SelectedPackage);

                string CurrentPackage = ds.Tables[0].Rows[0]["CPKG_CODE"].ToString();
                float CurrentPackagePrice = 0;
                float CurrentBV = 0;
                float CurrentEPoint = 0;

                DataSet DBSS = MemberShopCenterDB.GetCurrentPackageDetail(CurrentPackage);
                CurrentPackagePrice = float.Parse(DBSS.Tables[0].Rows[0]["CPKG_AMOUNT"].ToString());
                CurrentBV = float.Parse(DBSS.Tables[0].Rows[0]["CPKG_BV"].ToString());
                CurrentEPoint = float.Parse(DBSS.Tables[0].Rows[0]["CPKG_EUNIT"].ToString());

                DataSet dsPack = MemberShopCenterDB.GetPackageAndDescriptionForUpgrade(languageCode, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry, ((RegisterNewMemberModel)Session["regMember"]).SelectedRank);
                foreach (DataRow dr in dsPack.Tables[0].Rows)
                {
                    if (dr["CPKG_TYPE"].ToString() == "R")
                    {
                        if (Convert.ToInt32(dr["CPKG_ACCQTY"].ToString()) == 1)
                        {

                            if ((dr["CPKG_CODE"].ToString() == ((RegisterNewMemberModel)Session["regMember"]).SelectedPackage))
                            {
                                ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageAmount = (float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString("n2");
                                ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageBV = (float.Parse(dr["CPKG_BV"].ToString()) - CurrentBV).ToString("n2");
                                ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageEPoint = (float.Parse(dr["CPKG_EUNIT"].ToString()) - CurrentEPoint).ToString("n2");



                            }
                        }
                    }
                }



                return PartialView("UpgradeMemberPage4", ((RegisterNewMemberModel)Session["regMember"]));
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UpgradeMemberPage4Method(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                int ok = 0;
                string msg = "";

                var dataset = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                string Pin = dataset.Tables[0].Rows[0]["CUSR_PIN"].ToString();

                if (mem.SponsorSecurityPin != Authentication.Decrypt(Pin))
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgPINInvalid);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                ((RegisterNewMemberModel)Session["regMember"]).LoginUsername = Session["Username"].ToString();

                MemberShopCenterDB.UpgradePackage(((RegisterNewMemberModel)Session["regMember"]));


                DataSet dsPack = MemberShopCenterDB.GetPackageAndDescription(Session["LanguageChosen"].ToString(), ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);
                string PackageName = string.Empty;
                foreach (DataRow dr in dsPack.Tables[0].Rows)
                {
                    if (dr["CPKG_TYPE"].ToString() == "R")
                    {
                        if (mem.SelectedPackage == dr["CPKG_CODE"].ToString())
                        {
                            PackageName = dr["CMULTILANGPACKAGE_NAME"].ToString();
                        }

                    }
                }

                string MsgRsuult = ((RegisterNewMemberModel)Session["regMember"]).Member.Username + "\n" + "Account Upgrade Successfully !";

                return Json(MsgRsuult, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        //Btn Back from fourth page
        public ActionResult BackToUpgradeMemberPage3(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                return UpgradeMemberPage3(mem);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #endregion

        //Done 100%
        #region MP RegisterNewMember

        #region First Page - Upline & Sponser
        public ActionResult MPRegisterNewMemberPage1(string upMember = "", int position = 0, bool startFresh = true, string Rank = "", string MBintro = "", string CountryCode = "")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                if (Session["CountryCode"] == null || Session["CountryCode"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

                if (Session["RegisterPage"] != null && Session["RegisterPage"].ToString() == "RP")
                {
                    return RegisterNewMemberPage1(upMember, position, startFresh, MBintro);
                }

                int ok = 0;
                string msg = "";
                string languageCode = languageCode = Session["LanguageChosen"].ToString();

                var dataset = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
                DataRow row = dataset.Tables[0].Rows[0];
                string user = row["CUSR_USERNAME"].ToString();

                if (startFresh)
                {
                    Session["regMember"] = new RegisterNewMemberModel();

                    #region Upline
                    if (upMember == "")
                    {
                        ((RegisterNewMemberModel)Session["regMember"]).Member.Upline = null;

                    }
                    else
                    {
                        var ds = MemberDB.GetMemberByUsername(upMember, out ok, out msg);
                        DataRow rows = ds.Tables[0].Rows[0];
                        string upuser = rows["CUSR_USERNAME"].ToString();

                        ((RegisterNewMemberModel)Session["regMember"]).Member.Upline = upuser;
                        ((RegisterNewMemberModel)Session["regMember"]).UsernameID = upMember;


                    }
                    #endregion

                    #region Location
                    SelectListItem loc = new SelectListItem();
                    loc.Selected = (position == 0);
                    loc.Text = ECFBase.Resources.OneForAll.OneForAll.lblLeft;
                    loc.Value = "0";
                    ((RegisterNewMemberModel)Session["regMember"]).Locations.Add(loc);
                    loc = new SelectListItem();
                    loc.Selected = (position == 1);
                    loc.Text = ECFBase.Resources.OneForAll.OneForAll.lblRight;
                    loc.Value = "1";
                    ((RegisterNewMemberModel)Session["regMember"]).Locations.Add(loc);

                    ((RegisterNewMemberModel)Session["regMember"]).SelectedLocation = position.ToString();

                    if (loc.Selected = (position == 0))
                    {
                        ((RegisterNewMemberModel)Session["regMember"]).SelectedLocationString = ECFBase.Resources.OneForAll.OneForAll.lblLeft;
                    }
                    else if (loc.Selected = (position == 1))
                    {
                        ((RegisterNewMemberModel)Session["regMember"]).SelectedLocationString = ECFBase.Resources.OneForAll.OneForAll.lblRight;
                    }

                    #endregion
                }
                else if (!startFresh)
                {

                    ((RegisterNewMemberModel)Session["regMember"]).Member.Intro = MBintro;



                    var CheckRank = MemberDB.GetMemberByUsername(MBintro, out ok, out msg);

                    if (CheckRank.Tables[0].Rows.Count == 0)
                    {
                        Response.Write(Resources.OneForAll.OneForAll.warningSponsorNameNotExist);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                    ((RegisterNewMemberModel)Session["regMember"]).Member.IntroFullName = CheckRank.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();



                }

                if (((RegisterNewMemberModel)Session["regMember"]).Member.Upline == "")
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Member.Upline = null;

                }
                else
                {
                    var ds = MemberDB.GetMemberByUsername(((RegisterNewMemberModel)Session["regMember"]).Member.Upline, out ok, out msg);
                    DataRow rows = ds.Tables[0].Rows[0];
                    string upuser = rows["CUSR_USERNAME"].ToString();

                    ((RegisterNewMemberModel)Session["regMember"]).Member.Upline = upuser;

                    ((RegisterNewMemberModel)Session["regMember"]).UpLineFullName = rows["CUSR_FULLNAME"].ToString();
                }

                #region Country
                //combobox for country
                List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

                ((RegisterNewMemberModel)Session["regMember"]).Countries = from c in countries
                                                                           select new SelectListItem
                                                                           {
                                                                               Text = c.CountryName,
                                                                               Value = c.CountryCode
                                                                           };
                if (((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode == null)
                {
                    ((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode = countries[0].MobileCode;
                }
                #endregion

                #region Location

                if (((RegisterNewMemberModel)Session["regMember"]).SelectedLocation == "0")
                {
                    ((RegisterNewMemberModel)Session["regMember"]).SelectedLocationString = ECFBase.Resources.OneForAll.OneForAll.lblLeft;
                }
                else if (((RegisterNewMemberModel)Session["regMember"]).SelectedLocation == "1")
                {
                    ((RegisterNewMemberModel)Session["regMember"]).SelectedLocationString = ECFBase.Resources.OneForAll.OneForAll.lblRight;
                }

                #endregion


              ((RegisterNewMemberModel)Session["regMember"]).Member.OwnAccount = Misc.GetAllOwnAccount(Session["Username"].ToString());

                return PartialView("MPRegisterNewMemberPage1", ((RegisterNewMemberModel)Session["regMember"]));

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult MPRegisterNewMemberPage1Method(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                int ok = 0;
                string msg = "";

                #region Condition
                DataSet dsUpline = LoginDB.GetUserByUserName(mem.Member.Upline);
                string existLeft = dsUpline.Tables[0].Rows[0]["CMTREE_FOLLOWGROUPA"].ToString();
                string existRight = dsUpline.Tables[0].Rows[0]["CMTREE_FOLLOWGROUPB"].ToString();


                if (mem.SelectedLocation == "0")
                {
                    if (existLeft != "0")
                    {
                        Response.Write(Resources.OneForAll.OneForAll.msgLeftOccupied);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    if (existRight != "0")
                    {
                        Response.Write(Resources.OneForAll.OneForAll.msgRightOccupied);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }


                DataSet das = LoginDB.GetUserByUserName(mem.Member.SelectedOwnAccount);
                int tablecount = das.Tables[0].Rows.Count;
                if (tablecount == 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningInvalidSponsorID + mem.Member.SelectedOwnAccount);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                DataSet dsSponsor = LoginDB.GetUserByUserName(mem.Member.SelectedOwnAccount);

                if (dsSponsor.Tables[0].Rows.Count == 0)
                {
                    Response.Write("Invalid username: " + mem.Member.SelectedOwnAccount);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Set Data To Session
                ((RegisterNewMemberModel)Session["regMember"]).UsernameID = mem.UsernameID;
                ((RegisterNewMemberModel)Session["regMember"]).introid = mem.Member.SelectedOwnAccount;
                ((RegisterNewMemberModel)Session["regMember"]).Member.Intro = mem.Member.SelectedOwnAccount;
                ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry = mem.SelectedCountry;
                ((RegisterNewMemberModel)Session["regMember"]).SelectedLocation = mem.SelectedLocation;
                #endregion

                return MPRegisterNewMemberPage2();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Second Page - Option & Package
        public ActionResult MPRegisterNewMemberPage2(bool fresh = true, string SelectedPackage = "", string Rank = "")
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            string languageCode = languageCode = Session["LanguageChosen"].ToString();

            int ok = 0;
            string msg = "";
            List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

            if (fresh)
            {
                #region Package

                ((RegisterNewMemberModel)Session["regMember"]).LanguageCode = languageCode;
                DataSet dsPack = MemberShopCenterDB.GetPackageAndDescription(languageCode, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);
                ((RegisterNewMemberModel)Session["regMember"]).Packages.Clear();
                foreach (DataRow dr in dsPack.Tables[0].Rows)
                {
                    if (dr["CPKG_TYPE"].ToString() == "R")
                    {
                        if (Convert.ToInt32(dr["CPKG_ACCQTY"].ToString()) == 1)
                        {

                            PackageModel package = new PackageModel();
                            package.PackageID = int.Parse(dr["CPKG_ID"].ToString());
                            package.PackageCode = dr["CPKG_CODE"].ToString();
                            package.PackageDescription = dr["CMULTILANGPACKAGE_DESCRIPTION"] != null ? dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString() : string.Empty;
                            int extensionIndex = dr["CPRG_IMAGEPATH"].ToString().LastIndexOf('\\');
                            string imageName = dr["CPRG_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
                            package.Image = "http://" + Request.Url.Authority + "/Packages/" + package.PackageCode + "/" + imageName;
                            package.TotalAmount = float.Parse(dr["CPKG_AMOUNT"].ToString()).ToString("n2");
                            package.BV = float.Parse(dr["CPKG_BV"].ToString()).ToString("n2");
                            package.EPoint = float.Parse(dr["CPKG_EUNIT"].ToString()).ToString("n2");

                            package.PackageName = dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString();


                            ((RegisterNewMemberModel)Session["regMember"]).Countries = from c in countries
                                                                                       select new SelectListItem
                                                                                       {
                                                                                           Text = c.CountryName,
                                                                                           Value = c.CountryCode
                                                                                       };
                            package.Country = countries.Find(item => item.CountryCode == dr["CCOUNTRY_CODE"].ToString()).CountryName;

                            int extensionIndexs = Misc.GetCountryImageByCountryCode(dr["CCOUNTRY_CODE"].ToString()).LastIndexOf('\\');
                            string imageNameCountry = Misc.GetCountryImageByCountryCode(dr["CCOUNTRY_CODE"].ToString()).Substring(extensionIndexs + 1);
                            package.CountryImage = "http://" + Request.Url.Authority + "/Country/" + dr["CCOUNTRY_CODE"].ToString() + "/" + imageNameCountry;

                            ((RegisterNewMemberModel)Session["regMember"]).Packages.Add(package);
                        }
                    }
                }
                if (dsPack.Tables[0].Rows.Count == 0)
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = false;

                }
                else
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = true;
                }

                if (((RegisterNewMemberModel)Session["regMember"]).Packages.Count == 0)
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = false;
                    //Response.Write(Resources.OneForAll.OneForAll.msgNoPackage);
                    //return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                #endregion
            }
            else
            {
                #region Package
                ((RegisterNewMemberModel)Session["regMember"]).SelectedRank = Rank;
                ((RegisterNewMemberModel)Session["regMember"]).LanguageCode = languageCode;
                DataSet dsPack = MemberShopCenterDB.GetPackageAndDescription(languageCode, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);
                ((RegisterNewMemberModel)Session["regMember"]).Packages.Clear();

                foreach (DataRow dr in dsPack.Tables[0].Rows)
                {
                    if (dr["CPKG_TYPE"].ToString() == "R")
                    {
                        if (Convert.ToInt32(dr["CPKG_ACCQTY"].ToString()) == 1)
                        {

                            PackageModel package = new PackageModel();
                            package.PackageID = int.Parse(dr["CPKG_ID"].ToString());
                            package.PackageCode = dr["CPKG_CODE"].ToString();
                            package.PackageDescription = dr["CMULTILANGPACKAGE_DESCRIPTION"] != null ? dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString() : string.Empty;
                            int extensionIndex = dr["CPRG_IMAGEPATH"].ToString().LastIndexOf('\\');
                            string imageName = dr["CPRG_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
                            package.Image = "http://" + Request.Url.Authority + "/Packages/" + package.PackageCode + "/" + imageName;
                            if (((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption == "RP")
                            {
                                package.Rp = float.Parse(dr["CPKG_AMOUNT"].ToString()).ToString("n2");
                            }
                            else if (((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption == "MP")
                            {
                                package.Mp = float.Parse(dr["CPKG_AMOUNT"].ToString()).ToString("n2");
                            }
                            else if (((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption == "RPMP")
                            {
                                package.Rp = (float.Parse(dr["CPKG_AMOUNT"].ToString()) * 50 / 100).ToString("n2");
                                package.Mp = (float.Parse(dr["CPKG_AMOUNT"].ToString()) * 50 / 100).ToString("n2");
                            }
                            else if (((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption == "CP")
                            {
                                package.Cp = float.Parse(dr["CPKG_AMOUNT"].ToString()).ToString("n2");
                            }
                            else if (((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption == "RPCP")
                            {
                                package.Rp = (float.Parse(dr["CPKG_AMOUNT"].ToString()) * 50 / 100).ToString("n2");
                                package.Cp = (float.Parse(dr["CPKG_AMOUNT"].ToString()) * 50 / 100).ToString("n2");
                            }

                            package.TotalAmount = float.Parse(dr["CPKG_AMOUNT"].ToString()).ToString("n2");
                            package.BV = float.Parse(dr["CPKG_BV"].ToString()).ToString("n2");
                            package.EPoint = float.Parse(dr["CPKG_EUNIT"].ToString()).ToString("n2");


                            package.PackageName = dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString();


                            ((RegisterNewMemberModel)Session["regMember"]).Countries = from c in countries
                                                                                       select new SelectListItem
                                                                                       {
                                                                                           Text = c.CountryName,
                                                                                           Value = c.CountryCode
                                                                                       };
                            package.Country = countries.Find(item => item.CountryCode == dr["CCOUNTRY_CODE"].ToString()).CountryName;
                            int extensionIndexs = Misc.GetCountryImageByCountryCode(dr["CCOUNTRY_CODE"].ToString()).LastIndexOf('\\');
                            string imageNameCountry = Misc.GetCountryImageByCountryCode(dr["CCOUNTRY_CODE"].ToString()).Substring(extensionIndexs + 1);
                            package.CountryImage = "http://" + Request.Url.Authority + "/Country/" + dr["CCOUNTRY_CODE"].ToString() + "/" + imageNameCountry;


                            ((RegisterNewMemberModel)Session["regMember"]).Packages.Add(package);
                        }
                    }
                }
                if (dsPack.Tables[0].Rows.Count == 0)
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = false;

                }
                else
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = true;
                }

                if (((RegisterNewMemberModel)Session["regMember"]).Packages.Count == 0)
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = false;
                    //Response.Write(Resources.OneForAll.OneForAll.msgNoPackage);
                    //return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                ((RegisterNewMemberModel)Session["regMember"]).SelectedPackage = SelectedPackage;
                #endregion
            }


            return PartialView("MPRegisterNewMemberPage2", ((RegisterNewMemberModel)Session["regMember"]));
        }

        public ActionResult MPRegisterNewMemberPage2Method(RegisterNewMemberModel mem)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            ((RegisterNewMemberModel)Session["regMember"]).SelectedPackage = mem.SelectedPackage;
            if (mem.SelectedPackage == null)
            {

                Response.Write(Resources.OneForAll.OneForAll.warningPlsChooseAPackage);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            else
            {

                DataSet dsPack = MemberShopCenterDB.GetPackageAndDescription(Session["LanguageChosen"].ToString(), ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);

                foreach (DataRow dr in dsPack.Tables[0].Rows)
                {
                    if (dr["CPKG_TYPE"].ToString() == "R")
                    {
                        if (mem.SelectedPackage == dr["CPKG_CODE"].ToString())
                        {
                            ((RegisterNewMemberModel)Session["regMember"]).SelectedRank = dr["CRANKSET_CODE"].ToString();
                            ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageAmount = float.Parse(dr["CPKG_AMOUNT"].ToString()).ToString("n2");
                            ((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount = float.Parse(dr["CPKG_AMOUNT"].ToString()).ToString();

                        }

                    }
                }
            }
            return MPRegisterNewMemberPage3();
        }

        public ActionResult MPBackToRegisterNewMemberPage1()
        {
            return MPRegisterNewMemberPage1(((RegisterNewMemberModel)Session["regMember"]).UsernameID, Convert.ToInt32(((RegisterNewMemberModel)Session["regMember"]).SelectedLocation), false, ((RegisterNewMemberModel)Session["regMember"]).SelectedRank, ((RegisterNewMemberModel)Session["regMember"]).Member.Intro, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);
        }

        #endregion

        #region Third Page - Payment Method
        public ActionResult MPRegisterNewMemberPage3()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                if (Session["CountryCode"] == null || Session["CountryCode"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }


                int ok = 0;
                string msg = "";
                string languageCode = Session["LanguageChosen"].ToString();
                var dataset = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
                DataRow row = dataset.Tables[0].Rows[0];
                string user = row["CUSR_USERNAME"].ToString();

                ((RegisterNewMemberModel)Session["regMember"]).Member.RegisterWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
                ((RegisterNewMemberModel)Session["regMember"]).Member.RMPWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_RMPWALLET"].ToString());
                ((RegisterNewMemberModel)Session["regMember"]).Member.CRPWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_COMPANYWALLET"].ToString());


                #region Country
                //combobox for country
                List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

                ((RegisterNewMemberModel)Session["regMember"]).Countries = from c in countries
                                                                           select new SelectListItem
                                                                           {
                                                                               Text = c.CountryName,
                                                                               Value = c.CountryCode
                                                                           };
                #endregion

                float PackageAmount = 0;

                DataSet dsPack = MemberShopCenterDB.GetPackageAndDescription(Session["LanguageChosen"].ToString(), ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);

                foreach (DataRow dr in dsPack.Tables[0].Rows)
                {
                    if (dr["CPKG_TYPE"].ToString() == "R")
                    {
                        if (((RegisterNewMemberModel)Session["regMember"]).SelectedPackage == dr["CPKG_CODE"].ToString())
                        {
                            PackageAmount = float.Parse(dr["CPKG_AMOUNT"].ToString());
                        }

                    }
                }

                if (string.IsNullOrEmpty(((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterPaymentWallet))
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterPaymentWallet = (PackageAmount * 30 / 100).ToString("n2");
                    ((RegisterNewMemberModel)Session["regMember"]).Member.SCompanyPaymentWallet = (PackageAmount * 40 / 100).ToString("n2");
                    ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterMPPaymentWallet = (PackageAmount * 30 / 100).ToString("n2");
                }




                return PartialView("MPRegisterNewMemberPage3", ((RegisterNewMemberModel)Session["regMember"]));

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult MPRegisterNewMemberPage3Method(string RegisterPaymentWallet = "", string CompanyPaymentWallet = "" , string RegisterMPPaymentWallet = "")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                if (string.IsNullOrEmpty(RegisterPaymentWallet))
                {
                    RegisterPaymentWallet = "0";
                }
                if (string.IsNullOrEmpty(CompanyPaymentWallet))
                {
                    CompanyPaymentWallet = "0";
                }
                if (string.IsNullOrEmpty(RegisterMPPaymentWallet))
                {
                    RegisterMPPaymentWallet = "0";
                }

                float PackageAmount = float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount);

                if ((float.Parse(RegisterPaymentWallet) + float.Parse(CompanyPaymentWallet) + float.Parse(RegisterMPPaymentWallet)) > PackageAmount)
                {
                    Response.Write("(CRP + RP + RMP) Cant Be More Than Package Amount");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if ((float.Parse(RegisterPaymentWallet) + float.Parse(CompanyPaymentWallet) + float.Parse(RegisterMPPaymentWallet)) < PackageAmount)
                {
                    Response.Write("(CRP + RP + RMP) Cant Be Less Than Package Amount");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok = 0;
                string msg = string.Empty;

                var ds = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                float RP = float.Parse(ds.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
                float RMP = float.Parse(ds.Tables[0].Rows[0]["CMEM_RMPWALLET"].ToString());
                float CRP = float.Parse(ds.Tables[0].Rows[0]["CMEM_COMPANYWALLET"].ToString());

                float CRPMin = float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount) * 40 / 100;

                //float RMPMax = float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount) * 20 / 100;

                //if (CRPMin > float.Parse(CompanyPaymentWallet))
                //{
                //    Response.Write("Company Register Point (CRP) Cant Lower Than 40% ");
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                //if (float.Parse(RegisterMPPaymentWallet) > RMPMax)
                //{
                //    Response.Write("Register MP (RMP) Cant More Than 20% ");
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                if (float.Parse(CompanyPaymentWallet) > CRP)
                {
                    Response.Write("Company Register Point (CRP) Insufficient ");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (float.Parse(RegisterPaymentWallet) > RP)
                {
                    Response.Write("Register Point (RP) Insufficient ");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (float.Parse(RegisterMPPaymentWallet) > RMP)
                {
                    Response.Write("Register MP (RMP) Insufficient");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                ((RegisterNewMemberModel)Session["regMember"]).Member.RP = RegisterPaymentWallet;
                ((RegisterNewMemberModel)Session["regMember"]).Member.CRP = CompanyPaymentWallet;
                ((RegisterNewMemberModel)Session["regMember"]).Member.RMP = RegisterMPPaymentWallet;
                ((RegisterNewMemberModel)Session["regMember"]).Member.SCompanyPaymentWallet = float.Parse(CompanyPaymentWallet).ToString("n2");
                ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterMPPaymentWallet = float.Parse(RegisterMPPaymentWallet).ToString("n2");
                ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterPaymentWallet = float.Parse(RegisterPaymentWallet).ToString("n2");




                return MPRegisterNewMemberPage4();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult MPBackToRegisterNewMemberPage2(RegisterNewMemberModel mem)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
            }


            ((RegisterNewMemberModel)Session["regMember"]).Member.FRegisterPaymentWallet = mem.Member.FRegisterPaymentWallet;
            ((RegisterNewMemberModel)Session["regMember"]).Member.FMultiPointWallet = mem.Member.FMultiPointWallet;

            return MPRegisterNewMemberPage2(false, ((RegisterNewMemberModel)Session["regMember"]).SelectedPackage, null);
        }
        #endregion

        #region FouthPage-PersonalDetail

        public ActionResult MPRegisterNewMemberPage4()
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            if (Session["CountryCode"] == null || Session["CountryCode"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
            }

            try
            {
                int ok = 0;
                string msg = "";
                string languageCode = Session["LanguageChosen"].ToString();

                #region Country
                List<CountrySetupModel> countries = Misc.GetAllMobileCountryList(languageCode, ref ok, ref msg);
                ((RegisterNewMemberModel)Session["regMember"]).MobileCountries = from c in countries
                                                                                 select new SelectListItem
                                                                                 {
                                                                                     Text = c.CountryName,
                                                                                     Value = c.CountryCode
                                                                                 };

                string selectedcountry = ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry;


                ((RegisterNewMemberModel)Session["regMember"]).SelectedCountryString = countries.Find(item => item.CountryCode == selectedcountry).CountryName;



                #endregion

                #region GetMemberInformation
                var ds = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                ((RegisterNewMemberModel)Session["regMember"]).Member.FirstName = ds.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.IC = ds.Tables[0].Rows[0]["CUSRINFO_IC"].ToString();
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.CellPhone = ds.Tables[0].Rows[0]["CUSRINFO_CELLPHONE"].ToString();
                ((RegisterNewMemberModel)Session["regMember"]).Member.MemberEmail = ds.Tables[0].Rows[0]["CUSR_EMAIL"].ToString();
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode = ds.Tables[0].Rows[0]["CUSRINFO_PHONE_COUNTRY"].ToString();
                #endregion


                return PartialView("MPRegisterNewMemberPage4", ((RegisterNewMemberModel)Session["regMember"]));
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult MPRegisterNewMemberPage4Method(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }


                if (mem.LoginUsername == null)
                {
                    Response.Write(Resources.OneForAll.OneForAll.WarningUsernameSixChar);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (mem.LoginUsername == "")
                {
                    Response.Write(Resources.OneForAll.OneForAll.WarningUsernameSixChar);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.LoginUsername.Length < 6)
                {
                    Response.Write(Resources.OneForAll.OneForAll.WarningUsernameSixChar);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.LoginUsername.Contains(" "))
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningNotAllowSpacing);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (mem.LoginUsername.Contains(" "))
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningNotAllowSpacing);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok = 0;
                string msg = "";

                var dr = AdminMemberDB.GetMemberByUserName(mem.LoginUsername, out ok, out msg);

                if (dr.Tables[0].Rows.Count != 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningMemberIDExisted);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                ((RegisterNewMemberModel)Session["regMember"]).LoginUsername = mem.LoginUsername.ToUpper();

                #region GetMemberInformation
                var ds = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                ((RegisterNewMemberModel)Session["regMember"]).Member.FirstName = ds.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString().ToUpper();
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.IC = ds.Tables[0].Rows[0]["CUSRINFO_IC"].ToString();
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.CellPhone = ds.Tables[0].Rows[0]["CUSRINFO_CELLPHONE"].ToString();
                ((RegisterNewMemberModel)Session["regMember"]).Member.MemberEmail = ds.Tables[0].Rows[0]["CUSR_EMAIL"].ToString();
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.Nickname = ds.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString().ToUpper();
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode = ds.Tables[0].Rows[0]["CUSRINFO_PHONE_COUNTRY"].ToString();
                #endregion

                return MPRegisterNewMemberPage5();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult MPBackToRegisterNewMemberPage3(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                if (string.IsNullOrEmpty(mem.LoginUsername))
                {
                    ((RegisterNewMemberModel)Session["regMember"]).LoginUsername = "";
                }
                else
                {
                    ((RegisterNewMemberModel)Session["regMember"]).LoginUsername = mem.LoginUsername.ToUpper();
                }

                if (string.IsNullOrEmpty(mem.Member.FirstName))
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Member.FirstName = "";
                }
                else
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Member.FirstName = mem.Member.FirstName.ToUpper();
                }

                if (string.IsNullOrEmpty(mem.UserInfo.IC))
                {
                    ((RegisterNewMemberModel)Session["regMember"]).UserInfo.IC = "";
                }
                else
                {
                    ((RegisterNewMemberModel)Session["regMember"]).UserInfo.IC = mem.UserInfo.IC;
                }

                if (string.IsNullOrEmpty(mem.UserInfo.CellPhone))
                {
                    ((RegisterNewMemberModel)Session["regMember"]).UserInfo.CellPhone = "";
                }
                else
                {
                    ((RegisterNewMemberModel)Session["regMember"]).UserInfo.CellPhone = mem.UserInfo.CellPhone;
                }

                if (string.IsNullOrEmpty(mem.Member.MemberEmail))
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Member.MemberEmail = "";
                }
                else
                {
                    ((RegisterNewMemberModel)Session["regMember"]).Member.MemberEmail = mem.Member.MemberEmail;
                }

                ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry = mem.SelectedCountry;
                ((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode = mem.UserInfo.MobileCountryCode;


                //string testing = ((RegisterNewMemberModel)Session["regMember"]).SelectedRank;
                int ok = 0;
                string msg = "";

                var dataset = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
                ((RegisterNewMemberModel)Session["regMember"]).Member.BonusWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());




                return PartialView("MPRegisterNewMemberPage3", ((RegisterNewMemberModel)Session["regMember"]));

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region FithPage-Review

        public ActionResult MPRegisterNewMemberPage5()
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            if (Session["CountryCode"] == null || Session["CountryCode"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
            }

            try
            {

                string languageCode = Session["LanguageChosen"].ToString();

                var db = MemberShopCenterDB.CheckPackage(((RegisterNewMemberModel)Session["regMember"]).SelectedPackage);

                DataSet dsPack = MemberShopCenterDB.GetPackageAndDescription(languageCode, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);
                foreach (DataRow dr in dsPack.Tables[0].Rows)
                {
                    if (dr["CPKG_TYPE"].ToString() == "R")
                    {
                        if (Convert.ToInt32(dr["CPKG_ACCQTY"].ToString()) == 1)
                        {

                            if ((dr["CPKG_CODE"].ToString() == ((RegisterNewMemberModel)Session["regMember"]).SelectedPackage))
                            {
                                ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageName = dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString();
                                ((RegisterNewMemberModel)Session["regMember"]).TotalRD = (float.Parse(dr["CPKG_AMOUNT"].ToString())).ToString("n2");
                                ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageAmount = (float.Parse(dr["CPKG_AMOUNT"].ToString())).ToString("n2");
                                ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageBV = (float.Parse(dr["CPKG_BV"].ToString())).ToString("n2");
                                ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageEPoint = (float.Parse(dr["CPKG_EUNIT"].ToString())).ToString("n2");
                            }
                        }
                    }
                }

                string Name = string.Empty;

                string Quantity = db.Tables[0].Rows[0]["CPKG_ACCQTY"].ToString();

                if (Quantity != "1")
                {
                    int Quan = Convert.ToInt32(Quantity);

                    for (int A = 0; A < Quan; A++)
                    {
                        int B = A + 1;
                        Name += ((RegisterNewMemberModel)Session["regMember"]).LoginUsername + "0" + B + ",";
                        if (A == 3)
                        {
                            Name += System.Environment.NewLine;
                        }
                    }


                }
                else
                {
                    Name = ((RegisterNewMemberModel)Session["regMember"]).LoginUsername;
                }

                ((RegisterNewMemberModel)Session["regMember"]).DisplayName = Name;

                return PartialView("MPRegisterNewMemberPage5", ((RegisterNewMemberModel)Session["regMember"]));
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult MPRegisterNewMemberPage5Method(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                int ok = 0;
                string msg = "";

                var dataset = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
                DataRow row = dataset.Tables[0].Rows[0];
                string refername = row["CUSR_USERNAME"].ToString();

                ((RegisterNewMemberModel)Session["regMember"]).ReferralUsername = Session["Username"].ToString();
                ((RegisterNewMemberModel)Session["regMember"]).SponsorSecurityPin = mem.SponsorSecurityPin;

                //store existing value before the value get clear out after registration done, important for send sms                
                string selectedCountry = ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry.ToLower();
                string nickname = ((RegisterNewMemberModel)Session["regMember"]).UserInfo.Nickname;
                string username = ((RegisterNewMemberModel)Session["regMember"]).LoginUsername;

                // some logic to validate phone numbers
                string cellPhone = CellPhoneParser.Parse(selectedCountry, ((RegisterNewMemberModel)Session["regMember"]).UserInfo.CellPhone);

                //Encrypt pin and password
                string ic = ((RegisterNewMemberModel)Session["regMember"]).UserInfo.IC;
                string password = ic.Substring(ic.Length - 6);
                ((RegisterNewMemberModel)Session["regMember"]).Member.EncryptedPassword = Authentication.Encrypt(password);
                ((RegisterNewMemberModel)Session["regMember"]).Member.EncryptedPin = Authentication.Encrypt(password);
                ((RegisterNewMemberModel)Session["regMember"]).EncryptedSponsorSecurityPin = Authentication.Encrypt(((RegisterNewMemberModel)Session["regMember"]).SponsorSecurityPin);
                string invoiceNumber = "";

                ViewBag.SelectedMethod = ((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption;
                MemberShopCenterDB.RegisterMember(((RegisterNewMemberModel)Session["regMember"]), out ok, out msg, out invoiceNumber);

                if (ok == 1) //success
                {

                    if (!string.IsNullOrEmpty(cellPhone))
                    {
                        string PackageCode = ((RegisterNewMemberModel)Session["regMember"]).SelectedPackage;

                        var db = MemberShopCenterDB.CheckPackage(PackageCode);
                        string Name = string.Empty;
                        string Quantity = db.Tables[0].Rows[0]["CPKG_ACCQTY"].ToString();
                        if (Quantity == "3")
                        {
                            Name = ((RegisterNewMemberModel)Session["regMember"]).LoginUsername + "01" + " TO " + ((RegisterNewMemberModel)Session["regMember"]).LoginUsername + "03";
                        }
                        else if (Quantity == "7")
                        {
                            Name = ((RegisterNewMemberModel)Session["regMember"]).LoginUsername + "01" + " TO " + ((RegisterNewMemberModel)Session["regMember"]).LoginUsername + "07";
                        }
                        else
                        {
                            Name = ((RegisterNewMemberModel)Session["regMember"]).LoginUsername;
                        }

                        try
                        {
                            string path = string.Empty;
                            DataSet logo = AdminGeneralDB.GetCompanySetup();
                            foreach (DataRow dr in logo.Tables[0].Rows)
                            {
                                var IFile = new ImageFile();
                                IFile.FileName = dr["COM_IMAGEPATH"].ToString();

                                string TempPath = logo.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString().Replace("../..", "~");

                                path = Server.MapPath(TempPath);
                            }



                            string massege = string.Empty;
                            string Title = string.Empty;


                            Title = "Welcome to BVI.ASIA";
                            massege = "<img height=\"200\" width=\"300\" src=cid:myImageID /><br/><br/>{3},{4}<br/><br/>{0}<br/><br/>Congratulations and welcome to BVI.asia !<br/>We will do our best toward all the success, Let us work together to create wealth.<br/><br/>恭喜你已经成功注册成为BVI.asia平台的会员！BVI.asia平台欢迎大家的参与，并致力于大家一起创造财富！迎接辉煌！<br/><br/> 会员账号 Member ID         :{1} <br/>登录密码 Login Password ：{2} <br/><br/>Please login to www.bvi.asia and change your login password and security PIN to prevent unauthorized access.<br/><br/>请登录  www.bvi.asia 会员后台后，更改您的登录密码和安全密码，以防被人盗用。";


                            var contentSMS = string.Format(massege, nickname, Name, password, Name, ((RegisterNewMemberModel)Session["regMember"]).Member.FirstName);

                            string result = Misc.SendEmailGood("noreply@bvi.asia", ((RegisterNewMemberModel)Session["regMember"]).Member.MemberEmail, Title, contentSMS, path);




                        }
                        catch (Exception aa)
                        {
                            // failed to send sms is ok, but better dont block the registration flow
                        }
                    }

                    string MsgRsuult = Resources.OneForAll.OneForAll.msgRegMemberSucceed1 + "\n" + username + " " + nickname + "\n" + Resources.OneForAll.OneForAll.msgRegMemberSucceed2;



                    return Json(MsgRsuult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (msg == "-1")
                        Response.Write(Resources.OneForAll.OneForAll.msgMemberExist);
                    else if (msg == "-2")
                        Response.Write(Resources.OneForAll.OneForAll.msgPackageNotExists);
                    else if (msg == "-3")
                        Response.Write(Resources.OneForAll.OneForAll.msgRWalletInsufficient);
                    else if (msg == "-4")
                        Response.Write(Resources.OneForAll.OneForAll.msgIntroNotExist);
                    else if (msg == "-5")
                        Response.Write(Resources.OneForAll.OneForAll.msgPINInvalid);
                    else if (msg == "-6")
                        Response.Write(Resources.OneForAll.OneForAll.msgUpNotExist);
                    else if (msg == "-7")
                        Response.Write(Resources.OneForAll.OneForAll.msgLeftOccupied);
                    else if (msg == "-8")
                        Response.Write(Resources.OneForAll.OneForAll.msgRightOccupied);
                    else if (msg == "-9")
                        Response.Write(Resources.OneForAll.OneForAll.msgPositionIncorrect);

                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult MPBackToRegisterNewMemberPage4(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                return MPRegisterNewMemberPage4();

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #endregion

        //Done 100%
        #region MP Upgrade Member

        #region FirstPage-Ranking
        public ActionResult MPCheckUser(string Username = "")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                //int isBelongTo;
                //MemberDB.MemberIsInMarketTreeByUsername(Username, Session["Username"].ToString(), out isBelongTo);

                //if (isBelongTo == 0)//not belong to
                //{
                //    Response.Write(Resources.OneForAll.OneForAll.WarningInvalidUseridPlsTryAgain);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                int ok = 0;
                string msg = string.Empty;
                string CurrentPackage = string.Empty;

                if (Username == "0")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidUser);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                var db = MemberDB.GetMemberByUsername(Username, out ok, out msg);
                var ds = MemberDB.CheckRanking();

                CurrentPackage = db.Tables[0].Rows[0]["CPKG_CODE"].ToString();

                DataSet DBSS = MemberShopCenterDB.GetCurrentPackageDetail(CurrentPackage);

                if (DBSS.Tables[0].Rows[0]["CRANKSET_CODE"].ToString() == ds.Tables[0].Rows[0]["CRANKSET_CODE"].ToString())
                {
                    Response.Write(Resources.OneForAll.OneForAll.WarningHigherPackage);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                string Result = "";
                string Name = "";
                string Rank = "";
                string Country = "";

                Name = db.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                Rank = Misc.GetMemberRanking(db.Tables[0].Rows[0]["CRANK_CODE"].ToString());
                Country = db.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();

                Result = Name + "," + Rank + "," + Country;


                return Json(Result, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult MPUpgradeMemberPage1()
        {


            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                if (Session["CountryCode"] == null || Session["CountryCode"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

                //string Status = "";
                //MemberDB.GetTime(out Status);

                //if (Status == "1")
                //{
                //    Response.Write(Resources.OneForAll.OneForAll.warningRegNotAllowfrom12am);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}


                int ok = 0;
                string msg = "";
                string languageCode = Session["LanguageChosen"].ToString();
                Session["regMember"] = new RegisterNewMemberModel();


                var dataset = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                string user = dataset.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();

                ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry = dataset.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                ((RegisterNewMemberModel)Session["regMember"]).PaymentOption = "1";
                ((RegisterNewMemberModel)Session["regMember"]).Member.RegisterWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
                ((RegisterNewMemberModel)Session["regMember"]).Member.BonusWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_BONUSWALLET"].ToString());
                ((RegisterNewMemberModel)Session["regMember"]).Member.CompanyWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_COMPANYWALLET"].ToString());
                ((RegisterNewMemberModel)Session["regMember"]).Member.MultiPointWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_MULTIPOINT"].ToString());
                ((RegisterNewMemberModel)Session["regMember"]).Member.CashWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_CASHWALLET"].ToString());

                ((RegisterNewMemberModel)Session["regMember"]).Member.OwnAccount = Misc.GetAllOwnAccount(Session["Username"].ToString());
                ((RegisterNewMemberModel)Session["regMember"]).Member.SelectedOwnAccount = "0";
                #region Country
                //combobox for country
                List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

                ((RegisterNewMemberModel)Session["regMember"]).Countries = from c in countries
                                                                           select new SelectListItem
                                                                           {
                                                                               Text = c.CountryName,
                                                                               Value = c.CountryCode
                                                                           };
                if (((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode == null)
                {
                    ((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode = countries[0].MobileCode;
                }
                #endregion

                ((RegisterNewMemberModel)Session["regMember"]).Member.RegisterPaymentWallet = 100;
                ((RegisterNewMemberModel)Session["regMember"]).Member.BonusPaymentWallet = 0;
                ((RegisterNewMemberModel)Session["regMember"]).Member.CompanyPaymentWallet = 0;

                return PartialView("MPUpgradeMemberPage1", ((RegisterNewMemberModel)Session["regMember"]));

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        //Go To Next Page        
        public ActionResult MPUpgradeMemberPage1Method(RegisterNewMemberModel mem, string PaymentType = "", string Username = "")
        {
            try
            {

                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                if (Username == "0")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidUser);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                ((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption = PaymentType;
                ((RegisterNewMemberModel)Session["regMember"]).Member.Username = Username;

                return MPUpgradeMemberPage2(mem);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region SecondPage-Option&Package
        //Load Page
        public ActionResult MPUpgradeMemberPage2(RegisterNewMemberModel mem)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
            }

            string languageCode = languageCode = Session["LanguageChosen"].ToString();

            int ok = 0;
            string msg = "";
            string CurrPackage = "";
            List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);
            var ds = MemberDB.GetMemberByUsername(((RegisterNewMemberModel)Session["regMember"]).Member.Username, out ok, out msg);

            #region Package
            CurrPackage = ds.Tables[0].Rows[0]["CPKG_CODE"].ToString();

            DataSet GetRanking = MemberShopCenterDB.GetCurrentPackageDetail(CurrPackage);


            ((RegisterNewMemberModel)Session["regMember"]).SelectedRank = GetRanking.Tables[0].Rows[0]["CRANKSET_CODE"].ToString();
            ((RegisterNewMemberModel)Session["regMember"]).LanguageCode = languageCode;

            ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry = ds.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
            string CurrentPackage = ds.Tables[0].Rows[0]["CPKG_CODE"].ToString();
            float CurrentPackagePrice = 0;
            float CurrentBV = 0;
            float CurrentEPoint = 0;


            DataSet DBSS = MemberShopCenterDB.GetCurrentPackageDetail(CurrentPackage);
            CurrentPackagePrice = float.Parse(DBSS.Tables[0].Rows[0]["CPKG_AMOUNT"].ToString());
            CurrentBV = float.Parse(DBSS.Tables[0].Rows[0]["CPKG_BV"].ToString());
            CurrentEPoint = float.Parse(DBSS.Tables[0].Rows[0]["CPKG_EUNIT"].ToString());


            DataSet dsPack = MemberShopCenterDB.GetPackageAndDescriptionForUpgrade(languageCode, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry, ((RegisterNewMemberModel)Session["regMember"]).SelectedRank);
            ((RegisterNewMemberModel)Session["regMember"]).Packages.Clear();

            foreach (DataRow dr in dsPack.Tables[0].Rows)
            {
                if (dr["CPKG_TYPE"].ToString() == "R")
                {
                    if (Convert.ToInt32(dr["CPKG_ACCQTY"].ToString()) == 1)
                    {

                        PackageModel package = new PackageModel();
                        package.PackageID = int.Parse(dr["CPKG_ID"].ToString());
                        package.PackageCode = dr["CPKG_CODE"].ToString();
                        package.PackageDescription = dr["CMULTILANGPACKAGE_DESCRIPTION"] != null ? dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString() : string.Empty;
                        int extensionIndex = dr["CPRG_IMAGEPATH"].ToString().LastIndexOf('\\');
                        string imageName = dr["CPRG_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
                        package.Image = "http://" + Request.Url.Authority + "/Packages/" + package.PackageCode + "/" + imageName;
                        package.Rp = float.Parse(dr["CPKG_AMOUNT"].ToString()).ToString("n2");
                        if (((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption == "RP")
                        {
                            package.Rp = (float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString("n2");
                        }
                        else if (((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption == "MP")
                        {
                            package.Mp = (float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString("n2");
                        }
                        else if (((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption == "RPMP")
                        {
                            package.Rp = ((float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice) * 50 / 100).ToString("n2");
                            package.Mp = ((float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice) * 50 / 100).ToString("n2");
                        }
                        else if (((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption == "CP")
                        {
                            package.Cp = (float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString("n2");
                        }
                        else if (((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption == "RPCP")
                        {
                            package.Rp = ((float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice) * 50 / 100).ToString("n2");
                            package.Cp = ((float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice) * 50 / 100).ToString("n2");
                        }
                        package.TotalAmount = (float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString("n2");
                        package.BV = (float.Parse(dr["CPKG_BV"].ToString()) - CurrentBV).ToString("n2");
                        package.EPoint = (float.Parse(dr["CPKG_EUNIT"].ToString()) - CurrentEPoint).ToString("n2");



                        package.PackageName = dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString();


                        ((RegisterNewMemberModel)Session["regMember"]).Countries = from c in countries
                                                                                   select new SelectListItem
                                                                                   {
                                                                                       Text = c.CountryName,
                                                                                       Value = c.CountryCode
                                                                                   };
                        package.Country = countries.Find(item => item.CountryCode == dr["CCOUNTRY_CODE"].ToString()).CountryName;
                        int extensionIndexs = Misc.GetCountryImageByCountryCode(dr["CCOUNTRY_CODE"].ToString()).LastIndexOf('\\');
                        string imageNameCountry = Misc.GetCountryImageByCountryCode(dr["CCOUNTRY_CODE"].ToString()).Substring(extensionIndexs + 1);
                        package.CountryImage = "http://" + Request.Url.Authority + "/Country/" + dr["CCOUNTRY_CODE"].ToString() + "/" + imageNameCountry;


                        ((RegisterNewMemberModel)Session["regMember"]).Packages.Add(package);
                    }
                }
            }
            if (dsPack.Tables[0].Rows.Count == 0)
            {
                ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = false;

            }
            else
            {
                ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = true;
            }

            if (((RegisterNewMemberModel)Session["regMember"]).Packages.Count == 0)
            {
                ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = false;
                //Response.Write(Resources.OneForAll.OneForAll.msgNoPackage);
                //return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }



            #endregion



            return PartialView("MPUpgradeMemberPage2", ((RegisterNewMemberModel)Session["regMember"]));
        }

        public ActionResult MPUpgradeMemberPage2Method(RegisterNewMemberModel mem)
        {
            int ok = 0;
            string msg = string.Empty;
            string CurrPackage = string.Empty;

            var ds = MemberDB.GetMemberByUsername(((RegisterNewMemberModel)Session["regMember"]).Member.Username, out ok, out msg);
            CurrPackage = ds.Tables[0].Rows[0]["CPKG_CODE"].ToString();

            DataSet GetRanking = MemberShopCenterDB.GetCurrentPackageDetail(CurrPackage);



            ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry = ds.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
            ((RegisterNewMemberModel)Session["regMember"]).SelectedRank = GetRanking.Tables[0].Rows[0]["CRANKSET_CODE"].ToString();
            string CurrentPackage = ds.Tables[0].Rows[0]["CPKG_CODE"].ToString();
            float CurrentPackagePrice = 0;


            DataSet dssPack = MemberShopCenterDB.GetPackageDetail("en-US", ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry, CurrentPackage);
            foreach (DataRow dr in dssPack.Tables[0].Rows)
            {
                if (dr["CPKG_CODE"].ToString() == CurrentPackage)
                {
                    CurrentPackagePrice = float.Parse(dr["CPKG_AMOUNT"].ToString());

                }
            }


            ((RegisterNewMemberModel)Session["regMember"]).SelectedPackage = mem.SelectedPackage;

            if (mem.SelectedPackage == null)
            {
                Response.Write(Resources.OneForAll.OneForAll.warningPlsChooseAPackage);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            else
            {

                DataSet dsPack = MemberShopCenterDB.GetPackageAndDescription(Session["LanguageChosen"].ToString(), ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);

                foreach (DataRow dr in dsPack.Tables[0].Rows)
                {
                    if (dr["CPKG_TYPE"].ToString() == "R")
                    {
                        if (mem.SelectedPackage == dr["CPKG_CODE"].ToString())
                        {
                            ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageAmount = (float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString("n2");
                            ((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount = (float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString();
                        }

                    }
                }
            }

            DataSet packageInfo = AdminGeneralDB.GetPackageByCode(((RegisterNewMemberModel)Session["regMember"]).SelectedPackage, "en-US", out ok, out msg);
            float investment = float.Parse(packageInfo.Tables[0].Rows[0]["CPKG_AMOUNT"].ToString());
            float total = investment;
            ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageName = packageInfo.Tables[0].Rows[0]["CMULTILANGPACKAGE_NAME"].ToString();
            ((RegisterNewMemberModel)Session["regMember"]).TotalRD = (float.Parse(packageInfo.Tables[0].Rows[0]["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString("n2");

            return MPUpgradeMemberPage3(mem);
        }

        public ActionResult MPBackToUpgradeMemberPage1()
        {
            return MPUpgradeMemberPage1();
        }

        #endregion

        #region ThirdPage-PaymentMethod
        //Load Page
        public ActionResult MPUpgradeMemberPage3(RegisterNewMemberModel mem)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
            }

            int ok = 0;
            string msg = "";
            string languageCode = Session["LanguageChosen"].ToString();
            var dataset = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
            DataRow row = dataset.Tables[0].Rows[0];
            string user = row["CUSR_USERNAME"].ToString();

            ((RegisterNewMemberModel)Session["regMember"]).Member.RegisterWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
            ((RegisterNewMemberModel)Session["regMember"]).Member.RMPWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_RMPWALLET"].ToString());
            ((RegisterNewMemberModel)Session["regMember"]).Member.CRPWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_COMPANYWALLET"].ToString());




            #region Country
            //combobox for country
            List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

            ((RegisterNewMemberModel)Session["regMember"]).Countries = from c in countries
                                                                       select new SelectListItem
                                                                       {
                                                                           Text = c.CountryName,
                                                                           Value = c.CountryCode
                                                                       };
            #endregion

            float PackageAmount = 0;

            PackageAmount = float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount);

            if (string.IsNullOrEmpty(((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterPaymentWallet))
            {
                ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterPaymentWallet = (PackageAmount * 30 / 100).ToString("n2");
                ((RegisterNewMemberModel)Session["regMember"]).Member.SCompanyPaymentWallet = (PackageAmount * 40 / 100).ToString("n2");
                ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterMPPaymentWallet = (PackageAmount * 30 / 100).ToString("n2");
            }

            return PartialView("MPUpgradeMemberPage3", ((RegisterNewMemberModel)Session["regMember"]));
        }

        public ActionResult MPUpgradeMemberPage3Method(string RegisterPaymentWallet = "", string CompanyPaymentWallet = "" , string RegisterMPPaymentWallet = "")
        {
            #region Condition             

            if (string.IsNullOrEmpty(RegisterPaymentWallet))
            {
                RegisterPaymentWallet = "0";
            }
            if (string.IsNullOrEmpty(CompanyPaymentWallet))
            {
                CompanyPaymentWallet = "0";
            }
            if (string.IsNullOrEmpty(RegisterMPPaymentWallet))
            {
                RegisterMPPaymentWallet = "0";
            }

            float PackageAmount = float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount);

            if ((float.Parse(RegisterPaymentWallet) + float.Parse(CompanyPaymentWallet) + float.Parse(RegisterMPPaymentWallet)) > PackageAmount)
            {
                Response.Write("(CRP + RP + RMP) Cant Be More Than Package Amount");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if ((float.Parse(RegisterPaymentWallet) + float.Parse(CompanyPaymentWallet) + float.Parse(RegisterMPPaymentWallet)) < PackageAmount)
            {
                Response.Write("(CRP + RP + RMP) Cant Be Less Than Package Amount");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            int ok = 0;
            string msg = string.Empty;

            var ds = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

            float RP = float.Parse(ds.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
            float RMP = float.Parse(ds.Tables[0].Rows[0]["CMEM_RMPWALLET"].ToString());
            float CRP = float.Parse(ds.Tables[0].Rows[0]["CMEM_COMPANYWALLET"].ToString());

            float CRPMin = float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount) * 40 / 100;

            //float RMPMax = float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount) * 20 / 100;

            //if (CRPMin > float.Parse(CompanyPaymentWallet))
            //{
            //    Response.Write("Company Register Point (CRP) Cant Lower Than 40% ");
            //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
            //}

            //if (float.Parse(RegisterMPPaymentWallet) > RMPMax)
            //{
            //    Response.Write("Register MP (RMP) Cant More Than 20% ");
            //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
            //}

            if (float.Parse(CompanyPaymentWallet) > CRP)
            {
                Response.Write("Company Register Point (CRP) Insufficient ");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (float.Parse(RegisterPaymentWallet) > RP)
            {
                Response.Write("Register Point (RP) Insufficient ");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (float.Parse(RegisterMPPaymentWallet) > RMP)
            {
                Response.Write("Register MP (RMP) Insufficient");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

                ((RegisterNewMemberModel)Session["regMember"]).Member.RP = RegisterPaymentWallet;
            ((RegisterNewMemberModel)Session["regMember"]).Member.CRP = CompanyPaymentWallet;
            ((RegisterNewMemberModel)Session["regMember"]).Member.RMP = RegisterMPPaymentWallet;
            ((RegisterNewMemberModel)Session["regMember"]).Member.SCompanyPaymentWallet = float.Parse(CompanyPaymentWallet).ToString("n2");
            ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterMPPaymentWallet = float.Parse(RegisterMPPaymentWallet).ToString("n2");
            ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterPaymentWallet = float.Parse(RegisterPaymentWallet).ToString("n2");

            #endregion



            return MPUpgradeMemberPage4();
        }

        public ActionResult MPBackToUpgradeMemberPage2(RegisterNewMemberModel mem)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
            }

            return MPUpgradeMemberPage2(mem);
        }

        #endregion

        #region FouthPage-Review

        public ActionResult MPUpgradeMemberPage4()
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
            }

            if (Session["CountryCode"] == null || Session["CountryCode"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
            }

            try
            {

                string languageCode = Session["LanguageChosen"].ToString();

                int ok = 0;
                string msg = "";

                var ds = MemberDB.GetMemberByUsername(((RegisterNewMemberModel)Session["regMember"]).Member.Username, out ok, out msg);
                var db = MemberShopCenterDB.CheckPackage(((RegisterNewMemberModel)Session["regMember"]).SelectedPackage);

                string CurrentPackage = ds.Tables[0].Rows[0]["CPKG_CODE"].ToString();
                float CurrentPackagePrice = 0;
                float CurrentBV = 0;
                float CurrentEPoint = 0;


                DataSet DBSS = MemberShopCenterDB.GetCurrentPackageDetail(CurrentPackage);
                CurrentPackagePrice = float.Parse(DBSS.Tables[0].Rows[0]["CPKG_AMOUNT"].ToString());
                CurrentBV = float.Parse(DBSS.Tables[0].Rows[0]["CPKG_BV"].ToString());
                CurrentEPoint = float.Parse(DBSS.Tables[0].Rows[0]["CPKG_EUNIT"].ToString());

                DataSet dsPack = MemberShopCenterDB.GetPackageAndDescriptionForUpgrade(languageCode, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry, ((RegisterNewMemberModel)Session["regMember"]).SelectedRank);
                foreach (DataRow dr in dsPack.Tables[0].Rows)
                {
                    if (dr["CPKG_TYPE"].ToString() == "R")
                    {
                        if (Convert.ToInt32(dr["CPKG_ACCQTY"].ToString()) == 1)
                        {

                            if ((dr["CPKG_CODE"].ToString() == ((RegisterNewMemberModel)Session["regMember"]).SelectedPackage))
                            {

                                ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageAmount = (float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString("n2");
                                ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageBV = (float.Parse(dr["CPKG_BV"].ToString()) - CurrentBV).ToString("n2");
                                ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageEPoint = (float.Parse(dr["CPKG_EUNIT"].ToString()) - CurrentEPoint).ToString("n2");


                            }
                        }
                    }
                }
                ViewBag.SelectedMethod = ((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption;

                return PartialView("MPUpgradeMemberPage4", ((RegisterNewMemberModel)Session["regMember"]));
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult MPUpgradeMemberPage4Method(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

                int ok = 0;
                string msg = "";

                var dataset = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                string Pin = dataset.Tables[0].Rows[0]["CUSR_PIN"].ToString();

                if (mem.SponsorSecurityPin != Authentication.Decrypt(Pin))
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgPINInvalid);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                ((RegisterNewMemberModel)Session["regMember"]).LoginUsername = Session["Username"].ToString();

                MemberShopCenterDB.UpgradePackage(((RegisterNewMemberModel)Session["regMember"]));


                DataSet dsPack = MemberShopCenterDB.GetPackageAndDescription(Session["LanguageChosen"].ToString(), ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);
                string PackageName = string.Empty;
                foreach (DataRow dr in dsPack.Tables[0].Rows)
                {
                    if (dr["CPKG_TYPE"].ToString() == "R")
                    {
                        if (mem.SelectedPackage == dr["CPKG_CODE"].ToString())
                        {
                            PackageName = dr["CMULTILANGPACKAGE_NAME"].ToString();
                        }

                    }
                }

                string MsgRsuult = ((RegisterNewMemberModel)Session["regMember"]).Member.Username + "\n" + "Account Upgrade Successfully !";

                return Json(MsgRsuult, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        //Btn Back from fourth page
        public ActionResult MPBackToUpgradeMemberPage3(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

                return MPUpgradeMemberPage3(mem);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #endregion

        //Done 100%
        #region Self Upgrade Member

        #region First Page - Ranking

        public ActionResult SelfUpgradeMemberPage1()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                if (Session["CountryCode"] == null || Session["CountryCode"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }



                int ok = 0;
                string msg = "";
                string languageCode = Session["LanguageChosen"].ToString();
                Session["regMember"] = new RegisterNewMemberModel();


                var dataset = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                string CurrentPackage = string.Empty;

                var ds = MemberDB.CheckRanking();

                CurrentPackage = dataset.Tables[0].Rows[0]["CPKG_CODE"].ToString();

                DataSet DBSS = MemberShopCenterDB.GetCurrentPackageDetail(CurrentPackage);

                if (DBSS.Tables[0].Rows[0]["CRANKSET_CODE"].ToString() == ds.Tables[0].Rows[0]["CRANKSET_CODE"].ToString())
                {
                    Response.Write(Resources.OneForAll.OneForAll.WarningHigherPackage);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                ((RegisterNewMemberModel)Session["regMember"]).Member.SelectedOwnAccount = Session["Username"].ToString();
                ((RegisterNewMemberModel)Session["regMember"]).Member.FirstName = dataset.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                ((RegisterNewMemberModel)Session["regMember"]).Member.Rank = Misc.GetMemberRanking(dataset.Tables[0].Rows[0]["CRANK_CODE"].ToString());

                string user = dataset.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();

                ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry = dataset.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                ((RegisterNewMemberModel)Session["regMember"]).PaymentOption = "1";
                ((RegisterNewMemberModel)Session["regMember"]).Member.RegisterWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
                ((RegisterNewMemberModel)Session["regMember"]).Member.BonusWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_BONUSWALLET"].ToString());
                ((RegisterNewMemberModel)Session["regMember"]).Member.CompanyWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_COMPANYWALLET"].ToString());
                ((RegisterNewMemberModel)Session["regMember"]).Member.MultiPointWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_MULTIPOINT"].ToString());
                ((RegisterNewMemberModel)Session["regMember"]).Member.CashWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_CASHWALLET"].ToString());

                ((RegisterNewMemberModel)Session["regMember"]).Member.OwnAccount = Misc.GetAllOwnAccount(Session["Username"].ToString());

                #region Country
                //combobox for country
                List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

                ((RegisterNewMemberModel)Session["regMember"]).Countries = from c in countries
                                                                           select new SelectListItem
                                                                           {
                                                                               Text = c.CountryName,
                                                                               Value = c.CountryCode
                                                                           };
                if (((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode == null)
                {
                    ((RegisterNewMemberModel)Session["regMember"]).UserInfo.MobileCountryCode = countries[0].MobileCode;
                }
                #endregion

                ((RegisterNewMemberModel)Session["regMember"]).Member.RegisterPaymentWallet = 100;
                ((RegisterNewMemberModel)Session["regMember"]).Member.BonusPaymentWallet = 0;
                ((RegisterNewMemberModel)Session["regMember"]).Member.CompanyPaymentWallet = 0;

                return PartialView("SelfUpgradeMemberPage1", ((RegisterNewMemberModel)Session["regMember"]));

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        //Go To Next Page        
        public ActionResult SelfUpgradeMemberPage1Method(RegisterNewMemberModel mem)
        {
            try
            {

                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }


                int ok = 0;
                string msg = "";


                var dataset = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                string CurrentPackage = string.Empty;

                var ds = MemberDB.CheckRanking();

                CurrentPackage = dataset.Tables[0].Rows[0]["CPKG_CODE"].ToString();

                DataSet DBSS = MemberShopCenterDB.GetCurrentPackageDetail(CurrentPackage);

                if (DBSS.Tables[0].Rows[0]["CRANKSET_CODE"].ToString() == ds.Tables[0].Rows[0]["CRANKSET_CODE"].ToString())
                {
                    Response.Write(Resources.OneForAll.OneForAll.WarningHigherPackage);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                ((RegisterNewMemberModel)Session["regMember"]).Member.Username = Session["Username"].ToString();

                return SelfUpgradeMemberPage2(mem);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region Second Page - Option & Package
        //Load Page
        public ActionResult SelfUpgradeMemberPage2(RegisterNewMemberModel mem)
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            string languageCode = languageCode = Session["LanguageChosen"].ToString();

            int ok = 0;
            string msg = "";
            string CurrPackage = "";
            List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);
            var ds = MemberDB.GetMemberByUsername(((RegisterNewMemberModel)Session["regMember"]).Member.Username, out ok, out msg);

            #region Package
            CurrPackage = ds.Tables[0].Rows[0]["CPKG_CODE"].ToString();

            DataSet GetRanking = MemberShopCenterDB.GetCurrentPackageDetail(CurrPackage);


            ((RegisterNewMemberModel)Session["regMember"]).SelectedRank = GetRanking.Tables[0].Rows[0]["CRANKSET_CODE"].ToString();
            ((RegisterNewMemberModel)Session["regMember"]).LanguageCode = languageCode;

            ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry = ds.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
            string CurrentPackage = ds.Tables[0].Rows[0]["CPKG_CODE"].ToString();
            float CurrentPackagePrice = 0;
            float CurrentBV = 0;
            float CurrentEPoint = 0;


            DataSet DBSS = MemberShopCenterDB.GetCurrentPackageDetail(CurrentPackage);
            CurrentPackagePrice = float.Parse(DBSS.Tables[0].Rows[0]["CPKG_AMOUNT"].ToString());
            CurrentBV = float.Parse(DBSS.Tables[0].Rows[0]["CPKG_BV"].ToString());
            CurrentEPoint = float.Parse(DBSS.Tables[0].Rows[0]["CPKG_EUNIT"].ToString());


            DataSet dsPack = MemberShopCenterDB.GetPackageAndDescriptionForUpgrade(languageCode, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry, ((RegisterNewMemberModel)Session["regMember"]).SelectedRank);
            ((RegisterNewMemberModel)Session["regMember"]).Packages.Clear();

            foreach (DataRow dr in dsPack.Tables[0].Rows)
            {
                if (dr["CPKG_TYPE"].ToString() == "R")
                {
                    if (Convert.ToInt32(dr["CPKG_ACCQTY"].ToString()) == 1)
                    {

                        PackageModel package = new PackageModel();
                        package.PackageID = int.Parse(dr["CPKG_ID"].ToString());
                        package.PackageCode = dr["CPKG_CODE"].ToString();
                        package.PackageDescription = dr["CMULTILANGPACKAGE_DESCRIPTION"] != null ? dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString() : string.Empty;
                        int extensionIndex = dr["CPRG_IMAGEPATH"].ToString().LastIndexOf('\\');
                        string imageName = dr["CPRG_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
                        package.Image = "http://" + Request.Url.Authority + "/Packages/" + package.PackageCode + "/" + imageName;
                        package.Rp = float.Parse(dr["CPKG_AMOUNT"].ToString()).ToString("n2");
                        if (((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption == "RP")
                        {
                            package.Rp = (float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString("n2");
                        }
                        else if (((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption == "MP")
                        {
                            package.Mp = (float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString("n2");
                        }
                        else if (((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption == "RPMP")
                        {
                            package.Rp = ((float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice) * 50 / 100).ToString("n2");
                            package.Mp = ((float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice) * 50 / 100).ToString("n2");
                        }
                        else if (((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption == "CP")
                        {
                            package.Cp = (float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString("n2");
                        }
                        else if (((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption == "RPCP")
                        {
                            package.Rp = ((float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice) * 50 / 100).ToString("n2");
                            package.Cp = ((float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice) * 50 / 100).ToString("n2");
                        }
                        package.TotalAmount = (float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString("n2");
                        package.BV = (float.Parse(dr["CPKG_BV"].ToString()) - CurrentBV).ToString("n2");
                        package.EPoint = (float.Parse(dr["CPKG_EUNIT"].ToString()) - CurrentEPoint).ToString("n2");



                        package.PackageName = dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString();


                        ((RegisterNewMemberModel)Session["regMember"]).Countries = from c in countries
                                                                                   select new SelectListItem
                                                                                   {
                                                                                       Text = c.CountryName,
                                                                                       Value = c.CountryCode
                                                                                   };
                        package.Country = countries.Find(item => item.CountryCode == dr["CCOUNTRY_CODE"].ToString()).CountryName;
                        int extensionIndexs = Misc.GetCountryImageByCountryCode(dr["CCOUNTRY_CODE"].ToString()).LastIndexOf('\\');
                        string imageNameCountry = Misc.GetCountryImageByCountryCode(dr["CCOUNTRY_CODE"].ToString()).Substring(extensionIndexs + 1);
                        package.CountryImage = "http://" + Request.Url.Authority + "/Country/" + dr["CCOUNTRY_CODE"].ToString() + "/" + imageNameCountry;


                        ((RegisterNewMemberModel)Session["regMember"]).Packages.Add(package);
                    }
                }
            }
            if (dsPack.Tables[0].Rows.Count == 0)
            {
                ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = false;

            }
            else
            {
                ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = true;
            }

            if (((RegisterNewMemberModel)Session["regMember"]).Packages.Count == 0)
            {
                ((RegisterNewMemberModel)Session["regMember"]).Packagechecking = false;
                //Response.Write(Resources.OneForAll.OneForAll.msgNoPackage);
                //return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }



            #endregion



            return PartialView("SelfUpgradeMemberPage2", ((RegisterNewMemberModel)Session["regMember"]));
        }

        public ActionResult SelfUpgradeMemberPage2Method(RegisterNewMemberModel mem)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            int ok = 0;
            string msg = string.Empty;
            string CurrPackage = string.Empty;

            var ds = MemberDB.GetMemberByUsername(((RegisterNewMemberModel)Session["regMember"]).Member.Username, out ok, out msg);
            CurrPackage = ds.Tables[0].Rows[0]["CPKG_CODE"].ToString();

            DataSet GetRanking = MemberShopCenterDB.GetCurrentPackageDetail(CurrPackage);



            ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry = ds.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
            ((RegisterNewMemberModel)Session["regMember"]).SelectedRank = GetRanking.Tables[0].Rows[0]["CRANKSET_CODE"].ToString();
            string CurrentPackage = ds.Tables[0].Rows[0]["CPKG_CODE"].ToString();
            float CurrentPackagePrice = 0;


            DataSet dssPack = MemberShopCenterDB.GetPackageDetail("en-US", ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry, CurrentPackage);
            foreach (DataRow dr in dssPack.Tables[0].Rows)
            {
                if (dr["CPKG_CODE"].ToString() == CurrentPackage)
                {
                    CurrentPackagePrice = float.Parse(dr["CPKG_AMOUNT"].ToString());

                }
            }


            ((RegisterNewMemberModel)Session["regMember"]).SelectedPackage = mem.SelectedPackage;

            if (mem.SelectedPackage == null)
            {
                Response.Write(Resources.OneForAll.OneForAll.warningPlsChooseAPackage);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            else
            {

                DataSet dsPack = MemberShopCenterDB.GetPackageAndDescription(Session["LanguageChosen"].ToString(), ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);

                foreach (DataRow dr in dsPack.Tables[0].Rows)
                {
                    if (dr["CPKG_TYPE"].ToString() == "R")
                    {
                        if (mem.SelectedPackage == dr["CPKG_CODE"].ToString())
                        {
                            ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageAmount = (float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString("n2");
                            ((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount = (float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString();
                        }

                    }
                }
            }

            DataSet packageInfo = AdminGeneralDB.GetPackageByCode(((RegisterNewMemberModel)Session["regMember"]).SelectedPackage, "en-US", out ok, out msg);
            float investment = float.Parse(packageInfo.Tables[0].Rows[0]["CPKG_AMOUNT"].ToString());
            float total = investment;
            ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageName = packageInfo.Tables[0].Rows[0]["CMULTILANGPACKAGE_NAME"].ToString();
            ((RegisterNewMemberModel)Session["regMember"]).TotalRD = (float.Parse(packageInfo.Tables[0].Rows[0]["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString("n2");

            return SelfUpgradeMemberPage3(mem);
        }

        public ActionResult SelfBackToUpgradeMemberPage1()
        {
            return SelfUpgradeMemberPage1();
        }

        #endregion

        #region Third Page - Payment Method
        //Load Page
        public ActionResult SelfUpgradeMemberPage3(RegisterNewMemberModel mem)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            int ok = 0;
            string msg = "";
            string languageCode = Session["LanguageChosen"].ToString();
            var dataset = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
            DataRow row = dataset.Tables[0].Rows[0];
            string user = row["CUSR_USERNAME"].ToString();

            ((RegisterNewMemberModel)Session["regMember"]).Member.BonusWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_BONUSWALLET"].ToString());
            ((RegisterNewMemberModel)Session["regMember"]).Member.RMPWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_RMPWALLET"].ToString());
            ((RegisterNewMemberModel)Session["regMember"]).Member.CRPWallet = float.Parse(dataset.Tables[0].Rows[0]["CMEM_COMPANYWALLET"].ToString());

            float PackageAmount = 0;

            PackageAmount = float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount);

            if (string.IsNullOrEmpty(((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterPaymentWallet))
            {
                ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterPaymentWallet = (PackageAmount * 80 / 100).ToString("n2");
                ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterMPPaymentWallet = (PackageAmount * 20 / 100).ToString("n2");
            }

            #region Country
            //combobox for country
            List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

            ((RegisterNewMemberModel)Session["regMember"]).Countries = from c in countries
                                                                       select new SelectListItem
                                                                       {
                                                                           Text = c.CountryName,
                                                                           Value = c.CountryCode
                                                                       };
            #endregion


            return PartialView("SelfUpgradeMemberPage3", ((RegisterNewMemberModel)Session["regMember"]));
        }

        public ActionResult SelfUpgradeMemberPage3Method(RegisterNewMemberModel mem)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            #region Condition               
            int ok = 0;
            string msg = "";
            var dataset = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

            if (mem.SelectedPaymentOption == "RP")
            {
                ((RegisterNewMemberModel)Session["regMember"]).Member.RP = ((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount;
                ((RegisterNewMemberModel)Session["regMember"]).Member.CRP = "0";
                ((RegisterNewMemberModel)Session["regMember"]).Member.RMP = "0";
                if (float.Parse(((RegisterNewMemberModel)Session["regMember"]).Member.RP) > float.Parse(dataset.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString()))
                {
                    Response.Write("Register Point (RP) Insufficient ");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterMPPaymentWallet = float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount).ToString("n2");
                ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterPaymentWallet = float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount).ToString("n2");
            }

            if (mem.SelectedPaymentOption == "CRP")
            {
                ((RegisterNewMemberModel)Session["regMember"]).Member.CRP = ((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount;
                ((RegisterNewMemberModel)Session["regMember"]).Member.RP = "0";
                ((RegisterNewMemberModel)Session["regMember"]).Member.RMP = "0";
                if (float.Parse(((RegisterNewMemberModel)Session["regMember"]).Member.CRP) > float.Parse(dataset.Tables[0].Rows[0]["CMEM_COMPANYWALLET"].ToString()))
                {
                    Response.Write("Company Register Point (CRP) Insufficient ");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterMPPaymentWallet = float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount).ToString("n2");
                ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterPaymentWallet = float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount).ToString("n2");
            }

            if (mem.SelectedPaymentOption == "RMP")
            {
                ((RegisterNewMemberModel)Session["regMember"]).Member.RMP = ((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount;
                ((RegisterNewMemberModel)Session["regMember"]).Member.RP = "0";
                ((RegisterNewMemberModel)Session["regMember"]).Member.CRP = "0";
                if (float.Parse(((RegisterNewMemberModel)Session["regMember"]).Member.RMP) > float.Parse(dataset.Tables[0].Rows[0]["CMEM_RMPWALLET"].ToString()))
                {
                    Response.Write("Register MP (RMP) Insufficient ");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterMPPaymentWallet = float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount).ToString("n2");
                ((RegisterNewMemberModel)Session["regMember"]).Member.SRegisterPaymentWallet = float.Parse(((RegisterNewMemberModel)Session["regMember"]).stringPackageAmount).ToString("n2");
            }




            ((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption = mem.SelectedPaymentOption;








            #endregion

            return SelfUpgradeMemberPage4();
        }

        public ActionResult SelfBackToUpgradeMemberPage2(RegisterNewMemberModel mem)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            return SelfUpgradeMemberPage2(mem);
        }

        #endregion

        #region Fouth Page - Review

        public ActionResult SelfUpgradeMemberPage4()
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            if (Session["CountryCode"] == null || Session["CountryCode"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
            }

            try
            {

                string languageCode = Session["LanguageChosen"].ToString();

                int ok = 0;
                string msg = "";

                var ds = MemberDB.GetMemberByUsername(((RegisterNewMemberModel)Session["regMember"]).Member.Username, out ok, out msg);
                var db = MemberShopCenterDB.CheckPackage(((RegisterNewMemberModel)Session["regMember"]).SelectedPackage);

                string CurrentPackage = ds.Tables[0].Rows[0]["CPKG_CODE"].ToString();
                float CurrentPackagePrice = 0;
                float CurrentBV = 0;
                float CurrentEPoint = 0;


                DataSet DBSS = MemberShopCenterDB.GetCurrentPackageDetail(CurrentPackage);
                CurrentPackagePrice = float.Parse(DBSS.Tables[0].Rows[0]["CPKG_AMOUNT"].ToString());
                CurrentBV = float.Parse(DBSS.Tables[0].Rows[0]["CPKG_BV"].ToString());
                CurrentEPoint = float.Parse(DBSS.Tables[0].Rows[0]["CPKG_EUNIT"].ToString());

                DataSet dsPack = MemberShopCenterDB.GetPackageAndDescriptionForUpgrade(languageCode, ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry, ((RegisterNewMemberModel)Session["regMember"]).SelectedRank);
                foreach (DataRow dr in dsPack.Tables[0].Rows)
                {
                    if (dr["CPKG_TYPE"].ToString() == "R")
                    {
                        if (Convert.ToInt32(dr["CPKG_ACCQTY"].ToString()) == 1)
                        {

                            if ((dr["CPKG_CODE"].ToString() == ((RegisterNewMemberModel)Session["regMember"]).SelectedPackage))
                            {

                                ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageAmount = (float.Parse(dr["CPKG_AMOUNT"].ToString()) - CurrentPackagePrice).ToString("n2");
                                ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageBV = (float.Parse(dr["CPKG_BV"].ToString()) - CurrentBV).ToString("n2");
                                ((RegisterNewMemberModel)Session["regMember"]).SelectedPackageEPoint = (float.Parse(dr["CPKG_EUNIT"].ToString()) - CurrentEPoint).ToString("n2");


                            }
                        }
                    }
                }

                ViewBag.SelectedMethod = ((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption;

                return PartialView("SelfUpgradeMemberPage4", ((RegisterNewMemberModel)Session["regMember"]));
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SelfUpgradeMemberPage4Method(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                int ok = 0;
                string msg = "";

                var dataset = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                string Pin = dataset.Tables[0].Rows[0]["CUSR_PIN"].ToString();

                if (mem.SponsorSecurityPin != Authentication.Decrypt(Pin))
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgPINInvalid);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                ((RegisterNewMemberModel)Session["regMember"]).LoginUsername = Session["Username"].ToString();

                MemberShopCenterDB.UpgradePackage(((RegisterNewMemberModel)Session["regMember"]));


                DataSet dsPack = MemberShopCenterDB.GetPackageAndDescription(Session["LanguageChosen"].ToString(), ((RegisterNewMemberModel)Session["regMember"]).SelectedCountry);
                string PackageName = string.Empty;
                foreach (DataRow dr in dsPack.Tables[0].Rows)
                {
                    if (dr["CPKG_TYPE"].ToString() == "R")
                    {
                        if (mem.SelectedPackage == dr["CPKG_CODE"].ToString())
                        {
                            PackageName = dr["CMULTILANGPACKAGE_NAME"].ToString();
                        }

                    }
                }

                ViewBag.SelectedMethod = ((RegisterNewMemberModel)Session["regMember"]).SelectedPaymentOption;

                string MsgRsuult = ((RegisterNewMemberModel)Session["regMember"]).Member.Username + "\n" + "Account Upgrade Successfully !";

                return Json(MsgRsuult, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        //Btn Back from fourth page
        public ActionResult SelfBackToUpgradeMemberPage3(RegisterNewMemberModel mem)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                return SelfUpgradeMemberPage3(mem);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #endregion
    }

}