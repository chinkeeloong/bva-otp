﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using ECFBase.Components;
using ECFBase.Models;
using System.Web.Services;
using ECFBase.Helpers;
using System.IO;

namespace ECFBase.Controllers.Member
{
    public class MemberWalletController : Controller
    {
        #region Wallet Transaction

        public ActionResult NextWalletTransactionSpecialRedirect(string selectedPage, string walletType, DateTime date)
        {
            Session["page"] = "WalletTransactionSpecialRedirect";
            Session["selectedPage"] = selectedPage;
            Session["walletType"] = walletType;
            Session["date"] = date;

            return RedirectToAction("Home", "Member");
        }

        public ActionResult NextWalletTransaction(string selectedPage, string walletType, DateTime date)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }
                
                if (walletType == "CP")
                {
                    return RedirectToAction("CashWalletTransaction", "MemberWallet", new { selectedPage = selectedPage });
                }
                else if (walletType == "ICO")
                {
                    return RedirectToAction("ICOWalletTransaction", "MemberWallet", new { selectedPage = selectedPage });
                }
                else if (walletType == "GP")
                {
                    return RedirectToAction("GoldPointWalletTransaction", "MemberWallet", new { selectedPage = selectedPage});
                }
                else if (walletType == "MP")
                {
                    return RedirectToAction("MultiPointWalletTransaction", "MemberWallet", new { selectedPage = selectedPage });
                }
                else if (walletType == "RPC")
                {
                    return RedirectToAction("RPCWalletTransaction", "MemberWallet", new { selectedPage = selectedPage });
                }
                else if (walletType == "ICO")
                {
                    return RedirectToAction("ICOWalletTransaction", "MemberWallet", new { selectedPage = selectedPage });
                }
                else if (walletType == "RP")
                {
                    return RedirectToAction("RegisterWalletTransaction", "MemberWallet", new { selectedPage = selectedPage });
                }
                else if (walletType == "RMP")
                {
                    return RedirectToAction("RMPWalletTransaction", "MemberWallet", new { selectedPage = selectedPage });
                }
                else if (walletType == "CRP")
                {
                    return RedirectToAction("CompanyRegisterWalletTransaction", "MemberWallet", new { selectedPage = selectedPage });
                }
                else if (walletType == "TP")
                {
                    return RedirectToAction("TravelPointWalletTransaction", "MemberWallet", new { selectedPage = selectedPage });
                }
                return RedirectToAction("UnknownWalletTransaction", "MemberWallet", new { selectedPage = selectedPage});

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult NextWalletTransaction2(string selectedPage, string walletType, string cashname, string datetime, string startdate, string enddate)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                if (walletType == "CP")
                {
                    return RedirectToAction("CashWalletTransaction", "MemberWallet", new { selectedPage = selectedPage, cashname = cashname, startdate = startdate, enddate = enddate });
                }
                else if (walletType == "GP")
                {
                    return RedirectToAction("GoldPointWalletTransaction", "MemberWallet", new { selectedPage = selectedPage, cashname = cashname, startdate = startdate, enddate = enddate });
                }
                else if (walletType == "MP")
                {
                    return RedirectToAction("MultiPointWalletTransaction", "MemberWallet", new { selectedPage = selectedPage, cashname = cashname, startdate = startdate, enddate = enddate });
                }
                else if (walletType == "RPC")
                {
                    return RedirectToAction("RPCWalletTransaction", "MemberWallet", new { selectedPage = selectedPage, cashname = cashname, startdate = startdate, enddate = enddate });
                }
                else if (walletType == "RP")
                {
                    return RedirectToAction("RegisterWalletTransaction", "MemberWallet", new { selectedPage = selectedPage, cashname = cashname, startdate = startdate, enddate = enddate });
                }
                else if (walletType == "ICO")
                {
                    return RedirectToAction("ICOWalletTransaction", "MemberWallet", new { selectedPage = selectedPage, cashname = cashname, startdate = startdate, enddate = enddate });
                }
                else if (walletType == "RMP")
                {
                    return RedirectToAction("RMPWalletTransaction", "MemberWallet", new { selectedPage = selectedPage, cashname = cashname, startdate = startdate, enddate = enddate });
                }
                else if (walletType == "CRP")
                {
                    return RedirectToAction("CompanyRegisterWalletTransaction", "MemberWallet", new { selectedPage = selectedPage, cashname = cashname, startdate = startdate, enddate = enddate });
                }
                else if (walletType == "TP")
                {
                    return RedirectToAction("TravelPointWalletTransaction", "MemberWallet", new { selectedPage = selectedPage, cashname = cashname, startdate = startdate, enddate = enddate});
                }
                return RedirectToAction("UnknownWalletTransaction", "MemberWallet", new { selectedPage = selectedPage, cashname = cashname, startdate = startdate, enddate = enddate });

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RegisterWalletTransaction(int selectedPage = -1, string cashname = "0", string startdate = "01/01/1990", string enddate = "01/01/3000", string Username = "0", string Export = "0")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                int pages = 0;
                var model = new PaginationWalletLogModel();
                model.TypeOfWallet = "RP";

                DateTime sdate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
                DateTime ENDDATE = edate.AddDays(1);
                string ED = ENDDATE.ToString("yyyy-MM-dd");

                List<CashName> CName = Misc.DBGetAllCashNameList("RP");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };


                model.selectedname = cashname;

                DataSet dsBonusWalletLog = MemberWalletDB.GetAllRegisterWalletLogByUsername(cashname, Session["Username"].ToString(), selectedPage, SD, ED, Session["LanguageChosen"].ToString(), out pages);
                selectedPage = ConstructPageList(selectedPage, pages, model);

                foreach (DataRow dr in dsBonusWalletLog.Tables[0].Rows)
                {
                    var walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CREGWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CREGWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CREGWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CREGWL_WALLET"].ToString());
                    walletLog.AppUser = dr["CREGWL_APPUSER"].ToString();

                    if (dr["CREGWL_CASHNAME"].ToString() == "Trading Bonus")
                    {
                        walletLog.AppUser = "";
                    }
                    else
                    {
                        walletLog.AppUser = dr["CREGWL_APPUSER"].ToString();
                    }

                    var remark = Misc.GetReadableCashName(dr["CREGWL_APPOTHER"].ToString());
                    walletLog.AppOther = remark.Length > 40 ? remark.Substring(0, 40) + "..." : remark;

                    walletLog.AppNumber = dr["CREGWL_APPNUMBER"].ToString();
                    walletLog.AppRate = dr["CREGWL_APPRATE"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CREGWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    model.WalletLogList.Add(walletLog);
                }

                ViewBag.Module = Resources.OneForAll.OneForAll.mnuWalletMainTab;
                ViewBag.Title = Resources.OneForAll.OneForAll.mnuRegisterwallet;
                ViewBag.Title2 = Resources.OneForAll.OneForAll.mnuStatement;
                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CompanyRegisterWalletTransaction(int selectedPage = -1, string cashname = "0", string startdate = "01/01/1990", string enddate = "01/01/3000", string Username = "0", string Export = "0")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                int pages = 0;
                var model = new PaginationWalletLogModel();
                model.TypeOfWallet = "CRP";

                DateTime sdate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
                DateTime ENDDATE = edate.AddDays(1);
                string ED = ENDDATE.ToString("yyyy-MM-dd");

                List<CashName> CName = Misc.DBGetAllCashNameList("CRP");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };


                model.selectedname = cashname;
                DataSet dsBonusWalletLog = MemberWalletDB.GetAllCompanyRegisterWalletLogByUsername(cashname, Session["Username"].ToString(), selectedPage, SD, ED, Session["LanguageChosen"].ToString(),  out pages);



                selectedPage = ConstructPageList(selectedPage, pages, model);
                

                foreach (DataRow dr in dsBonusWalletLog.Tables[0].Rows)
                {
                    var walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CCOMWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CCOMWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CCOMWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CCOMWL_WALLET"].ToString());
                    walletLog.AppUser = dr["CCOMWL_APPUSER"].ToString();

                    if (dr["CCOMWL_CASHNAME"].ToString() == "Trading Bonus")
                    {
                        walletLog.AppUser = "";
                    }
                    else
                    {
                        walletLog.AppUser = dr["CCOMWL_APPUSER"].ToString();
                    }

                    var remark = Misc.GetReadableCashName(dr["CCOMWL_APPOTHER"].ToString());
                    walletLog.AppOther = remark.Length > 40 ? remark.Substring(0, 40) + "..." : remark;

                    walletLog.AppNumber = dr["CCOMWL_APPNUMBER"].ToString();
                    walletLog.AppRate = dr["CCOMWL_APPRATE"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CCOMWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    model.WalletLogList.Add(walletLog);
                }

                ViewBag.Module = Resources.OneForAll.OneForAll.mnuWalletMainTab;
                ViewBag.Title = Resources.OneForAll.OneForAll.mnuCRPLog;
                ViewBag.Title2 = Resources.OneForAll.OneForAll.mnuStatement;
                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult CashWalletTransaction(int selectedPage = -1, string cashname = "0", string startdate = "01/01/1990", string enddate = "01/01/3000", string Username = "0", string Export = "0")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                int pages = 0;
                var model = new PaginationWalletLogModel();
                model.TypeOfWallet = "CP";

                DateTime sdate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
                DateTime ENDDATE = edate.AddDays(1);
                string ED = ENDDATE.ToString("yyyy-MM-dd");
                List<CashName> CName = Misc.DBGetAllCashNameList("CP");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };


                model.selectedname = cashname;

                DataSet dsBonusWalletLog = MemberWalletDB.GetAllCashWalletLogByUsername(cashname, Session["Username"].ToString(), selectedPage, SD, ED, Session["LanguageChosen"].ToString(), out pages);
                selectedPage = ConstructPageList(selectedPage, pages, model);

                foreach (DataRow dr in dsBonusWalletLog.Tables[0].Rows)
                {
                    var walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CCSHWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CCSHWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CCSHWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CCSHWL_WALLET"].ToString());

                    if (dr["CCSHWL_APPOTHER"].ToString().Contains("Sell eFund"))
                    {
                        walletLog.AppUser = Resources.OneForAll.OneForAll.cnSelleFund;
                    }
                    else
                    {
                        walletLog.AppUser = dr["CCSHWL_APPUSER"].ToString();
                    }
                    
                    var remark = Misc.GetReadableCashName(dr["CCSHWL_APPOTHER"].ToString());

                    if (dr["CCSHWL_CASHNAME"].ToString() == "WDR")
                    {
                        if (dr["CCSHWL_TRANSTATE"].ToString() == "1")
                        {
                            remark = Resources.OneForAll.OneForAll.lblStatusApproved;
                        }
                        else if (dr["CCSHWL_TRANSTATE"].ToString() == "-1")
                        {
                            remark = Resources.OneForAll.OneForAll.lblStatusRefunded;
                        }
                        else if (dr["CCSHWL_TRANSTATE"].ToString() == "0")
                        {
                            remark = Resources.OneForAll.OneForAll.lblStatusPending;
                        }
                    }



                    
                    walletLog.AppOther = remark.Length > 40 ? remark.Substring(0, 40) + "..." : remark;

                    if (walletLog.AppOther.Contains("Sell eFund"))
                    {
                        if (Session["LanguageChosen"].ToString() == "zh-CN")
                        {
                            string Replacement = string.Empty;
                            Replacement = walletLog.AppOther.Replace("Sell", string.Empty);
                            Replacement = Replacement.Replace("eFund", string.Empty);

                            Replacement = Resources.OneForAll.OneForAll.cnSelleFund  + Replacement;

                            walletLog.AppOther= Replacement;
                        }
                    }

                    walletLog.AppNumber = dr["CCSHWL_APPNUMBER"].ToString();
                    walletLog.AppRate = dr["CCSHWL_APPRATE"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CCSHWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    model.WalletLogList.Add(walletLog);
                }

                ViewBag.Module = Resources.OneForAll.OneForAll.mnuWalletMainTab;
                ViewBag.Title = Resources.OneForAll.OneForAll.mnuCashwallet;
                ViewBag.Title2 = Resources.OneForAll.OneForAll.mnuStatement;
                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ICOWalletTransaction(int selectedPage = -1, string cashname = "0", string startdate = "01/01/1990", string enddate = "01/01/3000", string Username = "0", string Export = "0")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                int pages = 0;
                var model = new PaginationWalletLogModel();
                model.TypeOfWallet = "ICO";

                DateTime sdate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
                DateTime ENDDATE = edate.AddDays(1);
                string ED = ENDDATE.ToString("yyyy-MM-dd");


                List<CashName> CName = Misc.DBGetAllCashNameList("ICO");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };


                model.selectedname = cashname;

                DataSet dsBonusWalletLog = MemberWalletDB.GetAllICOWalletLogByUsername(cashname, Session["Username"].ToString(), selectedPage, SD, ED, Session["LanguageChosen"].ToString(), out pages);
                selectedPage = ConstructPageList(selectedPage, pages, model);

                foreach (DataRow dr in dsBonusWalletLog.Tables[0].Rows)
                {
                    var walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CICOWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CICOWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CICOWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CICOWL_WALLET"].ToString());
                    walletLog.AppUser = dr["CICOWL_APPUSER"].ToString();
                    var remark = Misc.GetReadableCashName(dr["CICOWL_APPOTHER"].ToString());
                    walletLog.AppOther = remark.Length > 40 ? remark.Substring(0, 40) + "..." : remark;

                    walletLog.AppNumber = dr["CICOWL_APPNUMBER"].ToString();
                    walletLog.AppRate = dr["CICOWL_APPRATE"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CICOWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    model.WalletLogList.Add(walletLog);
                }

                ViewBag.Module = Resources.OneForAll.OneForAll.mnuWalletMainTab;
                ViewBag.Title = Resources.OneForAll.OneForAll.mnuICOPoint;
                ViewBag.Title2 = Resources.OneForAll.OneForAll.mnuStatement;
                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult MultiPointWalletTransaction(int selectedPage = -1, string cashname = "0", string startdate = "01/01/1990", string enddate = "01/01/3000", string Username = "0", string Export = "0")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                int pages = 0;
                var model = new PaginationWalletLogModel();
                model.TypeOfWallet = "MP";

                DateTime sdate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
                DateTime ENDDATE = edate.AddDays(1);
                string ED = ENDDATE.ToString("yyyy-MM-dd");

                List<CashName> CName = Misc.DBGetAllCashNameList("MP");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };


                model.selectedname = cashname;

                DataSet dsBonusWalletLog = MemberWalletDB.GetAllMultiPointWalletLogByUsername(cashname, Session["Username"].ToString(), selectedPage, SD, ED, Session["LanguageChosen"].ToString(), out pages);
                selectedPage = ConstructPageList(selectedPage, pages, model);

                foreach (DataRow dr in dsBonusWalletLog.Tables[0].Rows)
                {
                    var walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CMPWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CMPWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CMPWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CMPWL_WALLET"].ToString());

                    if (dr["CMPWL_APPOTHER"].ToString().Contains("Sell eFund"))
                    {
                        walletLog.AppUser = Resources.OneForAll.OneForAll.cnSelleFund;
                    }
                    else
                    {
                        walletLog.AppUser = dr["CMPWL_APPUSER"].ToString();
                    }

                    if (dr["CMPWL_CASHNAME"].ToString() == "Trading Bonus")
                    {
                        walletLog.AppUser = "";
                    }
                    else
                    {
                        walletLog.AppUser = dr["CMPWL_APPUSER"].ToString();
                    }



                    var remark = Misc.GetReadableCashName(dr["CMPWL_APPOTHER"].ToString());
                    walletLog.AppOther = remark.Length > 40 ? remark.Substring(0, 40) + "..." : remark;

                    if (walletLog.AppOther.Contains("Sell eFund"))
                    {
                        if (Session["LanguageChosen"].ToString() == "zh-CN")
                        {
                            string Replacement = string.Empty;
                            Replacement = walletLog.AppOther.Replace("Sell", string.Empty);
                            Replacement = Replacement.Replace("eFund", string.Empty);

                            Replacement = Resources.OneForAll.OneForAll.cnSelleFund + Replacement;

                            walletLog.AppOther = Replacement;
                        }
                    }

                    walletLog.AppNumber = dr["CMPWL_APPNUMBER"].ToString();
                    walletLog.AppRate = dr["CMPWL_APPRATE"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CMPWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    model.WalletLogList.Add(walletLog);
                }

                ViewBag.Module = Resources.OneForAll.OneForAll.mnuWalletMainTab;
                ViewBag.Title = Resources.OneForAll.OneForAll.mnuMultiPoint;
                ViewBag.Title2 = Resources.OneForAll.OneForAll.mnuStatement;
                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GoldPointWalletTransaction(int selectedPage = -1, string cashname = "0", string startdate = "01/01/1990", string enddate = "01/01/3000", string Username = "0", string Export = "0")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                int pages = 0;
                var model = new PaginationWalletLogModel();
                model.TypeOfWallet = "GP";

                DateTime sdate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
                DateTime ENDDATE = edate.AddDays(1);
                string ED = ENDDATE.ToString("yyyy-MM-dd");

                List<CashName> CName = Misc.DBGetAllCashNameList("GP");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };


                model.selectedname = cashname;

                DataSet dsBonusWalletLog = MemberWalletDB.GetAllGoldPointWalletLogByUsername(cashname, Session["Username"].ToString(), selectedPage, SD, ED, Session["LanguageChosen"].ToString(),  out pages);
                selectedPage = ConstructPageList(selectedPage, pages, model);

                foreach (DataRow dr in dsBonusWalletLog.Tables[0].Rows)
                {
                    var walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["GLDWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["GLDWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["GLDWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["GLDWL_WALLET"].ToString());
                    walletLog.AppUser = dr["GLDWL_APPUSER"].ToString();


                    var remark = Misc.GetReadableCashName(dr["GLDWL_APPOTHER"].ToString());
                    walletLog.AppOther = remark.Length > 40 ? remark.Substring(0, 40) + "..." : remark;

                    walletLog.AppNumber = dr["GLDWL_APPNUMBER"].ToString();
                    walletLog.AppRate = dr["GLDWL_APPRATE"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["GLDWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    model.WalletLogList.Add(walletLog);
                }

                ViewBag.Module = Resources.OneForAll.OneForAll.mnuWalletMainTab;
                ViewBag.Title = Resources.OneForAll.OneForAll.mnuCompanywallet;
                ViewBag.Title2 = Resources.OneForAll.OneForAll.mnuStatement;
                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RPCWalletTransaction(int selectedPage = -1, string cashname = "0", string startdate = "01/01/1990", string enddate = "01/01/3000", string Username = "0", string Export = "0")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                int pages = 0;
                var model = new PaginationWalletLogModel();
                model.TypeOfWallet = "RPC";

                DateTime sdate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
                DateTime ENDDATE = edate.AddDays(1);
                string ED = ENDDATE.ToString("yyyy-MM-dd");

                List<CashName> CName = Misc.DBGetAllCashNameList("RPC");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };


                model.selectedname = cashname;

                DataSet dsBonusWalletLog = MemberWalletDB.GetAllRPCWalletLogByUsername(cashname, Session["Username"].ToString(), selectedPage, SD, ED, Session["LanguageChosen"].ToString(), out pages);
                selectedPage = ConstructPageList(selectedPage, pages, model);

                foreach (DataRow dr in dsBonusWalletLog.Tables[0].Rows)
                {
                    var walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CRPCWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CRPCWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CRPCWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CRPCWL_WALLET"].ToString());
                    //walletLog.AppUser = dr["CRPCWL_APPUSER"].ToString();

                    
                    if (dr["CRPCWL_APPOTHER"].ToString().Contains("Sell eFund"))
                    {
                        walletLog.AppUser = Resources.OneForAll.OneForAll.cnSelleFund;
                    }
                    else
                    {
                        walletLog.AppUser = dr["CRPCWL_APPUSER"].ToString();
                    }

                    if (dr["CRPCWL_CASHNAME"].ToString() == "Trading Bonus")
                    {
                        walletLog.AppUser = "";
                    }
                    else
                    {
                        walletLog.AppUser = dr["CRPCWL_APPUSER"].ToString();
                    }


                    var remark = Misc.GetReadableCashName(dr["CRPCWL_APPOTHER"].ToString());
                    walletLog.AppOther = remark.Length > 40 ? remark.Substring(0, 40) + "..." : remark;

                    if (walletLog.AppOther.Contains("Sell eFund"))
                    {
                        if (Session["LanguageChosen"].ToString() == "zh-CN")
                        {
                            string Replacement = string.Empty;
                            Replacement = walletLog.AppOther.Replace("Sell", string.Empty);
                            Replacement = Replacement.Replace("eFund", string.Empty);

                            Replacement = Resources.OneForAll.OneForAll.cnSelleFund + Replacement;

                            walletLog.AppOther = Replacement;
                        }
                    }

                    walletLog.AppNumber = dr["CRPCWL_APPNUMBER"].ToString();
                    walletLog.AppRate = dr["CRPCWL_APPRATE"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CRPCWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    model.WalletLogList.Add(walletLog);
                }

                ViewBag.Module = Resources.OneForAll.OneForAll.mnuWalletMainTab;
                ViewBag.Title = Resources.OneForAll.OneForAll.mnuRepurchase;
                ViewBag.Title2 = Resources.OneForAll.OneForAll.mnuStatement;
                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RMPWalletTransaction(int selectedPage = -1, string cashname = "0", string startdate = "01/01/1990", string enddate = "01/01/3000", string Username = "0", string Export = "0")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                int pages = 0;
                var model = new PaginationWalletLogModel();
                model.TypeOfWallet = "RMP";

                DateTime sdate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
                DateTime ENDDATE = edate.AddDays(1);
                string ED = ENDDATE.ToString("yyyy-MM-dd");

                List<CashName> CName = Misc.DBGetAllCashNameList("RMP");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };


                model.selectedname = cashname;

                DataSet dsBonusWalletLog = MemberWalletDB.GetAllWalletRMPLogByUsername(cashname, Session["Username"].ToString(), selectedPage, SD, ED, Session["LanguageChosen"].ToString(), out pages);
                selectedPage = ConstructPageList(selectedPage, pages, model);

                foreach (DataRow dr in dsBonusWalletLog.Tables[0].Rows)
                {
                    var walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CRMPWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CRMPWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CRMPWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CRMPWL_WALLET"].ToString());

                    if (dr["CRMPWL_APPOTHER"].ToString().Contains("Sell eFund"))
                    {
                        walletLog.AppUser = Resources.OneForAll.OneForAll.cnSelleFund;
                    }
                    else
                    {
                        walletLog.AppUser = dr["CRMPWL_APPUSER"].ToString();
                    }

                    if (dr["CRMPWL_CASHNAME"].ToString() == "Trading Bonus")
                    {
                        walletLog.AppUser = "";
                    }
                    else
                    {
                        walletLog.AppUser = dr["CRMPWL_APPUSER"].ToString();
                    }



                    var remark = Misc.GetReadableCashName(dr["CRMPWL_APPOTHER"].ToString());
                    walletLog.AppOther = remark.Length > 40 ? remark.Substring(0, 40) + "..." : remark;

                    if (walletLog.AppOther.Contains("Sell eFund"))
                    {
                        if (Session["LanguageChosen"].ToString() == "zh-CN")
                        {
                            string Replacement = string.Empty;
                            Replacement = walletLog.AppOther.Replace("Sell", string.Empty);
                            Replacement = Replacement.Replace("eFund", string.Empty);

                            Replacement = Resources.OneForAll.OneForAll.cnSelleFund + Replacement;

                            walletLog.AppOther = Replacement;
                        }
                    }

                    walletLog.AppNumber = dr["CRMPWL_APPNUMBER"].ToString();
                    walletLog.AppRate = dr["CRMPWL_APPRATE"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CRMPWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    model.WalletLogList.Add(walletLog);
                }

                ViewBag.Module = Resources.OneForAll.OneForAll.mnuWalletMainTab;
                ViewBag.Title = Resources.OneForAll.OneForAll.mnuRMPWallet;
                ViewBag.Title2 = Resources.OneForAll.OneForAll.mnuStatement;
                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult TravelPointWalletTransaction(int selectedPage = -1, string cashname = "0", string startdate = "01/01/1990", string enddate = "01/01/3000", string Username = "0", string Export = "0")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                int pages = 0;
                var model = new PaginationWalletLogModel();
                model.TypeOfWallet = "TP";

                DateTime sdate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
                DateTime ENDDATE = edate.AddDays(1);
                string ED = ENDDATE.ToString("yyyy-MM-dd");

                List<CashName> CName = Misc.DBGetAllCashNameList("TP");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };


                model.selectedname = cashname;

                DataSet dsBonusWalletLog = MemberWalletDB.GetAllWalletTPLogByUsername(cashname, Session["Username"].ToString(), selectedPage, SD, ED, Session["LanguageChosen"].ToString(),  out pages);
                selectedPage = ConstructPageList(selectedPage, pages, model);

                
                foreach (DataRow dr in dsBonusWalletLog.Tables[0].Rows)
                {
                    var walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CTRVWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CTRVWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CTRVWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CTRVWL_WALLET"].ToString());

                    if (dr["CTRVWL_APPOTHER"].ToString().Contains("Sell eFund"))
                    {
                        walletLog.AppUser = Resources.OneForAll.OneForAll.cnSelleFund;
                    }
                    else
                    {
                        walletLog.AppUser = dr["CTRVWL_APPUSER"].ToString();
                    }

                    if (dr["CTRVWL_CASHNAME"].ToString() == "Trading Bonus")
                    {
                        walletLog.AppUser = "";
                    }
                    else
                    {
                        walletLog.AppUser = dr["CTRVWL_APPUSER"].ToString();
                    }



                    var remark = Misc.GetReadableCashName(dr["CTRVWL_APPOTHER"].ToString());
                    walletLog.AppOther = remark.Length > 40 ? remark.Substring(0, 40) + "..." : remark;

                    if (walletLog.AppOther.Contains("Sell eFund"))
                    {
                        if (Session["LanguageChosen"].ToString() == "zh-CN")
                        {
                            string Replacement = string.Empty;
                            Replacement = walletLog.AppOther.Replace("Sell", string.Empty);
                            Replacement = Replacement.Replace("eFund", string.Empty);

                            Replacement = Resources.OneForAll.OneForAll.cnSelleFund + Replacement;

                            walletLog.AppOther = Replacement;
                        }
                    }

                    walletLog.AppNumber = dr["CTRVWL_APPNUMBER"].ToString();
                    walletLog.AppRate = dr["CTRVWL_APPRATE"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CTRVWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    model.WalletLogList.Add(walletLog);
                }

                ViewBag.Module = Resources.OneForAll.OneForAll.mnuWalletMainTab;
                ViewBag.Title = Resources.OneForAll.OneForAll.mnuTPLog;
                ViewBag.Title2 = Resources.OneForAll.OneForAll.mnuStatement;
                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Wallet Transfer

        #region Register Wallet

        public ActionResult RegisterWalletTransfer()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                int ok = 0;
                string msg = "";
                DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                DataRow memberRow = member.Tables[0].Rows[0];

                var model = new TransferWalletModel { Charge = 0 };

                model.Wallet = string.Format("{0:n2}", memberRow["CMEM_REGISTERWALLET"]);

                return PartialView("RegisterWalletTransfer", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterWalletTransferMethod(string username, string amount, string remark, string pin)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                if (String.Compare(username, Session["Username"].ToString(), StringComparison.OrdinalIgnoreCase) == 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgSameLoginUser);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int Result = 0;

                DataSet Upline = MemberShopCenterDB.SponsorUplineCheckingForTransfer(Session["Username"].ToString(), username, out Result);

                if (Result == 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msginvalidupline);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok = 0;
                string msg = "";
                int Status = 0;
                float transferAmount = 0;
                float.TryParse(amount, out transferAmount);

                MemberWalletDB.CheckMemberUpDownBinaryBased(Session["Username"].ToString() , username ,out Status);

                if (Status != 1)
                {
                    Response.Write("Transfer failed ! The userID Does not exist in your group !");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                //if (Math.Floor(transferAmount) != transferAmount)
                //{
                //    Response.Write(Resources.OneForAll.OneForAll.lblNoDecimal);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}
                
                //if (transferAmount % 10 != 0)
                //{
                //    Response.Write(Resources.OneForAll.OneForAll.WarningAmtMultipleOf10);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                if (transferAmount < 10)
                {
                    //Response.Write(Resources.OneForAll.OneForAll.WarningCantKeyInLessThanOne);
                    Response.Write(Resources.OneForAll.OneForAll.WarningMinAmt10);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                //encrypt the password
                pin = Authentication.Encrypt(pin);

                MemberWalletDB.RegisterWalletTransfer(username, transferAmount, pin, Session["Username"].ToString(), remark ,out ok, out msg);

                if (msg == "-1")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgUsernameNotExist);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-2")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgAmountNegative);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-3")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-4")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgTransferMoreThanBalance);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                Session["page"] = "RegisterTransfer";
                ViewBag.page = "RegisterTransfer";
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("RegisterWalletTransaction", "MemberWallet");
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Company Register Wallet

        public ActionResult CompanyRegisterWalletTransfer()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                int ok = 0;
                string msg = "";
                DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                DataRow memberRow = member.Tables[0].Rows[0];

                var model = new TransferWalletModel { Charge = 0 };

                model.Wallet = string.Format("{0:n2}", memberRow["CMEM_COMPANYWALLET"]);

                return PartialView("CompanyRegisterWalletTransfer", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CompanyRegisterWalletTransferMethod(string username, string amount, string remark, string pin)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                if (String.Compare(username, Session["Username"].ToString(), StringComparison.OrdinalIgnoreCase) == 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgSameLoginUser);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int Result = 0;

                DataSet Upline = MemberShopCenterDB.SponsorUplineCheckingForTransfer(Session["Username"].ToString(), username, out Result);

                if (Result == 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msginvalidupline);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok = 0;
                string msg = "";
                int Status = 0;
                float transferAmount = 0;
                float.TryParse(amount, out transferAmount);

                MemberWalletDB.CheckMemberUpDownBinaryBased(Session["Username"].ToString(), username, out Status);

                if (Status != 1)
                {
                    Response.Write("Transfer failed ! The userID Does not exist in your group !");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                //if (Math.Floor(transferAmount) != transferAmount)
                //{
                //    Response.Write(Resources.OneForAll.OneForAll.lblNoDecimal);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                //if (transferAmount % 10 != 0)
                //{
                //    Response.Write(Resources.OneForAll.OneForAll.WarningAmtMultipleOf10);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                if (transferAmount < 10)
                {
                    //Response.Write(Resources.OneForAll.OneForAll.WarningCantKeyInLessThanOne);
                    Response.Write(Resources.OneForAll.OneForAll.WarningMinAmt10);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                //encrypt the password
                pin = Authentication.Encrypt(pin);

                MemberWalletDB.CompanyWalletTransfer(username, transferAmount, pin, Session["Username"].ToString(), remark, out ok, out msg);

                if (msg == "-1")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgUsernameNotExist);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-2")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgAmountNegative);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-3")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-4")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgTransferMoreThanBalance);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                Session["page"] = "CompanyRegisterTransfer";
                ViewBag.page = "CompanyRegisterTransfer";
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("RegisterWalletTransaction", "MemberWallet");
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Company Wallet

        public ActionResult CompanyWalletTransfer()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                int ok = 0;
                string msg = "";
                DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                DataRow memberRow = member.Tables[0].Rows[0];

                var model = new TransferWalletModel { Charge = 0 };

                model.Wallet = string.Format("{0:n2}", memberRow["CMEM_COMPANYWALLET"]);

                return PartialView("CompanyWalletTransfer", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CompanyWalletTransferMethod(string username, string amount, string remark, string pin)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                if (String.Compare(username, Session["Username"].ToString(), StringComparison.OrdinalIgnoreCase) == 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgSameLoginUser);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

               

                int ok = 0;
                string msg = "";

                var dataSet = AdminMemberDB.GetMemberInfoByUserName(username, out ok, out msg);

                if (dataSet.Tables[0].Rows[0]["CMEM_MEMBER_TYPE"].ToString() != "MobileMember")
                {
                    Response.Write("Only allow to transfer to STOCKIST");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                float transferAmount = 0;
                float.TryParse(amount, out transferAmount);


                //if (transferAmount % 10 > 0)
                //{
                //    Response.Write(Resources.OneForAll.OneForAll.lblNoDecimal);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                //if (transferAmount % 10 != 0)
                //{
                //    Response.Write("Amount must be multiple of 10 ! Eg. 10,20,50,100,150 etc");
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}
                if (transferAmount < 10)
                {
                    Response.Write("Minimum Amount is 10. Please try again !");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                //encrypt the password
                pin = Authentication.Encrypt(pin);

                MemberWalletDB.CompanyWalletTransfer(username, transferAmount, pin, Session["Username"].ToString(), remark, out ok, out msg);

                if (msg == "-1")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgUsernameNotExist);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-2")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgAmountNegative);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-3")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-4")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgTransferMoreThanBalance);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("CompanyWalletTransaction", "MemberWallet");
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion 

        #region Cash Wallet

        public ActionResult CashWalletTransfer()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                int ok = 0;
                string msg = "";
                DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                DataRow memberRow = member.Tables[0].Rows[0];

                var model = new TransferWalletModel { Charge = 0 };

                model.Wallet = string.Format("{0:n2}", memberRow["CMEM_CASHWALLET"]);

                return PartialView("CashWalletTransfer", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CashWalletTransferMethod(string username,string Option, string amount, string remark, string pin)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }
                int Result = 0;

                DataSet Upline = MemberShopCenterDB.SponsorUplineCheckingForTransfer(Session["Username"].ToString(), username, out Result);

                if (Result == 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msginvalidupline);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int Status = 0;

                if (Session["Username"].ToString().ToLower() != username.ToLower())
                { 
                    MemberWalletDB.CheckMemberUpDownBinaryBased(Session["Username"].ToString(), username, out Status);

                    if (Status != 1)
                    {
                        Response.Write("Transfer failed ! The userID Does not exist in your group !");
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                if (Option == "0")
                {
                    Response.Write("Please Choose The Wallet Option");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(Option))
                {
                    Response.Write("Please Choose The Wallet Option");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok = 0;
                string msg = "";
                float transferAmount = 0;
                float.TryParse(amount, out transferAmount);

                //if (Math.Floor(transferAmount) != transferAmount)
                //{
                //    Response.Write(Resources.OneForAll.OneForAll.lblNoDecimal);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                if (Option == "RP")
                {
                    if (transferAmount < 1)
                    {
                        Response.Write(Resources.OneForAll.OneForAll.WarningCantKeyInLessThanOne);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                if (Option == "MP")
                {
                    if (transferAmount < 1)
                    {
                        Response.Write(Resources.OneForAll.OneForAll.WarningCantKeyInLessThanOne);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                if (Option == "RMP")
                {
                    if (transferAmount < 1)
                    {
                        Response.Write(Resources.OneForAll.OneForAll.WarningCantKeyInLessThanOne);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }


                //encrypt the password
                pin = Authentication.Encrypt(pin);

                MemberWalletDB.CashWalletTransfer(username,Option, transferAmount, pin, Session["Username"].ToString(), remark, out ok, out msg);

                if (msg == "-1")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgUsernameNotExist);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-2")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgAmountNegative);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-3")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-4")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgTransferMoreThanBalance);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                Session["page"] = "CashTransfer";
                ViewBag.page = "CashTransfer";
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("CashWalletTransaction", "MemberWallet");
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Cash Wallet To Own Account

        public ActionResult CPTransferCheckUser(string Username = "")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes", ExpiredSession = Session["ExpiredSession"].ToString() });
                }
                
                int ok = 0;
                string msg = string.Empty;
                string CurrentPackage = string.Empty;

                if (Username == "0")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidUser);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (Username.ToLower() == Session["Username"].ToString().ToLower())
                {
                    Response.Write("Cant Transfer To Current Account");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                var db = MemberDB.GetMemberByUsername(Username, out ok, out msg);

                string Result = "";
                string Name = "";

                Name = db.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();

                Result = Name;


                return Json(Result, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CashWalletTransferToOwnAccount()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                int ok = 0;
                string msg = "";
                DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                DataRow memberRow = member.Tables[0].Rows[0];

                var model = new TransferWalletModel { Charge = 0 };

                model.Wallet = string.Format("{0:n2}", memberRow["CMEM_CASHWALLET"]);

                model.OwnAccount = Misc.GetAllOwnAccount(Session["Username"].ToString());

                return PartialView("CashWalletTransferToOwnAccount", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CashWalletTransferToOwnAccountMethod(string username, string Option, string amount, string remark, string pin)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

                if (username == "0")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidUser);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (username.ToLower() == Session["Username"].ToString().ToLower())
                {
                    Response.Write("Cant Transfer To Current Account");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (Option == "0")
                {
                    Response.Write("Please Choose The Wallet Option");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(Option))
                {
                    Response.Write("Please Choose The Wallet Option");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int Result = 0;

                DataSet dsPaymentMode = PaymentModeSetupDB.GetAllOwnAccount(Session["Username"].ToString());

                foreach (DataRow dr in dsPaymentMode.Tables[0].Rows)
                {
                    if (dr["CUSR_USERNAME"].ToString() == username)
                    {
                        Result = 1;
                    }
                }

                if (Result == 0)
                {
                    Response.Write("Invalid Own Account");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }



                int ok = 0;
                string msg = "";
                float transferAmount = 0;
                float.TryParse(amount, out transferAmount);

                //if (Math.Floor(transferAmount) != transferAmount)
                //{
                //    Response.Write(Resources.OneForAll.OneForAll.lblNoDecimal);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                if (Option == "CP")
                {
                    if (transferAmount < 1)
                    {
                        Response.Write(Resources.OneForAll.OneForAll.WarningCantKeyInLessThanOne);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }


                //encrypt the password
                pin = Authentication.Encrypt(pin);

                MemberWalletDB.CashWalletTransfer(username, Option, transferAmount, pin, Session["Username"].ToString(), remark, out ok, out msg);

                if (msg == "-1")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgUsernameNotExist);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-2")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgAmountNegative);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-3")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-4")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgTransferMoreThanBalance);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                Session["page"] = "CashTransfer";
                ViewBag.page = "CashTransfer";
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("CashWalletTransaction", "MemberWallet");
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Shop Wallet

        public ActionResult ShopWalletTransfer()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                int ok = 0;
                string msg = "";
                DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                DataRow memberRow = member.Tables[0].Rows[0];

                var model = new TransferWalletModel { Charge = 0 };

                model.Wallet = string.Format("{0:n2}", memberRow["CMEM_SHOPWALLET"]);

                return PartialView("ShopWalletTransfer", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ShopWalletTransferMethod(string username, string amount, string remark, string pin)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                if (String.Compare(username, Session["Username"].ToString(), StringComparison.OrdinalIgnoreCase) == 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgSameLoginUser);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }



                int ok = 0;
                string msg = "";
                float transferAmount = 0;
                float.TryParse(amount, out transferAmount);


                //if (transferAmount % 10 > 0)
                //{
                //    Response.Write(Resources.OneForAll.OneForAll.lblNoDecimal);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                //if (transferAmount % 10 != 0)
                //{
                //    Response.Write("Amount must be multiple of 10 ! Eg. 10,20,50,100,150 etc");
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}
                if (transferAmount < 10)
                {
                    Response.Write("Minimum Amount is 10. Please try again !");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                //encrypt the password
                pin = Authentication.Encrypt(pin);

                MemberWalletDB.ShopWalletTransfer(username, transferAmount, pin, Session["Username"].ToString(), remark, out ok, out msg);

                if (msg == "-1")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgUsernameNotExist);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-2")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgAmountNegative);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-3")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-4")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgTransferMoreThanBalance);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("ShopWalletTransaction", "MemberWallet");
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Multi Wallet

        public ActionResult MultiWalletTransfer()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                int ok = 0;
                string msg = "";
                DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                DataRow memberRow = member.Tables[0].Rows[0];

                var model = new TransferWalletModel { Charge = 0 };

                model.Wallet = string.Format("{0:n2}", memberRow["CMEM_MULTIPOINT"]);

                model.OwnAccount = Misc.GetAllOwnAccount(Session["Username"].ToString());

                return PartialView("MultiWalletTransfer", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MultiWalletTransferMethod(string username, string amount, string remark, string pin)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                if (String.Compare(username, Session["Username"].ToString(), StringComparison.OrdinalIgnoreCase) == 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgSameLoginUser);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(username))
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidMemberID);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok = 0;
                string msg = "";
                int Status = 0;
                float transferAmount = 0;
                float.TryParse(amount, out transferAmount);

                //if (Math.Floor(transferAmount) != transferAmount)
                //{
                //    Response.Write(Resources.OneForAll.OneForAll.lblNoDecimal);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                //if (transferAmount % 10 != 0)
                //{
                //    Response.Write(Resources.OneForAll.OneForAll.WarningAmtMultipleOf10);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                if (transferAmount < 10)
                {
                    Response.Write(Resources.OneForAll.OneForAll.WarningMinAmt10);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                //encrypt the password
                pin = Authentication.Encrypt(pin);

                MemberWalletDB.MultiWalletTransfer(username, transferAmount, pin, Session["Username"].ToString(), remark, out ok, out msg);

                if (msg == "-1")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgUsernameNotExist);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-2")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgAmountNegative);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-3")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-4")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgTransferMoreThanBalance);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                Session["page"] = "MultiTransfer";
                ViewBag.page = "MultiTransfer";
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("MultiPointWalletTransaction", "MemberWallet");
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region RMP Wallet

        public ActionResult RMPWalletTransfer()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                int ok = 0;
                string msg = "";
                DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                DataRow memberRow = member.Tables[0].Rows[0];

                var model = new TransferWalletModel { Charge = 0 };

                model.Wallet = string.Format("{0:n2}", memberRow["CMEM_RMPWALLET"]);

                return PartialView("RMPWalletTransfer", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RMPWalletTransferMethod(string username, string amount, string remark, string pin)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

                if (String.Compare(username, Session["Username"].ToString(), StringComparison.OrdinalIgnoreCase) == 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgSameLoginUser);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int Result = 0;

                DataSet Upline = MemberShopCenterDB.SponsorUplineCheckingForTransfer(Session["Username"].ToString(), username, out Result);

                if (Result == 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msginvalidupline);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok = 0;
                string msg = "";
                int Status = 0;
                float transferAmount = 0;
                float.TryParse(amount, out transferAmount);

                MemberWalletDB.CheckMemberUpDownBinaryBased(Session["Username"].ToString(), username, out Status);

                if (Status != 1)
                {
                    Response.Write("Transfer failed ! The userID Does not exist in your group !");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (transferAmount < 10)
                {
                    Response.Write(Resources.OneForAll.OneForAll.WarningMinAmt10);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                //encrypt the password
                pin = Authentication.Encrypt(pin);

                MemberWalletDB.RMPWalletTransfer(username, transferAmount, pin, Session["Username"].ToString(), remark, out ok, out msg);

                if (msg == "-1")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgUsernameNotExist);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-2")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgAmountNegative);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-3")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-4")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgTransferMoreThanBalance);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                Session["page"] = "RMPTransfer";
                ViewBag.page = "RMPTransfer";
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("RegisterWalletTransaction", "MemberWallet");
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #endregion

        #region Wallet Exchange

        #region Register Wallet        
        public ActionResult RegisterWalletExchange()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                var model = new ExchangeWalletModel();

                int ok = 0;
                string msg = "";
                DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                DataRow memberRow = member.Tables[0].Rows[0];


                model.FromWallet = string.Format("{0:n2}", memberRow["CMEM_REGISTERWALLET"]);
                model.ToWallet = string.Format("{0:n2}", memberRow["CMEM_MULTIPOINT"]);


                return PartialView("RegisterWalletExchange", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RegisterWalletExchangetMethod(string amount, string pin)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }


                int ok = 0;
                string msg = "";
                float transferAmount = 0;
                float.TryParse(amount, out transferAmount);



                if (transferAmount % 1 > 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.lblNoDecimal);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (Convert.ToInt32(amount) < 1)
                {
                    Response.Write("The Minimum Amount Must Be 1");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                //if (transferAmount % 10 > 0)
                //{
                //    Response.Write(Resources.OneForAll.OneForAll.msgMultyplyBy10);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                //encrypt the password
                pin = Authentication.Encrypt(pin);


                MemberWalletDB.ExchangeRegisterWalletToMultiWallet(Session["Username"].ToString(), Convert.ToInt32(amount), pin, out ok, out msg);

                if (msg == "-1")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgUsernameNotExist);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-2")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgAmountNegative);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-3")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-4")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgTransferMoreThanBalance);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("RegisterWalletTransaction", "MemberWallet");
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Cash Wallet        
        public ActionResult CashWalletExchange()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                var model = new ExchangeWalletModel();

                int ok = 0;
                string msg = "";
                DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                DataRow memberRow = member.Tables[0].Rows[0];

                  
                model.FromWallet = string.Format("{0:n2}", memberRow["CMEM_CASHWALLET"]);
                model.ToWallet = string.Format("{0:n2}", memberRow["CMEM_BONUSWALLET"]);
                    
                
                return PartialView("CashWalletExchange", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CashWalletExchangetMethod( string amount, string pin)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }


                int ok = 0;
                string msg = "";
                float transferAmount = 0;
                float.TryParse(amount, out transferAmount);

                

                if (transferAmount % 1 > 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.lblNoDecimal);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (Convert.ToInt32(amount) < 100)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgMinimumAmountEx100);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (transferAmount % 10 > 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgMultyplyBy10);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                //encrypt the password
                pin = Authentication.Encrypt(pin);


                MemberWalletDB.ExchangeCashWalletToBonusWallet(Session["Username"].ToString(), Convert.ToInt32(amount), pin, out ok, out msg);       
         
                if (msg == "-1")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgUsernameNotExist);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-2")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgAmountNegative);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-3")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-4")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgTransferMoreThanBalance);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("CashWalletTransaction", "MemberWallet");
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #endregion

        #region Wallet Withdrawal      

        public ActionResult CashWalletWithdrawal()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                string Status = "";
                MemberDB.GetTime(out Status);

                if (Session["Admin"] == null)
                {
                    if (Status == "1")
                    {
                        return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                    }

                }

                int ok = 0;
                string msg = "";
                DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                DataRow memberRow = member.Tables[0].Rows[0];
                ViewBag.Wallet = string.Format("{0:n2}", memberRow["CMEM_CASHWALLET"]);

                WithdrawWalletModel model = new WithdrawWalletModel();
                string language = Session["LanguageChosen"].ToString();

                #region Mailing Address
                DataSet dsMember = AdminMemberDB.GetMemberInfoByUserName(Session["Username"].ToString(), out ok, out msg);
                if(dsMember.Tables[0].Rows.Count!=0)
                {
                    model.Address = dsMember.Tables[0].Rows[0]["CUSRINFO_MAIL_ADDRESS"].ToString();
                    model.SelectedCity = dsMember.Tables[0].Rows[0]["CUSRINFO_MAIL_CITY"].ToString();
                    model.State = dsMember.Tables[0].Rows[0]["CUSRINFO_MAIL_STATE"].ToString();
                    model.Postcode = dsMember.Tables[0].Rows[0]["CUSRINFO_MAIL_POSTCODE"].ToString();
                    model.MailingCountry = dsMember.Tables[0].Rows[0]["CUSRINFO_MAIL_COUNTRY"].ToString();
                    if(model.Address == "" || model.Address == null)
                    {
                        Response.Write(Resources.OneForAll.OneForAll.warningUpdateMailingAddress);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                #endregion

                #region Bank Info
                DataSet dsMemberBank = MemberProfileDB.GetMemberBankByUsername(Session["Username"].ToString(), out ok, out msg);
                if (dsMemberBank.Tables[0].Rows.Count != 0)
                {
                  
                    model.BranchName = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_BRANCHNAME"].ToString();
                    model.BranchAdd1 = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_ADDRESS1"].ToString() + dsMemberBank.Tables[0].Rows[0]["CMEMBANK_ADDRESS2"].ToString();
                    //model.BranchAdd2 = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_ADDRESS2"].ToString();

                    if(model.BranchAdd1 == "" || model.BranchAdd1 == null)
                    {
                        Response.Write(Resources.OneForAll.OneForAll.warningUpdateBankAddress);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                    model.BranchSwiftCode = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_SWIFTCODE"].ToString();
                    model.SelectedCountry = dsMemberBank.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();

                    model.SelectedBank = dsMemberBank.Tables[0].Rows[0]["CBANK_CODE"].ToString();
                    model.ChinaBank = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_NAME"].ToString();
                    //combobox for bank
                    List<BankModel> banks = Misc.GetAllBankByCountry("en-us", model.SelectedCountry, ref ok, ref msg);
                    model.BankList = from c in banks
                                     select new SelectListItem
                                     {
                                         Selected = c.BankCode == model.SelectedBank ? true : false,
                                         Text = c.BankName,
                                         Value = c.BankCode
                                     };
                }
                else
                {
                    Response.Write(Resources.OneForAll.OneForAll.warningUpdateBankInfo);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                #endregion

                model.hiddenbankcharge = Misc.GetParaByName("CashWithdrawalCharge");

                return PartialView("CashWalletWithdrawal", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CashWalletWithdrawalMethod(string amount, string pin)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }
                    int ok = 0;
                    string msg = "";
                    float convertAmount = 0;
                    float.TryParse(amount, out convertAmount);
                    //encrypt the password
                    pin = Authentication.Encrypt(pin);

                    double numberToSplit = convertAmount;
                    double decimalresult = (int)numberToSplit - numberToSplit;

                    //if (decimalresult < 0.00)
                    //{
                    //    Response.Write(Resources.OneForAll.OneForAll.msgNodecimal);
                    //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    //}

                    if (convertAmount < 100)
                    {
                        Response.Write(Resources.OneForAll.OneForAll.msgMinimumAmountNotReach);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                    //if (convertAmount % 1 > 0)
                    //{
                    //    Response.Write(Resources.OneForAll.OneForAll.lblNoDecimal);
                    //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    //}

                    //if (convertAmount % 100 > 0)
                    //{
                    //    Response.Write("This Amount Must Multiply By 100");
                    //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    //}

                    MemberWalletDB.WithdrawCashWallet(Session["Username"].ToString(), convertAmount, pin, out ok, out msg);

                    if (msg == "-1")
                    {
                        Response.Write(Resources.OneForAll.OneForAll.msgUsernameNotExist);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                    else if (msg == "-2")
                    {
                        Response.Write(Resources.OneForAll.OneForAll.msgAmountNegative);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                    else if (msg == "-3")
                    {
                        Response.Write(Resources.OneForAll.OneForAll.msgInvalidPIN);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                    else if (msg.Contains("-4"))
                    {
                        string[] errMsg = msg.Split(new string[] { "," }, StringSplitOptions.None);
                        string minAmount = errMsg[1];
                        string maxAmount = errMsg[2];
                        Response.Write(string.Format(Resources.OneForAll.OneForAll.msgAmountMustBetween, minAmount, maxAmount));
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                    else if (msg == "-5")
                    {
                        Response.Write(Resources.OneForAll.OneForAll.msgEWithdrawMoreThanBalance);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                    else if (msg == "-7")
                    {
                        Response.Write(Resources.OneForAll.OneForAll.msgWithdrawOneTimeOnly);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                    else if (msg == "-8")
                    {
                        Response.Write(Resources.OneForAll.OneForAll.msgMultyplyBy10);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                    else if (msg == "-9")
                    {
                        Response.Write(Resources.OneForAll.OneForAll.warningWithdrawalpendingleft);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                ViewBag.page = "CashWithraw";
                Session["page"] = "CashWithraw";
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult WithdrawalCashLog(int selectedPage = 1)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                if (selectedPage == 0)
                {
                    selectedPage = 1;
                }

                int pages = 0;
                var model = new PaginationWalletLogModel();

                var dsCashWalletLog = new DataSet();
                dsCashWalletLog =  MemberWalletDB.GetAllCashWithdrawalLogMember(selectedPage, Session["Username"].ToString(), "0", out pages);

                selectedPage = ConstructPageList(selectedPage, pages, model);

                if (dsCashWalletLog.Tables.Count != 0)
                {
                    foreach (DataRow dr in dsCashWalletLog.Tables[0].Rows)
                    {
                        WalletLogModel walletLog = new WalletLogModel();
                        walletLog.No = dr["rownumber"].ToString();
                        walletLog.ID = int.Parse(dr["CCSHWL_ID"].ToString());
                        walletLog.Username = dr["CUSR_USERNAME"].ToString();
                        walletLog.IsChecked = false;
                        walletLog.CashOut = float.Parse(dr["CCSHWL_CASHOUT"].ToString());
                        walletLog.CountryName = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                        walletLog.Bank = dr["CCSHWL_BANKNAME"].ToString();
                        walletLog.AccNo = dr["CCSHWL_USERBANKACC"].ToString();
                        walletLog.FullName = dr["CUSR_FULLNAME"].ToString();
                        walletLog.TranState = Misc.GetWithdrawalStatus(dr["CCSHWL_TRANSTATE"].ToString());
                        walletLog.CreatedDate = Convert.ToDateTime(dr["CCSHWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                        walletLog.TranState = Convert.ToDateTime(dr["CCSHWL_UPDATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                        walletLog.refference = dr["REF_CONTENT"].ToString();
                        walletLog.refferenceID = dr["REF_ID"].ToString();
                        string Sta = dr["CCSHWL_TRANSTATE"].ToString();

                        if (Sta == "1")
                        {
                            walletLog.status = Resources.OneForAll.OneForAll.lblStatusApproved;
                        }
                        else if (Sta == "-1")
                        {
                            walletLog.status = Resources.OneForAll.OneForAll.lblStatusRefunded;
                        }
                        else if (Sta == "0")
                        {
                            walletLog.status = Resources.OneForAll.OneForAll.lblStatusPending;
                        }

                        #region Mailing Address

                        walletLog.Address = dr["CCSHWL_MAILINGADDRESS"].ToString();

                        #endregion

                        #region Bank Info

                        walletLog.BranchName = dr["CCSHWL_BRANCHNANE"].ToString();
                        walletLog.BranchAdd = dr["CCSHWL_BANKADDRESS"].ToString();
                        walletLog.BranchSwiftCode = dr["CCSHWL_BANKSWIFTCODE"].ToString();

                        walletLog.SelectedBank = dr["CBANK_CODE"].ToString();

                        #endregion
                        model.WalletLogList.Add(walletLog);
                    }
                }

                return PartialView("WithdrawalCashLog", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Shared Method

        [HttpPost]
        public JsonResult CheckNickNameTransfer(string username)
        {
            var ok = 0;
            var result = string.Empty;
            var msg = string.Empty;
            int Result = 0;

            var dataSet = AdminMemberDB.GetMemberInfoByUserName(username, out ok, out msg);

            DataSet Upline = MemberShopCenterDB.SponsorUplineCheckingForTransfer(Session["Username"].ToString(), username, out Result);

            if (Result == 0)
            {
                Response.Write(Resources.OneForAll.OneForAll.warninginvalidsponsoridtryagain);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (dataSet.Tables[0].Rows.Count == 0)
            {
                Response.Write("Invalid MemberID");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                result = Helper.NVL(dataSet.Tables[0].Rows[0]["CUSRINFO_NICKNAME"]);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private int ConstructPageList(int selectedPage, int pages, PaginationWalletLogModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 0; z < pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = (c + 1).ToString(),
                              Value = c.ToString()
                          };

            //if the selected page is -1, then set the first selected page
            if (model.Pages.Count() != 0 && selectedPage == -1)
            {
                model.Pages.First().Selected = true;
                selectedPage = int.Parse(model.Pages.First().Value);
            }
            return selectedPage;
        }

        [HttpPost]
        public JsonResult CheckNickName(string username)
        {
            var ok = 0;
            var result = string.Empty;
            var msg = string.Empty;
            var dataSet = AdminMemberDB.GetMemberInfoByUserName(username, out ok, out msg);

            if (dataSet.Tables[0].Rows.Count == 0)
            {
                Response.Write("Invalid MemberID");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                result = Helper.NVL(dataSet.Tables[0].Rows[0]["CUSRINFO_NICKNAME"]);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CheckNickNameOnlyCP(string username)
        {
            var ok = 0;
            var result = string.Empty;
            var msg = string.Empty;
            var dataSet = AdminMemberDB.GetMemberInfoByUserName(username, out ok, out msg);

            if (dataSet.Tables[0].Rows.Count == 0)
            {
                Response.Write("Invalid MemberID");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (dataSet.Tables[0].Rows[0]["CMEM_MEMBER_TYPE"].ToString() != "MobileMember")
            {
                Response.Write("Only allow to transfer to STOCKIST");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                result = Helper.NVL(dataSet.Tables[0].Rows[0]["CUSRINFO_NICKNAME"]);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Wallet Report 

        public ActionResult SponsorBonusReport(int selectedPage = -1 , string year = "0", string Month = "0")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                int pages = 0;
                var model = new PaginationWalletLogModel();

                if (Month == "Months")
                {
                    Month = "0";
                }

                if (year == "Years")
                {
                    year = "0";
                }

                DataSet dsBonusWalletLog = MemberWalletDB.GetSponsorBonusByUsername(Session["Username"].ToString(), Convert.ToInt32(year), Convert.ToInt32(Month), selectedPage, out pages);
                selectedPage = ConstructPageList(selectedPage, pages, model);

                float TotalPackageAmount = 0;
                float TotalAmount = 0;

                foreach (DataRow dr in dsBonusWalletLog.Tables[0].Rows)
                {
                    var walletLog = new WalletLogModel();
                    walletLog.Username = dr["CSPB_USERNAME"].ToString();
                    walletLog.FullName = dr["CSPB_NAME"].ToString();
                    walletLog.Rank = dr["CSPB_RANK"].ToString();
                    if (dr["CSPB_CREATEDBY"].ToString() == "SYSROLLUP")
                    {
                        walletLog.Remark = Resources.OneForAll.OneForAll.cnDirectRollUpBonus;
                    }
                    else
                    {
                        walletLog.Remark = Resources.OneForAll.OneForAll.cnSponsorBonus;
                    }

                    
                    walletLog.Amount = float.Parse(dr["CSPB_AMOUNT"].ToString()).ToString("n2");
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CSPB_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                    walletLog.BV = float.Parse(dr["CSPB_BV"].ToString()).ToString("n2"); 
                    TotalPackageAmount += float.Parse(dr["CSPB_BV"].ToString());
                    TotalAmount += float.Parse(dr["CSPB_AMOUNT"].ToString());
                    model.WalletLogList.Add(walletLog);
                }

                model.TotalPackageAmount= TotalPackageAmount.ToString("n2");
                model.TotalAmount = TotalAmount.ToString("n2");
                model.Months = Misc.ConstructsMonth().ToList();
                model.Years = Misc.ConstructYears(2013).ToList();
               
                    model.SelectedYears = year;
                    model.SelectedMonth = Month;
                

               

                return PartialView("SponsorBonusReport", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PairingBonusReport(string month = "0", string year = "0", int selectedPage = -1)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                if(month=="Months")
                {
                    month = "0";
                }

                if (year == "Years")
                {
                    year = "0";
                }

                int ok = 0;
                string message = string.Empty;
                string username = Session["Username"].ToString();

                int pages = 0;
                var model = new PaginationPairingBonusLogModel();

                if (month == "Months")
                {
                    month = "0";
                }

                if (year == "Years")
                {
                    year = "0";
                }

                var data = MemberWalletDB.GetPairingBonusByUsername(username, Convert.ToInt32(month), Convert.ToInt32(year), selectedPage, out pages, out ok, out message);

                var pageList = new List<int>();
                for (var z = 0; z < pages; z++)
                {
                    pageList.Add(z);
                }

                model.Pages = from c in pageList
                              select new SelectListItem
                              {
                                  Selected = (c.ToString() == selectedPage.ToString()),
                                  Text = (c + 1).ToString(),
                                  Value = c.ToString()
                              };

                //if the selected page is -1, then set the first selected page
                if (model.Pages.Count() != 0 && selectedPage == -1)
                {
                    model.Pages.First().Selected = true;
                    selectedPage = int.Parse(model.Pages.First().Value);
                }

                float TotalNewGroupA = 0;
                float TotalNewGroupB = 0;
                float TotalGroupA = 0;
                float TotalGroupB = 0;
                float TotalBonus = 0;
                float TotalPair = 0;

                foreach (DataRow dr in data.Tables[0].Rows)
                {
                    var pairingBonusModel = new PairingBonusLogModel();
                    pairingBonusModel.CurrentLeftSales = dr["CPAIRBONUSLOG_LEFTSALES"].ToString();
                    TotalNewGroupA += float.Parse(dr["CPAIRBONUSLOG_LEFTSALES"].ToString());

                    pairingBonusModel.CurrentRightSales = dr["CPAIRBONUSLOG_RIGHTSALES"].ToString();
                    TotalNewGroupB += float.Parse(dr["CPAIRBONUSLOG_RIGHTSALES"].ToString());

                    pairingBonusModel.TotalLeftSales = dr["CPAIRBONUSLOG_TOTOALLEFTSALES"].ToString();
                    TotalGroupA += float.Parse(dr["CPAIRBONUSLOG_TOTOALLEFTSALES"].ToString());

                    pairingBonusModel.TotalRightSales = dr["CPAIRBONUSLOG_TOTOALRIGHTSALES"].ToString();
                    TotalGroupB += float.Parse(dr["CPAIRBONUSLOG_TOTOALRIGHTSALES"].ToString());

                    pairingBonusModel.Pair = dr["CPAIRBONUSLOG_PAIR"].ToString();
                    pairingBonusModel.PairBonus = string.Format("{0:n2}", dr["CPAIRBONUSLOG_PAIRBONUS"]);
                    TotalPair += float.Parse(dr["CPAIRBONUSLOG_PAIR"].ToString());

                    TotalBonus += float.Parse(dr["CPAIRBONUSLOG_PAIRBONUS"].ToString());
                    pairingBonusModel.LogTime = Convert.ToDateTime(dr["CPAIRBONUSLOG_CREATEDON"]).ToString("dd/MM/yyyy");
                    model.ModelCollection.Add(pairingBonusModel);
                }

                model.TotalNewGroupA = TotalNewGroupA.ToString("n2") ;
                model.TotalNewGroupB = TotalNewGroupB.ToString("n2");
                model.TotalGroupA = TotalGroupA.ToString("n2");
                model.TotalGroupB = TotalGroupB.ToString("n2");
                model.TotalBonus = TotalBonus.ToString("n2");
                model.TotalPairingBonus = TotalPair.ToString("n2");

                model.Months = Misc.ConstructsMonth().ToList();
                model.Years = Misc.ConstructYears(2013).ToList();



                
                    model.SelectedYears = year;
                    model.SelectedMonth = month;
                
                    
                

                return PartialView("PairingBonusReport", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        
        public ActionResult LeadershipBonusReport(string month = "0", string year = "0", int selectedPage = -1)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                int ok = 0;
                string message = string.Empty;
                string username = Session["Username"].ToString();

                int pages = 0;
                var model = new PaginationPairingBonusLogModel();

                var data = MemberWalletDB.GetMatchingBonusByUsername(username, Convert.ToInt32(month), Convert.ToInt32(year), selectedPage, out pages, out ok, out message);

                var pageList = new List<int>();
                for (var z = 0; z < pages; z++)
                {
                    pageList.Add(z);
                }

                model.Pages = from c in pageList
                              select new SelectListItem
                              {
                                  Selected = (c.ToString() == selectedPage.ToString()),
                                  Text = (c + 1).ToString(),
                                  Value = c.ToString()
                              };

                //if the selected page is -1, then set the first selected page
                if (model.Pages.Count() != 0 && selectedPage == -1)
                {
                    model.Pages.First().Selected = true;
                    selectedPage = int.Parse(model.Pages.First().Value);
                }
                float TotalBonus = 0;

                foreach (DataRow dr in data.Tables[0].Rows)
                {
                    var pairingBonusModel = new PairingBonusLogModel();
                    pairingBonusModel.LogTime = Convert.ToDateTime(dr["CBNSWL_CREATEDON"]).ToString("dd/MM/yyyy");
                    pairingBonusModel.Username = dr["CBNSWL_APPUSER"].ToString();
                    pairingBonusModel.Name = dr["CUSR_FULLNAME"].ToString();
                    pairingBonusModel.Level = dr["CBNSWL_APPOTHER"].ToString();
                    pairingBonusModel.Amount = float.Parse(dr["CBNSWL_CASHIN"].ToString()).ToString("n2");
                    TotalBonus += float.Parse(dr["CBNSWL_CASHIN"].ToString());
                    model.ModelCollection.Add(pairingBonusModel);
                }

                model.TotalBonus = TotalBonus.ToString("n2");
                model.Months = Misc.ConstructsMonth().ToList();
                model.Years = Misc.ConstructYears(2013).ToList();

                if (year == "0")
                {
                    model.SelectedMonth = DateTime.Now.Month.ToString();
                    model.SelectedYears = DateTime.Now.Year.ToString();
                }
                else
                {
                    model.SelectedYears = year;
                    model.SelectedMonth = month;
                }    
                

                return PartialView("MatchingBonusReport", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult BonusSummaryReport(int selectedPage = -1, string year = "0", string Month = "0")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

                int pages = 0;
                var model = new PaginationWalletLogModel();

                DataSet dsBonusWalletLog = MemberWalletDB.GetBonusSummaryByUsername(Session["Username"].ToString(), Convert.ToInt32(year), Convert.ToInt32(Month), selectedPage, out pages);
                selectedPage = ConstructPageList(selectedPage, pages, model);

                float TotalSponsorBonus = 0;
                float TotalPairingBonus = 0;
                float TotalMatchingBonus = 0;
                float TotalAmount = 0;

                foreach (DataRow dr in dsBonusWalletLog.Tables[0].Rows)
                {
                    var walletLog = new WalletLogModel();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CBNSWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                    walletLog.SponsorAmount = float.Parse(dr["SPONSORBONUS"].ToString()).ToString("n2");
                    TotalSponsorBonus += float.Parse(dr["SPONSORBONUS"].ToString());
                    walletLog.PairingAmount = float.Parse(dr["PAIRINGBONUS"].ToString()).ToString("n2");
                    TotalPairingBonus += float.Parse(dr["PAIRINGBONUS"].ToString());
                    walletLog.MatchingAmount = float.Parse(dr["MATCHINGBONUS"].ToString()).ToString("n2");
                    TotalMatchingBonus += float.Parse(dr["MATCHINGBONUS"].ToString());
                    walletLog.TotalAmount = (float.Parse(dr["SPONSORBONUS"].ToString()) + float.Parse(dr["PAIRINGBONUS"].ToString()) + float.Parse(dr["MATCHINGBONUS"].ToString())).ToString("n2");
                    TotalAmount += (float.Parse(dr["SPONSORBONUS"].ToString()) + float.Parse(dr["PAIRINGBONUS"].ToString()) + float.Parse(dr["MATCHINGBONUS"].ToString()));
                    model.WalletLogList.Add(walletLog);
                }

                model.TotalSponsorAmount = TotalSponsorBonus.ToString("n2");
                model.TotalPairingAmount = TotalPairingBonus.ToString("n2");
                model.TotalMatchingAmount = TotalMatchingBonus.ToString("n2");
                model.TotalGrandAmount = TotalAmount.ToString("n2");

                model.Months = Misc.ConstructsMonth().ToList();
                model.Years = Misc.ConstructYears(2013).ToList();
                if (year == "0")
                {
                    model.SelectedYears = DateTime.Now.Year.ToString();
                    model.SelectedMonth = DateTime.Now.Month.ToString();
                }
                else
                {
                    model.SelectedYears = year;
                    model.SelectedMonth = Month;
                }

               

                return PartialView("BonusSummaryReport", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }



        #endregion

        #region Mobile User
        public ActionResult RequesTopUp()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true });
                }

                int ok = 0;
                string msg = "";
                DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                DataRow memberRow = member.Tables[0].Rows[0];

                var model = new TransferWalletModel { Charge = 0 };

                model.Wallet = string.Format("{0:n2}", memberRow["CMEM_REGISTERWALLET"]);
                model.CurrencyBuy = memberRow["CCURRENCY_BUY"].ToString();
                model.PaymentMethod = Misc.GetAllPaymentMode();

                return PartialView("RequestTopUp", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RequesTopUpMethod()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true });
                }

                string Amount = Request.Form["Amount"];
                if (string.IsNullOrEmpty(Amount))
                {
                    Response.Write("Please Key In Request Amount");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                string Remark = Request.Form["Remark"];
                string SelectedPaymentMethod = Request.Form["SelectedPaymentMethod"];
                string Pin = Request.Form["PIN"];
                string CurrencyBuy = Request.Form["CurrencyBuy"];
                string LocalAmount = (float.Parse(Amount) * float.Parse(CurrencyBuy)).ToString("0.00");



                HttpPostedFile Image = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                string ProductImagePath = "";

                if (Image != null && Image.FileName != string.Empty)
                {
                    string basePath = Server.MapPath("~/Images/Payment/");
                    if (Directory.Exists(basePath) == false)
                    {
                        Directory.CreateDirectory(basePath);
                    }
                    ProductImagePath = basePath + "\\" + Image.FileName;
                    Image.SaveAs(ProductImagePath);
                }


                int ok = 0;
                string msg = "";
                float transferAmount = 0;
                float.TryParse(Amount, out transferAmount);


                if (transferAmount % 1 > 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.lblNoDecimal);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (transferAmount % 10 != 0)
                {
                    Response.Write("Amount must be multiple of 10 ! Eg. 10,20,50,100,150 etc");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (transferAmount < 10)
                {
                    Response.Write("Minimum Amount is 10. Please try again !");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                //encrypt the password
                Pin = Authentication.Encrypt(Pin);

                MemberWalletDB.RequestWalletTopUp(Session["Username"].ToString(), transferAmount, Pin, Remark, SelectedPaymentMethod, ProductImagePath, float.Parse(LocalAmount), "RP", out ok, out msg);

                if (msg == "-1")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgUsernameNotExist);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-2")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgAmountNegative);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-3")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return RequesTopUpList();
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RequesTopUpList(int selectedPage = 1)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true });
                }

                int pages = 0;
                var model = new PaginationWalletLogModel();

                DataSet dsBonusWalletLog = MemberWalletDB.GetAllRequestTopUpListByMemberID(selectedPage, Session["Username"].ToString(), out pages);
                selectedPage = ConstructPageList(selectedPage, pages, model);

                foreach (DataRow dr in dsBonusWalletLog.Tables[0].Rows)
                {
                    var walletLog = new WalletLogModel();
                    walletLog.ID = Convert.ToInt32(dr["CTUR_ID"].ToString());
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CTUR_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                    walletLog.refference = dr["CTUR_REFFERALID"].ToString();
                    walletLog.TopUpFor = dr["CTUR_FROM"].ToString();
                    walletLog.PaymentMethod = dr["CTUR_PAYMENT_METHOD"].ToString();
                    walletLog.CountryName = dr["CTUR_COUNTRY"].ToString();
                    walletLog.Currency = dr["CTUR_CURRENCY"].ToString();
                    walletLog.LocalAmount = float.Parse(dr["CTUR_LOCAL_AMOUNT"].ToString());
                    walletLog.AmountUSD = float.Parse(dr["CTUR_AMOUNT"].ToString());
                    walletLog.Remark = dr["CTUR_REMARK"].ToString();
                    walletLog.status = dr["CTUR_STATUS"].ToString();
                    walletLog.ApproveOn = dr["CTUR_GENERATEON"].ToString();

                    model.WalletLogList.Add(walletLog);
                }

                ViewBag.Module = Resources.OneForAll.OneForAll.mnuWalletMainTab;
                ViewBag.Title = Resources.OneForAll.OneForAll.lblTransaction;
                ViewBag.Title2 = "Request Top Up List";
                return PartialView("TopUpRequestList", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CancelTopUp(int? ID)
        {
            try
            {

                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true });
                }

                // Delete existing currency
                int ok;
                string msg;



                MemberWalletDB.CancelRequestTopUp(ID, out ok, out msg);

                return RequesTopUpList();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region ROI
        public ActionResult GoldMindPackage(int selectedPage = -1, string cashname = "0")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                     return RedirectToAction("MemberLogin", "Member" , new {ReloadPage = "Yes" , Expired = "Yes"});
                }

               
                var model = new PaginationWalletLogModel();               

                DataSet dsBonusWalletLog = MemberWalletDB.GetMemberROIPackage( Session["Username"].ToString());

                float Total = 0;
                float TotalDays = 0;
                float TotalAmount2 = 0;


                foreach (DataRow dr in dsBonusWalletLog.Tables[0].Rows)
                {
                    var walletLog = new WalletLogModel();
                    walletLog.ROIPACKAGE = dr["CPKG_CODE"].ToString();
                    walletLog.Days = dr["CROI_DAYS"].ToString();
                    walletLog.Amount = float.Parse(dr["CROI_AMOUNT"].ToString()).ToString("n2");
                    walletLog.GoldMinePoint = float.Parse(dr["CROI_ORI_AMOUNT"].ToString()).ToString("n2");

                    Total = Total +  float.Parse(dr["CROI_ORI_AMOUNT"].ToString());
                    TotalAmount2 = TotalAmount2 + float.Parse(dr["CROI_AMOUNT"].ToString());
                    TotalDays = TotalDays + float.Parse(dr["CROI_DAYS"].ToString());

                    model.WalletLogList.Add(walletLog);
                }

                model.TotalAmount = Total.ToString("n2");
                model.TotalAmount2 = TotalAmount2.ToString("n2");
                model.TotalDay = TotalDays.ToString("n2");



                return PartialView("ROIPackage", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Purchase RP

        public ActionResult PurchaseRP(string Country = "CN", string Bank = "0", string Province = "0" , string Amount = "0")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }




                int ok = 0;
                string msg = "";
                DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                DataRow memberRow = member.Tables[0].Rows[0];

                var model = new TransferWalletModel { Charge = 0 };

                model.ProvinceCode = Province;
                model.BankCode = Bank;

                model.Wallet = string.Format("{0:n2}", memberRow["CMEM_REGISTERWALLET"]);
                //model.CurrencyBuy = memberRow["CCURRENCY_BUY"].ToString();
                model.CurrencySell = memberRow["CCURRENCY_SELL"].ToString();
                model.SelectedCountryCode = Country;

                var countryList = Misc.GetCountryList(Session["LanguageChosen"].ToString(), ref ok, ref msg);
                model.CountryList = from c in countryList
                                    select new SelectListItem
                                    {
                                        Selected = c.Value == Country,
                                        Text = c.Text,
                                        Value = c.Value
                                    };




                List<BankModel> banks = Misc.GetAllBankByCountryProvince(model.SelectedCountryCode, model.ProvinceCode, Session["LanguageChosen"].ToString());
                model.BankList = from c in banks
                                 select new SelectListItem
                                 {
                                     Selected = false,
                                     Text = c.BankName,
                                     Value = c.BankCode
                                 };




                List<ProvinceModel> provinces = Misc.GetAllProvinceByCountry(Session["LanguageChosen"].ToString(), model.SelectedCountryCode, ref ok, ref msg);
                model.ProvinceList = from c in provinces
                                     select new SelectListItem
                                     {
                                         Selected = false,
                                         Text = c.ProvinceName,
                                         Value = c.ProvinceCode
                                     };

                if (Bank != "0")
                {
                    DataSet dsBankSetup = AdminGeneralDB.GetBankProvinceById(Convert.ToInt32(Bank), out ok, out msg);
                    model.MinAmount = float.Parse(dsBankSetup.Tables[0].Rows[0]["CBP_MIN_AMOUNT"].ToString()).ToString("n2");
                    model.MaxAmount = float.Parse(dsBankSetup.Tables[0].Rows[0]["CBP_MAX_AMOUNT"].ToString()).ToString("n2");
                    model.AccountHolderName = dsBankSetup.Tables[0].Rows[0]["CBP_ACCOUNT_HOLDER_NAME"].ToString();
                    model.AccountNumber = dsBankSetup.Tables[0].Rows[0]["CBP_ACCOUNT_NUMBER"].ToString();
                    model.Branch = dsBankSetup.Tables[0].Rows[0]["CBP_BRANCH"].ToString();
                }

                if (Amount != "0")
                {
                    if (!string.IsNullOrEmpty(Amount))
                    {
                        float RealAmount = float.Parse(Amount);
                        float Currency = float.Parse(model.CurrencySell);
                        float Result = RealAmount / Currency;

                        model.LocalAmount = Result.ToString("n2");
                    }
                    
                }




                return PartialView("PurchaseRP", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PurchaseRPMethod()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

                string Amount = Request.Form["Amount"];
                if (string.IsNullOrEmpty(Amount))
                {
                    Response.Write("Please Key In Request Amount");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                string Remark = Request.Form["Remark"];
                //string SelectedPaymentMethod = Request.Form["SelectedPaymentMethod"];
                string Pin = Request.Form["PIN"];
                string CurrencySell = Request.Form["CurrencySell"];
                string Province = Request.Form["Province"];
                string Bank = Request.Form["Bank"];
                string LocalAmount = (float.Parse(Amount) / float.Parse(CurrencySell)).ToString("0.00");
                string Branch = Request.Form["Branch"];
                //string AccountHolderName = Request.Form["AccountHolderName"];
                //string AccountNumber = Request.Form["AccountNumber"];

                if(Province == "0")
                {
                    Response.Write("Please Select Province");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (Bank =="0")
                {
                    Response.Write("Please Select Bank");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok;
                string msg;
                float min, max;
                float requestAmt = float.Parse(Request.Form["Amount"]);
                var ds = AdminGeneralDB.GetBankProvinceByID(Bank, Session["LanguageChosen"].ToString(), out ok, out msg);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    Response.Write("No available bank at the selected province");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    min = float.Parse(ds.Tables[0].Rows[0]["CBP_MIN_AMOUNT"].ToString());
                    max = float.Parse(ds.Tables[0].Rows[0]["CBP_MAX_AMOUNT"].ToString());
                }
                
                if(requestAmt < min)
                {
                    Response.Write("Min request amount cannot less than " + min);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (requestAmt > max)
                {
                    Response.Write("Max request amount cannot more than " + max);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                HttpPostedFile Image = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                string ProductImagePath = "";

                if (Image != null && Image.FileName != string.Empty)
                {
                    string basePath = Server.MapPath("~/Images/Payment/");
                    if (Directory.Exists(basePath) == false)
                    {
                        Directory.CreateDirectory(basePath);
                    }
                    ProductImagePath = basePath + "\\" + Image.FileName;
                    Image.SaveAs(ProductImagePath);
                }


                //int ok = 0;
                //string msg = "";
                float transferAmount = 0;
                float.TryParse(Amount, out transferAmount);


                if (transferAmount % 1 > 0)
                {
                    Response.Write(Resources.OneForAll.OneForAll.lblNoDecimal);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (transferAmount % 10 != 0)
                {
                    Response.Write("Amount must be multiple of 10 ! Eg. 10,20,50,100,150 etc");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                //if (transferAmount < 10)
                //{
                //    Response.Write("Minimum Amount is 10. Please try again !");
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                //encrypt the password
                Pin = Authentication.Encrypt(Pin);

                MemberWalletDB.PurchaseRP(Session["Username"].ToString(), transferAmount, Pin, Remark, Province, Bank, ProductImagePath, float.Parse(LocalAmount), "RP", Branch, out ok, out msg);


                try
                {
                    string path = Server.MapPath(@"~/Images/MSGLogo.png"); // my logo is placed in images folder

                    string massege = string.Empty;
                    string Title = string.Empty;


                    Title = "BVI Top Up Request";
                    massege = "Member ID          :{0}<br/><br/>Amount Top Up:{1}<br/><br/>";


                    var contentSMS = string.Format(massege, Session["Username"].ToString(), transferAmount.ToString("n2"));

                    string result = Misc.SendEmailGood("noreply@BVA.com", "elweebva@hotmail.com", Title, contentSMS, path);




                }
                catch (Exception)
                {
                    // failed to send sms is ok, but better dont block the registration flow
                }

                if (msg == "-1")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgUsernameNotExist);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-2")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgAmountNegative);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else if (msg == "-3")
                {
                    Response.Write(Resources.OneForAll.OneForAll.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                //return PurchaseRPList();

                //return RedirectToAction("PurchaseRPList", "MemberWallet");

                Session["page"] = "PurchaseRPList";
                ViewBag.page = "PurchaseRPList";
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PurchaseRPList(int selectedPage = -1)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

                int pages = 0;
                var model = new PaginationWalletLogModel();

                DataSet dsBonusWalletLog = MemberWalletDB.GetAlPurchaseRPListByMemberID(selectedPage, Session["Username"].ToString(), out pages);
                selectedPage = ConstructPageList(selectedPage, pages, model);



                foreach (DataRow dr in dsBonusWalletLog.Tables[0].Rows)
                {
                    var walletLog = new WalletLogModel();
                    walletLog.ID = Convert.ToInt32(dr["CTUR_ID"].ToString());
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CTUR_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                    walletLog.refference = dr["CTUR_REFFERALID"].ToString();
                    walletLog.TopUpFor = dr["CTUR_FROM"].ToString();
                    walletLog.PaymentMethod = dr["CTUR_PAYMENT_METHOD"].ToString();
                    walletLog.CountryName = dr["CTUR_COUNTRY"].ToString();
                    walletLog.Currency = dr["CTUR_CURRENCY"].ToString();
                    walletLog.LocalAmount = float.Parse(dr["CTUR_LOCAL_AMOUNT"].ToString());
                    walletLog.AmountUSD = float.Parse(dr["CTUR_AMOUNT"].ToString());
                    walletLog.Remark = dr["CTUR_REMARK"].ToString();
                    walletLog.status = dr["CTUR_STATUS"].ToString();
                    //walletLog.ApproveOn = dr["CTUR_GENERATEON"].ToString();
                    if (walletLog.status == "Reject")
                    {
                        walletLog.ApproveOn = Convert.ToDateTime(dr["CTUR_APPROVETIME"]).ToString("dd/MM/yyyy hh:mm:ss tt"); 
                    }
                    else
                    {
                        walletLog.ApproveOn = Convert.ToDateTime(dr["CTUR_GENERATEON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                        if (Convert.ToDateTime(walletLog.ApproveOn).ToString("dd/MM/yyyy") == "01/01/1900")
                        {
                            walletLog.ApproveOn = "";
                        }
                    }
                   
                    walletLog.Province = dr["CTUR_PROVINCE"].ToString();
                    walletLog.BranchName = dr["CTUR_PROVINCE"].ToString();
                    walletLog.AccHolderName = dr["CTUR_BRANCH"].ToString();
                    walletLog.AccNo = dr["CTUR_ACCOUNT_NUMBER"].ToString();
                    walletLog.Bank = Misc.GetBankCountryProvincebyID(dr["CTUR_BANK"].ToString(), Session["LanguageChosen"].ToString());

                    model.WalletLogList.Add(walletLog);
                }


                ViewBag.Module = Resources.OneForAll.OneForAll.mnuWalletMainTab;
                ViewBag.Title = Resources.OneForAll.OneForAll.mnuRegisterwallet;
                ViewBag.Title2 = Resources.OneForAll.OneForAll.mnuMobileRequestTopUpHis;
                return PartialView("PurchaseRPList", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CancelPurchaseRP(int? ID)
        {
            try
            {

                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
                }

                // Delete existing currency
                int ok;
                string msg;



                MemberWalletDB.CancelPurchaseRP(ID, out ok, out msg);

                return PurchaseRPList();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}
