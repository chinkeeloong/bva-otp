﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECFBase.Models;
using ECFBase.Components;
using System.Data;
using System.Globalization;
using System.IO;

namespace ECFBase.Controllers.User
{
    public class UserController : Controller
    {

        #region Home

        public ActionResult Home(string register)
        {
            var url = Request.Headers["HOST"];
            var index = url.IndexOf(".");

            if (index < 0)
                return null;

            var subDomain = url.Substring(0, index);

            var um = new UserHomeModel();

            um.userIntro = subDomain;

            if (register == null || register == "")
            {
                ViewBag.ReloadURL = "Y";
            }

            return View("Home", um);
        }

        #endregion

    }
}