﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Globalization;
using ECFBase.Components;

namespace ECFBase
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.aspx/{*pathInfo}");
            routes.IgnoreRoute("Pages/");

            //Set Default Page
            //routes.Add(new WildCardRoute()); 
           
            routes.MapRoute(
            name: "Default",
            url: "{controller}/{action}/{id}",
            defaults: new { controller = "Default", action = "Index", id = UrlParameter.Optional }
            );

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            BundleTable.Bundles.RegisterTemplateBundles();
        }

        protected void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            try
            {
                Session.Timeout = 10;

                if (Session["LanguageChosen"] == null)
                {
                    Session["LanguageChosen"] = ProjectStaticString.DefaultLanguage;
                }

                System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["LanguageChosen"].ToString());
                System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["LanguageChosen"].ToString());

                
            }
            catch (Exception)
            {
                //throw;
            }
        }

    }
}