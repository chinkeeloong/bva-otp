﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace ECFBase.Models
{

    #region SponsorBonusModel

    public class SponsorBonusModelList
    {
        public List<SponsorBonusModel> SponsorBonuses { get; set; }

        public SponsorBonusModelList()
        {
            SponsorBonuses = new List<SponsorBonusModel>();
        }
    }

    public class SponsorBonusModel
    {
        /* sponsor bonus param summary */
        public int? SponID { get; set; }

        public int SponLvl { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningSponBonusValue")]
        public float SponBonus { get; set; }

        public string SponUnit { get; set; }
    }
    #endregion

    #region PairingBonusModel

    public class PairingBonusModelList
    {
        public List<PairingBonusModel> PairingBonuses { get; set; }

        public PairingBonusModelList()
        {
            PairingBonuses = new List<PairingBonusModel>();
        }
    }

    public class PairingBonusModel
    {
        /* pairing bonus param summary */
        public int? PairID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningPairBonusValue")]
        public float PairBonus { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningPairDailyCap")]
        public int PairDailyCap { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningPairCarryFwdCap")]
        public int PairDailyCarryFwdCap { get; set; }

        public string PairUnit { get; set; }
    }
    #endregion

    #region MatchingBonusModel

    public class MatchingBonusModelList
    {
        public List<MatchingBonusModel> MatchingBonuses { get; set; }

        public MatchingBonusModelList()
        {
            MatchingBonuses = new List<MatchingBonusModel>();
        }
    }

    public class MatchingBonusModel
    {
        /* matching bonus param summary */

        public int? MatchID { get; set; }

        public int MatchLvl { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningMatchingBonusValue")]
        public float MatchBonus { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningMatchingDirectSponReq")]
        public int MatchDirectSponReq { get; set; }

        public string MatchUnit { get; set; }
    }
    #endregion

    #region WorldPoolBonusModel

    public enum WorldPoolRankingType
    {
        Supervisor = 0,
        Manager,
        Director,
        President,
        Chairman,
        SponsoredMember,
        None
    }

    public class WorldPoolRanking
    {
        public WorldPoolRankingType Name { get; set; }

        public WorldPoolRanking(WorldPoolRankingType name)
        {
            Name = name;
        }
    }

    //public class WorldPoolRankingList
    //{
    //    public List<WorldPoolRanking> WorldPoolRankingTypes { get; private set; }

        //static private WorldPoolRankingList _instance;
        //static public WorldPoolRankingList Instance
        //{
        //    get
        //    {
        //        if (_instance == null)
        //        {
        //            _instance = new WorldPoolRankingList();
        //        }

        //        return _instance;
        //    }
        //}

    //    public IEnumerable<SelectListItem> GetRankList(string id = "", bool isDefaultDisplay = true)
    //    {
    //        if (id == null)
    //            return new List<SelectListItem>();

    //        if (isDefaultDisplay)
    //        {
    //            WorldPoolRankingTypes = new List<WorldPoolRanking>
    //                                {
    //                                    new WorldPoolRanking(WorldPoolRankingType.Supervisor),
    //                                    new WorldPoolRanking(WorldPoolRankingType.Manager),
    //                                    new WorldPoolRanking(WorldPoolRankingType.Director),
    //                                    new WorldPoolRanking(WorldPoolRankingType.President),
    //                                    new WorldPoolRanking(WorldPoolRankingType.Chairman)
    //                                };
    //        }
    //        else
    //        {
    //            WorldPoolRankingTypes = new List<WorldPoolRanking>
    //                                {
    //                                    new WorldPoolRanking(WorldPoolRankingType.Supervisor),
    //                                    new WorldPoolRanking(WorldPoolRankingType.Manager),
    //                                    new WorldPoolRanking(WorldPoolRankingType.Director),
    //                                    new WorldPoolRanking(WorldPoolRankingType.President),
    //                                    new WorldPoolRanking(WorldPoolRankingType.Chairman),
    //                                    new WorldPoolRanking(WorldPoolRankingType.SponsoredMember),
    //                                    new WorldPoolRanking(WorldPoolRankingType.None)
    //                                };
    //        }

    //        var ranks = WorldPoolRankingTypes.Select(m => new SelectListItem()
    //        {
    //            Text = m.Name.ToString(),
    //            Value = m.Name.ToString(),
    //            Selected = id != "" && id.ToUpper() == m.Name.ToString().ToUpper()
    //        }).ToList();

    //        return ranks;
    //    }
    //}
    public class WorldPoolBonusModelList
    {
        public IEnumerable<SelectListItem> RankList { get; set; }
        public string SelectedRanking { get; set; } 
        public List<WorldPoolBonusModel> WorldPoolBonuses { get; set; }

        public WorldPoolBonusModelList()
        {
            WorldPoolBonuses = new List<WorldPoolBonusModel>();
        }
    }

    public class WorldPoolBonusModel
    {
        /* world pool param summary */

        public int? WorldPoolID { get; set; }

        public string WorldPoolRank { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningWPoolBonusValue")]
        public float WorldPoolBonus { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningWPoolEntitle")]
        public int WorldPoolBonusEntitlement { get; set; }

        //World Pool - qualification for rank
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "WarningWPoolSales")]
        public int WorldPoolQlfSalesNeeded { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningWPoolRankAmt")]
        public int WorldPoolQlfRankAmountReq { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningRankingType")]
        public string WorldPoolQlfRankingTypeReq { get; set; }

        public string WorldPoolBonusUOM { get; set; }
    }


    #endregion

    #region CurrentSales/Bonus Payout Summary
    public class PaginationCurrentSalesModel
    {
        public List<CurrentSalesModel> CurrentSalesModel { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public IEnumerable<SelectListItem> Countries { get; set; }
        public IEnumerable<SelectListItem> RankList { get; set; }
        
        public string SelectedCountry { get; set; }
        public string SelectedRank { get; set; }

        public string TotalPairing { get; set; }
        public string TotalSponsor { get; set; }
        public string TotalMatching { get; set; }
        public string TotalAmount { get; set; }
        public string DateFrom  { get; set; }
        public string DateTo { get; set; }

        public string TotalRegisterOut { get; set; }
        public string TotalUpgradeOut { get; set; }
        public string TotalExgMP { get; set; }
        public string TotalTransferMember { get; set; }
        public string TotalTransferFromCP { get; set; }
        public string TotalTopUpIn { get; set; }
        public string TotalAdjOut { get; set; }
        public string TotalTotalIn { get; set; }
        public string TotalTotalOut { get; set; }
        public string TotalTotalBalance { get; set; }
        public string TotalWithdrawal { get; set; }
        public string TotalTransferRP { get; set; }
        public string TotalTransferMP { get; set; }


        public PaginationCurrentSalesModel()
        {
            CurrentSalesModel = new List<CurrentSalesModel>();
        }
    }

    public class CurrentSalesModel
    {
        public string MemberID { get;set; }
        public string SalesDate { get; set; }
        public float MemberUpgrade { get; set; }
        public float NewRegSalesAmount { get; set; }
        public float TotalSalesInBVAmount { get; set; }
        public float MaintainanceSalesAmount { get; set; }
        public float TopupPVSalesAmount { get; set; }
        public float TotalSalesAmount { get; set; }
        public float TopupBonusAmount { get; set; }
        public float SponsorBonusAmount { get; set; }
        public float PairingBonusAmount { get; set; }
        public float MatchingBonusAmount { get; set; }
        public float WorldPoolBonusAmount { get; set; }
        public float TotalBonusAmount { get; set; }
        public float UniLevelAmount { get; set; }
        public float RegisterRpoint { get; set; }
        public float RegisterCRpoint { get; set; }
        public float UpgradeRpoint { get; set; }
        public float UpgradeCRpoint { get; set; }
        public string Name { get; set; }
        public string ID { get; set; }
        public string Rank { get; set; }
        public string Package { get; set; }
        public string RP { get; set; }
        public string CRP { get; set; }
        public string Total { get; set; }
        public float EPBV { get; set; }
        public float SPBV { get; set; }
        public float WRPBV { get; set; }
        public float Rpoint { get; set; }
        public float CRpoint { get; set; }

        public float RPP { get; set; }
        public float GP { get; set; }

        public float CP { get; set; }
        public float PP { get; set; }

   
        public float RegisterOut { get; set; }
        public float UpgradeOut { get; set; }
        public float ExgMP { get; set; }
        public float TransferMember { get; set; }
        public float TransferFromCP { get; set; }
        public float TopUpIn { get; set; }
        public float AdjOut { get; set; }
        public float TotalIn { get; set; }
        public float TotalOut { get; set; }
        public float TotalBalance { get; set; }
        public float Withdraw { get; set; }
        public float TransferRP { get; set; }
        public float TransferMP { get; set; }
        public string Country { get; set; }
        public string Quantity { get; set; }

        public CurrentSalesModel()
        {
            NewRegSalesAmount = 0;
            MaintainanceSalesAmount = 0;
            TopupPVSalesAmount = 0;
            TotalSalesAmount = 0;
            TopupBonusAmount = 0;
            SponsorBonusAmount = 0;
            PairingBonusAmount = 0;
            MatchingBonusAmount = 0;
            WorldPoolBonusAmount = 0;
            TotalSalesInBVAmount = 0;
            UniLevelAmount = 0;
        }
    }

    public class sortOnSalesDate : IComparer<CurrentSalesModel>
    {
        public int Compare(CurrentSalesModel a, CurrentSalesModel b)
        {
            DateTime dtA = DateTime.ParseExact(a.SalesDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            DateTime dtB = DateTime.ParseExact(b.SalesDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            if (dtA.Date > dtB.Date) return 1;
            else if (dtA.Date < dtB.Date) return -1;
            else return 0;
        }
    }

    #endregion
}