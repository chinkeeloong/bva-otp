﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ECFBase.Models
{
    public class MemberPermissionModel : MemberBaseModel
    {
        public string UserName { get; set; }
        public string UserFullName { get; set; }
        public bool IsBlocked { get; set; }
    }

    public class MemberBaseModel
    {
        public DateTime DateJoined;
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public float BonusWallet;
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public float PVWallet;
        public string MemberPhoto;
    }

    public class ChangePasswordModel : MemberBaseModel
    {
        public string UserName { get; set; }

        public string TotalUserID { get; set; }
        public string TotalMembers { get; set; }
        public string TotalReserved { get; set; }
        public string TotalBasic { get; set; }
        public string TotalAdvance { get; set; }
        public string TotalPremium { get; set; }
        public string TotalElite { get; set; }
        public string TotalEnterprise { get; set; }
        public IEnumerable<SelectListItem> Countries { get; set; }
        public string SelectedCountry { get; set; }
        public string UserEmail { get; set; }
        public string UserMobile { get; set; }
        public string UserIC { get; set; }
        public string UserFullName { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgReqCurrPwd")]
        public string CurrentPassword { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgReqNewPwd")]
        public string NewPassword { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningConfirmNewPwd")]
        public string ConfirmNewPassword { get; set; }
        public bool NoRedirectionRequired { get; set; }
    }

    public class ChangeRankingModel : MemberBaseModel
    {
        public List<SelectListItem> RankList { get; set; }
        public string UserName { get; set; }
        public string SelectedRanking { get; set; }        
        public string CurrentRank { get; set; }
    }

    public class ChangeOwnershipModel : MemberBaseModel
    {
        public string Username { get; set; }
        public string SurName { get; set; }
        public string FirstName { get; set; }
    }

    public class ChangeSponsorModel : MemberBaseModel
    {
        public string Username { get; set; }
        public string CurrIntro { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningNewIntro")]
        public string NewIntro { get; set; }

    }

    public class ChangeUplineModel : MemberBaseModel
    {
        public string Username { get; set; }
        public string NewUpline { get; set; }
        public string CurrentUpline { get; set; }
        public string SelectedPosition { get; set; }
        public List<SelectListItem> Position { get; set; }

    }

    public class MultiAccount
    {
        public string Username { get; set; }
        public string Nickname { get; set; }
        public string NewSubAccount { get; set; }
        public string MasterAccount { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public List<MultiAccountSub> MemberList { get; set; }

        public MultiAccount()
        {
            MemberList = new List<MultiAccountSub>();
            MasterAccount = "";
        }
    }

    public class MultiAccountSub
    {
        public string MultiID { get; set; }
        public string No { get; set; }
        public string MemberID { get; set; }
        public string Nickname { get; set; }
        public string Admin { get; set; }
        public string Date { get; set; }
    }

    public class ChangeCenterModel : MemberBaseModel
    {
        public string Username { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningLeftMember")]
        public string LeftMember { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningRightMember")]
        public string RightMember { get; set; }
    }

    public class ChangeMemberTreeModel : MemberBaseModel
    {
        public string Username { get; set; }
        public string LeftMember { get; set; }
        public string RightMember { get; set; }
    }

    public class ManualAdjustMemberAccountModel
    {
        public string FromMember { get; set; }
        public string ToMember { get; set; }
        public float Amount { get; set; }
        public string Reason { get; set; }

        public string SelectedWalletType { get; set; }
        public IEnumerable<SelectListItem> WalletType { get; set; }
       
        public string SelectedAdjustType { get; set; }
        public IEnumerable<SelectListItem> AdjustType { get; set; }
    }

}