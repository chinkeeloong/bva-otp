﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Data.Objects;

namespace ECFBase.Models
{
    #region Admin - Home

    public class AdminHomeModel
    {
        public string eWallet { get; set; }
        public string pvWallet { get; set; }
        public bool Premember { get; set; }
        public IEnumerable<SelectListItem> LanguageList { get; set; }
        public string AdminSelectedLanguage { get; set; }
        public List<ImageFile> ImageList { get; set; }

        public AdminHomeModel()
        {
            LanguageList = new List<SelectListItem>();
            ImageList = new List<ImageFile>();
        }
       

    } 
    public class ImageFile
            {
                public string FileName { get; set; }
                public string FileTitle { get; set; }
                public string FileSize { get; set; }
            }
  
    #endregion
}