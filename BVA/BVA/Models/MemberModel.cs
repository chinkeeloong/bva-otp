﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using ECFBase.Components;
using System.Web.Mvc;

namespace ECFBase.Models
{
    #region PaginationMemberModel
    public class PaginationMemberModel
    {
        public List<MemberModel> MemberList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }
        public IEnumerable<SelectListItem> Days { get; set; }
        public IEnumerable<SelectListItem> Months { get; set; }
        public IEnumerable<SelectListItem> Years { get; set; }
        public IEnumerable<SelectListItem> FilteringCriteria { get; set; }


        public string MegaPack { get; set; }
        public string MemberType { get; set; }
        public string SelectedDays { get; set; }
        public string SelectedMonth { get; set; }
        public string SelectedYears { get; set; }
        public string SelectedFilteringCriteria { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime? StartDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime? EndDate { get; set; }
        public double SponsorAmount { get; set; }
        public string SearchFirstName { get; set; }

        public List<EditMemberModel> EditMember { get; set; }




        public PaginationMemberModel()
        {
            MemberList = new List<MemberModel>();
            EditMember = new List<EditMemberModel>();
        }
    }
    #endregion

    #region MemberModel
    public class MemberModel
    {
        public string Withdrawal { get; set; }
        public string CPTORP { get; set; }
        public string CPTOMP { get; set; }

        public string SecondGrandTotal { get; set; }

        public string SellEFund { get; set; }
        public string DirectBonus { get; set; }
        public string DirectRollUp  { get; set; }
        public string GroupBonus { get; set; }
        public string TradingBonus { get; set; }
        public string GrandTotal { get; set; }
        public string Premember { get; set; }
        public string Number { get; set; }
        public string MemberId { get; set; }
        public string MemberFrom { get; set; }
        public int HasMultipleAccFunction { get; set; }
        public bool NewMember { get; set; }
        public bool Mwallet { get; set; }
        public bool Block { get; set; }
        public string CRP { get; set; }
        public string InvoiceID { get; set;  }

        public string BTC { get; set; }
        public string ETH { get; set; }

        public string BTCRate { get; set; }
        public string ETHRate { get; set; }

        public bool WRP { get; set; }

        public string TotalMember01 { get; set; }
        public string TotalMember02 { get; set; }
        public string TotalMember03 { get; set; }
        public string TotalMember04 { get; set; }

        public string WRPSubAcc01 { get; set; }
        public string WRPSubAcc02 { get; set; }
        public string WRPSubAcc03 { get; set; }
        public string WRPSubAcc04 { get; set; }

        public string WRPMax01 { get; set; }
        public string WRPMax02 { get; set; }
        public string WRPMax03 { get; set; }
        public string WRPMax04 { get; set; }

        public string WRPPay01 { get; set; }
        public string WRPPay02 { get; set; }
        public string WRPPay03 { get; set; }
        public string WRPPay04 { get; set; }

        public string WRPBal01 { get; set; }
        public string WRPBal02 { get; set; }
        public string WRPBal03 { get; set; }
        public string WRPBal04 { get; set; }

        public string WRP1Meber01 { get; set; }
        public string WRP1Meber02 { get; set; }
        public string WRP1Meber03 { get; set; }
        public string WRP1Meber04 { get; set; }

        public string WRP1SubAcc01 { get; set; }
        public string WRP1SubAcc02 { get; set; }
        public string WRP1SubAcc03 { get; set; }
        public string WRP1SubAcc04 { get; set; }

        public string WRP1Max01 { get; set; }
        public string WRP1Max02 { get; set; }
        public string WRP1Max03 { get; set; }
        public string WRP1Max04 { get; set; }

        public string WRP1Total01 { get; set; }
        public string WRP1Total02 { get; set; }
        public string WRP1Total03 { get; set; }
        public string WRP1Total04 { get; set; }

        public string WRP1Pay01 { get; set; }
        public string WRP1Pay02 { get; set; }
        public string WRP1Pay03 { get; set; }
        public string WRP1Pay04 { get; set; }

        public string WRP1Bal01 { get; set; }
        public string WRP1Bal02 { get; set; }
        public string WRP1Bal03 { get; set; }
        public string WRP1Bal04 { get; set; }

        public string WRP2Meber01 { get; set; }
        public string WRP2Meber02 { get; set; }
        public string WRP2Meber03 { get; set; }
        public string WRP2Meber04 { get; set; }

        public string WRP2SubAcc01 { get; set; }
        public string WRP2SubAcc02 { get; set; }
        public string WRP2SubAcc03 { get; set; }
        public string WRP2SubAcc04 { get; set; }

        public string WRP2Max01 { get; set; }
        public string WRP2Max02 { get; set; }
        public string WRP2Max03 { get; set; }
        public string WRP2Max04 { get; set; }

        public string WRP2Total01 { get; set; }
        public string WRP2Total02 { get; set; }
        public string WRP2Total03 { get; set; }
        public string WRP2Total04 { get; set; }

        public string WRP2Pay01 { get; set; }
        public string WRP2Pay02 { get; set; }
        public string WRP2Pay03 { get; set; }
        public string WRP2Pay04 { get; set; }

        public string WRP2Bal01 { get; set; }
        public string WRP2Bal02 { get; set; }
        public string WRP2Bal03 { get; set; }
        public string WRP2Bal04 { get; set; }
        public List<ImageFile> ImageList { get; set; }



        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgReqUsername")]
        public string Username { get; set; }
        public string UsernameID { get; set; }

        public string SelectedOption { get; set; }
        public string PaymentOption { get; set; }

        public IEnumerable<SelectListItem> Package { get; set; }
        public string SelectedPackage { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgReqPwd")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string EncryptedPassword { get; set; }

        //[DataType(DataType.Password)]
        //[Compare("Password", ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgPwdNotMatch")]
        //public string ConfirmPassword {get;set;}

        [DataType(DataType.Password)]
        public string CurrentPassword { get; set; }
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }
        [DataType(DataType.Password)]
        public string ConfirmNewPassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgReqPIN")]
        [DataType(DataType.Password)]
        public string Pin { get; set; }
        public string EncryptedPin { get; set; }
        
        //[DataType(DataType.Password)]
        //[Compare("Pin", ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgPINNotMatch")]
        //public string ConfirmPin { get; set; }

        [DataType(DataType.Password)]
        public string CurrentPin { get; set; }
        [DataType(DataType.Password)]
        public string NewPin { get; set; }
        [DataType(DataType.Password)]
        public string ConfirmNewPin { get; set; }


        public string SubAcc { get; set; }
        public string QueNo { get; set; }
        public string MaxWRP { get; set; }
        public string PaidWRP { get; set; }
        public string BalWRP { get; set; }



        
        public string SurName { get; set; }
        public string JPName { get; set; }
        public string JPPostCode { get; set; }
        public string JPAddress { get; set; }
        public string JPAddress1 { get; set; }
        public string JPAddress2 { get; set; }
        public string JPAddress3 { get; set; }

        public string MemberType { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgReqFullname")]
        public string FirstName { get; set; }

        public string PackageName { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float ProductQty { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float BonusWallet { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float RegisterWallet { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float GoldPointWallet { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]

        public float GoldPointOnly { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float MultiPointWallet { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]

        
        public float CRPWallet { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float RMPWallet { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float RPCWallet { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        
        public float CompanyWallet { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float CashWallet { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float ShareWallet { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float ShopWallet { get; set; }

        public string RPPoint { get; set; }


        public string EshareUnit { get; set; }

        public int CompanyPaymentWallet { get; set; }
        public int BonusPaymentWallet { get; set; }
        public int RegisterPaymentWallet { get; set; }

        public int RegisterMPPaymentWallet { get; set; }
        public float FCompanyPaymentWallet { get; set; }
        public float FBonusPaymentWallet { get; set; }
        public float FRegisterPaymentWallet { get; set; }
        public float FMultiPointWallet { get; set; }

        public string SCompanyPaymentWallet { get; set; }
        public string SBonusPaymentWallet { get; set; }
        public string SRegisterPaymentWallet { get; set; }
        public string SRegisterMPPaymentWallet { get; set; }

        public string RP { get; set; }
        public string RMP { get; set; }

        public List<PackageModel> Packages { get; set; }

        public bool Packagechecking { get; set; }

        public string BankAccountNo { get; set; }
        public string BankHolderName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgReqIntro")]
        public string Intro { get; set; }
        public string SelectedOwnAccount { get; set; }

        public IEnumerable<SelectListItem> OwnAccount { get; set; }

        public string IntroFullName { get; set; }

        public bool MegaPack { get; set; }

        public string Mega { get; set; }

        public string Invest { get; set; }
        public string Upline { get; set; }
        public string RegisteredBy { get; set; }
        public string Rank { get; set; }

        public string PackageAmount { get; set; }
        public string DaysForGold { get; set; }
        public string DaysForPlatinum { get; set; }
        public string CountryCode { get; set; }
        public IEnumerable<SelectListItem> UpgradePackageList { get; set; }
        public string SelectedUpgradePkg { get; set; }
        public string PackageCode { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime JoinedDate { get; set; }

        public DateTime? UpgradeDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public string JoinedDateString { get; set; }
        public string Address { get; set; }
        public bool UserActive { get; set; }
        public string MemberPhoto { get; set; }
        public float TopUpUpgradeVIP { get; set; }
        public string MemberCurrency { get; set; }
        public string MemberCountry { get; set; }
        public string MemberPackage { get; set; }
        public string Nickname { get; set; }
        public string CurrentRankDate { get; set; }
        public string PreviousRank { get; set; }
       // public string CurrentRankDate { get; set; }
        public float Quantity { get; set; }

        public string CurrentPackage { get; set; }

        public string IC { get; set; }
        /*member image*/
        public string MemberImageName { get; set; }
        public string MemberImagePath { get; set; }
        public HttpPostedFileBase MemberImage { get; set; }
        
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgEmailInvalid")]
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgReqEmail")]
        [Display(Name = "Email address*")]
        public string MemberEmail { get; set; }
        public string ResetPasswordPin { get; set; }

        public int PPUNIT { get; set; }

        public IEnumerable<SelectListItem> LanguageList { get; set; }
        public string MemberSelectedLanguage { get; set; }

        #region homePage
        public string TotalOCT { get; set;   }
        public bool OperationLogShow { get; set; }
        public string OperationLog { get; set; }
        public string TotalDirectSponsor { get; set; }
        public string TotalNewSponsor { get; set; }
        public int NewMessage { get; set; }
        public int TotalMessages { get; set; }
        public Ranking Ranking { get; set; }
        public MaintenanceStatus MaintenanceStatus { get; set; }
        public int NewSponsor { get; set; }
        public float TotalTransfer { get; set; }
        public float TotalWithdrawal { get; set; }
        public float TotalMaintenance { get; set; }
        public int TotalLeftMembers { get; set; }
        public int TotalRightMembers { get; set; }
        public int TotalCurrentLeftMembers { get; set; }
        public int TotalCurrentRightMembers { get; set; }
        public int TotalLeftSupervisor { get; set; }
        public int TotalRightSupervisor { get; set; }
        public int TotalLeftManager { get; set; }
        public int TotalRightManager { get; set; }
        public int TotalLeftDirector { get; set; }
        public int TotalRightDirector { get; set; }
        public int TotalLeftPresident { get; set; }
        public int TotalRightPresident { get; set; }
        public int TotalLeftChairman {get; set;}
        public int TotalRightChairman { get; set; }
        public int CurrentLeftSalesVolume { get; set; }
        public int CurrentRightSalesVolume { get; set; }
        public List<InfoDeskModel> EventAndPromotions { get; set; }
        public List<InfoDeskModel> CorporateNews { get; set; }
        public List<InfoDeskModel> MyNews { get; set; }
        public List<SubAcc> SubAccount { get; set; }

        public string CurrentPersonalBV { get; set; }
        public string CurrentDPGBV { get; set; }
        public string CurrentGroupBV { get; set; }
        public MaintenanceStatus CurrentIsMaintain { get; set; }

        public string PreviousPersonalBV { get; set; }
        public string PreviousDPGBV { get; set; }
        public string PreviousGroupBV { get; set; }
        public MaintenanceStatus PreviousIsMaintain { get; set; }

        public List<CurrencySetupModel> Currency { get; set; }
        
        #endregion


      

        #region Share
        //public string ShareRate { get; set; }


        //public string test { get; set; }


        //public string Value { get; set; }


        public string ShareValue { get; set; }

        public string ShareUnitBalance { get; set; }
        public string sales { get; set; }

        //public string FullUnitRate { get; set; }
        //public string FullUnit { get; set; }
        public string SalesIndex { get; set; }
        //public string CurrentRate { get; set; }
        public string OpeningPrices { get; set; }
        public string LowerPrices { get; set; }
        public string HigherPrices { get; set; }
        public string TodayPrices { get; set; }
        public string Swallet { get; set; }
        public string TotalUnit { get; set; }
        public string TotalUnitInNumber { get; set; }
        public string TotalUnitPrices { get; set; }
        //public string CountDown { get; set; }
        //public string CompanyShare { get; set; }
        //public string MemberShare { get; set; }
        //public string UnitLeft { get; set; }
        //public string CompanyShareValue { get; set; }
        //public string MemberShareValue { get; set; }
        //public string ValueTotal { get; set; }
        public string LasttimePrices { get; set; }
        //public string ShareTotal { get; set; }
        //public string UnitTotal { get; set; }
        public string profit { get; set; }
        public string percentage { get; set; }
        public string Totalsellunit { get; set; }
        //public string TotalUnitBalance { get; set; }
        public string ShareQuantity { get; set; }

        //public string BuyUnitTotal { get; set; }
        //public string SellUnitTotal { get; set; }

        //public string FilterRate { get; set; }

        public string TheRate { get; set; }
        public string Fee { get; set; }
        public string Total { get; set; }
        public string EPin { get; set; }
        public string MaxSales { get; set; }

        //public string FullName { get; set; }

        //public string MemberID { get; set; }

        public List<ShareListLogModel> ShareList { get; set; }

        public List<ShareListLogModel> PersonalSalesInformation { get; set; }

        public List<ShareListLogModel> GroupSalesInformation { get; set; }

        public List<ShareListLogModel> MemberPreTreadeList { get; set; }

        public IEnumerable<SelectListItem> Pages { get; set; }

        #endregion

        public MemberModel()
        {
            

            Packages = new List<PackageModel>();
            LanguageList = new List<SelectListItem>();
            UpgradePackageList = new List<SelectListItem>();
            TotalTransfer = 0;
            TotalWithdrawal = 0;
            TotalMaintenance = 0;
            TotalLeftMembers = 0;
            TotalRightMembers = 0;
            TotalLeftSupervisor = 0;
            TotalRightSupervisor = 0;
            TotalLeftManager = 0;
            TotalRightManager = 0;
            TotalLeftDirector = 0;
            TotalRightDirector = 0;
            TotalLeftPresident = 0;
            TotalRightPresident = 0;
            TotalLeftChairman = 0;
            TotalRightChairman = 0;
            CurrentLeftSalesVolume = 0;
            CurrentRightSalesVolume = 0;
            TotalDirectSponsor = "0";
            TotalNewSponsor = "0";
            Ranking = Ranking.rankAgent;
            MaintenanceStatus = Components.MaintenanceStatus.Qualify;
            EventAndPromotions = new List<InfoDeskModel>();
            CorporateNews = new List<InfoDeskModel>();
            MyNews = new List<InfoDeskModel>();
            Currency = new List<CurrencySetupModel>();
            SubAccount = new List<SubAcc>();
            ShareList = new List<ShareListLogModel>();
            PersonalSalesInformation = new List<ShareListLogModel>();
            GroupSalesInformation = new List<ShareListLogModel>();
            MemberPreTreadeList = new List<ShareListLogModel>();
            ImageList = new List<ImageFile>();
        }
    }

    #endregion

  

    #region PaginationMemberWalletModel
    public class PaginationMemberWalletModel
    {
        public List<MemberWalletModel> MemberWalletList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }
        public IEnumerable<SelectListItem> Months { get; set; }
        public IEnumerable<SelectListItem> Years { get; set; }
        public IEnumerable<SelectListItem> FilteringCriteria { get; set; }

        public string SelectedMonth { get; set; }
        public string SelectedYears { get; set; }
        public string SelectedFilteringCriteria { get; set; }

        public string RegisterWallet { get; set; }

        public string CompanyWallet { get; set; }
        public string CashWallet { get; set; }
        public string GoldPoint { get; set; }
        public string MultiPoint { get; set; }
        public string RPCWallet { get; set; }
        public string TotalTotal { get; set; }

        public string RMPWallet { get; set; }

        public string TotalRegisterWallet { get; set; }

        public string TotalCompanyWallet { get; set; }
        public string TotalCashWallet { get; set; }
        public string TotalGoldPoint { get; set; }
        public string TotalMultiPoint { get; set; }

        public string TotalRMPWallet { get; set; }
        public string TotalRPCWallet { get; set; }

        public string TotalCashIn { get; set; }
        public string TotalCashOut { get; set; }

        public PaginationMemberWalletModel()
        {
            MemberWalletList = new List<MemberWalletModel>();
        }
    }
    #endregion

    #region MemberWalletModel
    public class MemberWalletModel
    {
        public string Number { get; set; }
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string Rank { get; set; }
        
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float RegisterWallet { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float CashWallet { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float CompanyWallet { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float ShareWallet { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float BonusWallet { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float MaintainWallet { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float CarWallet { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float ShopWallet { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float ProductWallet { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float GoldPoint { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float MultiPoint { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float RPCWallet { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float ICOWallet { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float Total { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float RMPWallet { get; set; }
        
    }
    #endregion

    #region MarketTreeModel
    public class MarketTreeModel
    {
        public List<string> MemberList { get; set; }
        public List<string> LogoList { get; set; }
        public List<string> TooltipList { get; set; }
        public List<string> PackageList { get; set; }
        public List<string> DateList { get; set; }
        public string MainUser { get; set; }

        public List<string> AccYJLeftList { get; set; }
        public List<string> AccYJRightList { get; set; }
        public List<string> CFBalLeftList { get; set; }
        public List<string> CFBalRightList { get; set; }
        public List<string> SalesLeftList { get; set; }
        public List<string> SalesRightList { get; set; }

        public List<string> TotalDownlineLeft { get; set; }
        public List<string> TotalDownlineRight { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningTotalLeftSalesMandatory")]
        //[Required(ErrorMessage = "New Total Left Sales is mandatory.")]
        public string MainLeftYJ { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningTotalRightSalesMandatory")]
        //[Required(ErrorMessage = "New Total Right Sales is mandatory.")]
        public string MainRightYJ { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningBalanceLeftMandatory")]
        //[Required(ErrorMessage = "New Balance Left is mandatory.")]
        public string MainLeftBalance { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningBalanceRightMandatory")]
        //[Required(ErrorMessage = "New Balance Right is mandatory.")]
        public string MainRightBalance { get; set; }
        public string MainRightSales { get; set; }
        public string MainLeftSales { get; set; }

        public string FirstLeftYJ { get; set; }
        public string FirstRightYJ { get; set; }
        public string FirstLeftBalance { get; set; }
        public string FirstRightBalance { get; set; }
        public string FirstRightSales { get; set; }
        public string FirstLeftSales { get; set; }
        public string SecondLeftYJ { get; set; }
        public string SecondRightYJ { get; set; }
        public string SecondLeftBalance { get; set; }
        public string SecondRightBalance { get; set; }
        public string SecondRightSales { get; set; }
        public string SecondLeftSales { get; set; }

        public string SecondRightTotalDownline { get; set; }
        public string SecondLeftTotalDownline { get; set; }

        public string Stockist { get; set; }

        public string country { get; set; }

        public string Picture { get; set; }

        public List<RankModel> RankList { get; set; }
        public string RankIconName { get; set; }
        public string RankIconPath { get; set; }
        public HttpPostedFileBase RankIcon { get; set; }

        public string FirstLevelUsername { get; set; }
        public string FirstLevelFullName { get; set; }
        public string FirstLevelSponsor { get; set; }
        public string FirstLevelUpline { get; set; }
        public string FirstLevelLevel { get; set; }
        public string FirstLevelRank { get; set; }
        public string FirstLevelCreatedDate { get; set; }
        public string FirstLevelRankIcon { get; set; }
        public MarketTreeModel()
        {
            MemberList = new List<string>();
            LogoList = new List<string>();
            TooltipList = new List<string>();
            PackageList = new List<string>();
            DateList = new List<string>();
            RankList = new List<RankModel>();
            SponsorList = new List<IntroListModel>();
            AccYJLeftList = new List<string>();
            AccYJRightList = new List<string>();
            CFBalLeftList = new List<string>();
            CFBalRightList = new List<string>();
            SalesLeftList = new List<string>();
            SalesRightList = new List<string>();
            TotalDownlineLeft = new List<string>();
            TotalDownlineRight = new List<string>();
        }
        public List<IntroListModel> SponsorList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }
    }
    #endregion

    #region SHARE
    public class ShareLogModel
    {
        public string ShareRate { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string SalesIndex { get; set; }

        public string test { get; set; }

        public List<InfoDeskModel> CorporateNews { get; set; }

        public string Value { get; set; }


        public string ShareValue { get; set; }

        public string ShareUnitBalance { get; set; }
        public string sales { get; set; }

        public string FullUnitRate { get; set; }
        public string FullUnit { get; set; }

        public string CurrentRate { get; set; }
        public string OpeningPrices { get; set; }
        public string LowerPrices { get; set; }
        public string HigherPrices { get; set; }
        public string TodayPrices { get; set; }

        public string TotalRateUp { get; set; }
        public string Swallet { get; set; }
        public string TotalUnit { get; set; }


        public string CurrentUnit { get; set; }
        public string AvailableUnit { get; set; }

        public string AvailableUnitString { get; set; }
        public string PackageAmount { get; set; }

        public string TradingOrder { get; set; }
        public string KeyinUnit { get; set; }

        public string SelectedRate { get; set; }

        public string GoldPointBalance { get; set; }
        public string RPCPointBalance { get; set; }
        public string HiddenGoldPointBalance { get; set; }
        public string HiddenRPCPointBalance { get; set; }
        public string TotalUnitInNumber { get; set; }
        public string TotalUnitPrices { get; set; }
        public string CountDown { get; set; }
        public string CompanyShare { get; set; }
        public string MemberShare { get; set; }
        public string UnitLeft { get; set; }

        public string Oversubscribed { get; set; }
        public string OversubscribedUnit { get; set; }
        public string CompanyShareValue { get; set; }
        public string MemberShareValue { get; set; }
        public string ValueTotal { get; set; }
        public string LasttimePrices { get; set; }
        public string ShareTotal { get; set; }
        public string UnitTotal { get; set; }
        public string profit { get; set; }
        public string percentage { get; set; }
        public string Totalsellunit { get; set; }
        public string TotalUnitBalance { get; set; }
        public string BuyAmount { get; set; }
        public string BuyUnitTotal { get; set; }
        public string SellUnitTotal { get; set; }

        public string FilterRate { get; set; }

        public string BuyQuantity { get; set; }
        public string BuyTheRate { get; set; }

        public string RATEMAX { get; set; }
        public string BuyFee { get; set; }
        public string BuyTotal { get; set; }
        public string BuyEPin { get; set; }
        public string Quantity { get; set; }
        //public string TheRate { get; set; }

        public IEnumerable<SelectListItem> TheRate { get; set; }
        public string Fee { get; set; }
        public string Total { get; set; }

        public string TotalBalanceUnit { get; set; }
        public string TotalOriUnit { get; set; }
        public string EPin { get; set; }
        public string MaxSales { get; set; }

        public string FullName { get; set; }

        public string MemberID { get; set; }

        public string CurrentSalesIndex { get; set; }
        public string DeductVolume { get; set; }
        public string LatestSalesIndex { get; set; }


        public List<ShareListLogModel> ShareList { get; set; }

        public List<ShareListLogModel> PersonalSalesInformation { get; set; }

        public List<ShareListLogModel> GroupSalesInformation { get; set; }

        public List<ShareListLogModel> MemberPreBuyList { get; set; }

        public List<ShareListLogModel> MemberPreTreadeList { get; set; }

        public IEnumerable<SelectListItem> Pages { get; set; }
        public ShareLogModel()
        {
            ShareList = new List<ShareListLogModel>();
            PersonalSalesInformation = new List<ShareListLogModel>();
            GroupSalesInformation = new List<ShareListLogModel>();
            MemberPreTreadeList = new List<ShareListLogModel>();
            MemberPreBuyList = new List<ShareListLogModel>();

        }
    }

    public class ShareListLogModel
    {

        public string MemberSelling { get; set; }
        public string CompanySelling { get; set; }
        public string TotalSelling { get; set; }
        
        public string CompanyEatUnit { get; set; }

        public string ShareUnit { get; set; }


        public string OriUnit { get; set; }
        public float SharePrices { get; set; }
        public string ShareValue { get; set; }
        public string PlaceOrderDate { get; set; }
        public string Option { get; set; }
        public string Amount { get; set; }
        public string Quantity { get; set; }
        public string SellOrderQuantity { get; set; }
        public string DataID { get; set; }
        public string Date { get; set; }
        public string ID { get; set; }
        public string Username { get; set; }

        public string Fullname { get; set; }

        public string MaxUnit { get; set; }

        public string TotalIn { get; set; }
        public string TotalOut { get; set; }
        public string Package { get; set; }
        public string Ranking { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Rate { get; set; }

        public string Status { get; set; }
        public string Buy { get; set; }
        public string Sell { get; set; }
        public string UnitBalance { get; set; }
        public string Who { get; set; }
        public string Full { get; set; }
    }
    #endregion


 
}