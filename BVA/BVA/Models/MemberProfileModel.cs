﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Data.Objects;

namespace ECFBase.Models
{
    #region AdminPassword

    public class MemberBankModel
    {
        public int? MemberBankID { get; set; }

        public IEnumerable<SelectListItem> CountryList { get; set; }
        public string SelectedCountry { get; set; }
        public string SelectedMailingCountry { get; set; }
        public string Address { get; set; }
        public string SelectedCity { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }

        public IEnumerable<SelectListItem> BankList { get; set; }
        public string SelectedBank { get; set; }
        public string ChinaBank { get; set; }

        public string Username { get; set; }
        public string SelectedCountryStraing { get; set; }
        public string BranchName { get; set; }
        public string BranchAdd1 { get; set; }
        public string BranchAdd2 { get; set; }
        public string BranchSwiftCode { get; set; }
        
        public string AccountNumber { get; set; }
        public string BeneficiaryName { get; set; }
        public string BeneficiaryIC { get; set; }
        public string BeneficiaryRelationship { get; set; }
        public string BeneficiaryPhone { get; set; }
        public bool EnableEdit { get; set; }
        public bool EnableEditAll { get; set; }

        public MemberBankModel()
        {
            CountryList = new List<SelectListItem>();
            BankList = new List<SelectListItem>();
            EnableEdit = false;
            EnableEditAll = false;
        }
    }

    public class MemberPwdPINModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgReqUsername")]
        public string Username { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgReqCurrPwd")]
        public string CurrPassword { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningCurrPIN")]
        public string CurrPIN { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgReqNewPwd")]
        public string NewPassword { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningConfirmNewPIN")]
        public string NewPIN { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningConfirmNewPwd")]
        public string ConfirmNewPassword { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningConfirmNewPIN")]
        public string ConfirmNewPIN { get; set; }

    }

    #endregion

}