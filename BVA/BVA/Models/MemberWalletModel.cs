﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ECFBase.Models
{
    public class WithdrawWalletModel
    {
        [RegularExpression(@"^[0-9]*(?:\.[0-9]*)?$", ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgInvalidAmount")]
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgReqAmount")]
        public float? Amount { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgReqPIN")]
        public string PIN { get; set; }

        public IEnumerable<SelectListItem> CountryList { get; set; }
        public string SelectedCountry { get; set; }
        public IEnumerable<SelectListItem> BankList { get; set; }
        public string SelectedBank { get; set; }
        public string ChinaBank { get; set; }
        public string BranchAdd1 { get; set; }
        public string BranchAdd2 { get; set; }
        public string BranchSwiftCode { get; set; }
        public string MailingCountry { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string SelectedCity { get; set; }
        public string hiddenbankcharge { get; set; }
        public string bankcharge { get; set; }
        public string netamount { get; set; }
        public string Auto { get; set; }
        public int? MemberBankID { get; set; }
        public string BranchName { get; set; }
        public string AccountNumber { get; set; }
        public string BeneficiaryName { get; set; }
        public string BeneficiaryIC { get; set; }
        public string BeneficiaryPhone { get; set; }
        public string BeneficiaryRelationship { get; set; }

        public int wallet { get; set; }
    }

    public class TransferWalletModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningUsername")]
        public string Username { get; set; }

        [RegularExpression(@"^[0-9]*(?:\.[0-9]*)?$", ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgInvalidAmount")]
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgInvalidAmount")]
        public float? Amount { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgReqPIN")]
        public string PIN { get; set; }

        public string Remark { get; set; }

        public string NickName { get; set; }

        public string Wallet { get; set; }

        public string CurrencySell { get; set; }
        public string CurrencyBuy { get; set; }

        public string LocalAmount { get; set; }

        public IEnumerable<SelectListItem> CountryList { get; set; }
        public string SelectedCountryCode { get; set; }

        public IEnumerable<SelectListItem> ProvinceList { get; set; }

        public string ProvinceCode { get; set; }

        public IEnumerable<SelectListItem> BankList { get; set; }

        public string BankCode { get; set; }

        public string AccountHolderName { get; set; }

        public string AccountNumber { get; set; }

        public string Branch { get; set; }

        public string MinAmount { get; set; }

        public string MaxAmount { get; set; }

        public IEnumerable<SelectListItem> PaymentMethod { get; set; }

        public string SelectedOwnAccount { get; set; }

        public IEnumerable<SelectListItem> OwnAccount { get; set; }

        public string PaymentImageName { get; set; }
        public string PaymentImagePath { get; set; }
        public HttpPostedFileBase PaymentImage { get; set; }

        public string SelectedPaymentMethod { get; set; }

        public string OtherWallet { get; set; }

        public string OtherWallet2 { get; set; }

        public float Charge { get; set; }

        public IEnumerable<SelectListItem> WalletLists { get; set; }

        public string SelectedWallet { get; set; }

        public string FromWallet { get; set; }

        public string ToWallet { get; set; }

        public float deductamount { get; set; }

        public string AdminCharge { get; set; }

        public string GrandTotal { get; set; }

        public bool AutoCheck { get; set; }

        public TransferWalletModel()
        {
            CountryList = new List<SelectListItem>();
            BankList = new List<SelectListItem>();
            ProvinceList = new List<SelectListItem>();
        }
    }

    public class ExchangeWalletModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "warningUsername")]
        public string Username { get; set; }

        [RegularExpression(@"^[0-9]*(?:\.[0-9]*)?$", ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgInvalidAmount")]
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgInvalidAmount")]
        public float? Amount { get; set; }

        public string AdminCharge { get; set; }

        public string GrandTotal { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgReqPIN")]
        public string PIN { get; set; }

        public string Remark { get; set; }

        public string NickName { get; set; }

        public string FromWallet { get; set; }

        public string ToWallet { get; set; }

        public float deductamount { get; set; }

        public string OTCSharePrice { get; set; }

        public string OTCShareUnit { get; set; }

        public string CPBalance { get; set; }

        public string PurchaseUnit { get; set; }

    }

    public class TransferMwallet
    {
        [RegularExpression(@"^[0-9]*(?:\.[0-9]*)?$", ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgInvalidAmount")]
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgInvalidAmount")]
        public float Amount { get; set; }

        public string username { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgReqPIN")]
        public string PIN { get; set; }

    }

    public class ConvertEWalletToPVModel
    {
        [RegularExpression(@"^[0-9]*(?:\.[0-9]*)?$", ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgInvalidAmount")]
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgInvalidAmount")]
        public float? Amount { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "msgReqPIN")]
        public string PIN { get; set; }
    }

    public class ShareCertPurchaseModel : TransferWalletModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "lblRequired")]
        public string Name { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.OneForAll.OneForAll), ErrorMessageResourceName = "lblRequired")]
        public string IC { get; set; }
        public string Remarkss { get; set; }
        public string Created { get; set; }
        public string No { get; set; }
    }

    public class PaginationShareCertPurchaseModel
    {
        public List<ShareCertPurchaseModel> ModelCollection { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public PaginationShareCertPurchaseModel()
        {
            ModelCollection = new List<ShareCertPurchaseModel>();
        }
    }
}