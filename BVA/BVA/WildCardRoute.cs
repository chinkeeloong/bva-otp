﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;

namespace ECFBase
{

    public class WildCardRoute: RouteBase
    {

        public override RouteData GetRouteData(HttpContextBase httpContext)
        {
            var url = httpContext.Request.Headers["HOST"];
            var index = url.IndexOf(".");

            if (index < 0)
                return null;

            var subDomain = url.Substring(0, index);

            if (subDomain == "test")
                return null;

            if (subDomain == "www")
                return null;

            if (subDomain == "yicommm")
                return null;

            var routeData = new RouteData(this, new MvcRouteHandler());
            routeData.Values.Add("controller", "User");
            routeData.Values.Add("action", "Home");
            return routeData;
        }

        public override VirtualPathData GetVirtualPath(RequestContext requestContext, RouteValueDictionary values)
        {
            //Implement your formating Url formating here
            return null;
        }
    }

}
