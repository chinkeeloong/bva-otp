USE [BVA]
GO
/****** Object:  StoredProcedure [dbo].[SP_AppendMarketTeam]    Script Date: 9/25/2018 5:14:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SP_AppendMarketTeam]
(
	@username		NVARCHAR(20),
	@packageCode	NVARCHAR(20),
	@recharge		FLOAT = 0
)
AS
	SET NOCOUNT ON
	
	DECLARE @previousUsername NVARCHAR(20) = ''
	DECLARE @upUsername NVARCHAR(20) = ''
	DECLARE @memberInvest FLOAT = 0 -- member invest package
	DECLARE @followGroupA NVARCHAR(20) = ''
	DECLARE @followGroupB NVARCHAR(20) = ''
	DECLARE @followGroupC NVARCHAR(20) = ''
	DECLARE @followID NVARCHAR(20) = ''
	DECLARE @id INT = 0, @exists INT = 0
	DECLARE @leftYJ INT = 0, @rightYJ INT = 0
	DECLARE @currentTime VARCHAR(50) = SUBSTRING(CONVERT([varchar](12), GETDATE(), (120)), (1), (10)) + ' 00:00'
	DECLARE @memberQuantity INT = 1
			
	-- select member's up username in the market
	SELECT TOP 1 @upUsername = marketTree.CMTREE_UPMEMBER, @memberInvest = package.CPKG_BV, @memberQuantity = CPKG_ACCQTY
	FROM dbo.CVD_MEMBER_MARKETTREE marketTree, dbo.CVD_MEMBER_PACKAGE memberPackage, dbo.CVD_PACKAGE package
	WHERE marketTree.CUSR_USERNAME = memberPackage.CUSR_USERNAME
		AND memberPackage.CPKG_CODE = package.CPKG_CODE
		AND marketTree.CUSR_USERNAME = @username
		AND package.CPKG_CODE = @packageCode
	
	--set followID to current username
	SET @followID = @username
	
	--To cater multiple user, so the pv must be divided by teh member quantity
	SET @memberInvest = @memberInvest 
	
	IF @recharge > 0
	BEGIN
		SET @memberInvest = @recharge
		
		SELECT TOP 1 @upUsername = marketTree.CMTREE_UPMEMBER
		FROM dbo.CVD_MEMBER_MARKETTREE marketTree
		WHERE marketTree.CUSR_USERNAME = @username
	END
	
	--KARHONG Due to customer request to have BV 0 package, they would like to add in the total group A/B member - I remove this RETURN
	--return if investment 0, no point to continue
	--IF @memberInvest <= 0
	--	RETURN
	
	WHILE ISNULL(@upUsername, '0') <> '0'
	BEGIN	
	
		SET @id = 0
		SET @exists = 0
		
		SELECT @followGroupA = CMTREE_FOLLOWGROUPA, @followGroupB = CMTREE_FOLLOWGROUPB, @followGroupC = CMTREE_FOLLOWGROUPC
		FROM dbo.CVD_MEMBER_MARKETTREE
		WHERE CUSR_USERNAME = @upUsername
		
		IF @followGroupA = @followID
		BEGIN
			UPDATE dbo.CVD_MEMBER_MARKETTREE
			SET CMTREE_GROUPAYJ = CMTREE_GROUPAYJ + @memberInvest , CMTREE_GROUPAGP = CMTREE_GROUPAGP + @memberInvest , CMTREE_LEFTSALESTODAY = CMTREE_LEFTSALESTODAY + @memberInvest
				, CMTREE_TGROUPAMEMBER = CMTREE_TGROUPAMEMBER + 1
			WHERE CUSR_USERNAME = @UpUsername
			
			-- select total left and right sales, here is all the bullshit for logs
			SET @leftYJ = 0
			SET @rightYJ = 0
			SELECT @leftYJ = CMTREE_GROUPAYJ, @rightYJ = CMTREE_GROUPBYJ
			FROM CVD_MEMBER_MARKETTREE
			WHERE CUSR_USERNAME = @UpUsername
			
			SELECT @exists = 1, @id = CPAIRBONUSLOG_ID
			FROM CVD_PAIRINGBONUSLOG
			WHERE CUSR_USERNAME = @UpUsername AND CPAIRBONUSLOG_CREATEDON = @currentTime
			
			IF @exists = 1
			BEGIN
				UPDATE CVD_PAIRINGBONUSLOG
				SET CPAIRBONUSLOG_LEFTSALES = CPAIRBONUSLOG_LEFTSALES + @memberInvest
					, CPAIRBONUSLOG_TOTOALLEFTSALES = @leftYJ
				WHERE CPAIRBONUSLOG_ID = @id
			END
			ELSE
			BEGIN
				INSERT INTO CVD_PAIRINGBONUSLOG (CUSR_USERNAME, CPAIRBONUSLOG_LEFTSALES,
					CPAIRBONUSLOG_RIGHTSALES, CPAIRBONUSLOG_TOTOALLEFTSALES, CPAIRBONUSLOG_TOTOALRIGHTSALES,
					CPAIRBONUSLOG_PAIR, CPAIRBONUSLOG_PAIRBONUS, CPAIRBONUSLOG_CREATEDBY, CPAIRBONUSLOG_UPDATEDBY)
				VALUES (@UpUsername, @memberInvest, 0, @leftYJ, @rightYJ, 0, 0, 'SYS', 'SYS')
			END
			
		END
		ELSE IF @followGroupB = @followID
		BEGIN
			UPDATE dbo.CVD_MEMBER_MARKETTREE
			SET CMTREE_GROUPBYJ = CMTREE_GROUPBYJ + @memberInvest , CMTREE_GROUPBGP = CMTREE_GROUPBGP + @memberInvest , CMTREE_RIGHTSALESTODAY = CMTREE_RIGHTSALESTODAY + @memberInvest
				, CMTREE_TGROUPBMEMBER = CMTREE_TGROUPBMEMBER + 1
			WHERE CUSR_USERNAME = @UpUsername
			
			-- select total left and right sales, here is all the bullshit for logs
			SET @leftYJ = 0
			SET @rightYJ = 0
			SELECT @leftYJ = CMTREE_GROUPAYJ, @rightYJ = CMTREE_GROUPBYJ
			FROM CVD_MEMBER_MARKETTREE
			WHERE CUSR_USERNAME = @UpUsername
			
			SELECT @exists = 1, @id = CPAIRBONUSLOG_ID
			FROM CVD_PAIRINGBONUSLOG
			WHERE CUSR_USERNAME = @UpUsername AND CPAIRBONUSLOG_CREATEDON = @currentTime
			
			IF @exists = 1
			BEGIN
				UPDATE CVD_PAIRINGBONUSLOG
				SET CPAIRBONUSLOG_RIGHTSALES = CPAIRBONUSLOG_RIGHTSALES + @memberInvest
					, CPAIRBONUSLOG_TOTOALRIGHTSALES = @rightYJ
				WHERE CPAIRBONUSLOG_ID = @id
			END
			ELSE
			BEGIN
				INSERT INTO [CVD_PAIRINGBONUSLOG] (CUSR_USERNAME, CPAIRBONUSLOG_LEFTSALES,
					CPAIRBONUSLOG_RIGHTSALES, CPAIRBONUSLOG_TOTOALLEFTSALES, CPAIRBONUSLOG_TOTOALRIGHTSALES,
					CPAIRBONUSLOG_PAIR, CPAIRBONUSLOG_PAIRBONUS, CPAIRBONUSLOG_CREATEDBY, CPAIRBONUSLOG_UPDATEDBY)
				VALUES (@UpUsername, 0, @memberInvest, @leftYJ, @rightYJ, 0, 0, 'SYS', 'SYS')
			END
			
		END
				
		-- set the followID to upID
		SET @followID = @upUsername
		
		-- need this to do validation when to stop running
		SET @previousUsername = @upUsername
		
		-- select the next Up member
		SELECT @upUsername = CMTREE_UPMEMBER
		FROM dbo.CVD_MEMBER_MARKETTREE
		WHERE CUSR_USERNAME = @previousUsername
		
		-- 386552 is the first member, and the upUsername is set 386552
		IF @previousUsername = @upUsername AND @upUsername = '0' -- 
			BREAK
			
	END 
		
	RETURN
