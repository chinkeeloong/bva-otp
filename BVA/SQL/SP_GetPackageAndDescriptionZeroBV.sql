USE [BVA]
GO

/****** Object:  StoredProcedure [dbo].[SP_GetPackageAndDescriptionZeroBV]    Script Date: 9/25/2018 3:15:19 PM ******/
DROP PROCEDURE [dbo].[SP_GetPackageAndDescriptionZeroBV]
GO

/****** Object:  StoredProcedure [dbo].[SP_GetPackageAndDescriptionZeroBV]    Script Date: 9/25/2018 3:15:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_GetPackageAndDescriptionZeroBV]
(
	@languageCode	NVARCHAR(10),
	@CountryCode	NVARCHAR(20)
)
AS
	SET NOCOUNT ON
	
	--select the package
	SELECT package.*, multiLang.*
	FROM dbo.CVD_PACKAGE package, dbo.CVD_MULTILANGUAGEPACKAGE multiLang
	WHERE package.CPKG_DELETIONSTATE = 0
		AND multiLang.CPKG_CODE = package.CPKG_CODE
		AND multiLang.CLANG_CODE = @languageCode
		AND package.CCOUNTRY_CODE = @CountryCode
		AND package.CPKG_CODE <> 'FREE001'
		AND ( package.CPKG_BV = 0
			  OR (	package.CPKG_VALIDITY_START <= GetDate()
					AND package.CPKG_VALIDITY_END >= GetDate() ) 
			)
	ORDER BY package.CPKG_AMOUNT
	
	RETURN

GO


