USE [BVA]
GO
/****** Object:  StoredProcedure [dbo].[SP_TransferCashWallet]    Script Date: 9/5/2018 3:55:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[SP_TransferCashWallet]
(
	@username	NVARCHAR(50),
	@Option		NVARCHAR(50),
	@amount		MONEY,
	@from		NVARCHAR(50),
	@pin		NVARCHAR(50),
	@remark		NVARCHAR(100),
	@ok			INT OUTPUT,
	@msg		VARCHAR(50) OUTPUT
)
AS
	SET NOCOUNT ON
	SET @ok = 0
	
	--A transfer to B 500, B get 499 Register wallet, A minus 499 minus 1 charge
	--SET @amount = @amount - 1
	
	DECLARE @exist INT = 0
	DECLARE @bonus MONEY = 0
	DECLARE @isStockies INT = 0		
	DECLARE @FromMemberCountry INT = -1
	DECLARE @ToMemberCountry INT = -2
	
	SELECT @FromMemberCountry = CCOUNTRY_ID
	FROM dbo.CVD_USER	
	WHERE CUSR_USERNAME = @from
	AND CROLE_ID = 2
	
	SELECT @ToMemberCountry = CCOUNTRY_ID
	FROM dbo.CVD_USER	
	WHERE CUSR_USERNAME = @username
	AND CROLE_ID = 2
	
	
	SELECT @bonus = CMEM_CASHWALLET
	FROM dbo.CVD_MEMBER
	WHERE CUSR_USERNAME = @from
	
	SELECT @exist = 1
	FROM dbo.CVD_MEMBER
	WHERE CUSR_USERNAME = @username
	AND CMEM_DELETIONSTATE = 0
	
	--check if user exist or not
	IF @exist = 0
	BEGIN
		--'Username does not exist!'
		SET @msg = '-1'
		RETURN
	END
	
	--Amount must be more than 0
	IF @amount < 0
	BEGIN
		--'Amount cannot be negative'
		SET @msg = '-2'
		RETURN
	END
	
	--check if the pin matches
	SET @exist = 0
	SELECT @exist = 1
	FROM dbo.CVD_USER
	WHERE CUSR_USERNAME = @from
		AND CUSR_PIN = @pin
	
	IF @exist = 0
	BEGIN
		--'Pin Invalid!'
		SET @msg = '-3'
		RETURN
	END
	
	IF @amount > @bonus
	BEGIN
		SET @msg = '-4'
		--SET @msg = 'Cannot withdraw more than your wallet balance!'
		RETURN
	END

	DECLARE @CASHNAME NVARCHAR(50)

	IF @Option = 'RP'
	BEGIN
		SET @CASHNAME = 'CP Transfer RP'
		EXECUTE [dbo].[SP_WalletRegisterOperation]@username, @CASHNAME , @amount, @from, '', 0, 0, @remark
	END    	
	
	IF @Option = 'MP'
	BEGIN
		SET @CASHNAME = 'CP Transfer MP'
		EXECUTE [dbo].[SP_WalletMultiPointOperation]@username, @CASHNAME , @amount, @from, '', 0, 0, @remark
	END 	

	IF @Option = 'RMP'
	BEGIN
		SET @CASHNAME = 'CP Transfer RMP'
		EXECUTE [dbo].[SP_WalletRMPOperation]@username, @CASHNAME , @amount, @from, '', 0, 0, @remark
	END 	

	IF @Option = 'CP'
	BEGIN
		SET @CASHNAME = 'CP Transfer'
		EXECUTE [dbo].[SP_WalletCashOperation]@username, @CASHNAME , @amount, @from, '', 0, 0, @remark
	END  
	
	SET @amount = 0 - @amount
	
	EXECUTE dbo.SP_WalletCashOperation @from, @CASHNAME , @amount, @username, '', 0, 0, @remark
	
	SET @ok = 1
	SET @msg = 'Success'
	
	RETURN
